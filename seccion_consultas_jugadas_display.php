<?php
session_start();

// Incluye datos generales y conexion a DB
require_once 'config.ini.php';
require_once 'conectadb.php';
require_once 'lib/seccion_datos_importacion_listas_lib.php';

/*******************COMPRADORES SEGUROS******************************/
$sqlVendedores = "SELECT U.ID as usuario_id, U.NombreUsuario as nombre_usuario, f_jugada as paga_jugada
                  FROM Usuarios U
                  WHERE TipoUsuario=5 and FlagActivo=1 and ActivoFlag=1
                  order by U.NombreUsuario";

$stmtVendedores = $pdoConn->prepare($sqlVendedores);
$stmtVendedores->execute();
$vendedores = $stmtVendedores->fetchAll(PDO::FETCH_ASSOC);
/********************************************************************/

/*******************SORTEO******************************/
$sqlSorteosDelDia = "SELECT SP.ID AS sorteo_prog_id, SD.NombreSorteo as nombre_sorteo
                     FROM SorteosProgramacion SP
                     JOIN SorteosDefinicion SD
                     ON SP.IDSorteoDefinicion = SD.ID
                     WHERE SP.ID = ?";

$stmtSorteosDelDia = $pdoConn->prepare($sqlSorteosDelDia);
$stmtSorteosDelDia->execute(array($_GET['SID']));
$sorteosDelDia = $stmtSorteosDelDia->fetchAll(PDO::FETCH_ASSOC);
$idSorteo = 0;
foreach($sorteosDelDia as $sorteo){
    $idSorteo = $sorteo['sorteo_prog_id'];
    $nombreSorteo = $sorteo['nombre_sorteo'];
}
/****************************************************************/
$estado = 0;
?>

<style type="text/css">
    .fancybox-custom .fancybox-skin {
        box-shadow: 0 0 50px #222;
    }

    input[type=checkbox] {
        visibility: visible;
        height: 15px;
    }
</style>

<div class="divTable">
    <div class="divRow" style="width: 250px">
        <div class="divCellHeader2" style="width: 180px">Vendedor</div>
        <div class="divCellHeader2" style="width: 100px">Numero</div>
        <div class="divCellHeader2" style="width: 100px">Comprar</div>
        <div class="divCellHeader2" style="width: 100px">Numero</div>
        <div class="divCellHeader2" style="width: 100px">Comprar</div>
    </div>
    <?php foreach ($vendedores as $vendedor): ?>
        <div class="divRow2">
            <div class="divCellCen2" style="width: 180px; text-align: center; font-weight: bold; font-size: 15px">
                <?php echo $vendedor['nombre_usuario'] ?>
            </div>
            <?php
            $sqlJugadas = "SELECT jugada1, jugada2, revision_monto, monto_terminal
                           FROM COF_Revision_Log
                           WHERE cof_revision_id =
                           (SELECT id FROM COF_Revision WHERE sorteo_programacion_id = :sorteo_id
                                                        and usuario_vendedor_calle_id = :user_id) AND cof_new_status_id = 3";

            $stmtJugadas = $pdoConn->prepare($sqlJugadas);
            $stmtJugadas->execute(array(':sorteo_id' => $idSorteo, ':user_id' => $vendedor['usuario_id']));
            $jugadas = $stmtJugadas->fetchAll(PDO::FETCH_ASSOC);

            $pagaPorcentaje = $vendedor['paga_jugada'];
            ?>
            <?php foreach($jugadas as $jugada):?>
                <?php
                $estado = 1;
                  if($jugada['jugada1'] != -1){

                    $compra = ($jugada['revision_monto'] + $jugada['monto_terminal']) / $pagaPorcentaje;
                    $compra = round($compra, 0, PHP_ROUND_HALF_UP);//ELIMINA DECIMALES
                    $redondear = substr($compra, sizeof($compra)-3);//CAPTURA LOS 2 ULTIMOS DIGITOS PARA APLICAR EL REDONDEO
                    $redondear = 100 - $redondear;//PARA SABER SI SE RENDODEA 100
                    if($redondear < 50){//SI LA DIFENCIA ES MENOS DE 50 SE REDONDEA AL 100 PROXIMO
                        $compra = $compra + $redondear;//
                    }elseif($redondear > 50 && $redondear < 100){//SI LA DIFENCIA ES MAYOR A DE 50 Y MENOR A 99 SE REDONDEA AL 50 PROXIMO
                        $redondear = substr($compra, sizeof($compra)-3);//CAPTURA LOS 2 ULTIMOS DIGITOS PARA APLICAR EL REDONDEO
                        $redondear = 50 - $redondear;//PARA SABER LA DIFERECIA ENTRE LOS 2 ULTI DIGITOS HASTA 50
                        $compra = $compra + $redondear;
                    }?>
                      <div class="divCellCen2" style="width: 100px; text-align: center; font-weight: bold; font-size: 15px">
                          <?php echo $jugada['jugada1'];?>
                      </div>
                      <div class="divCellCen2" style="width: 100px; text-align: center; font-weight: bold; font-size: 15px">
                          <?php echo $compra;?>
                      </div>

                  <?php }else{?>
                      <div class="divCellCen2" style="width: 100px; text-align: center; font-weight: bold; font-size: 15px">
                          <?php echo '-';?>
                      </div>
                      <div class="divCellCen2" style="width: 100px; text-align: center; font-weight: bold; font-size: 15px">
                          <?php echo '-';?>
                      </div>
                  <?php }?>

                <?php if($jugada['jugada2'] != -1){?>
                    <?php
                    $compra = ($jugada['revision_monto'] + $jugada['monto_terminal']) / $pagaPorcentaje;
                    $compra = round($compra, 0, PHP_ROUND_HALF_UP);//ELIMINA DECIMALES
                    $redondear = substr($compra, sizeof($compra)-3);//CAPTURA LOS 2 ULTIMOS DIGITOS PARA APLICAR EL REDONDEO
                    $redondear = 100 - $redondear;//PARA SABER SI SE RENDODEA 100
                    if($redondear < 50){//SI LA DIFENCIA ES MENOS DE 50 SE REDONDEA AL 100 PROXIMO
                        $compra = $compra + $redondear;//
                    }elseif($redondear > 50 && $redondear < 100){//SI LA DIFENCIA ES MAYOR A DE 50 Y MENOR A 99 SE REDONDEA AL 50 PROXIMO
                        $redondear = substr($compra, sizeof($compra)-3);//CAPTURA LOS 2 ULTIMOS DIGITOS PARA APLICAR EL REDONDEO
                        $redondear = 50 - $redondear;//PARA SABER LA DIFERECIA ENTRE LOS 2 ULTI DIGITOS HASTA 50
                        $compra = $compra + $redondear;
                    }?>

                    <div class="divCellCen2" style="width: 100px; text-align: center; font-weight: bold; font-size: 15px">
                        <?php echo $jugada['jugada2'];?>
                    </div>
                    <div class="divCellCen2" style="width: 100px; text-align: center; font-weight: bold; font-size: 15px">
                        <?php echo $compra;?>
                    </div>

                <?php }else{?>
                    <div class="divCellCen2" style="width: 100px; text-align: center; font-weight: bold; font-size: 15px">
                        <?php echo '-';?>
                    </div>
                    <div class="divCellCen2" style="width: 100px; text-align: center; font-weight: bold; font-size: 15px">
                        <?php echo '-';?>
                    </div>
                <?php }?>



            <?php endforeach; ?>
            <?php if($estado === 0){ ?>
                <div class="divCellCen2" style="width: 100px; text-align: center; font-weight: bold; font-size: 15px">
                    <?php echo '-';?>
                </div>
                <div class="divCellCen2" style="width: 100px; text-align: center; font-weight: bold; font-size: 15px">
                    <?php echo '-';?>
                </div>
                <div class="divCellCen2" style="width: 100px; text-align: center; font-weight: bold; font-size: 15px">
                    <?php echo '-';?>
                </div>
                <div class="divCellCen2" style="width: 100px; text-align: center; font-weight: bold; font-size: 15px">
                    <?php echo '-';?>
                </div>
            <?php } ?>

        </div>
    <?php $estado = 0;
    endforeach; ?>

</div>