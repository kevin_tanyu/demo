<?php
session_start();

require_once 'config.ini.php';
require_once 'conectadb.php';
require_once 'lib/seccion_usuarios_vendedores_parametros_lib.php';

$vendedorCalleId = $_GET['id'];

$sqlDatosUsuario = "SELECT * FROM Usuarios WHERE ID = ?";
$stmtDatosUsuario = $pdoConn->prepare($sqlDatosUsuario);
$stmtDatosUsuario->execute(array($vendedorCalleId));
$datosUsuario = $stmtDatosUsuario->fetch();

?>

<form method="get" id="form_modifica_vendedor_tablet" action="seccion_usuarios_vendedores_datos_action.php">
    <legend><h3>Modificacion datos para vendedor calle</h3></legend>
    <input type="hidden" value="<?php echo $vendedorCalleId ?>" name="uid" >
    <fieldset>
        <div class="divTable" >
            <div class="divRow">
                <div class="divCellIzq" style="width: 15em; font-size: 18px" >
                    <table>
                        <tr>
                            <td><label>Paga en la calle</label></td>
                            <td><input type="text" style="width: 50px" id="txtPaga" name="txtPaga" value="<?php echo $datosUsuario['PagaPorcentaje']?>">
                            </td>
                        </tr>
                        <tr>
                            <td><label>Banca Paga Al</label></td>
                            <td><input type="text" style="width: 50px" id="txtPagaReal" name="txtPagaReal" value="<?php echo $datosUsuario['PagaReal']?>">
                            </td>
                        </tr>
                        <tr>
                            <td><label>Monto Arranque</label></td>
                            <td><input type="text" style="width: 50px" id="txtMontoArranque" name="txtMontoArranque" value="<?php echo $datosUsuario['montoArranque']?>"></td>
                        </tr>
                        <tr>
                            <td><label>Comision</label></td>
                            <td><input type="text" style="width: 50px" id="txtComision" name="txtComision" value="<?php echo $datosUsuario['comision']?>">
                            </td>
                        </tr>
                        <tr>
                            <td><label>Jugada</label></td>
                            <td><input type="text" style="width: 50px" id="txtJugada" name="txtJugada" value="<?php echo $datosUsuario['f_jugada']?>"></td>
                        </tr>
                        <tr>
                            <td><label>Da jugada</label></td>
                            <td><select id="slcEstado" name="slcDaJugada">
                                    <?php if($datosUsuario['da_jugada'] == 0){?>
                                        <option value="0" selected>No</option>
                                        <option value="1">Si</option>
                                    <?php }else{?>
                                        <option value="0">No</option>
                                        <option value="1" selected>Si</option>
                                    <?php }?>

                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Devolucion</label></td>
                            <td><input type="text" style="width: 50px" id="txtDevolucion" name="txtDevolucion" value="<?php echo $datosUsuario['devolucion']?>">
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </fieldset>

    <input type="submit" value="Guardar" class="button">

    <div id="params_result"></div>
</form>

<script>
    $('#form_modifica_vendedor_tablet').ajaxForm({
        target: '#params_result',
        beforeSubmit: function () {
            $('#form_modifica_vendedor_tablet').block();
        },
        success: function () {
            $('#form_modifica_vendedor_tablet').unblock();
        }
    });
    $('.chzn-select').chosen();
</script>