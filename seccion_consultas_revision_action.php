<?php
session_start();

require_once 'config.ini.php';
require_once 'conectadb.php';

$action       = $_GET['action'];
$vendedorId   = $_GET['vendedor_id'];
$sorteoProgId = $_GET['sorteo_prog_id'];

$result = array('message' => '', 'error' => true, 'error_message' => '');

//check content of new action
$sqlCheckAction  = "SELECT id FROM COF_Status WHERE name = ?";
$stmtCheckAction = $pdoConn->prepare($sqlCheckAction);
$stmtCheckAction->execute(array($action));
if ($stmtCheckAction->rowCount() > 0)
{
    $statusRow       = $stmtCheckAction->fetch(PDO::FETCH_ASSOC);
    $statusId        = $statusRow['id'];
    $sqlInsertAction = "INSERT INTO COF_Revision_Log (cof_revision_id, cof_new_status_id, created_at, user_id)
     SELECT C.id, :status_id, NOW(), :user_id
     FROM COF_Revision C
     WHERE C.sorteo_programacion_id = :sorteo_prog_id AND C.usuario_vendedor_calle_id = :user_id
    ";

    try
    {
        $stmtInserAction = $pdoConn->prepare($sqlInsertAction);
        $stmtInserAction->execute(array(':status_id' => $statusId, ':user_id' => $vendedorId, ':sorteo_prog_id' => $sorteoProgId));
        $result['error'] = false;
    } catch (PDOException $e)
    {
        $result['error']         = true;
        $result['error_message'] = 'Problemas con la base de datos, intente de nuevo';
    }


} else
{
    $result['error']         = true;
    $result['error_message'] = 'Accion incorrecta';
}

echo json_encode($result);