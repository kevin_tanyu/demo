<?php 
session_start();
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<title>Dashboard I Admin Panel</title>
	<meta name="viewport" content="width=600, height=1024, initial-scale=0.0, maximum-scale=0.0, user-scalable=yes" />
	<link href='http://fonts.googleapis.com/css?family=Share+Tech+Mono' rel='stylesheet' type='text/css'>
	<link href='styles.css' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
</head>
<body style="background-color:#ccc; margin:auto; text-align:center;">
<div style="width:100%; height:100%; border:1px solid #666; background-color:#FFF; margin:auto; text-align:center;"> 
<?php
		include("config.ini.php");
		include("header.php");
?>
<div style="width:100%;margin-top:30px;">     </div>
	
	<div id="Reversion"><a href="Reversion.php">Reversion</a></div>

	<div id="CerrarDia"><a href="CierraAccion.php">Cerrar el Dia</a></div>
	<div id="CerrarDiaComentario"><span style="font-weight:bold;">ATENCION!</span>: Una vez cerrado el dia, ya no podra introducir mas apuestas hasta el dia de mañana.</div>
</div>
</body>
</html>