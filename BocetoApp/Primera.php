<?php
session_start();
$_SESSION['IDUsuario'] = $_GET['IDUsuario'];
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>Dashboard I Admin Panel</title>
    <meta name="viewport"
          content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <link href='http://fonts.googleapis.com/css?family=Share+Tech+Mono' rel='stylesheet' type='text/css'>
    <link href='styles.css' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
</head>
<body style="background-color:#ccc; text-align:center;width:99.7%;overflow:hidden;">
<div style="width:100%; height:100%; border:1px solid #666; background-color:#FFF; margin:auto; text-align:center;">
    <?php
    include("config.ini.php");
    //		include("header.php");
    ?>
    <div style="width:100%;margin-top:30px;"></div>
    <?php

    // Cantidad de Apuestas
    $QUERY = "SELECT count(id) FROM Ticket_View WHERE IDUsuario=" . $_SESSION['IDUsuario'] . " AND FechayHora>='" . date('Y-m-d 00:00:00') . "'";
    $rs = mysql_query($QUERY);
    $Cantidad = mysql_fetch_row($rs);

    // Suma de Apuestas
    $QUERY = "SELECT sum(Cantidad) FROM SorteoApuesta_Validas WHERE IDUsuario=" . $_SESSION['IDUsuario'] . " AND FechayHora>='" . date('Y-m-d 00:00:00') . "'";
    $rs = mysql_query($QUERY);
    $Monto = mysql_fetch_row($rs);

    // Suma en Restringidos
    //$QUERY = "SELECT SUM(Cantidad) as Cantidad FROM SorteoApuesta_Validas WHERE Numero in (SELECT Numero FROM `SorteosNumerosRestringidos` WHERE IDSorteoProgramacion in (SELECT IDSorteoProgramacion from SorteoApuesta_Validas WHERE IDUsuario=" . $_SESSION['IDUsuario'] . ")) AND FechayHora>='" . date('Y-m-d 00:00:00') . "'";
    $QUERY = "select sum(sv.Cantidad) as Cantidad
            from SorteoApuesta_Validas sv
              join SorteosProgramacion sp on sp.FechayHora >= '" . date('Y-m-d 00:00:00') . "' and sp.ID = sv.IDSorteoProgramacion
              join SorteosNumerosRestringidos snr on snr.IDSorteoProgramacion = sp.ID and snr.Numero = sv.Numero
              where sv.IDUsuario = " . $_SESSION['IDUsuario'];
    $rs = mysql_query($QUERY);
    $MontoRestringidos = mysql_fetch_row($rs);

    // Proximo Sorteo
    $QUERY = "SELECT D.NombreSorteo, D.PagaPorcentaje, P.FechayHora FROM SorteosDefinicion D, SorteosProgramacion P WHERE D.ID=P.IDSorteoDefinicion ORDER BY P.FechayHora DESC Limit 0,1";
    $rs = mysql_query($QUERY);
    $ProximoSorteo = mysql_fetch_row($rs);

    $sql = "SELECT ID FROM CierreDelDia WHERE DATEDIFF('" . date("Y-m-d") . "',FechaCierre) = 0 AND IDUsuario = " . $_SESSION['IDUsuario'];
    $rs = mysql_query($sql);
    $diaCerradoRecord = mysql_fetch_assoc($rs);
    $diaCerrado = $diaCerradoRecord ? true : false;

    ?>
    <script type="text/javascript">
        google.load("visualization", "1", {packages: ["corechart"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Tipo', 'Monto'],
                ['Normal', <?php echo $Monto[0]-$MontoRestringidos[0]; ?>],
                ['Restringido', <?php echo $MontoRestringidos[0]; ?>]
            ]);

            var options = {
                title: 'Distribucion'
            };

            var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        }
    </script>

    <div id="chart_div" style="width: 600px; height: 300px;"></div>
    <div id="TablaPeq">
        <div id="TablaPeqTR">
            <div id="TablaPeqTDIzq">Cantidad de Tickets</div>
            <div id="TablaPeqTDDer"><?= $Cantidad[0]; ?></div>
        </div>
        <div id="TablaPeqTR">
            <div id="TablaPeqTDIzq">Total Recolectado</div>
            <div id="TablaPeqTDDer">&cent; <?= number_format($Monto[0], 0, ".", ","); ?></div>
        </div>
        <div id="TablaPeqTR">
            <div id="TablaPeqTDIzq">Restringidos</div>
            <div id="TablaPeqTDDer">&cent; <?= number_format($MontoRestringidos[0], 0, ".", ","); ?></div>
        </div>
        <div id="TablaPeqTR">
            <div id="TablaPeqTDIzq">Proximo Sorteo</div>
            <div id="TablaPeqTDDer">Loteria <?= $ProximoSorteo[0]; ?> paga <?= $ProximoSorteo[1]; ?> veces y juega
                el <?php list($a, $b) = explode(" ", $ProximoSorteo[2]);
                list($Y, $m, $d) = explode("-", $a);
                echo $d . "-" . $m . "-" . $Y; ?> a las <?= $b; ?></div>
        </div>
        <div id="TablaPeqTR">
            <div id="TablaPeqTDIzq">Status</div>
            <div id="TablaPeqTDDer"><?php if ($diaCerrado)
                { ?><span style="color:red">CERRADO</span><?php } else
                { ?><span style="color:green">ABIERTO</span><?php } ?></div>
        </div>
    </div>

</div>
</body>
</html>
