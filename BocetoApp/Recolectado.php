<?php session_start(); $_SESSION['IDUsuario'] = $_GET['IDUsuario'] ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<title>Dashboard I Admin Panel</title>
	<meta name="viewport" content="width=device-width, height=1024, initial-scale=1, maximum-scale=1, user-scalable=yes" />
	<link href='http://fonts.googleapis.com/css?family=Share+Tech+Mono' rel='stylesheet' type='text/css'>
	<link href='styles.css' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
</head>
<body style="background-color:#ccc; margin:auto; width:99.6%; text-align:center;">
<div style="width:100%; height:100%; border:1px solid #666; background-color:#FFF; margin:auto; text-align:center; overflow-y:scroll; "> 
<?php
		include("config.ini.php");
//		include("header.php");
?>
<div style="width:100%;margin-top:30px;">     </div>
<?php


		$QUERY="
			SELECT 	SorteosDefinicion.NombreSorteo,
							SorteosProgramacion.FechayHora, 
							SorteoApuesta_Validas.Numero,
							SUM(SorteoApuesta_Validas.Cantidad) as Cantidad
			FROM 		SorteosDefinicion, SorteosProgramacion, SorteoApuesta_Validas
			WHERE	SorteosDefinicion.ID=SorteosProgramacion.IDSorteoDefinicion 
				AND 	SorteosProgramacion.ID= SorteoApuesta_Validas.IDSorteoProgramacion
				AND     SorteoApuesta_Validas.FechayHora >='" . date('Y-m-d 00:00:00') . "'
			GROUP BY 	SorteosDefinicion.NombreSorteo,
								SorteosProgramacion.FechayHora, 
								SorteoApuesta_Validas.Numero";
$rs = mysql_query($QUERY);

if (mysql_affected_rows()>0) {
			?>
			Apuestas recolectadas para el dia de hoy (<?=date('d-m-Y'); ?>)
			<br><br>
			<div id="Tabla_container">
				<div id="Tabla_row_head">
					<div id="Tabla_left">Sorteo</div>
					<div id="Tabla_middle">Juega</div>
					<div id="Tabla_middle">Numero</div>
					<div id="Tabla_right">Apuesta</div>
				</div>
			<?php
			$TOT=0;
			while ($row=mysql_fetch_row($rs)) {
		?>
				<div id="Tabla_row">
					<div id="Tabla_left"><?=$row[0]; ?></div>
					<div id="Tabla_middle"><?php list($a,$b)=explode(" ",$row[1]); list($Y,$m,$d)=explode("-",$a); echo $d."-".$m."-".$Y; ?></div>
					<div id="Tabla_middle"><span style="font-weight:bold;"><?=$row[2]; ?><span></div>
					<div id="Tabla_right">&cent; <?=number_format($row[3],0,",",","); ?></div>
				</div>
		<?php
				$TOT=$TOT+$row[3];
			}
		?>
				<div id="Tabla_row_final">
					<div id="Tabla_left"></div>
					<div id="Tabla_middle"></div>
					<div id="Tabla_middle">Total</div>
					<div id="Tabla_right">&cent; <?=number_format($TOT); ?></div>
				</div>

		</div>
		<?php
		} else {
		?>
			<div class="SorteosCalendarizadosVacio">No hay Loterias Disponibles. </div>
		<?php
		}
	?>
</div>
</body>
</html>
