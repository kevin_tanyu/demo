<?php
session_start();

if (!isset($_SESSION['ActivoFlag']))
{
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

//  Full texts 	ID 	NombreUsuario 	Contrasena 	TipoUsuario 	ActivoFlag 	IDPadre 	Email 	FlagActivo 	PINRecolector 
$wherePadre = "";
if ($_SESSION['TipoUsuarioInt'] == NIVEL_PERMISO_USUARIO_WEB)
{
    $wherePadre = " AND IDPadre = " . $_SESSION['IDUsuario'];
}
// get data and store in a json array 
$QUERY = "SELECT * FROM Usuarios_View WHERE TipoUsuario=20 and FlagActivo=1 and ActivoFlag=1 " . $wherePadre; // ADMINISTRADORES
if (isset($_GET['update']))
{
    // UPDATE COMMAND
    $QUERY_UPDATE = "UPDATE 	Usuarios
									SET			NombreUsuario='" . $_GET['NombreUsuario'] . "',
													Contrasena='" . $_GET['Contrasena'] . "',
													Email='" . $_GET['Email'] . "',
													jugador_pregunta='" . $_GET['Pregunta'] . "',
													jugador_respuesta='" . $_GET['Respuesta'] . "'
									WHERE	id=" . $_GET['ID'];

    $result = mysql_query($QUERY_UPDATE) or die("SQL Error 1: " . mysql_error());
    echo $result;
} else
{
    if (isset($_GET['delete']))
    {
        // DELETE COMMAND
        $QUERY_UPDATE = "UPDATE 	Usuarios
											SET			FlagActivo=0,
															ActivoFlag=0
											WHERE	id=" . $_GET['ID'];

        $result = mysql_query($QUERY_UPDATE) or die("SQL Error 1: " . mysql_error());
        echo $result;
    } else
    {
        // SELECT COMMAND
        $result = mysql_query($QUERY) or die("SQL Error 1: " . mysql_error());
        while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
        {
            $resultados[] = array(
                'NombreUsuario' => $row['NombreUsuario'],
//						'Contrasena' => $row['Contrasena'],
                'Email' => $row['Email'],
                'NumeroTelCasa' => $row['jugador_tel_casa'],
                'NumeroTelCelular' => $row['jugador_tel_celular'],
                'Pregunta' => $row['jugador_pregunta'],
                'Respuesta' => $row['jugador_respuesta'],
                'ID' => $row['ID']
            );
        }

        echo json_encode($resultados);
    }
}
?>