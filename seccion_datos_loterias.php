<?php
session_start();

if (!isset($_SESSION['ActivoFlag']))
{
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

// Incluye Header
include("header.php");

include("seccion_datos_submenu.php");
?>
<script type="text/javascript">
    $(document).ready(function () {
        $(".various").fancybox({
            fitToView: true,
            width: '70%',
            height: '70%',
            autoSize: true,
            closeClick: false,
            openEffect: 'fade',
            closeEffect: 'elastic'
        });
        TablaLoterias();
    });

    function AbreVentanaLoterias() {
        $(".various2").fancybox({
            fitToView: true,
            width: '70%',
            height: '70%',
            autoSize: true,
            closeClick: false,
            openEffect: 'fade',
            closeEffect: 'elastic'
        });

        return false;
    }

    function TablaLoterias() {
        $.ajax({
            type: "GET",
            url: 'seccion_datos_loterias_tabla.php',
            DataType: 'html',
            success: function (response) {
                document.getElementById('TablaLoterias').innerHTML = response;
            }
        });

        return false;
    }


    function FormularioAddLoteria() {
        $.ajax({
            type: "POST",
            url: 'seccion_datos_loterias_add.php',
            data: $('#formulario1').serialize(),
            success: function (response) {
                TablaLoterias();
                $.fancybox.close();
            }
        });

        return false;
    }

    function FormularioDeleteLoteria(id) {

        if (confirm("Esta seguro de querer deshabilitar esta Loteria?")) {
            $.ajax({
                type: "POST",
                url: 'seccion_datos_loterias_delete_accion.php?id=' + id,
                success: function (response) {
                    TablaLoterias();
                    $.fancybox.close();
                }
            });
        }
        return false;
    }


</script>
<style type="text/css">
    .fancybox-custom .fancybox-skin {
        box-shadow: 0 0 50px #222;
    }
</style>

<div id="ContenedorGeneral">
    <div id="TablaLoterias"></div>
    <!--
                    <p><a id="ajax" class='ajax' href="seccion_datos_loterias_add_form.php" title="Agregar Loteria">Agregar Loteria</a></p>
                    <script>
                        jQuery('a.ajax').colorbox();
                     </script>-->
    <br><br>
    <a class="various  fancybox.ajax button" href="seccion_datos_loterias_add_form.php"
       style="font-size:10px; float:right;">Agregar Loteria</a>

</div>
<?php
// Incluye Footer
include("footer.php");
?>
