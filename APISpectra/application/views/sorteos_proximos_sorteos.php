<?php if( 0 < count($result) ): ?>

<ul data-role="listview" data-theme="d" data-divider-theme="d" id="sorteos_list" >
    <?php foreach($result as $sorteoProgramacion): ?>
        <li data-role="list-divider"><?php echo $sorteoProgramacion->NombreSorteo ?><span class="ui-li-count">1</span></li>
        <li><a data-sorteo_definicion_id="<?php echo $sorteoProgramacion->SorteoDefinicionID ?>" data-sorteo_id="<?php echo $sorteoProgramacion->ID ?>" href="#" class="sorteo_item" >
                <h3>Fecha de sorteo:</h3>
                <p> <?php echo $sorteoProgramacion->FechaYHora ?></p>
                <p class="ui-li-aside">Numero de la suerte <?php echo $sorteoProgramacion->NumeroDeSuerte ?></p>
            </a>
        </li>
    <?php endforeach; ?>
</ul>
<script>
    //esto se ejecuta del lado del cliente
    $("#sorteos_list").listview();
</script>
<?php else: ?>
    <div><h2>No hay sorteos que mostrar</h2></div>
<?php endif; ?>