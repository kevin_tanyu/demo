<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Revision_lib
{
    private $ci;
    private $db;

    public function __construct()
    {
       $this->ci = & get_instance();
       $this->db = $this->ci->load->database('suerte', true);
       // $this->db = $this->ci->load->database('pdo', true);
    }

    //METODO QUE PROCESA LA SOLICITUD DE REVISION
    public function insertRevision($userId, $sorteoId, $monto, $jugada1, $jugada2){
        $return = array('isValid' => true, 'msg' => 'Enviado');


        try{
            $sqlParamsSelect = array(':user_id' => $userId, ':sorteo_prog_id' => $sorteoId);
            $sqlgetCOFRevision = "SELECT * FROM COF_Revision
                                  WHERE usuario_vendedor_calle_id = ? and sorteo_programacion_id = ?";
            $result = $this->db->query($sqlgetCOFRevision, $sqlParamsSelect)->result();

            if(sizeof($result) > 0){
                $COF = $result[0]->id;
            }else{
                $sqlInsertRevision = "INSERT INTO COF_Revision(usuario_vendedor_calle_id, sorteo_programacion_id) VALUES(?,?)";
                $sqlParamsInsertRevision = array(':user_id' => $userId, ':sorteo_prog_id' => $sorteoId);
                $this->db->query($sqlInsertRevision, $sqlParamsInsertRevision);

                $sqlParamsSelect = array(':user_id' => $userId, ':sorteo_prog_id' => $sorteoId);
                $result = $this->db->query($sqlgetCOFRevision, $sqlParamsSelect)->result();
                $COF = $result[0]->id;
            }


            $sqlParamsInsert = array(':Cof' => $COF, ':status' => 3, ':hora' => date("Y-m-d H:i:s"), ':user_id' => $userId,
                ':jud1' => $jugada1, ':jud2' => $jugada2, ':monto' => $monto, ':terminal' => 0);
            $sqlInsertEstado = "INSERT INTO COF_Revision_Log(cof_revision_id, cof_new_status_id, created_at, user_id, jugada1, jugada2, revision_monto, monto_terminal)
                        VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
            $this->db->query($sqlInsertEstado, $sqlParamsInsert);




        }catch (Exception $e){
            $return['isValid']   = false;
            $return['msg'] = 'Problema ingresando la solicitud';
        }

        return $return;

    }//FIN insertRevision

    public function consultaRevision($userId, $sorteoId){
        $return = array( 'msg' => '');
        try{

            $sqlParamsSelect = array(':user_id' => $userId, ':sorteo_prog_id' => $sorteoId);
            //PAra obtener el estado de revision del sorteo
            $sqlStatusSorteo = "SELECT S.display_name
                                FROM COF_RevisionStatus_View RS
                                JOIN COF_Status S ON RS.status_id = S.id
                                WHERE RS.usuario_vendedor_calle_id = ? and RS.sorteo_programacion_id = ?";
            $result = $this->db->query($sqlStatusSorteo, $sqlParamsSelect)->result();


            if(count($result) == 0){
                $return['msg'] = 'Sin solicitud';
            }else{
                $return['msg'] = $result[0]->display_name;
            }



        }catch (Exception $e){
            $return['isValid']   = false;
            $return['msg'] = 'Problema ingresando la solicitud';
        }

        return $return;

    }//FIN consultaRevision

}