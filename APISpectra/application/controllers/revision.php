<?php


class Revision extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('revision_lib');
    }

    public function recibirRevisionSPECTRA($userId, $sorteoId, $monto, $jugada1, $jugada2){
        $result         = $this->revision_lib->insertRevision($userId, $sorteoId, $monto, $jugada1, $jugada2);
        echo json_encode($result);

    }

    public function consultaRevisionSPECTRA($userId, $sorteoId){
        $result         = $this->revision_lib->consultaRevision($userId, $sorteoId);
        echo json_encode($result);
    }



}
