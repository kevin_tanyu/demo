<?php
$usuariosViewDatos = array(NIVEL_PERMISO_USUARIO_ADMIN);
$usuariosViewJugadas = array(NIVEL_PERMISO_USUARIO_ADMIN);
$usuariosViewConsultas = array(NIVEL_PERMISO_USUARIO_ADMIN, NIVEL_PERMISO_USUARIO_WEB);
$usuariosViewUsuarios = array(NIVEL_PERMISO_USUARIO_ADMIN, NIVEL_PERMISO_USUARIO_WEB);
$usuariosViewPitazo = array();
$usuariosViewParlay = array(NIVEL_PERMISO_USUARIO_ADMIN);
$usuariosViewBalance = array(NIVEL_PERMISO_USUARIO_WEB);
$usuariosViewCupones = array(NIVEL_PERMISO_USUARIO_WEB);
$usuarioImportPuestos = array(NIVEL_PERMISO_USUARIO_CALLE);
$usuarioCompraSeguro = array(1);

if (!isset($_SESSION['IDUsuario']))
{
    header('Location: index.php');
    exit;
}

?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>- - ElDiaDeSuerte.net - -</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main_main.css">
    <link rel="stylesheet" href="css/colorbox.css">
    <link rel="stylesheet" href="js/bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css"/>

    <script type="text/javascript" src="js/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.4"></script>
    <script type="text/javascript" src="js/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/plugins.js"></script>
    <script type="text/javascript" src="js/jquery.form.min.js"></script>
    <script type="text/javascript" src="js/bPopup.js"></script>

    <link href="jtable/themes/redmond/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
    <link href="jtable/scripts/jtable/themes/metro/darkorange/jtable.css" rel="stylesheet" type="text/css" />

    <script src=jtable/"scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="jtable/scripts/jtable/jquery.jtable.js" type="text/javascript"></script>


    <link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.4" media="screen"/>

    <script src="js/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/printThis.js" type="text/javascript"></script>
    <script type="text/javascript">
        var config = {
            '.chzn-select': {},
            '.chzn-select-deselect': {allow_single_deselect: true},
            '.chzn-select-no-single': {disable_search_threshold: 10},
            '.chzn-select-no-results': {no_results_text: 'Sin Resultados!'},
            '.chzn-select-width': {width: "95%"}
        };
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    </script>
    <link rel="stylesheet" href="js/chosen.css"/>
    <style type="text/css" media="all">
        /* fix rtl for demo */
        .chzn-rtl .chzn-search {
            left: -9000px;
        }

        .chzn-rtl .chzn-drop {
            left: -9000px;
        }
    </style>

</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to
    improve your experience.</p>
<![endif]-->

<div class="header-container">
    <header class="wrapper clearfix">
        <h1 class="title">ElDiaDeSuerte.net</h1>

        <nav>
            <ul>
                <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuariosViewCupones)): ?>
                    <li><a href="seccion_cupones.php">Cupones</a></li>
                <?php endif; ?>
                <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuariosViewBalance)): ?>
                    <li><a href="seccion_balances.php">Balances</a></li>
                <?php endif; ?>
                <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuariosViewParlay)): ?>
                    <!--<li><a href="seccion_parlay.php">Parlay</a></li>-->
                <?php endif; ?>
                <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuariosViewPitazo)): ?>
                    <li><a href="pitazo_seccion_datos.php">Pitazo</a></li>
                <?php endif; ?>
                <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuariosViewUsuarios)): ?>
                    <li><a href="seccion_usuarios.php">Usuarios</a></li>
                <?php endif; ?>
                <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuariosViewConsultas)): ?>
                    <li><a href="seccion_consultas.php">Consultas</a></li>
                <?php endif; ?>

                <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuariosViewDatos)): ?>
                    <li><a href="seccion_datos.php">Datos</a></li>
                <?php endif; ?>
                <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuarioImportPuestos)): ?>
                    <li><a href="seccion_enviar_lista_manual.php">Recepcion Ventas</a></li>
                <?php endif; ?>
                <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuarioImportPuestos)): ?>
                    <li><a href="seccion_enviar_archivo.php">Subir Lista</br></a></li>
                <?php endif; ?>
                <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuarioImportPuestos)): ?>
                    <li><a href="seccion_enviar_revision.php">Revision</br></br></a></li>
                <?php endif; ?>
                <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuarioImportPuestos)): ?>
                    <li><a href="seccion_enviar_historial.php">Historial</br></br></a></li>
                <?php endif; ?>
                <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuarioImportPuestos)
                    && in_array($_SESSION['Seguros'], $usuarioCompraSeguro)): ?>
                    <li><a href="seccion_seguros.php">Seguros</br></br></a></li>
                <?php endif; ?>

            </ul>
        </nav>
    </header>
</div>
<div style="width: 100%; text-align: right;">Bienvenido: <?php if (strpos($_SESSION['NombreUsuario'],'IMPORT') === false) {
                                         echo $_SESSION['NombreUsuario'];
                                      }else{echo substr($_SESSION['NombreUsuario'], 7);} ?>. <a href="logout.php"
                                                                                                      style=" padding-right: 0.5em; ">Salir</a>
</div>
<div class="main-container">
    <div class="main wrapper clearfix">