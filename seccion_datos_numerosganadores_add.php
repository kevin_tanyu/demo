<?php
session_start();

if (!isset($_SESSION['ActivoFlag']))
{
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
require_once("config.ini.php");
require_once("conectadb.php");
require_once("lib/seccion_datos_numerosganadores_lib.php");

$idUser = $_SESSION['IDUsuario'];

$idSorteoProgramacion = $_POST['IDSorteoProgramacion'];

for ($x = 1; $x <= $_POST['CantidadNumeros']; $x++)
{

    // Ingresa numero ganador en base de datos
    $QUERY = "INSERT INTO SorteosNumerosGanadores
											(Numero, Orden, FechayHora, ActivoFlag, IDSorteoProgramacion, IDSorteoDefinicion)
						VALUES
											(" . $_POST['NumeroGanador' . $x] . ", " . $x . ", now(), 1, " . $_POST['IDSorteoProgramacion'] . ", " . $_POST['IDSorteoDefinicion'] . ")";

    $numeroGanador1 = $_POST['NumeroGanador' . $x];
    $rs = mysql_query($QUERY);

    $QUERY     = "SELECT LAST_INSERT_ID()";
    $rs        = mysql_query($QUERY);
    $IDGanador = mysql_fetch_row($rs);

    if($_POST['IDSorteoDefinicion'] == 28){
        // Ingresa numero ganador en base de datos
        $x = $x + 1;
        $QUERY = "INSERT INTO SorteosNumerosGanadores
											(Numero, Orden, FechayHora, ActivoFlag, IDSorteoProgramacion, IDSorteoDefinicion)
						VALUES
											(" . $_POST['NumeroGanador' . $x] . ", " . $x . ", now(), 1, " . $_POST['IDSorteoProgramacion'] . ", " . $_POST['IDSorteoDefinicion'] . ")";

        $numeroGanador = $_POST['NumeroGanador' . $x];
        $rs = mysql_query($QUERY);

        $x = $x + 1;
        // Ingresa numero ganador en base de datos
        $QUERY = "INSERT INTO SorteosNumerosGanadores
											(Numero, Orden, FechayHora, ActivoFlag, IDSorteoProgramacion, IDSorteoDefinicion)
						VALUES
											(" . $_POST['NumeroGanador' . $x] . ", " . $x . ", now(), 1, " . $_POST['IDSorteoProgramacion'] . ", " . $_POST['IDSorteoDefinicion'] . ")";

        $numeroGanador = $_POST['NumeroGanador' . $x];
        $rs = mysql_query($QUERY);
    }

    if ($_POST['CantidadNumeros'] == 1)
    {
        // En caso de ser un sorteo de un solo numero actualiza todos los ganadores
        $QUERY = "UPDATE SorteoApuesta
								SET			FlagGanador=1,
												IDNumeroGanador=" . $IDGanador[0] . "
								WHERE	IDSorteoProgramacion=" . $_POST['IDSorteoProgramacion'] . "
								AND			Numero=" . $_POST['NumeroGanador1'];

        $QUERY_CLIENTES = "UPDATE CLIENTE_SorteoApuesta
								SET			FlagGanador=1,
												IDNumeroGanador=" . $IDGanador[0] . "
								WHERE	IDSorteoProgramacion=" . $_POST['IDSorteoProgramacion'] . "
								AND			Numero=" . $_POST['NumeroGanador1'];

    } else
    {

        // En caso de ser un sorteo de multiples ganadores uso el FlagGanador para dejar los acertados en "StandBy"
        $QUERY = "UPDATE SorteoApuesta
								SET			FlagGanador=99,
												IDNumeroGanador=" . $IDGanador[0] . "
								WHERE	IDSorteoProgramacion=" . $_POST['IDSorteoProgramacion'] . "
								AND			Numero=" . $_POST['NumeroGanador1'];

    }

    $rs                     = mysql_query($QUERY);
    $GANADORES[$x]          = mysql_affected_rows();
    $rsClientes             = mysql_query($QUERY_CLIENTES);
    $GANADORES_CLIENTES[$x] = mysql_affected_rows();
}

// Si son multimples hay que hacer el grading de los que resultaron ganadores
if ($_POST['CantidadNumeros'] > 1)
{

    $QUERY = "SELECT 		IDUsuario,
										IDTicket,
										count(id)
					FROM 			SorteoApuesta
					WHERE		FlagGanador=99
					AND				IDSorteoProgramacion=" . $_POST['IDSorteoProgramacion'] . "
					GROUP BY	IDUsuario,
										IDTicket";
    $rs    = mysql_query($QUERY);

    if (mysql_affected_rows() > 0)
    {

        // Hay Resultados
        // Recorro el ResultSet a ve si hay coincidencias segun MultipleMinimoGana

        if ($row = mysql_fetch_row($rs))
        {

            if ($row[2] >= $_POST['MultipleMinimoGana'])
            {

                $QUERY = "UPDATE	SorteoApuesta
								SET			FlagGanador=88,
								WHERE	IDUsuario=" . $row[0] . "
								AND 		IDTicket=" . $row[1] . "
								AND			IDSorteoProgramacion=" . $_POST['IDSorteoProgramacion'];
                $rs    = mysql_query($QUERY);

            }

        }

    }


}


//SQL PARA LOS USUARIOS BANCA
$sqlBancas = "SELECT U.ID, U.PagaPorcentaje, U.Recepcion, U.Reparticion, U.Comision
                           FROM Usuarios U JOIN Usuarios P ON U.IDPadre = P.ID
                           WHERE U.NivelUsuario
                           IN (SELECT id from UsuariosNivel where jerarquia = 'Bank') and U.FlagActivo=1 and U.ActivoFlag=1";
$stmtBancas = $pdoConn->prepare($sqlBancas);
$stmtBancas->execute();
$bancas = $stmtBancas->fetchAll(PDO::FETCH_ASSOC);

//SQL INSERCCION
$sqlBalances = "INSERT INTO Balances_Bancas(sorteo_id, user_id, segurosTotal, comision, premio, balance, fechayhora)
                VALUES (:sorteoID,:userID,:segurosTotal,:comision,:premio,:balance, NOW())";
$stmtBalances = $pdoConn->prepare($sqlBalances);

//SQL CAPTURA BALANCES TOTALES
$sqlCaptura = "SELECT * FROM Balances_Total_Banca
                 WHERE user_id = ?";
$stmtCapturaTotal = $pdoConn->prepare($sqlCaptura);

//SQL ACTUALIZA BALANCES TOTALES
$sqlActualiza = "UPDATE Balances_Total_Banca
                 SET balanceTotal = ?
                 WHERE user_id = ?";
$stmtActualiza = $pdoConn->prepare($sqlActualiza);

//SQL PARA RECUPERAR LOS NUMEROS Y MONTOS POR USUARIO
$sqlSeguros = "SELECT numero, monto FROM seg_lista_seguros_banca
        WHERE sorteo_id = :sorteo_id and user_id = :id_usuario
        ORDER BY numero ASC";
$stmtSeguros = $pdoConn->prepare($sqlSeguros);


foreach ($bancas as $banca){
    $totalSeguros = 0;
    $comision = 0;
    $premio = 0;
    $balance = 0;

    /*CAPTURA TOTAL ACUMULADO**/
    $stmtCapturaTotal->execute(array($banca['ID']));
    $capturaTotal = $stmtCapturaTotal->fetch();
    $balanceTotal = $capturaTotal['balanceTotal'];

    $stmtSeguros->execute(array(':sorteo_id' => $idSorteoProgramacion, ':id_usuario' => $banca['ID']));
    $numerosSeguros = $stmtSeguros->fetchAll(PDO::FETCH_ASSOC);

    foreach($numerosSeguros as $numero){
        $totalSeguros = $totalSeguros + $numero['monto'];

        if($numero['numero'] == $numeroGanador1){
            $premio = $numero['monto'] * 75;
        }


    }//FIN FOREACH NUMERO

    $porcen = $banca['Comision'] / 100;
    $comision = $porcen * $totalSeguros;

    $balance = $totalSeguros - $comision - $premio;

    //INSERTA LOS BALANCES DE CADA SORTEO
    $stmtBalances->execute(array(':sorteoID' => $idSorteoProgramacion, ':userID' => $banca['ID'], ':segurosTotal' => $totalSeguros,
        ':comision' => $comision,':premio' => $premio, ':balance' => $balance));

    $balanceTotal = $balanceTotal + $balance;

    //ACTUALIZA EL TOTAL ACUMULADO
    $stmtActualiza->execute(array($balanceTotal,$banca['ID']));

}//FIN FOREACH BANCA

//inserta los premios a los jugadores clientes
$results = array();
mysql_query("BEGIN");
$sqlUpdateBolsa = "UPDATE CLIENTE_balance_bolsa_premios AS bbp
  JOIN Usuarios client ON client.ID = bbp.IDUsuario
  JOIN Usuarios revendedor ON revendedor.ID = client.IDPadre
  JOIN CLIENTE_Ticket t ON t.IDUsuario = client.ID
  JOIN CLIENTE_SorteoApuesta sa ON sa.FlagGanador = 1 AND sa.IDTicket = t.id AND sa.IDSorteoProgramacion = $idSorteoProgramacion
SET bbp.disponible = bbp.disponible + (sa.Cantidad*revendedor.PagaPorcentaje)";
$result[] = mysql_query($sqlUpdateBolsa);

$sqlInsertDeposito = "INSERT INTO CLIENTE_balance_deposito (IDUsuarioCliente,created_at,IDTicket,IDTipoDeposito,monto)
    SELECT client.ID,now(),t.id,bdt.id,(sa.Cantidad*revendedor.PagaPorcentaje)
    FROM CLIENTE_balance_bolsa_premios AS bbp
      JOIN Usuarios client ON client.ID = bbp.IDUsuario
      JOIN Usuarios revendedor ON revendedor.ID = client.IDPadre
      JOIN CLIENTE_Ticket t ON t.IDUsuario = client.ID
      JOIN CLIENTE_SorteoApuesta sa ON sa.FlagGanador = 1 AND sa.IDTicket = t.id AND sa.IDSorteoProgramacion = $idSorteoProgramacion
      JOIN CLIENTE_balance_deposito_tipo bdt ON bdt.nombre = 'premio'";
$result[] = mysql_query($sqlInsertDeposito);

if (in_array(false, $result))
{
    mysql_query("ROLLBACK");
    $sqlLog = "INSERT INTO Log (IDUsuario, Seccion, FechayHora, Detalles) VALUES ($usuarioId,'numerosganadores_add',now(),'$sqlUpdateBolsa;$sqlInsertDeposito')";
    mysql_query($sqlLog);
} else
{
    mysql_query("COMMIT");
}

//scoring para parlay
//un solo numero
$numeroGanadorParlay = $_POST['NumeroGanador1'];

$sqlObtenerEventInfo = "SELECT id, par_event_id FROM PAR_Event_Part WHERE sorteo_prog_id = ?";
$stmtEventInfo       = $pdoConn->prepare($sqlObtenerEventInfo);
$stmtEventInfo->execute(array($idSorteoProgramacion));
$eventInfo = $stmtEventInfo->fetch(PDO::FETCH_ASSOC);

$paramsEventResult                   = array();
$paramsEventResult[':event_part_id'] = $eventInfo['id'];
$paramsEventResult[':event_id']      = $eventInfo['par_event_id'];
$paramsEventResult[':number']        = $numeroGanadorParlay;

$sqlInsertEventResult  = "INSERT INTO PAR_Event_Result (par_event_id, par_event_part_id, number)
                         VALUES (:event_id, :event_part_id, :number)";
$stmtInsertEventResult = $pdoConn->prepare($sqlInsertEventResult);
try
{
    $stmtInsertEventResult->execute($paramsEventResult);
} catch (PDOException $e)
{
    //todo handle exception
    echo $e->getMessage();
}

//datos de vendedores para envio de correos
$vendedores = sdn_obtener_vendedores_calle_con_tickets($pdoConn, $idSorteoProgramacion);

//datos de revendedores para envio de correos
$re_vendedores = sdn_obtener_vendedores_web_con_tickets($pdoConn, $idSorteoProgramacion);

$usuariosAgrupados = array('calle' => $vendedores, 'web' => $re_vendedores);

//envio de correos
sdn_enviaCorreos($usuariosAgrupados, $infoSorteo);

//GENERAR BALANCES A LA BANCA
try{




}catch (PDOException $e){

    echo $e->getMessage();
}