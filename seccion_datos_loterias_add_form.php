
<h3 style="line-height:1px;">Agregar Loteria</h3>
					<div style="text-align:center;">
						<form name="formulario1" id="formulario1">
						<div class="divTable">
							<div class="divRow">
								<div class="divCellIzq">Nombre de la Loteria</div>
								<div class="divCellDer"><input type="text" name="NombreSorteo" class="campotexto" required placeholder="Nombre"></div>
							</div>
							<div class="divRow">
								<div class="divCellIzq">Cantidad numeros Ganadores</div>
								<div class="divCellDer"><input type="number" name="NumerosGanadores" class="campotexto" style="width:40px;" required min="1" max="10" value="1"></div>
							</div>
							<div class="divRow">
								<div class="divCellIzq" style="vertical-align:center; top-margin:20px;">Debe acertar multiples numeros?</div>
								<div class="divCellDer"><div class="squaredTwo"><input type="checkbox" id="FlagMultiple" name="FlagMultiple" value="1" checked="false"><label for="FlagMultiple"></label></div></div>
							</div>
							<div class="divRow">
								<div class="divCellIzq">Cantidad de Numeros a Acertar</div>
							    <div class="divCellCen"><input type="number" name="FlagMultipleMinimoGana" class="campotexto" style="width:40px;" placeholder="1-10"></div>
							</div>
							<div class="divRow">
								<div class="divCellIzq">Paga</div>
								<div class="divCellDer"><input type="number" name="PagaPorcentaje" class="campotexto" style="width:40px;" placeholder="Veces"></div>
							</div>
						</div>
						<div class="divTable">
							<div class="divRow">
								<div class="divCellHeader">L</div>
								<div class="divCellHeader">M</div>
								<div class="divCellHeader">M</div>
								<div class="divCellHeader">J</div>
								<div class="divCellHeader">V</div>
								<div class="divCellHeader">S</div>
								<div class="divCellHeader">D</div>
							</div>
							<div class="divRow">
								<section><div class="divCellCen"><div class="squaredTwo"><input type="checkbox" id="Lunes" name="Lunes" value="1" checked="false"><label for="Lunes"></label></div></div></section>
								<section><div class="divCellCen"><div class="squaredTwo"><input type="checkbox" id="Martes" name="Martes" value="1" checked="false"><label for="Martes"></label></div></div></section>
								<section><div class="divCellCen"><div class="squaredTwo"><input type="checkbox" id="Miercoles" name="Miercoles" value="1" checked="false"><label for="Miercoles"></label></div></div></section>
								<section><div class="divCellCen"><div class="squaredTwo"><input type="checkbox" id="Jueves" name="Jueves" value="1" checked="false"><label for="Jueves"></label></div></div></section>
								<section><div class="divCellCen"><div class="squaredTwo"><input type="checkbox" id="Viernes" name="Viernes" value="1" checked="false"><label for="Viernes"></label></div></div></section>
								<section><div class="divCellCen"><div class="squaredTwo"><input type="checkbox" id="Sabado" name="Sabado" value="1" checked="false"><label for="Sabado"></label></div></div></section>
								<section><div class="divCellCen"><div class="squaredTwo"><input type="checkbox" id="Domingo" name="Domingo" value="1" checked="false"><label for="Domingo"></label></div></div></section>
							</div>
						</div>
						<input type="button" value="Agregar" class="button" id="BotonFormulario" onclick="javascript:FormularioAddLoteria();">
						</form>

					</div>
					<div id="Resultado"></div>
					<br><br>
