<?php
session_start();

if (!isset($_SESSION['ActivoFlag']))
{
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");
require_once 'parametros.ini.php';

$montoTopeInicial = $MONTO_TOPE_INICIAL;

$fecha = $_POST['Fecha'];
$hora = $_POST["Hora"];
$minutos = $_POST["Minutos"];
$fechaCompleta = $fecha . " " . $hora . ":" . $minutos . ":00";
$idSorteoDefinicion = $_POST["IDSorteoDefinicionValor"];
$numeroSuerte = $_POST['NumeroSuerte'];


$QUERY = "INSERT INTO SorteosProgramacion (FechayHora, IDSorteoDefinicion, NumeroDeSuerte, NumeroDeSuerte2, MontoTope )
VALUES ( ? , ?, ?, ?, ? )";
$stmtCreate = $pdoConn->prepare($QUERY);
$stmtCreate->execute(array($fechaCompleta, $idSorteoDefinicion, $numeroSuerte[0], $numeroSuerte[1], $montoTopeInicial));

$IDPROGRAMACION = $pdoConn->lastInsertId();

$created_at = date('Y-m-d H:i:s');

if ($_POST["NumeroRestringido"] != "")
{
    $QUERY                  = "INSERT INTO SorteosNumerosRestringidos (IDSorteoProgramacion, Numero, ReglaPorcentaje) VALUES (?, ?, ?)";
    $stmtCreateRestringidos = $pdoConn->prepare($QUERY);
    foreach ($_POST["NumeroRestringido"] as $CAD)
    {
        $stmtCreateRestringidos->execute(array($IDPROGRAMACION, $CAD, $REGLA_PORCENTAJE));
    }

    $QUERYCLIENTES         = "INSERT INTO CLIENTE_SorteosNumerosRestringidosIniciales (IDSorteoProgramacion, Numero, created_at) SELECT IDSorteoProgramacion, Numero, ? FROM SorteosNumerosRestringidos WHERE IDSorteoProgramacion = ? ";
    $stmtCreateClientesRes = $pdoConn->prepare($QUERYCLIENTES);
    $stmtCreateClientesRes->execute(array($created_at, $IDPROGRAMACION));
}

//ingreso de topes minimos para los clientes
$sqlInsertTopesMinimos =
    "INSERT INTO CLIENTE_SorteosMontosTope (IDSorteoProgramacion, TopeRestringidos, TopeRestringidosMixtos, TopeNoRestringidos)
    VALUES (?,?,?,? )";
$stmtInsertTopesMinimos = $pdoConn->prepare($sqlInsertTopesMinimos);
$stmtInsertTopesMinimos->execute(array($IDPROGRAMACION, CLIENTE_TOPE_MINIMO_RESTRINGIDOS, CLIENTE_TOPE_MINIMO_RESTRINGIDOS_MIXTOS, CLIENTE_TOPE_MINIMO_NO_RESTRINGIDOS));

$sqlRestringidosClientes = "INSERT INTO CLIENTE_SorteosNumerosRestringidos (IDSorteoProgramacion, Numero, created_at) SELECT  IDSorteoProgramacion,Numero,created_at FROM CLIENTE_SorteosNumerosRestringidosIniciales WHERE IDSorteoProgramacion = ?";
$stmtRestringidosClientes = $pdoConn->prepare($sqlRestringidosClientes);
$stmtRestringidosClientes->execute(array($IDPROGRAMACION));

$sqlNoRestringidos = "INSERT INTO CLIENTE_SorteosNumerosAbiertos (IDSorteoProgramacion, numero, created_at) SELECT ?,N.Numero,? FROM Numeros N LEFT JOIN CLIENTE_SorteosNumerosRestringidos R ON N.numero = R.numero AND R.IDSorteoProgramacion = ? WHERE R.created_at IS NULL ";
$stmtNoRestringidos = $pdoConn->prepare($sqlNoRestringidos);
$stmtNoRestringidos->execute(array($IDPROGRAMACION, $created_at, $IDPROGRAMACION));

if (isset($_POST["VariasFechas"]) && $_POST["VariasFechas"] == 1)
{
    // Eligio Varias Fechas
    for ($x = 1; $x <= 30; $x++)
    {
        // Inicia Ciclo
        if ($_POST["SuerteAdd" . $x] != 0)
        {
            // Hay adicional
            $QUERY = "INSERT INTO SorteosProgramacion (FechayHora, IDSorteoDefinicion, NumeroDeSuerte) VALUES ('" . $_POST["FechaAdd" . $x] . "'," . $_POST["IDSorteoDefinicionValor"] . "," . $_POST["SuerteAdd" . $x] . ")";
            $rs    = mysql_query($QUERY);

            $QUERY = "SELECT LAST_INSERT_ID()";
            $rx    = mysql_query($QUERY);
            $rox   = mysql_fetch_row($rx);

            print_r($_POST["NumeroResAdd" . $x]);

            if (isset($_POST["NumeroResAdd" . $x]))
            {
                echo "Entro al IF \n";
                foreach ($_POST["NumeroResAdd" . $x] as $CAD)
                {
                    echo "Entro al for \n";
                    echo $QUERY = "INSERT INTO SorteosNumerosRestringidos (IDSorteoProgramacion, Numero, ReglaPorcentaje) VALUES (" . $rox[0] . "," . $CAD . "," . $REGLA_PORCENTAJE . ")";
                    $rs = mysql_query($QUERY);
                }
            }
        }
    }
}

//creacion del evento para parlay
$sorteoBaseParlay = 'MEDIO DIA';

//primero averiguar si el sorteo es de mediodia
//obtener el id del sorteoDefinicion mediodia
$sqlIdSorteoMed = "SELECT ID FROM SorteosDefinicion SD WHERE SD.NombreSorteo = ?";
$stmtIdSorteoMed = $pdoConn->prepare($sqlIdSorteoMed);
$stmtIdSorteoMed->execute(array($sorteoBaseParlay));
$sorteoBaseParlayIdArray = $stmtIdSorteoMed->fetch(PDO::FETCH_ASSOC);
$sorteoBaseParlayId = $sorteoBaseParlayIdArray['ID'];

$sqlCheckSiBaseParlay = "SELECT SP.ID FROM SorteosProgramacion SP WHERE SP.ID = ? AND SP.IDSorteoDefinicion = ?";
$stmtCheckSiBaseParlay = $pdoConn->prepare($sqlCheckSiBaseParlay);
$stmtCheckSiBaseParlay->execute(array($IDPROGRAMACION, $sorteoBaseParlayId));
$countBaseParlay = $stmtCheckSiBaseParlay->rowCount();

if ($countBaseParlay > 0)
{
//obtener el ultimo sorteoprog del dia anterior
//  a la fecha del sorteo
    $sqlGetLastSorteo = "SELECT SP.ID, SP.FechayHora FROM SorteosProgramacion SP WHERE DATEDIFF( SP.FechayHora, DATE_ADD( ?, INTERVAL -1 DAY ) ) = 0 ORDER BY SP.FechayHora DESC";
    $stmtLastSorteo   = $pdoConn->prepare($sqlGetLastSorteo);
    $stmtLastSorteo->execute(array($fechaCompleta));
    $lastSorteo = $stmtLastSorteo->fetch(PDO::FETCH_ASSOC);
    //Una vez que se tengan los id y las fechas de los dos sorteos, se crea un evento nuevo
    // con fecha de display hasta el primer sorteo que se vaya a dar
    // luego de que se crea el evento, obtener el id del mismo
    if ($lastSorteo !== false)
    {
        $sqlCreateEvent  = "INSERT INTO PAR_Event (display_until) VALUES (?)";
        $stmtCreateEvent = $pdoConn->prepare($sqlCreateEvent);
        $stmtCreateEvent->execute(array($lastSorteo['FechayHora']));
        $eventId         = $pdoConn->lastInsertId();
        $sorteosToInsert = array($lastSorteo['ID'], $IDPROGRAMACION);
        // y crear los event_part, con los sorteos recien creados
        $sqlCreateEventPart = "INSERT INTO PAR_Event_Part (par_event_id, sorteo_prog_id, hit_order) VALUES (?,?,?)";
        $stmtCreateEventPart = $pdoConn->prepare($sqlCreateEventPart);
        $eventPartHitOrder  = 1;
        foreach ($sorteosToInsert as $sorteoId)
        {
            $params = array($eventId, $sorteoId, $eventPartHitOrder);
            $stmtCreateEventPart->execute($params);
            ++$eventPartHitOrder;
        }
    }

}


//creacion de archivo para la descarga de restringidos en el ftp

$codigosLoterias = array(
    'MEDIO DIA' => '01',
    'DIGITALES' => '02',
    'COSTA RICA' => '03',
    'PANAMA' => '04'
);

$sqlNextSorteos = "SELECT SP.ID, SP.FechayHora, SD.NombreSorteo
                    FROM SorteosProgramacion SP
                    JOIN SorteosDefinicion SD ON SD.ID = SP.IDSorteoDefinicion AND SD.FlagActivo = 1
                    WHERE SP.FechayHora >= NOW() ";
$stmtNextSorteos = $pdoConn->prepare($sqlNextSorteos);

$sqlGetRestringidos = "SELECT Numero FROM SorteosNumerosRestringidos WHERE IDSorteoProgramacion = :sorteo_prog";
$stmtGetRestringidos = $pdoConn->prepare($sqlGetRestringidos);

$stmtNextSorteos->execute();
$fileLines = array();
//get info
foreach ($stmtNextSorteos->fetchAll(PDO::FETCH_ASSOC) as $sorteo)
{
    $stmtGetRestringidos->execute(array(':sorteo_prog' => $sorteo['ID']));
    $restringidos = $stmtGetRestringidos->fetchAll(PDO::FETCH_ASSOC);
    $restringidosReal = array();
    //formateo y extraccion de restringidos
    foreach($restringidos as $resInfo)
        $restringidosReal[] = str_pad($resInfo['Numero'], 2, '0', STR_PAD_LEFT);

    //fecha
    $time = strtotime($sorteo['FechayHora']);

    $fileLines[] =
        'R: ' . implode(' ', $restringidosReal)
        . ' L: ' . $codigosLoterias[$sorteo['NombreSorteo']]
        . ' S: ' . $sorteo['ID']
        . ' F: ' . date('dmyHi', $time)
    ;
}

//write to file
$txt = implode("\n", $fileLines);
var_dump($txt);
$file = fopen(HS_RESTRINGIDOS_FTP_FILE, 'w');
flock($file, LOCK_EX); //obtiene lock exclusivo sobre el archivo
$result = fwrite($file, $txt);
flock($file, LOCK_UN); //libera el lock para que otros puedan leer
fclose($file);
if (is_bool($result) && $result === false)
{
    echo "Error while writing R file.";
}

?>
