<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 10/30/14
 * Time: 5:47 PM
 */

session_start();

require_once 'config.ini.php';
require_once 'conectadb.php';
require_once 'lib/seccion_usuarios_vendedores_parametros_lib.php';

$vendedorCalleId = $_GET['id'];


$loteriasSql = "SELECT ID, NombreSorteo FROM SorteosDefinicion WHERE FlagActivo = 1";
$loteriasStmt = $pdoConn->prepare($loteriasSql);
$loteriasStmt->execute();
$loterias = $loteriasStmt->fetchAll(PDO::FETCH_ASSOC);

$parametrosUsuarioSql = "SELECT UCvp.IDUsuario, UCvp.IDSorteoDefinicion,
    UCvp.porcentaje_restriccion, UCvp.tope_inicial
    , SD.NombreSorteo
    FROM USUARIOS_CALLE_venta_parametros UCvp
     JOIN SorteosDefinicion SD ON SD.ID = UCvp.IDSorteoDefinicion
    WHERE IDUsuario = :usuario_id";
$parametrosUsuarioStmt = $pdoConn->prepare($parametrosUsuarioSql);
$parametrosUsuarioStmt->execute(array(':usuario_id' => $vendedorCalleId));
$parametros = $parametrosUsuarioStmt->fetchAll(PDO::FETCH_ASSOC);

$opcionesLoteria = get_opciones_desplegar($parametros, $loterias, $vendedorCalleId);

$seguroUsuarioSql = "SELECT compra_seguros FROM Usuarios where ID = :usuario_id";
$stmtSeguroUsuarios = $pdoConn->prepare($seguroUsuarioSql);
$stmtSeguroUsuarios->execute(array(':usuario_id' => $vendedorCalleId));
$seguroUsuario = $stmtSeguroUsuarios->fetchAll(PDO::FETCH_ASSOC);
$ifCompraSeg = $seguroUsuario[0]['compra_seguros'];

/*
 * Asi es cada uno de los arrays que devuelve get_opciones_desplegar
    ('user_id' => $userId,
    'sorteo_definicion' => $loteria['ID'],
    'sorteo_definicion_nombre' => $parametro['NombreSorteo'],
    'porcentaje_restriccion' => 15,
    'tope_inicial' => 2500)
 */

?>

<form method="post" id="form_modifica_vendedor_parametros" action="seccion_usuarios_vendedores_parametros_action.php">
    <legend>Modificacion de parametros de vendedor calle</legend>
    <?php foreach ($opcionesLoteria as $opcion): ?>
        <?php
        $loteriaId      = $opcion['sorteo_definicion'];
        $nameUser       = 'loteria[' . $loteriaId . '][user_id]';
        $nameTope       = 'loteria[' . $loteriaId . '][tope_inicial]';
        $namePorcentaje = 'loteria[' . $loteriaId . '][porcentaje_restringido]';
        ?>
        <input type="hidden" name="<?php echo $nameUser ?>" value="<?php echo $vendedorCalleId ?>"/>
        <fieldset>
            <legend><?php echo $opcion['sorteo_definicion_nombre'] ?></legend>
            <div class="divTable">
                <div class="divRow">
                    <div class="divCellIzq letra"><label>Monto Tope Inicial</label></div>
                    <div class="divCellDer"><input name="<?php echo $nameTope ?>" type="number"
                                                   value="<?php echo $opcion['tope_inicial'] ?>"/></div>
                </div>
                <div class="divRow">
                    <div class="divCellIzq letra"><label>Porcentaje Restriccion</label></div>
                    <div class="divCellDer"><input name="<?php echo $namePorcentaje ?>" type="number"
                                                   value="<?php echo $opcion['porcentaje_restriccion'] ?>"></div>
                </div>

            </div>
        </fieldset>
    <?php endforeach; ?>
    <div class="divRow">
        <div class="divCellIzq letra"><label style="font-weight: bold; font-size: 16px">Compra seguros</label></div>
        <div class="divCellDer">
           <select id="slcSeguro" name="slcSeguro">
               <?php if($ifCompraSeg == 1){?>
                 <option value="1" selected>Si</option>
                 <option value="0">No</option>
               <?php }else{?>
                   <option value="1">Si</option>
                   <option value="0" selected>No</option>
               <?php }?>
           </select>
        </div>
    </div>
    <input type="submit" value="Guardar" class="button">

    <div id="params_result"></div>
</form>
<script>
    $('#form_modifica_vendedor_parametros').ajaxForm({
        target: '#params_result',
        beforeSubmit: function () {
            $('#form_modifica_vendedor_parametros').block();
        },
        success: function () {
            $('#form_modifica_vendedor_parametros').unblock();
        }
    });

</script>