<!DOCTYPE html>

<!-- *********************************************************************************************

Login form tutorial (CSS3 + jQuery) [Tutorial]
"Login form tutorial (CSS3 + jQuery)" that was specially made for DesignModo by our friend Valeriu Timbuc.

Links:
http://vtimbuc.net/
https://twitter.com/vtimbuc
http://designmodo.com/futurico
http://vladimirkudinov.com
http://rockablethemes.com

********************************************************************************************* -->

<html lang="en">

<head>

    <meta charset="utf-8">

    <title>Acceso</title>

    <link rel="stylesheet" href="css/login-form.css" media="screen">

    <style>
        .login-form {
            margin: 200px auto;
        }</style>
    <meta name="robots" content="noindex,follow"/>
</head>

<body>

<div class="login-form">

    <h1>Acceso</h1>

    <form action="login.php" method="post" >

        <input type="text" name="username" placeholder="Nombre de usuario">

        <input type="password" name="password" placeholder="Contrase&ntilde;a">

		<span>
			<input type="checkbox" name="checkbox">
			<label for="checkbox">Recordar</label>
		</span>

        <input type="submit" value="Acceder">

    </form>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>

    $(document).ready(function () {

        // Check if JavaScript is enabled
        $('body').addClass('js');

        // Make the checkbox checked on load
        $('.login-form span').addClass('checked').children('input').attr('checked', true);

        // Click function
        $('.login-form span').on('click', function () {

            if ($(this).children('input').attr('checked')) {
                $(this).children('input').attr('checked', false);
                $(this).removeClass('checked');
            }

            else {
                $(this).children('input').attr('checked', true);
                $(this).addClass('checked');
            }

        });

    });
</script>

</body>

</html>