<?php
	//$ch = curl_init("http://www.4300k.cn/ServiceForm.aspx?webAction=personalLocation&userIdArray=197135");
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://www.4300k.cn/ServiceForm.aspx?webAction=personalLocation&userIdArray=197135");
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 0);

$a=curl_exec($ch);
curl_close($ch);
	
	list($ad,$at)=explode("usin_last_lat:",$a);
	list($add,$att)=explode("',usin_last_lng:",$at);
	list($addd,$attt)=explode("',usin_last_course:'",$att);
	
	$add=str_replace("'","",$add);
	$addd=str_replace("'","",$addd);
	
?><!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map_canvas { height: 100% }
    </style>
    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBQJTVjAm8X8geFbNBROXKghJYkrq3-TNw&sensor=false">
    </script>
    <script type="text/javascript">
      function initialize() {
        var mapOptions = {
          center: new google.maps.LatLng(<?php echo $add; ?>, <?php echo $addd; ?>),
          zoom: 18,
          mapTypeId: google.maps.MapTypeId.SATELLITE
        };
		
        var map = new google.maps.Map(document.getElementById("map_canvas"),
            mapOptions);
			
			setMarkers(map, beaches);

      }
		var beaches = [
  ['Hyundai', <?php echo $add; ?>, <?php echo $addd; ?>]
 ];
	
	function setMarkers(map, locations) {
		  
		  var image = new google.maps.MarkerImage('images/LogoMarker.png',
			  // This marker is 20 pixels wide by 32 pixels tall.
			  new google.maps.Size(20, 32),
			  // The origin for this image is 0,0.
			  new google.maps.Point(0,0),
			  // The anchor for this image is the base of the flagpole at 0,32.
			  new google.maps.Point(0, 32));
		  var shadow = new google.maps.MarkerImage('images/LogoMarker_s.png',
			  // The shadow image is larger in the horizontal dimension
			  // while the position and offset are the same as for the main image.
			  new google.maps.Size(37, 32),
			  new google.maps.Point(0,0),
			  new google.maps.Point(0, 32));
			  // Shapes define the clickable region of the icon.
			  // The type defines an HTML <area> element 'poly' which
			  // traces out a polygon as a series of X,Y points. The final
			  // coordinate closes the poly by connecting to the first
			  // coordinate.
		  var shape = {
			  coord: [1, 1, 1, 20, 18, 20, 18 , 1],
			  type: 'poly'
		  };
		  for (var i = 0; i < locations.length; i++) {
			var beach = locations[i];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
		  }

	
	}
	


	  </script>
  </head>
  <body onload="initialize()">
    <div id="map_canvas" style="width:100%; height:100%"></div>
  </body>
</html>