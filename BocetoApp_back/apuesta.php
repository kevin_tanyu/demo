<?php
session_start();

include("config.ini.php");

$usuarioId = $_SESSION['IDUsuario'];
$sorteoId = $_GET['SorteoID'];

$SQL="SELECT SP.NumeroDeSuerte, SP.FechayHora,
SD.NombreSorteo, SP.IDSorteoDefinicion
FROM SorteosProgramacion SP, SorteosDefinicion SD 
WHERE SP.ID=".$_GET['SorteoID']." AND SP.IDSorteoDefinicion=SD.ID";
$rs=mysql_query($SQL);
$InfoLoteria=mysql_fetch_row($rs);

$SQL="select SUM(Total) as totalDiario
from SorteoApuesta_TotalDiario
where IDUsuario = ". $usuarioId ." and IDSorteoProgramacion = ". $sorteoId . "
group by IDUsuario,IDSorteoProgramacion";

$rs=mysql_query($SQL);
$InfoRecaudado=mysql_fetch_assoc($rs);

if ($InfoRecaudado['totalDiario']==0) $Dividendo=1;
else $Dividendo=$InfoRecaudado['totalDiario'];

$SQL="SELECT srt.IDUsuario,srt.IDSorteoProgramacion
  ,srt.Numero
  ,(3/100)*std.Total AS CantidadARespetar
  ,((3/100)*std.Total) - srt.Total AS Balance
  FROM SorteoApuesta_RestringidoTotalCantidad srt
    JOIN SorteoApuesta_TotalDiario std
      ON std.IDSorteoProgramacion = srt.idsorteoprogramacion
      AND std.IDUsuario = srt.IDUsuario
  WHERE srt.IDUsuario = ". $usuarioId ." AND srt.IDSorteoProgramacion = ". $sorteoId ."
		AND ((3/100)*std.Total) - srt.Total < 0
LIMIT 0,2";
$rs=mysql_query($SQL);

$x=0;
while ($row=mysql_fetch_assoc($rs)) {
	$Rest[$x]['Numero']=$row['Numero'];
	$Rest[$x]['Monto']=$row['Balance']*-1;
	$x++;
}


$SQL="SELECT Numero, ReglaPorcentaje
FROM SorteosNumerosRestringidos
WHERE IDSorteoProgramacion=".$_GET['SorteoID'];
$rs=mysql_query($SQL);
$RESTRINGIDOS="";
if (mysql_affected_rows()>0) {
	while ($row=mysql_fetch_row($rs)) {
		if ($RESTRINGIDOS!="") $RESTRINGIDOS.=",";
		$RESTRINGIDOS.=$row[0];
		$REGLA_PORCENTAJE=$row[1];
	}
}

?>
<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=768, height=1024, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<title>Dashboard I Admin Panel</title>
	<link href='http://fonts.googleapis.com/css?family=Share+Tech+Mono' rel='stylesheet' type='text/css'>
	<link href='styles.css' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script type="text/javascript">
		
		var Papelito=0;
		var Foco="Dinero";
		var ApuestaResumenAp=[];
		var ApuestaResumenNu=[];
		var ApuestaRestringidos= [<?=$RESTRINGIDOS; ?>];
		var ApuestaRestringidosPorcentaje=<?=$REGLA_PORCENTAJE; ?>;
		var TotalRestringidos=0;
		var FlagValido=1;
		
		function FinalizaTicket() {
			if (FlagValido==1) {
				if (Papelito>0) {
					/* ############################### Finaliza ######*/
						$.ajax({
						type: "POST",
						url: "apuesta_guarda.php",
						data: { numeros: ApuestaResumenNu, apuestas: ApuestaResumenAp, Total: Papelito, Restringidos: ApuestaRestringidos, SorteoID: <?=$_GET['SorteoID']; ?> }
						}).done(function( msg ) {
							document.location.href="apuesta.php?SorteoID=<?=$_GET['SorteoID']; ?>";
						});
				}
				else {
					alert('No se pueden procesar apuestas en cero.');
				}
			}
			else {
				alert('No se puede procesar apuesta por limite en restringidos');
			}
		}
		
		function CambiaFoco (Op) {
			Foco=Op;
			if (Foco=="Dinero")	{ 
				$("#DisplayDineroInt").addClass("Iluminado");
				$("#DisplayInt").removeClass("Iluminado");
			} else {
				$("#DisplayDineroInt").removeClass("Iluminado");
				$("#DisplayInt").addClass("Iluminado");
			}
		}
		
		function AbreVentana(x) {
			 $("#dialog-confirm").css("visibility", "visible");
			 $(function() {
				$( "#dialog-confirm" ).dialog({
					resizable: false,
					height:190,
					modal: true,
					buttons: {
						"Borrar": function() {
							ApuestaResumenNu.splice(x-1,1);
							ApuestaResumenAp.splice(x-1,1);
							MuestraSumarizado();
							$( this ).dialog( "close" );
							$("#dialog-confirm").css("visibility", "hidden");
						},
						"Cancelar": function() {
							$( this ).dialog( "close" );
							$("#dialog-confirm").css("visibility", "hidden");
						}
					}
				});
				
			});		
		
		
		}
		
		function MuestraSumarizado() {
			var HTML="";
			var Total=0;
			var TotalRestringidos=0;
			
			for (x in ApuestaResumenAp){
				var Color="#000";
				if ($.inArray(parseInt(ApuestaResumenNu[x]), ApuestaRestringidos)>-1) { 
					Color="#ff0000"; 
					TotalRestringidos=TotalRestringidos+parseInt(ApuestaResumenAp[x]);
				}
				HTML	+='<div id="Apuesta" style="color:'+Color+'" onclick="AbreVentana(x);">';
				HTML	+='<div class="Numero" style="color:'+Color+'">'+ApuestaResumenNu[x]+'</div>';
				HTML	+='<div class="Monto" style="color:'+Color+'">&cent; '+ApuestaResumenAp[x]+'</div>';
				HTML	+='</div>';
				
				Total=Total+parseInt(ApuestaResumenAp[x]);
			};
			
			var Porcentaje=TotalRestringidos*100/Total;
			
			if (Porcentaje>ApuestaRestringidosPorcentaje) {
				HTML	+='<div class="ApuestaInvalida">Apuesta Invalida. Restringidos en '+parseInt(Porcentaje)+'%</div>';
				FlagValido=2;
			} else {
				HTML	+='<div class="ApuestaValida">Apuesta Valida</div>';
				FlagValido=1;
			}
			
			Papelito=Total;
			
			$("#Resumen").html(HTML);
			$("#PapelitoTotal").html('&cent; '+Total);
		}
		
		function Sumarizar (elem) {

			var num=$("#DisplayDineroInt").html();
			ApuestaResumenNu.push(elem);
			ApuestaResumenAp.push(num);
			
		}
	
		function AgregaCalc(N) { 
				
				var Cont;
				var Obj;
				
				if (Foco=="Dinero") Obj=$("#DisplayDineroInt");
				else Obj=$("#DisplayInt");
				
				Cont = Obj.html();
				
				if (N=="Ent") { // ################################	Enter
					if (Foco=="Dinero") {
						CambiaFoco("Numeros");
					} else {
						var arrayOfApuestas = Cont.split("+");
						arrayOfApuestas.forEach ( Sumarizar );
						MuestraSumarizado();
						$("#DisplayDineroInt").html("0");
						$("#DisplayInt").html("0");
						CambiaFoco ("Dinero");
					}
				}
				else {
					if (N=="<") {  	// ##############################	Backspace
						if (Cont!="0") {
							var Cantidad=Cont.length;
							Cont=Cont.substr(0,Cantidad-1) 
							if (Cont=="") Cont="0";
							Obj.html(Cont);
						}
					} else {	// ###################################	Numeros
						if (Foco=="Dinero" &&  N=="+") { 
							Obj.html(Cont);
						}
						else {
							if (Cont=="0") Obj.html(N);
							else {
								Obj.html(Cont+N);
								if (N!="+") {
									var arrayOfApuestas = Cont.split("+");
									var NumeroAEvaluar=arrayOfApuestas[arrayOfApuestas.length-1]
									if ( NumeroAEvaluar.length>1 && Foco=="Numeros")  Obj.html(Cont);
								} else {
									if (Cont.substr(Cont.length - 1)=="+") Obj.html(Cont);
								}
							}
						}
					}
				}
				//alert (Cont+N);
		}
		
		function Volver() {
			document.location.href="sorteos_loterias.php";
		}
		
		jQuery(document).ready(function(){
			$(".CuadradoCalculadora").filter(function() { return $.trim($(this).text()); }).each(function() {
					$(this).wrap("<a href=\"#\" onclick=\"AgregaCalc('"  + this.innerHTML + "');\" />");
			});
			$(".CuadradoCalculadoraDoble").filter(function() { return $.trim($(this).text()); }).each(function() {
					$(this).wrap("<a href=\"#\" onclick=\"AgregaCalc('"  + this.innerHTML + "');\" />");
			});
			$("#DisplayDineroInt").filter(function() { return $.trim($(this).text()); }).each(function() {
					$(this).wrap("<a href=\"#\" onclick=\"CambiaFoco('Dinero');\" />");
			});
			$("#DisplayInt").filter(function() { return $.trim($(this).text()); }).each(function() {
					$(this).wrap("<a href=\"#\" onclick=\"CambiaFoco('Numeros');\" />");
			});
			$("#Finalizar").filter(function() { return $.trim($(this).text()); }).each(function() {
					$(this).wrap("<a href=\"#\" onclick=\"FinalizaTicket();\" />");
			});
			$("#Volver").filter(function() { return $.trim($(this).text()); }).each(function() {
					$(this).wrap("<a href=\"#\" onclick=\"Volver();\" />");
			});
			$("#aaaaaa").filter(function() { return $.trim($(this).text()); }).each(function() {
					$(this).wrap("<a href=\"#\" onclick=\"window.print();\" />");
			});
			CambiaFoco('Dinero');
		});
		
	</script>
</head>
<body style="background-color:#ccc; margin:auto; text-align:center;">
	<div style="width:100%; height:100%; border:1px solid #666; background-color:#FFF; margin:auto;"> 
	<?php
		include("header.php");
	?>
<div style="width:100%;margin-top:30px;border-bottom:3px solid #666666;">     </div>
		<!-- A partir de aqui -->
				<div id="BarraIzquierda">
					<div id="Titulo">RESUMEN DE APUESTA</div>
					<div id="Encabezado">
						<div id="EncabezadoRow">
							<div id="EncabezadoCellIzq">Fecha y Hora</div>
							<div id="EncabezadoCell"><?=date('d/m/Y H:i:s'); ?></div>
						</div>
						<div id="EncabezadoRow">
							<div id="EncabezadoCellIzq">Evento</div>
							<div id="EncabezadoCell"><?php echo $InfoLoteria[2]; ?></div>
						</div>
						<div id="EncabezadoRow">
							<div id="EncabezadoCellIzq">Numero Suerte</div>
							<div id="EncabezadoCell"><?php echo $InfoLoteria[0]; ?></div>
						</div>
						<div id="EncabezadoRow">
							<div id="EncabezadoCellIzq">Restringidos</div>
							<div id="EncabezadoCell"><?php echo $RESTRINGIDOS; ?> (<?php echo $REGLA_PORCENTAJE; ?>%)</div>
						</div>

					</div>
					<div id="Resumen"></div>
				</div>
				<div id="TotalDeApuesta"><p class="ComentarioVerde">Papelito</p><span id="PapelitoTotal">&cent; 0</span></div>
				<div id="TotalDelDia"><p class="ComentarioNaranja">Total del Dia</p>&cent; <?=number_format($InfoRecaudado['totalDiario'], 0, '.', ','); ?></div>
				<div id="TotalRestringidos"><p class="ComentarioGris">Restringidos del Dia</p>
				<?php
                    $tmpCount = count($Rest);
					for ($x=0; $x<$tmpCount; $x++) {
						echo "<span style=\"color:#333;margin-left:20px;\">".$Rest[$x]['Numero']."</span> - &cent ".number_format($Rest[$x]['Monto'], 0, '.', ',')."<br>";
					}
				?>
				</div>
				<div id="DisplayDinero"><div id="DisplayDineroInt">0</div></div>
				<div id="Display"><div id="DisplayInt">0</div></div>
				<div id="MasyEnter">
					<div class="CuadradoCalculadoraDoble">+</div>
					<div class="CuadradoCalculadoraDoble">Ent</div>
				</div>
				<div id="Teclado">
					<div class="CuadradoCalculadora">7</div>
					<div class="CuadradoCalculadora">8</div>
					<div class="CuadradoCalculadora">9</div>
					<div class="CuadradoCalculadora">4</div>
					<div class="CuadradoCalculadora">5</div>
					<div class="CuadradoCalculadora">6</div>
					<div class="CuadradoCalculadora">1</div>
					<div class="CuadradoCalculadora">2</div>
					<div class="CuadradoCalculadora">3</div>
					<div class="CuadradoCalculadora2"></div>
					<div class="CuadradoCalculadora">0</div>
					<div class="CuadradoCalculadora"><</div>
				</div>
				<div id="Finalizar">Finalizar</div>
				<div id="Volver" class="SorteoCuadroVolver" style="margin-left:300px;">Volver</div>
				
				<div id="aaaaaa" class="SorteoCuadroVolver" style="margin-left:300px;">IMPRIMIR</div>
		<!-- Hasta aqui -->
	<div id="dialog-confirm" title="Borrar Apuesta?" style="visibility:hidden">
			Esta seguro que desea borrar la apuesta seleccionada?
	</div>
		</div>
</body>
</html>
