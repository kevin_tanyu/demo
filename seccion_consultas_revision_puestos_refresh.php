<?php
require_once 'config.ini.php';
require_once 'conectadb.php';
require_once 'lib/seccion_datos_importacion_listas_lib.php';

session_start();


//Captura el array json que se envia desde el ajax
$arrayDataRevision = file_get_contents('php://input');
//Posicion 0 = id del sorteo
//Posicion 1 = nombre del vendedor o nombre del archivo sin la extension .xls
//Posicion 2 = id de vendedor
//Posicion 3 = Total del vendedor
//Posicion 4 = Total del sorteo
$arrayDataRevision = json_decode($arrayDataRevision);

//Inserta los numero y montos a una tabla temporal para ser traaspasados a sorteoApuestaPuesto luego
$existeArchivo = insertLinesToArchivoPuesto($pdoConn, $arrayDataRevision[1]);

if($existeArchivo == true){
    //Crear el ticket con el ID del vendedor y la fecha y hora
    $sqlTicketPuesto  = "INSERT INTO ticket_puestos (IDUsuario, FechayHora) VALUES (:id_usuario,:date)";
    $stmtTicketPuesto = $pdoConn->prepare($sqlTicketPuesto);

//Inserta los numeros y montos que se guardaron del excel a la tabla de sorteoapuestapuestos
    $sqlSorteoApuestaPuesto = "INSERT INTO sorteoapuesta_puestos (IDUsuario, IDSorteoProgramacion
                           , FechayHora, IDTicket, Cantidad, Numero) VALUES
                           (:id_usuario, :id_sorteo, :dateT, :ticket_id, :amount, :numbers)";
    $stmtSorteoApuestaPuesto = $pdoConn->prepare($sqlSorteoApuestaPuesto);

//Select de los numero y montos temporales
    $sqlArchivoPuesto = "SELECT amount as monto, number as numero FROM il_archivo_puestos";
    $stmtArchivoPuesto = $pdoConn->prepare($sqlArchivoPuesto);

//Borra los montos y numeros temporales
    $sqlDeleteTemporal = "Delete from il_archivo_puestos";
    $stmtDeleteTemporal = $pdoConn->prepare($sqlDeleteTemporal);

//Select para comprobar si el ticket y los numeros se ha ingresado en la tabla sorteoapuesta_puestos
    $sqlComprobarTicket = "SELECT * FROM sorteoapuesta_puestos
                           WHERE IDUsuario = :id_user and IDSorteoProgramacion = :id_sorteo";
    $stmtComprobarTicket = $pdoConn->prepare($sqlComprobarTicket);
    $stmtComprobarTicket->execute(array(':id_user' => $arrayDataRevision[2], ':id_sorteo' => $arrayDataRevision[0]));


//Update de los numeros y montos del ticket correspondiente
    $sqlUpdateMontos = "UPDATE sorteoapuesta_puestos
                    SET Cantidad = :amount
                    WHERE IDUsuario = :id_user and IDSorteoProgramacion = :id_sorteo and Numero = :numbers";
    $stmtUpdateMontos = $pdoConn->prepare($sqlUpdateMontos);



//Si no se ha ingresado el ticket realiza inserccion de lo contrario hace un update
    if($stmtComprobarTicket ->rowCount() == 0){

        try{
            $stmtArchivoPuesto->execute();
            $numeros = $stmtArchivoPuesto->fetchAll(PDO::FETCH_ASSOC);//Captura todos los numeros y montos en un array
            $stmtTicketPuesto->execute(array(':id_usuario' => $arrayDataRevision[2], ':date' => date("Y-m-d H:i:s")));
            $ticketId = $pdoConn->lastInsertId();

            $tempo = 0;

            foreach($numeros as $numero){
                $stmtSorteoApuestaPuesto->execute(array(':id_usuario' => $arrayDataRevision[2], ':id_sorteo' => $arrayDataRevision[0],
                    ':dateT' => date("Y-m-d H:i:s"), ':ticket_id' => $ticketId,
                    ':amount' => $numero['monto'], ':numbers' => $numero['numero']));
                // $tempo = $tempo + $numero['amount'];
            }






        }catch (Exception $e){

            echo $e->getMessage();
            die;
        }

    }else{
        try{
            $stmtArchivoPuesto->execute();
            $numeros = $stmtArchivoPuesto->fetchAll(PDO::FETCH_ASSOC);//Captura todos los numeros y montos en un array

            foreach($numeros as $numero){
                $stmtUpdateMontos-> execute(array(':amount' => $numero['monto'],
                    ':id_user' => $arrayDataRevision[2],
                    ':id_sorteo' => $arrayDataRevision[0],
                    ':numbers' => $numero['numero']));

            }


        }catch (Exception $e){
            echo $e->getMessage();
            die;
        }
    }




    $stmtDeleteTemporal->execute();

    $monto = readTotal($arrayDataRevision[1]);
    $total = $arrayDataRevision[4] + $monto - $arrayDataRevision[3];


    echo(system_number_money_format($monto) . "/" .system_number_money_format($total));
}else{

    $sqlDeleteSorteoApuesta = "DELETE FROM sorteoapuesta_puestos
                               WHERE IDUsuario = :id_usuario AND IDSorteoProgramacion = :id_sorteo";
    $stmtDeleteSorteoApuesta = $pdoConn->prepare($sqlDeleteSorteoApuesta);
    $stmtDeleteSorteoApuesta->execute(array(':id_usuario' => $arrayDataRevision[2],
                                            ':id_sorteo' => $arrayDataRevision[0]));

    $total = $arrayDataRevision[4] - $arrayDataRevision[3];
    echo("Error" . "/" . system_number_money_format($total) . "/" . system_number_money_format(0));
}



?>