<?php
session_start();

if (!isset($_SESSION['ActivoFlag'])) {
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

// Incluye Header
include("header.php");

//QUERY PARA RECUPERAR EL MENSAJE
$sqlMensaje = "SELECT * FROM Mensaje";
$stmtMensaje = $pdoConn->prepare($sqlMensaje);
$stmtMensaje->execute();
$mensaje = $stmtMensaje->fetch();
?>
<!--
      <article>
           <header>
               <h1>Sorteo CR</h1>
               <h3>Miercoles 16 de Abril de 2013</h3>
               <p>Sin Favorecidos para premio o Jugada Siguiente.</p>
               <div class="BolaRoja">31</div>
           </header>
           <section>
               <h1>Sorteo CR</h1>
               <h3>Miercoles 16 de Abril de 2013</h3>
               <p>Sin Favorecidos para premio o Jugada Siguiente.</p>
               <div class="BolaRoja">31</div>
           </section>
       </article>
--><?php if($_SESSION['TipoUsuarioInt'] == 5):?>
       <aside>
           <h3>¡IMPORTANTE!</h3>
           <p><?php echo $mensaje['mensaje']?></p>
       </aside>
    <?php endif ?>


    <div
        style="width:100%; height: 90%; vertical-align:middle; text-align:center;padding-top:200px;padding-bottom:200px; font-family:Arial, Helvetica, Sans-serif; font-size:18px;">
        Sistema de Administracion de Sorteos V1.0
    </div>

<?php
// Incluye Footer
include("footer.php");
?>