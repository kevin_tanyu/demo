<?php
session_start();

if (!isset($_SESSION['ActivoFlag'])) {
	// Verifica si hay session creada, de lo contrario redirige al index
	header("Location: index.php?IDM=TO");
	exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

	$QUERY="SELECT 	sp.FechayHora, 
									LPAD(sp.NumeroDeSuerte,2,0), 
									sd.NombreSorteo, 
									sd.PagaPorcentaje,
									sp.id,
									sd.NumerosGanadores,
									sd.id,
									sd.FlagMultipleMinimoGana
					FROM 		SorteosProgramacion sp, 
									SorteosDefinicion sd
					WHERE 	sp.IDSorteoDefinicion=sd.ID 
						  AND 	sp.id=".$_GET['id']."
					ORDER BY sp.FechayHora DESC
						  ";
	$rs=mysql_query($QUERY);
	if (mysql_affected_rows()>0) {
	$row=mysql_fetch_row($rs);
?>
<h3 style="line-height:1px;">Agregar Numeros Ganadores</h3>
<h6 style="line-height:1px;">Para Sorteo de loteria <span style="color:red"><?=$row[2]; ?></span> efectuado fecha y hora <span style="color:red"><?=$row[0]; ?></span></h6>
					<div style="text-align:center;" id="form_add_container" >
						<form name="formulario1" id="formulario1">
						<input type="hidden" name="IDSorteoProgramacion" value="<?=$_GET['id']; ?>">
						<input type="hidden" name="IDSorteoDefinicion" value="<?=$row[6]; ?>">
						<input type="hidden" name="CantidadNumeros" value="<?=$row[5]; ?>">
						<input type="hidden" name="MultipleMinimoGana" value="<?=$row[7]; ?>">
						<div class="divTable" style="width:100%">
							<div class="divRow">
								<div class="divCellIzq letra">Numero de la suerte en Juego</div>
								<div class="divCellCen letra"><?=$row[1]; ?></div>
							</div>
							<div class="divRow">
								<div class="divCellIzq letra">Cantidad numeros Ganadores a Ingresar</div>
                                <?php if($row[6] == 28){?>
                                    <div class="divCellCen letra"><?=$row[5] + 2; ?></div>
                                <?php }else{?>
                                    <div class="divCellCen letra"><?=$row[5]; ?></div>
                                <?php }?>

							</div>
							<?php
							for ($x=1;$x<=$row[5];$x++) {
							?>
							<div class="divRow">
								<div class="divCellIzq letra">Numero Ganador #<?=$x; ?></div>
								<div class="divCellcen"><input type="number" name="NumeroGanador<?=$x;?>" class="campotexto letra" style="width:40px;" required></div>
							</div>
                                <?php if($row[6] == 28):?>
                                    <div class="divRow">
                                        <div class="divCellIzq letra">Numero Ganador #<?=$x+1; ?></div>
                                        <div class="divCellcen"><input type="number" name="NumeroGanador<?=$x+1;?>" class="campotexto letra" style="width:40px;" required></div>
                                    </div>
                                    <div class="divRow">
                                        <div class="divCellIzq letra">Numero Ganador #<?=$x+2; ?></div>
                                        <div class="divCellcen"><input type="number" name="NumeroGanador<?=$x+2;?>" class="campotexto letra" style="width:40px;" required></div>
                                    </div>
                                <?php endif?>
                            <?php
							}
							?>
						</div>
						<div class="divTable" style="width:400px;">
							<div class="divRow">
								<div class="divCellCen letra" style="width:400px; color:#ff0000;">ATENCION! - Al presionar el bot&oacute;n "Agregar" usted estara convalidando el premio y enviando las notificaciones por correo electr&oacute;nico a los favorecidos, adem&aacute;as de acreditar dinero de premios a los jugadores. Este paso no se puede modificar.</div>
							</div>
						</div>

						<input type="button" value="Agregar" class="button" id="BotonFormulario" onclick="javascript:FormularioAddNumerosGanadores();">
						</form>
					</div>
					<div id="Resultado"></div>
					<br><br>
<?php
} else {
?>
<h3>Atencion! : Sorteo indicado no existe en base de datos/</h3>
<?php } ?>