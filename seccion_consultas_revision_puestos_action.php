<?php
session_start();

require_once 'config.ini.php';
require_once 'conectadb.php';

$action       = $_GET['action'];
$vendedorId   = $_GET['vendedor_id'];
$sorteoProgId = $_GET['sorteo_prog_id'];
$montoRevision = $_GET['montoEnviado'];
$montoRecarga = $_GET['montoRecarga'];
$terminal = $_GET['terminal'];
$jugada1 = $_GET['jugada1'];
$jugada2 = $_GET['jugada2'];

if($jugada1 === ''){
    $jugada1 = -1;
}

if($jugada2 === ''){
    $jugada2 = -1;
}


$result = array('message' => '', 'error' => true, 'error_message' => '');

//check content of new action
$sqlCheckAction  = "SELECT id FROM COF_Status WHERE name = ?";
$stmtCheckAction = $pdoConn->prepare($sqlCheckAction);
$stmtCheckAction->execute(array($action));
if ($stmtCheckAction->rowCount() > 0)
{
    $statusRow       = $stmtCheckAction->fetch(PDO::FETCH_ASSOC);
    $statusId        = $statusRow['id'];
    if($action === "sent"){
        $sqlAction = "UPDATE COF_Revision_Log
                      SET cof_new_status_id = 5
                      WHERE cof_revision_id =(SELECT C.id FROM COF_Revision C
                                              WHERE C.sorteo_programacion_id = :sorteo_prog_id
                                              AND C.usuario_vendedor_calle_id = :user_id)";
       /* $sqlAction = "DELETE FROM COF_Revision_Log WHERE
        cof_revision_id =(SELECT C.id FROM COF_Revision C
        WHERE C.sorteo_programacion_id = :sorteo_prog_id AND C.usuario_vendedor_calle_id = :user_id)";*/

    }else{
        $sqlAction = "INSERT INTO COF_Revision_Log (cof_revision_id, cof_new_status_id, created_at, user_id, jugada1,jugada2, revision_monto, monto_terminal, monto_recarga)
     SELECT C.id, :status_id, NOW(), :user_id, :jugada_1, :jugada_2, :monto, :terminal, :recarga
     FROM COF_Revision C
     WHERE C.sorteo_programacion_id = :sorteo_prog_id AND C.usuario_vendedor_calle_id = :user_id
    ";
    }

    try
    {
        $stmtAction = $pdoConn->prepare($sqlAction);
        if($action === "sent"){
            $stmtAction->execute(array(':user_id' => $vendedorId, ':sorteo_prog_id' => $sorteoProgId));
        }else{
            $stmtAction->execute(array(':status_id' => $statusId, ':user_id' => $vendedorId, ':sorteo_prog_id' => $sorteoProgId,
                ':jugada_1' => $jugada1, ':jugada_2' => $jugada2, ':monto' => $montoRevision, ':terminal' => $terminal, ':recarga' => $montoRecarga ));
        }

        $result['error'] = false;
    } catch (PDOException $e)
    {
        $result['error']         = true;
        $result['error_message'] = 'Problemas con la base de datos, intente de nuevo';
    }


} else
{
    $result['error']         = true;
    $result['error_message'] = 'Accion incorrecta';
}

echo json_encode($result);