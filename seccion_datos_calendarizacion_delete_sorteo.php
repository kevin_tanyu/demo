<?php

require_once 'config.ini.php';
require_once 'conectadb.php';

$spid = $_POST['spid'];

$response = array('success' => true, 'msg' => 'Se eliminó el sorteo');

//revisar si el sorteo tiene tickets
$sqlRevisaTickets = "SELECT COUNT(ID) as qty FROM SorteoApuesta_Validas SA WHERE SA.IDSorteoProgramacion = ? ";
$stmtRevisa       = $pdoConn->prepare($sqlRevisaTickets);
$stmtRevisa->execute(array($spid));
$revisaRes = $stmtRevisa->fetch(PDO::FETCH_ASSOC);

$sqlRevisaTicketsCliente = "SELECT COUNT(ID) AS qty FROM CLIENTE_SorteoApuesta_Validas SA WHERE SA.IDSorteoProgramacion = ?";
$stmtRevisaCli           = $pdoConn->prepare($sqlRevisaTicketsCliente);
$stmtRevisaCli->execute(array($spid));
$revisaResCli = $stmtRevisaCli->fetch(PDO::FETCH_ASSOC);

$tieneTickets = $revisaRes['qty'] > 0 || $revisaResCli['qty'] > 0;

if (!$tieneTickets)
{
    $sqlDeleteClientesSorteoMontosTope   = "DELETE FROM CLIENTE_SorteosMontosTope WHERE IDSorteoProgramacion = ?";
    $stmtDeleteClientesSorteosMontosTope = $pdoConn->prepare($sqlDeleteClientesSorteoMontosTope);

    $sqlDeleteCliNumAbiertos  = "DELETE FROM CLIENTE_SorteosNumerosAbiertos WHERE IDSorteoProgramacion = ?";
    $stmtDeleteCliNumAbiertos = $pdoConn->prepare($sqlDeleteCliNumAbiertos);

    $sqlEliminaSorteo = "DELETE FROM SorteosProgramacion WHERE ID = ?";
    $stmtElimina      = $pdoConn->prepare($sqlEliminaSorteo);

    try
    {
        $stmtDeleteClientesSorteosMontosTope->execute(array($spid));
        $stmtDeleteCliNumAbiertos->execute(array($spid));
        $stmtElimina->execute(array($spid));

    } catch (PDOException $e)
    {
        $response['success'] = false;
        $response['success'] = 'Error, codigo: ' . $e->getCode();

    }

} else
{
    $response['success'] = false;
    $response['msg']     = 'El sorteo tiene tickets, no se puede eliminar';
}

echo json_encode($response);