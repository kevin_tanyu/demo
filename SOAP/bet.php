<?php


require_once "./lib/nusoap.php";
require_once "conectadb.php";

$db = $pdoConn;

//FUNCON PARA INSERTAR UNA APUESTA
//RECIBE EL ID DE USUARIO, ID DEL SORTEO, FECHA, ARREGLO CON NUMEROS, ARREGLO CON LOS MONTOS
function insertarApuesta($usuarioID, $sorteoID, $date, $arrayNumeros, $arrayMontos){

    try{

      //  $date = date('Y-m-d H:i:s');
        //SQL INSERCCION DEL TICKET
        $sqlInsertTicketPuesto = "INSERT INTO ticket_puestos(IDUsuario, FechayHora) VALUES(?, ?)";
        $stmtInsertTicketPuesto = $GLOBALS['db']->prepare($sqlInsertTicketPuesto);
        $stmtInsertTicketPuesto->execute(array($usuarioID, $date));

        $ticketID = $GLOBALS['db']->lastInsertId();

        //Inserta los numeros y montos que se guardaron del excel a la tabla de sorteoapuestapuestos
        $sqlSorteoApuestaPuesto = "INSERT INTO sorteoapuesta_puestos (IDUsuario, IDSorteoProgramacion
                              , FechayHora, IDTicket, Cantidad, Numero) VALUES
                              (:id_usuario, :id_sorteo, :dateT, :ticket_id, :amount, :numbers)";
        $stmtSorteoApuestaPuesto = $GLOBALS['db']->prepare($sqlSorteoApuestaPuesto);

        for($i = 0; $i < sizeof($arrayNumeros); $i++){
            $stmtSorteoApuestaPuesto->execute(array(':id_usuario' => $usuarioID, ':id_sorteo' => $sorteoID,
                                                    ':dateT' => $date, ':ticket_id' => $ticketID,
                                                    ':amount' => $arrayMontos[$i], ':numbers' => $arrayNumeros[$i]));
        }

        $result = array('isValid' => true, 'message' => 'Tiquete ingresado con exito', 'ticketID' => $ticketID);
        return $result;

    }catch (Exception $e){
        $result = array('isValid' => false, 'message' => 'Ha ocurrido un error, intente de nuevo');
        return $result;
    }


}//Fin insertarApuesta

function revertirTiquete($idTiquete, $date){

    try{

        //  $date = date('Y-m-d H:i:s');
        //SQL INSERCCION DEL TICKET
        $sqlRevertido = "UPDATE ticket_puestos
                         SET fechaRevertido = ?
                         WHERE id = ?";
        $stmtRevertido = $GLOBALS['db']->prepare($sqlRevertido);
        $stmtRevertido->execute(array($date, $idTiquete));

        $result = array('isValid' => true, 'message' => 'Tiquete revertido con exito');
        return $result;

    }catch (Exception $e){
        $result = array('isValid' => false, 'message' => 'Ha ocurrido un error, intente de nuevo');
        return $result;
    }

}

$server = new soap_server();
$server->register("insertarApuesta");
$server->register("revertirTiquete");
$server->service($HTTP_RAW_POST_DATA);