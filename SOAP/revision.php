<?php

require_once "./lib/nusoap.php";
require_once "conectadb.php";
include("../config.ini.php");

$db = $pdoConn;

/*FUNCION QUE REALIZA EL PROCESO DE REVISION*/
function enviaRevision($usuarioID, $sorteoID, $montoEnviado, $jugada1, $jugada2 ){

    try{
        crearExcelPapelito($usuarioID, $sorteoID);
        $sqlgetCOFRevision = "SELECT * FROM COF_Revision
                              WHERE usuario_vendedor_calle_id = :user_id and sorteo_programacion_id = :sorteo_prog_id";
        $stmtgetCOFRevision = $GLOBALS['db']->prepare($sqlgetCOFRevision);
        $stmtgetCOFRevision->execute(array(':user_id' => $usuarioID, ':sorteo_prog_id' => $sorteoID));
        $idCOF = $stmtgetCOFRevision->fetch(PDO::FETCH_ASSOC);

        $COF = $idCOF['id'];

        $sqlInsertEstado = "INSERT INTO COF_Revision_Log(cof_revision_id, cof_new_status_id, created_at, user_id, jugada1, jugada2, revision_monto, devolucion)
                            VALUES(:Cof, :status, :hora, :user_id, :jud1, :jud2, :monto, :devolucion)";
        $stmtInsertEstado = $GLOBALS['db']->prepare($sqlInsertEstado);
        $stmtInsertEstado->execute(array(':Cof' => $COF, ':status' => 8 , ':hora' => date("Y-m-d H:i:s"), ':user_id' => $usuarioID,
                                         ':jud1' => $jugada1, ':jud2' => $jugada2, ':monto' => $montoEnviado, ':devolucion' => 0));//NO HUBO DEVO

        $result = array('isValid' => true, 'message' => 'Revisado');

        return $result;

    }catch (Exception $e){

        $result = array('isValid' => false, 'message' => $e->getMessage());
     //   $result = array('isValid' => false, 'message' => 'Error, envie de nuevo');
        return $result;

    }

}//Fin enviarRevision

/*FUNCION PARA CONSULTAR EL ESTADO DE LA REVISION*/
function consultaRevision($usuarioID, $sorteoID){
    try{
        $sqlStatusSorteo = "SELECT  S.name, S.display_name
                            FROM COF_RevisionStatus_View RS
                            JOIN COF_Status S ON RS.status_id = S.id
                            WHERE RS.sorteo_programacion_id = ? AND RS.usuario_vendedor_calle_id = ?";
        $stmtStatusSorteo = $GLOBALS['db']->prepare($sqlStatusSorteo);
        $stmtStatusSorteo->execute(array($sorteoID, $usuarioID));
        $status = $stmtStatusSorteo->fetch();

        if($stmtStatusSorteo->rowcount() > 0){
            $result = array('message' => $status['display_name']);
        }else{
            $result = array('message' => 'No hay solicitud');
        }


        return $result;

    }catch (Exception $e){
        $result = array('message' => 'Ha ocurrido un error, intente de nuevo');
        return $result;
    }

}//Fin consultaRevision


function crearExcelPapelito($IDUsuario, $sorteoID){

    //Prepara el excel para creado y escrito
    require_once '../lib/phpExcel/PHPExcel.php';
    require_once '../lib/phpExcel/PHPExcel/Writer/Excel5.php';
    //Centra el texto de los cuadros
    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );

    $spreadsheet = new PHPExcel();
    $spreadsheet->setActiveSheetIndex(0);

    $worksheet = $spreadsheet->getActiveSheet();
    $worksheet->getDefaultStyle()->applyFromArray($style);
    $worksheet->getColumnDimensionByColumn(0)->setAutoSize(true);

    //ESCRIBE LA FECHA DEL DIA
    //$worksheet->SetCellValueByColumnAndRow(0, 2, date("d/m/Y"));

    //ESCRIBE EL ENCABEZADO
    $worksheet->SetCellValueByColumnAndRow(0, 1, "Numeros");
    $worksheet->SetCellValueByColumnAndRow(1, 1, "Total");

    //ESCRIBE LOS NUMEROS DE 0-99
    for($i = 1; $i < 101; $i++){
        $worksheet->SetCellValueByColumnAndRow(0, $i+1, $i);
    }


    //Select para comprobar si el ticket y los numeros se ha ingresado en la tabla sorteoapuesta_puestos
    $sqlNumeroTicket = "SELECT SUM(Cantidad) as 'total' FROM sorteoapuesta_validas_puestos
                        WHERE IDUsuario = :id_user and IDSorteoProgramacion = :id_sorteo and Numero = :numero";
    $stmtNumeroTicket = $GLOBALS['db']->prepare($sqlNumeroTicket);
    /*
    $stmtComprobarTicket->execute(array(':id_user' => $IDUsuario, ':id_sorteo' => $sorteoID));
    $Tiquetes = $stmtComprobarTicket->fetchAll(PDO::FETCH_ASSOC);*/

    $total = 0;
    for($i = 0; $i < 100; $i++){
        $stmtNumeroTicket->execute(array(':id_user' => $IDUsuario, ':id_sorteo' => $sorteoID, 'numero' => $i));
        $numero = $stmtNumeroTicket->fetch();
        if($i == 0){
            if($numero['total'] == ''){
                $worksheet->SetCellValueByColumnAndRow(1, 101, 0);
            }else{
                $worksheet->SetCellValueByColumnAndRow(1, 101, $numero['total']);
            }
        }else{
            if($numero['total'] == ''){
                $worksheet->SetCellValueByColumnAndRow(1, $i+1, 0);
            }else{
                $worksheet->SetCellValueByColumnAndRow(1, $i+1, $numero['total']);
            }

        }

        $total = $total +  $numero['total'];

    }//Fin for

    $sqlGetFilename = "SELECT NombreUsuario FROM Usuarios WHERE ID = ?";
    $stmtGetFilename = $GLOBALS['db']->prepare($sqlGetFilename);
    $stmtGetFilename->execute(array($IDUsuario));
    $Nombre = $stmtGetFilename->fetch();


    $worksheet->SetCellValueByColumnAndRow(2, 1,  $total);

    if (strpos($Nombre['NombreUsuario'],'IMPORT') === false){
        $filename = $Nombre['NombreUsuario'];
    }else{
        $filename = substr($Nombre['NombreUsuario'], 7);
    }

    $nombreArchivo = trim($filename, " ").'.xls';//Nombre del excel
    $writer = new PHPExcel_Writer_Excel5($spreadsheet);
    $writer->save(IMPORTACION_DIR . '/' . $nombreArchivo);
    // $writer->save('./Descargas/'. $nombreArchivo);

    // return $total;



}// FIN crearExcelSpectra

$server = new soap_server();
$server->register("crearExcelPapelito");
$server->register("enviaRevision");
$server->register("consultaRevision");
$server->service($HTTP_RAW_POST_DATA);