<?php


require_once "./lib/nusoap.php";
require_once "conectadb.php";

$db = $pdoConn;

//DEVUELTE LOS SORTEOS DE DIA SEPARADOS EN LA LINEA POR UNA COMA Y ENTRE SORTEOS CON UN CAMBIO DE LINEA
//EJEMPLO
//102,18:30:00, COSTA RICA, 34
//103,12:00:00, DIGITALES, 99
function sorteoDelDia(){
    $sqlSorteosDelDia = "SELECT SP.ID, SP.FechayHora, SD.NombreSorteo
                         FROM SorteosProgramacion SP
                         JOIN SorteosDefinicion SD
                         ON SP.IDSorteoDefinicion = SD.ID
                         WHERE DATEDIFF(NOW(), SP.FechayHora) = 0
                         ORDER BY SD.NombreSorteo ASC";
    $stmtSorteosDelDia = $GLOBALS['db']->prepare($sqlSorteosDelDia);
    $stmtSorteosDelDia->execute();
    $sorteosDelDia = $stmtSorteosDelDia->fetchAll(PDO::FETCH_ASSOC);

    $Sorteos = "";
    //CONCATENA LOS SORTEOS DEL DIA
    foreach($sorteosDelDia as $sorteo){
        $Sorteos = $Sorteos . $sorteo['ID'] . "," . $sorteo['FechayHora'] . "," . $sorteo['NombreSorteo'] .  "</br>";
    }//Fin foreach

    $result = array('Sorteos'=> $Sorteos );

    return $result;

}//Fin sorteoDelDia

//DEVUELTE LOS NUMEROS RESTRINGIDOS EN UNA CADENA SEPARADOS POR UNA COMA
//EJ: 1,2,5,7,8,15,14
function restrigidosPorSorteo($sorteoID){
    $sqlRestringidosSorteo = "SELECT Numero
                              FROM SorteosNumerosRestringidos
                              WHERE IDSorteoProgramacion = ?";
    $stmtRestringidosSorteo = $GLOBALS['db']->prepare($sqlRestringidosSorteo);
    $stmtRestringidosSorteo->execute(array($sorteoID));
    $Restringidos = $stmtRestringidosSorteo->fetchAll(PDO::FETCH_ASSOC);

    $Numeros = "";

    foreach($Restringidos as $restringido){
        $Numeros = $Numeros . $restringido['Numero'] . ",";
    }//Fin foreach

    $result = array('Restringidos' => substr($Numeros, 0, -1));

    return $result;

}//Fin restrigidosPorSorteo

//FUNCION QUE DEVULEVE EL NUMERO GANADOR DE ACUERDO AL SORTEO
function numeroGanador($sorteoID){

    $sqlNumeroGandador = "SELECT Numero
                          FROM  SorteosNumerosGanadores
                          WHERE IDSorteoProgramacion = ?";
    $stmtNumeroGandador = $GLOBALS['db']->prepare($sqlNumeroGandador);
    $stmtNumeroGandador->execute(array($sorteoID));
    $numeroGanador = $stmtNumeroGandador->fetchAll(PDO::FETCH_ASSOC);

    $Numeros = "";

    foreach($numeroGanador as $ganador){
        $Numeros = $Numeros . $ganador['Numero'] . ",";
    }

    $result = array('Ganador' => substr($Numeros, 0, -1));
    return $result;

}//Fin numeroGandador

$server = new soap_server();
$server->register("sorteoDelDia");
$server->register("restrigidosPorSorteo");
$server->register("numeroGanador");
$server->service($HTTP_RAW_POST_DATA);