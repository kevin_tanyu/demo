<?php

require_once "./lib/nusoap.php";
require_once "conectadb.php";

$db = $pdoConn;

function loginWEB($username, $password){

    $result = array('isValid'=> false);
    $sqlLogin = "SELECT * FROM Usuarios WHERE NombreUsuario = ? AND Contrasena = ? AND TipoUsuario = 5";
    $stmtLogin = $GLOBALS['db']->prepare($sqlLogin);
    $stmtLogin->execute(array($username, $password));

    if($stmtLogin->rowcount() == 0){
        $username = 'IMPORT_' . $username;
        $stmtLogin->execute(array($username, $password));

        if($stmtLogin->rowcount() == 0){
            $result['message'] = 'Usuario No Existe';
            return $result;
        }else{
            $usuario = $stmtLogin->fetch();
            $result['isValid'] = true;
            $result['message'] = '';
            $result['ID'] = $usuario['ID'];
            $result['PagaPorcentaje'] = $usuario['PagaPorcentaje'];
            $result['devolucion'] = $usuario['devolucion'];
            $result['jugada'] = $usuario['f_jugada'];
            $result['MontoArranque'] = $usuario['montoArranque'];
            return $result;
        }//Fin if/else
    }else{
        $usuario = $stmtLogin->fetch();
        $result['isValid'] = true;
        $result['message'] = '';
        $result['ID'] = $usuario['ID'];
        $result['PagaPorcentaje'] = $usuario['PagaPorcentaje'];
        $result['devolucion'] = $usuario['devolucion'];
        $result['jugada'] = $usuario['f_jugada'];
        $result['MontoArranque'] = $usuario['montoArranque'];
        return $result;
    }//Fin if/else

    $result['message'] = 'Usuario No Existe';
    return $result;

}


$server = new soap_server();
$server->register("loginWEB");
$server->service($HTTP_RAW_POST_DATA);
