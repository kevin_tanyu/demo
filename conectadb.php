<?php
$db = mysql_connect($DB_Servidor, $DB_Username, $DB_Password);

if (!$db) {
    die('Could not connect: ' . mysql_error());
}

mysql_select_db($DB_Database, $db);

try {
    //PDO connection, for progressive replacement of mysql_* functions
    $pdoConn = new PDO("mysql:host=$DB_Servidor;dbname=$DB_Database", $DB_Username, $DB_Password);
} catch (PDOException $e) {
    echo $e->getMessage();
}

