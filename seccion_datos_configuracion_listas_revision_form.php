<?php
/**
 * Created by PhpStorm.
 * User: KTY
 * Date: 20/03/15
 * Time: 01:57 PM
 */

session_start();
require_once 'config.ini.php';
require_once 'conectadb.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    //Update para los cambioss en los radiobutton de las listas de revision
     $updateSql = "UPDATE Usuarios SET en_lista_revision = :en_lista_revision WHERE NombreUsuario = :nombre_usuario";
     $info      = isset($_POST['setting_lista_revision']) && is_array($_POST['setting_lista_revision']) ? $_POST['setting_lista_revision'] : array();

    //Consulta para verificar si el usuario de excel existe o no
    $selectUsuario = "SELECT NombreUsuario FROM Usuarios WHERE NombreUsuario = :nombre_usuario";



     $updateStatement = $pdoConn->prepare($updateSql);
     $stmtUsuario = $pdoConn->prepare($selectUsuario);
    //ExcelList = nombre del excel y value= 1 si esta activo y 0 desactivado
     foreach ($info as $ExcelList => $value)
     {

         $stmtUsuario->execute(array(':nombre_usuario' => "IMPORT_" . substr($ExcelList,0 , strlen($ExcelList)-4)));

         if($stmtUsuario->rowCount() === 0 and $value == 1){

             //Insert SQL para crear un usuario al excel que esta en el FTP
             $insertUsuario = "INSERT INTO Usuarios(NombreUsuario, Contrasena, TipoUsuario, ActivoFlag, IDPadre, Email, FlagActivo)
              VAlUES(:nombre_usuario,'lista' , :tipo_usuario, 1, :id_padre, :email, 1)";

             $insertStatement = $pdoConn->prepare($insertUsuario);

             $insertStatement->execute(array(':nombre_usuario' => "IMPORT_" . substr($ExcelList,0 , strlen($ExcelList)-4),
                                             ':tipo_usuario' => NIVEL_PERMISO_USUARIO_CALLE,
                                             ':id_padre' => $_SESSION['IDUsuario'],
                                             ':email' =>  "IMPORT_" . substr($ExcelList,0 , strlen($ExcelList)-4) . "@gmail.com"));





         }else{
             $updateStatement->execute(array(':en_lista_revision' => $value, ':nombre_usuario' => "IMPORT_" . substr($ExcelList,0 , strlen($ExcelList)-4)));

         }
     }

}//Fin if


$revisionSql = "SELECT U.en_lista_revision FROM Usuarios_View U WHERE U.NombreUsuario = :nombre_usuario";
$revisionStatement = $pdoConn->prepare($revisionSql);

$ExcelList = array();
$directorio = opendir(IMPORTACION_DIR );
while ($archivo = readdir($directorio)){

    if (!is_dir($archivo)){
        array_push($ExcelList, $archivo);

    }

}



?>

<h3 style="line-height:1px;">Configuración de listas de puestos</h3>
<h6 style="line-height:1px;">Administrar las listas de puestos a ser mostradas</h6>
<form id="configuracion_listas_revision_form" method="post" action="seccion_datos_configuracion_listas_revision_form.php">

    <div class="divTable">
        <div class="divRow">
            <div class="divCellHeader2" style="width:160px;">Lista</div>
            <div class="divCellHeader2" style="width:120px;">Mostrar</div>
        </div>


            <?php foreach ($ExcelList as $archivo): ?>
              <?php $revisionStatement->execute(array(':nombre_usuario' => "IMPORT_" . substr($archivo,0 , strlen($archivo)-4)));
                    $revisionTemporal = $revisionStatement->fetch(PDO::FETCH_ASSOC);

                ?>
                <div class="divRow2 various2 fancybox.ajax">
                    <div class="divCellCen2"
                         style="width:160px; text-align:left;"><?= $archivo; ?></div>
                    <div class="divCellCen2" style="width:120px; text-align:center;">
                        <label for="usr_lst_revision_si_<?php echo $archivo ?>">Si</label>
                        <input type="radio" value="1" name="setting_lista_revision[<?php echo $archivo ?>]"
                               id="usr_lst_revision_si_<?php echo $archivo ?>"
                               style="visibility: visible; height: inherit"
                            <?php echo ($revisionTemporal['en_lista_revision'] == 1) ? "checked" : ""; ?>
                            />

                        <label for="usr_lst_revision_no_<?php echo $archivo ?>">No</label>
                        <input type="radio" value="0" name="setting_lista_revision[<?php echo $archivo ?>]"
                               id="usr_lst_revision_no_<?php echo $archivo ?>"
                               style="visibility: visible; height: inherit"
                            <?php echo ($revisionTemporal['en_lista_revision'] == 0) ? "checked" : ""; ?>
                            />
                    </div>
                </div>
            <?php endforeach; ?>


    </div>
    <input type="submit" value="Guardar Cambios" class="button">



</form>
<script>
    $('#configuracion_listas_revision_form').ajaxForm({
        target: '#TablaListas',
        success: function () {
            $('#TablaListas').unblock();
        },
        beforeSubmit: function (formData, jqForm, options) {
            $('#TablaListas').block();
            return true;
        }
    });
</script>

