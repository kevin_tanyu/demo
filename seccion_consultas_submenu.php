<?php

$_SESSION['enter'] = 2;

$canSeeListas = array(NIVEL_PERMISO_USUARIO_ADMIN);
$canSeeTickets = array(NIVEL_PERMISO_USUARIO_ADMIN);
$canSeeJugadores = array(NIVEL_PERMISO_USUARIO_WEB);
$canSeeApuestasDepositos = array(NIVEL_PERMISO_USUARIO_WEB);

$nivelPermisoHouse = array(1);

?>

<aside_submenu>
    <h3>Opciones</h3>

    <div id="list4">

        <?php if (in_array($_SESSION['NivelUsuario'], $nivelPermisoHouse)): ?>
            <ul>
                <!--<li><a href="seccion_consultas_listas.php">Listas</a></li>-->
            </ul>
            <ul>
                <!--<li><a href="seccion_consultas_listas_internet.php">Listas Internet</a></li>-->
            </ul>
            <ul>
                 <li><a href="seccion_consultas_listas_numeros.php">Lista General Totales</a></li>
            </ul>
            <ul>
                <!-- <li><a href="seccion_consultas_lista_general.php">Lista General</a></li>-->
            </ul>
            <ul>
                <!--<li><a href="seccion_consultas_revision.php">Lista Revision Tablets</a></li>-->
            </ul>
            <ul>
                <li><a href="seccion_consultas_revision_puestos.php">Lista Revision Puestos</a></li>
            </ul>
            <ul>
                <!--<li><a href="seccion_consultas_revision_papelitos.php">Lista Revision Peluquero</a></li>-->
            </ul>
            <ul>
                <!--<li><a href="seccion_consultas_banca_seguros.php">Seguros del Dia</a></li>-->
            </ul>
            <ul>
                <!--<li><a href="seccion_consulta_hojacobro.php">Hoja de Cobro</a></li>-->
            </ul>
            <ul>
                <!--<li><a href="seccion_consultas_hojacobro_tablet.php">Hoja de Cobro Tablets</a></li>-->
            </ul>
            <ul>
                <!-- <!--<li><a href="seccion_consultas_seguros.php">Lista Seguros</a></li>-->
            </ul>
            <ul>
                <!-- <!--<li><a href="seccion_consultas_jugadas.php">Lista Jugada</a></li>-->
            </ul>
        <?php endif; ?>
        <?php if (in_array($_SESSION['TipoUsuarioInt'], $canSeeTickets)): ?>
            <ul>
                <!--<li><a href="seccion_consultas_tickets.php">Tickets</a></li>-->
            </ul>
            <ul>
                <!-- <li><a href="seccion_consultas_monto_ganadores.php">Montos Ganadores</a></li>-->
            </ul>
        <?php endif; ?>

        <?php if (in_array($_SESSION['TipoUsuarioInt'], $canSeeJugadores)): ?>
            <ul>
                <!--<li><a href="seccion_consultas_jugadores_jugadores.php">Jugadores Ticket</a></li>-->
            </ul>
        <?php endif ?>

        <?php if (in_array($_SESSION['TipoUsuarioInt'], $canSeeApuestasDepositos)): ?>
            <ul>
                <!--<li><a href="seccion_consultas_cliente_reporte_apuestas.php">Reporte Apuestas</a></li>-->
            </ul>
            <ul>
                <!--<li><a href="seccion_consultas_cliente_reporte_depositos.php">Reporte Depósitos</a></li>-->
            </ul>
            <ul>
                <!--<li><a href="seccion_consultas_cliente_reporte_listas_x_dia.php">Reporte Listas</a></li>-->
            </ul>
        <?php endif ?>

    </div>
</aside_submenu>
<div style="visibility: hidden" id="auth_dialog" title="Restringido">
    <form action="" method="post" id="form_auth">
        <fieldset id="filtros">
            <label for="_pin">PIN</label>
            <input value="" name="PIN_REPORTS" type="password" id="_pin"/>
        </fieldset>
        <br>

        <div style="text-align: center;"><input type="submit" value="Ingresar"></div>
        <input type="hidden" name="redirect_to" value="<?php echo 'seccion_consultas.php'; ?>">
    </form>
</div>
<script>
    $('.auth').on('click', function (e) {
        e.preventDefault();
        var $dialog = $('#auth_dialog');
        var $form = $dialog.find('#form_auth');
        $form.attr('action', this.href);
        $dialog.css("visibility", "visible");
        $dialog.dialog({
            resizable: false,
            height: 230,
            modal: true
        });
    });
</script>