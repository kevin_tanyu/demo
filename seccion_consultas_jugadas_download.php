<?php
session_start();

// Incluye datos generales y conexion a DB
require_once 'config.ini.php';
require_once 'conectadb.php';
require_once 'lib/seccion_datos_importacion_listas_lib.php';

/*******************COMPRADORES JUGADAS******************************/
$sqlVendedores = "SELECT U.ID as usuario_id, U.NombreUsuario as nombre_usuario, f_jugada as paga_jugada, U.recargaLocura
                  FROM Usuarios U
                  WHERE TipoUsuario=5 and FlagActivo=1 and ActivoFlag=1
                  order by U.NombreUsuario";

$stmtVendedores = $pdoConn->prepare($sqlVendedores);
$stmtVendedores->execute();
$vendedores = $stmtVendedores->fetchAll(PDO::FETCH_ASSOC);
/********************************************************************/

/*******************SORTEO******************************/
$sqlSorteosDelDia = "SELECT SP.ID AS sorteo_prog_id, SD.NombreSorteo as nombre_sorteo
                     FROM SorteosProgramacion SP
                     JOIN SorteosDefinicion SD
                     ON SP.IDSorteoDefinicion = SD.ID
                     WHERE SP.ID = ?";

$stmtSorteosDelDia = $pdoConn->prepare($sqlSorteosDelDia);
$stmtSorteosDelDia->execute(array($_GET['SID']));
$sorteosDelDia = $stmtSorteosDelDia->fetchAll(PDO::FETCH_ASSOC);
/****************************************************************/

$accion = $_GET['accion'];

$nombreArchivo = writeExcelJugadas($vendedores,$sorteosDelDia, $pdoConn, $accion);
if($accion === 'Guardar'){
    echo($nombreArchivo);
}else{
    echo('./Descargas/' . $nombreArchivo);
}


//echo('./LISTASEGUROS.xls')
?>