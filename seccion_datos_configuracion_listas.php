<?php
session_start();

if (!isset($_SESSION['ActivoFlag']))
{
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

// Incluye Header
include("header.php");

include("seccion_datos_submenu.php");
?>
<script type="text/javascript">
    $(document).ready(function () {
        $(".various").fancybox({
            fitToView: true,
            width: '70%',
            height: '70%',
            autoSize: true,
            closeClick: false,
            openEffect: 'fade',
            closeEffect: 'elastic'
        });
        TablaUsuarios();

    });

    function AbreVentanaLoterias() {
        $(".various2").fancybox({
            fitToView: true,
            width: '70%',
            height: '70%',
            autoSize: true,
            closeClick: false,
            openEffect: 'fade',
            closeEffect: 'elastic'
        });

        return false;
    }

    function TablaUsuarios() {
        $('#TablaUsuarios').load('seccion_datos_configuracion_listas_form.php');
        return false;
    }

</script>
<style type="text/css">
    .fancybox-custom .fancybox-skin {
        box-shadow: 0 0 50px #222;
    }
</style>

<div id="ContenedorGeneral">
    <div id="TablaUsuarios"></div>
    <br><br>

</div>
<?php
// Incluye Footer
include("footer.php");
?>
