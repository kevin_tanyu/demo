<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 4/9/15
 * Time: 12:58 PM
 */

/**
 * @param $parametros
 * @param $fieldByGroup
 * @return array options
 */
function group_options_by($parametros, $fieldByGroup)
{
    $options = array();
    foreach ($parametros as $parametro)
    {
        if (!array_key_exists($parametro[$fieldByGroup], $options))
        {
            $options[$parametro[$fieldByGroup]] = array();
        }
        $options[$parametro[$fieldByGroup]][] = $parametro;
    }

    return $options;
}