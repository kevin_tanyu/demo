<?php
/**
 * Funciones para la parte de seccion_datos_numerosganadores
 * User: jorge
 * Date: 10/24/14
 * Time: 11:42 AM
 */

/**
 * Obtiene los vendedores calle (vendedores antes) y sus tickets
 * @param $pdoConn PDO conexion a base de datos
 * @param $idSorteoProgramacion int el sorteo programacion del cual se quieren los tickets ganadores
 * @return array los vendedores con sus tickets respectivos
 */
function sdn_obtener_vendedores_calle_con_tickets($pdoConn, $idSorteoProgramacion)
{
    $vendedoresSql       = "SELECT distinct u.NombreUsuario, u.Email, u.ID
  FROM Usuarios_View u
  where u.TipoUsuario = :tipo_usuario";
    $vendedoresStatement = $pdoConn->prepare($vendedoresSql);
    $vendedoresStatement->execute(array(':tipo_usuario' => NIVEL_PERMISO_USUARIO_CALLE));

    $vendedores = array();
    while ($vendedor = $vendedoresStatement->fetch(PDO::FETCH_ASSOC))
        $vendedores[] = $vendedor;

    foreach ($vendedores as &$vendedor)
    {
        $sqlTicketsGanadores  = "SELECT TV.ID AS TicketId
          , SA.Numero
          , SA.Cantidad
          , SD.NombreSorteo
          , SP.FechayHora
          FROM SorteosNumerosGanadores SNG
          JOIN SorteoApuesta SA ON SA.Numero = SNG.Numero AND SA.IDSorteoProgramacion = SNG.IDSorteoProgramacion
          JOIN Ticket_View TV ON TV.ID = SA.IDTicket
          JOIN SorteosProgramacion SP ON SP.ID = SA.IDSorteoProgramacion
          JOIN SorteosDefinicion SD ON SD.ID = SP.IDSorteoDefinicion
          WHERE TV.IDUsuario = :vendedor_id AND SNG.IDSorteoProgramacion = :id_sorteo_prog";
        $ticketsGanadoresStmt = $pdoConn->prepare($sqlTicketsGanadores);
        $ticketsGanadoresStmt->execute(array(':vendedor_id' => $vendedor['ID'], ':id_sorteo_prog' => $idSorteoProgramacion));

        $vendedor['tickets'] = array();
        while ($ticket = $ticketsGanadoresStmt->fetch(PDO::FETCH_ASSOC))
        {
            $vendedor['tickets'][] = $ticket;
        }
    }

    return $vendedores;
}

/**
 * Obtiene los vendedores web (revendedores antes) y sus tickets
 * @param $pdoConn PDO conexion a base de datos
 * @param $idSorteoProgramacion int el sorteo programacion del cual se quieren los tickets ganadores
 * @return array los vendedores web con sus tickets respectivos
 */
function sdn_obtener_vendedores_web_con_tickets($pdoConn, $idSorteoProgramacion)
{
    //reenviar correo a los revendedores
    $re_vendedoresSql = "SELECT distinct u.NombreUsuario, u.Email, u.ID
                      FROM Usuarios_View u
                      where u.TipoUsuario = :tipo_usuario";

    $re_vendedoresStatement = $pdoConn->prepare($re_vendedoresSql);
    $re_vendedoresStatement->execute(array(':tipo_usuario' => NIVEL_PERMISO_USUARIO_WEB));

    $re_vendedores = array();
    while ($re_vendedor = $re_vendedoresStatement->fetch(PDO::FETCH_ASSOC))
        $re_vendedores[] = $re_vendedor;

    foreach ($re_vendedores as &$re_vendedor)
    {
        $sqlTicketsGanadores  = "SELECT TV.ID AS TicketId
          , SA.Numero
          , SA.Cantidad
          FROM SorteosNumerosGanadores SNG
          JOIN CLIENTE_SorteoApuesta SA ON SA.Numero = SNG.Numero AND SA.IDSorteoProgramacion = SNG.IDSorteoProgramacion
          JOIN CLIENTE_Ticket_View TV ON TV.ID = SA.IDTicket
          JOIN Usuarios_View U ON U.ID = SA.IDUsuario
          WHERE U.IDPadre = :re_vendedor_id AND SNG.IDSorteoProgramacion = :id_sorteo_prog";
        $ticketsGanadoresStmt = $pdoConn->prepare($sqlTicketsGanadores);
        $ticketsGanadoresStmt->execute(array(':re_vendedor_id' => $re_vendedor['ID'], ':id_sorteo_prog' => $idSorteoProgramacion));
        $re_vendedor['tickets'] = array();
        while ($ticket = $ticketsGanadoresStmt->fetch(PDO::FETCH_ASSOC))
        {
            $re_vendedor['tickets'][] = $ticket;
        }
    }

    return $re_vendedores;
}

/***
 * Envia los corres que vengan para los usuarios en un array de la siguiente forma
 * ('group_name' => ( ('usuario_1_info','tickets'), ('usuario_2_info','tickets'), ...  )
 */
function sdn_enviaCorreos($usuariosAgrupados, $infoSorteo)
{
    foreach ($usuariosAgrupados as $usuarios)
    {
        foreach ($usuarios as $usuario)
        {
            if (count($usuario['tickets']) > 0)
            {
                $output = "<html><body><b>Estos son los tickets ganadores:</b><br/><table border='1px' >";
                $output .= "<thead> <tr><th>Ticket #</th><th>Numero</th><th>Cantidad</th></tr> </thead>";
                $output .= "<tbody>";
                foreach ($usuario['tickets'] as $ticket)
                {
                    $output .= "<tr>";
                    $output .= "<td>" . $ticket['TicketId'] . "</td>";
                    $output .= "<td>" . $ticket['Numero'] . "</td>";
                    $output .= "<td>" . $ticket['Cantidad'] . "</td>";
                    $output .= "</tr>";
                }
                $output .= "</tbody>";
                $output .= "</table>";
                $output .= "</body></html>";
            } else
            {
                $output = "<html><body><b>Estos son los tickets ganadores:</b><br/>";
                $output .= "<h3>No hay tickets ganadores</h3>";
                $output .= "</body></html>";
            }

            $recipient = $usuario['Email'] ? : "info@followyourluck.com";
            $subject   = "Premios de sorteo " . $infoSorteo['NombreSorteo'] . " " . $infoSorteo['FechayHora'] . " " . $usuario['NombreUsuario'];
            $mailmsg   = $output;

            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
//            $headers .= 'To: ' . $recipient . "\r\n";
            $headers .= 'From: info@eldiadesuerte.com' . "\r\n";
            mail($recipient, $subject, $mailmsg, $headers);
//            mail("jorge@dozvox.com", $subject, $mailmsg, $headers);
//            mail("kioto6@gmail.com", $subject, $mailmsg, $headers);
//            echo "id: " . $usuario['ID'] . " email: " . $recipient;
//            echo "<br/>";
        }
    }
}