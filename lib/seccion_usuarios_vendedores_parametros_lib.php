<?php
/**
 * Funciones para vendedores parametros action
 * User: jorge
 * Date: 10/30/14
 * Time: 6:05 PM
 */


/**
 * devuelve un array para desplegar las opciones para loteria
 * @param $parametros los parametros
 * @param $loterias las loterias disponibles
 * @param $userId el usuario para el cual se piden las opciones
 * @return array las opciones
 */
function get_opciones_desplegar($parametros, $loterias, $userId)
{
    $opciones = array();
    $searchIn = array();
    foreach ($parametros as $parametro)
    {
        $opciones[]                                 = array('user_id' => $parametro['IDUsuario'],
            'sorteo_definicion' => $parametro['IDSorteoDefinicion'],
            'sorteo_definicion_nombre' => $parametro['NombreSorteo'],
            'porcentaje_restriccion' => $parametro['porcentaje_restriccion'],
            'tope_inicial' => $parametro['tope_inicial']);
        $searchIn[$parametro['IDSorteoDefinicion']] = true;
    }

    foreach ($loterias as $loteria)
    {
        if (!array_key_exists($loteria['ID'], $searchIn))
        {
            $opciones[] = array('user_id' => $userId,
                'sorteo_definicion' => $loteria['ID'],
                'sorteo_definicion_nombre' => $loteria['NombreSorteo'],
                'porcentaje_restriccion' => 15,
                'tope_inicial' => 2500);
        }
    }

    return $opciones;
}