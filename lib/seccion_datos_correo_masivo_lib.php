<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 10/27/14
 * Time: 6:11 PM
 */


/**
 * @param $pdoConn PDO conexion a bd para traer los datos
 * @return array los vendedores web
 */
function getVendedoresWeb($pdoConn)
{
    $sql  = "SELECT ID,NombreUsuario FROM Usuarios_View WHERE TipoUsuario = :tipo_usuario";
    $stmt = $pdoConn->prepare($sql);
    $stmt->execute(array(':tipo_usuario' => NIVEL_PERMISO_USUARIO_WEB));

    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

/**
 *
 * @throws Exception when no param $id is provided
 *
 * @param $pdo PDO
 * @param $id
 * @return array
 */
function getClientesDeVendedor($pdo, $id)
{
    if (empty($id)) throw new Exception('Param id cant be null');
    $sql  = "SELECT ID,Email FROM Usuarios_View WHERE IDPadre = :id_vendedor_web";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(array(':id_vendedor_web' => $id));

    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}