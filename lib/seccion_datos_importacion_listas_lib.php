<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 10/27/14
 * Time: 6:11 PM
 */


/**
 * @param $dir string dir with
 * @return array con los files
 */
function getXlsAvailableFiles($dir)
{
    $files       = getAvailableFiles($dir);
    $returnFiles = array();
    foreach ($files as $file)
    {
        if (strstr($file, ".xls"))
        {
            $name          = getFileName($file, ".xls");
            $path          = $dir . "/" . $file;
            $returnFiles[] = array('name' => $name, 'path' => $path);
        }
    }

    return $returnFiles;
}


/**
 * @param $dir string directory from where get files
 * @return array all files (including '.', '..')
 * @throws BadMethodCallException
 */
function getAvailableFiles($dir)
{
    if (empty($dir))
        throw new BadMethodCallException('Paramenter $dir can\'t  be empty');
    $files = scandir($dir);

    return $files;
}


/**
 * @param $file string complete name of file
 * @param $ext string the file extension to be removed for get the name
 * @return mixed
 */
function getFileName($file, $ext)
{
    $name = str_replace($ext, "", $file);

    return $name;
}


/**
 * @param $files array files selected by user
 * @return array
 */
function prepareParameterFiles($files)
{
    $preparedFiles = array();

    foreach ($files as $file)
    {
        $preparedFiles[] = array('name' => $file,
            'full_name' => $file . '.xls',
            'path' => IMPORTACION_DIR . '/' . $file . '.xls');
    }

    return $preparedFiles;
}

/**
 * @param $pdoConn PDO connection to db for insert data
 * @param $files array all files to insert
 * @param $uniqueId string id of 'actual import process'
 * @param $userId int user id who triggers this import
 * @return bool
 * @throws Exception
 */
function insertFilesMetaInfo($pdoConn, $files, $uniqueId, $userId)
{
    $success            = true;
    $sql                = "INSERT INTO IL_ArchivosASubir (name, full_name, full_path, IDUsuario, import_id) VALUES (?,?,?,?,?)";
    $stmt               = $pdoConn->prepare($sql);
    $preparedParameters = prepareParameterFiles($files);
    foreach ($preparedParameters as $file)
    {
        $stmt->execute(array($file['name'],
                $file['full_name'],
                $file['path'],
                $userId,
                $uniqueId
            )
        );
        if ('00000' !== $stmt->errorCode()) throw new Exception("Hubo un error con el ingreso de los archivos. " . print_r($stmt->errorInfo(), true));
    }

    return $success;
}


/**
 * @param $pdoConn PDO db connection
 * @param $importProcessId string id of import process
 */
function importFilesInfo($pdoConn, $importProcessId)
{
    $filesSql = "SELECT * FROM IL_ArchivosASubir WHERE import_id = :import_id";
    $stmt     = $pdoConn->prepare($filesSql);
    $stmt->execute(array(':import_id' => $importProcessId));

    foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $file)
    {
        $fullPath = $file['full_path'];
      //  $lines    = getFileLines($fullPath);
        insertFileLines($pdoConn, $file['id'], $fullPath);
    }

}

//Funcion para recuperar el total de un numero en "x" excel
function getTotalFromNumber($filename, $number){

    require_once 'phpExcel/PHPExcel.php';
    $reader      = new PHPExcel_Reader_Excel5();
    $Path =      IMPORTACION_DIR . '/' . $filename  . '.xls';//Ruta del excel
    $obj         = $reader->load($Path);
    $activeSheet = $obj->getActiveSheet();

    //Para determinar la posicion del numero en el excel
    if($number == 0){
        $number = 101;
    }else{
        $number = $number + 1;
    }

    return $activeSheet->getCell("B$number")->getOldCalculatedValue();


}//FIN getTotalFromNumber

//Funcion para leer el total directamente desde el excel
//$filename = nombre del archivo sin el ".xls" a recorrer
function readTotal($filename)//Recibe el nombre del archivo
{
    require_once 'phpExcel/PHPExcel.php';
    $reader      = new PHPExcel_Reader_Excel5();
    $Path =      IMPORTACION_DIR . '/' . $filename  . '.xls';//Ruta del excel
    $obj         = $reader->load($Path);
    $activeSheet = $obj->getActiveSheet();



    $total = 0;//Monto total

    //Recorre la columnas en donde se encuentra los montos del 00 al 99
    for ($i = 2; $i <= 101; ++$i)
    {
        if($activeSheet->getCell("B$i")->getOldCalculatedValue() == NULL){
            $amount =      $activeSheet->getCell("B$i")->getValue();
        }else{
            $amount =      $activeSheet->getCell("B$i")->getOldCalculatedValue();
        }
        $total = $total + $amount;
    }


    return $total;
}

/*Funcion que ingresa temporalmente los numeros y los montos del excel que se requiera;
$pdoConn = Conexion a la BD
$filename = nombre del archivo sin el ".xls"
*/
function insertLinesToArchivoPuesto($pdoConn, $filename){

    try{

        //Preparacion del sql para insertar los numeros y montos
        $sqlInsert = "INSERT INTO il_archivo_puestos(NombreArchivo, number, amount) VALUES (?, ?, ?)";
        $stmtInsert = $pdoConn->prepare($sqlInsert);

        //Prepara el excel para ser leido
        require_once 'phpExcel/PHPExcel.php';
        $reader      = new PHPExcel_Reader_Excel5();
        $Path =      IMPORTACION_DIR . '/' . $filename  . '.xls';//Ruta del excel
        $obj         = $reader->load($Path);
        $activeSheet = $obj->getActiveSheet();

        //Recorre la columnas en donde se encuentra los montos del 00 al 99
        for ($i = 2; $i <= 101; ++$i)
        {
            $number = $i - 1;
            if($number == 100){
                $number = 0;
            }
            if($activeSheet->getCell("B$i")->getOldCalculatedValue() == NULL){
                $amount =      $activeSheet->getCell("B$i")->getValue();
            }else{
                $amount =      $activeSheet->getCell("B$i")->getOldCalculatedValue();
            }

            $stmtInsert->execute(array($filename, $number, $amount));
        }

        return true;

    }catch (Exception $e){

        return false;
    }



}//Fin insertLinesToArchivoPuesto

/**
 * @param $fullPath string path to xls file
 * @return array
 */
function getFileLines($fullPath)
{
    require_once 'phpExcel/PHPExcel.php';
    $lines = array();

    $reader      = new PHPExcel_Reader_Excel5();
    $obj         = $reader->load($fullPath);
    $activeSheet = $obj->getActiveSheet();


    //para el 00 que en la lista viene como el 100
    $number  = 0;
    if($activeSheet->getCell("B101")->getOldCalculatedValue() == NULL){
        $amount =      $activeSheet->getCell("B101")->getValue();
    }else{
        $amount =      $activeSheet->getCell("B101")->getOldCalculatedValue();
    }

    array_push($lines, array('number' => $number, 'amount' => $amount));
    //hasta el 99
    for ($i = 2; $i <= 100; $i++)
    {

       //if($activeSheet->getCell("B" .$i)->getOldCalculatedValue() == NULL){
          $amount =      $activeSheet->getCell("B" .$i)->getValue();
    //   }else{
     //       $amount =      $activeSheet->getCell("B" .$i)->getOldCalculatedValue();
    //   }

       //     $lines[] = array('number' => $i -1, 'amount' => $amount);
        array_push($lines, array('number' => $i -1, 'amount' => $amount));

    }

    return $lines;
}


/**
 * @param $pdoConn PDO connection to db
 * @param $lines mixed array with lines to insert
 * @param $fileId int file id
 * @return bool result
 */
function insertFileLines($pdoConn, $fileId, $fullPath)
{

    try{

        //Preparacion del sql para insertar los numeros y montos
        $sqlInsert = "INSERT INTO IL_Archivo (file_id, number, amount) VALUES (?, ?, ?);";
        $stmtInsert = $pdoConn->prepare($sqlInsert);

        //Prepara el excel para ser leido
        require_once 'phpExcel/PHPExcel.php';
        $reader      = new PHPExcel_Reader_Excel5();
        $obj         = $reader->load($fullPath);
        $activeSheet = $obj->getActiveSheet();

        //Recorre la columnas en donde se encuentra los montos del 00 al 99
        for ($i = 2; $i <= 101; ++$i)
        {
            $number = $i - 1;
            if($number == 100){
                $number = 0;
            }
            if($activeSheet->getCell("B$i")->getOldCalculatedValue() == NULL){
                $amount =      $activeSheet->getCell("B$i")->getValue();
            }else{
                $amount =      $activeSheet->getCell("B$i")->getOldCalculatedValue();
            }

            $stmtInsert->execute(array($fileId, $number, $amount));
        }

        return true;

    }catch (Exception $e){

        return false;
    }
}

function importarATablet($pdoConn, $filename, $userID, $sorteoID)
{

    try{

        $sqlComprobarTicket = "SELECT * FROM SorteoApuesta WHERE IDUsuario = ? AND IDSorteoProgramacion = ? LIMIT 1";
        $stmtComprobarTicket = $pdoConn->prepare($sqlComprobarTicket);
        $stmtComprobarTicket->execute(array($userID, $sorteoID));

        if($stmtComprobarTicket->rowcount() > 0){
            $ticket = $stmtComprobarTicket->fetch(PDO::FETCH_ASSOC);
            $sqlDeleteTicket = "DELETE FROM Ticket WHERE id = ?";
            $stmtDeleteTicket = $pdoConn->prepare($sqlDeleteTicket);
            $stmtDeleteTicket->execute(array($ticket['IDTicket']));
        }

        //Preparacion del sql para insertar los numeros y montos
        $sqlInsert = "INSERT INTO SorteoApuesta (IDUsuario, IDSorteoProgramacion, FechayHora, IDTicket, Cantidad, Numero)
                                         VALUES (?, ?, ?, ?, ?, ?);";
        $stmtInsert = $pdoConn->prepare($sqlInsert);

        $sqlInsertTicket = "INSERT INTO Ticket(IDUsuario, FechayHora) VALUES(?,?)";
        $stmtInsertTicket = $pdoConn->prepare($sqlInsertTicket);
        $stmtInsertTicket->execute(array($userID, date('Y-m-d H:i:s')));

        $ticketID = $pdoConn->lastInsertId();

        //Prepara el excel para ser leido
        require_once 'phpExcel/PHPExcel.php';
        $reader      = new PHPExcel_Reader_Excel5();
        $obj         = $reader->load(IMPORTACION_DIR . '/' . $filename . '.xls');
        $activeSheet = $obj->getActiveSheet();

        //Recorre la columnas en donde se encuentra los montos del 00 al 99
        for ($i = 2; $i <= 101; ++$i)
        {
            $number = $i - 1;
            if($number == 100){
                $number = 0;
            }
            if($activeSheet->getCell("B$i")->getOldCalculatedValue() == NULL){
                $amount =      $activeSheet->getCell("B$i")->getValue();
            }else{
                $amount =      $activeSheet->getCell("B$i")->getOldCalculatedValue();
            }

            $stmtInsert->execute(array($userID, $sorteoID, date('Y-m-d H:i:s'), $ticketID, $amount, $number));
        }

        return true;

    }catch (Exception $e){

        return false;
    }
}

/**
 * @param $pdoConn PDO connection to db
 * @param $userId int user id to reset process info
 */
function clearPreviousProcess($pdoConn, $userId)
{
    $sqlDeleteFiles = "DELETE FROM IL_ArchivosASubir WHERE IDUsuario = ?";
    $stmt           = $pdoConn->prepare($sqlDeleteFiles);
    $stmt->execute(array($userId));
}

/**
 * @param $pdoConn PDO db connection
 * @param $importProcessId string import process id
 * @param $userId int current user id
 * @return mixed
 */
function getAvailableFilesToUpload($pdoConn, $importProcessId, $userId)
{
    $files = array();

    $sqlTotales = "SELECT *
                FROM IL_ArchivosASubir AAS
                  LEFT JOIN (SELECT file_id, SUM(amount) as total FROM IL_Archivo GROUP BY file_id) AS TT ON TT.file_id = AAS.id
                  WHERE AAS.IDUsuario = :id_usuario AND AAS.import_id = :import_id";
    $stmt       = $pdoConn->prepare($sqlTotales);
    $stmt->execute(array(':id_usuario' => $userId, ':import_id' => $importProcessId));

    foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $file)
    {
        $files[] = array('id' => $file['id'], 'name' => $file['full_name'], 'total' => $file['total']);
    }

    return $files;
}

/**
 * Create users for files if necessary
 * @param $pdoConn PDO db connection
 * @param $importProcessId string actual import process id
 * @param $userId int actual user id
 */
function createUsersIfNecesary($pdoConn, $importProcessId, $userId)
{
    $sqlFilesWithoutUser = "SELECT AAS.name, full_name, CONCAT('IMPORT_',AAS.name) AS final_name
                            FROM IL_ArchivosASubir AAS
                              LEFT JOIN Usuarios_View UV ON CONCAT('IMPORT_',AAS.name) = UV.NombreUsuario
                                                            AND import_id = :import_id
                                                            AND UV.TipoUsuario = :tipo_usuario
                            WHERE UV.ID IS NULL";

    $stmt = $pdoConn->prepare($sqlFilesWithoutUser);
    $stmt->execute(array(':import_id' => $importProcessId, ':tipo_usuario' => NIVEL_PERMISO_USUARIO_CALLE));

    $sqlInsertNewUser = "INSERT INTO Usuarios
                        (NombreUsuario, Contrasena, TipoUsuario, ActivoFlag, IDPadre
                        , Email, FlagActivo, PagaPorcentaje, en_lista_general)
                        VALUES (?,?,?,?,?,?,?,?,?)";
    $stmtNewUser      = $pdoConn->prepare($sqlInsertNewUser);
    foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $newUser)
    {
        $newUserName = $newUser['final_name'];
        $params      = array($newUserName, '', NIVEL_PERMISO_USUARIO_CALLE
        , 1, $userId, $newUserName . '@gmail.com', 1, 80, 0);
        $stmtNewUser->execute($params);
    }

}

/*$arrayVendedores = arreglo con la lista de vendedores
 *$arraySorteos = arreglo con los sorteos del dia
 *$pdoConn = conexion a la BD
*/
//Metodo para crear el excel de la lista de seguros
function writeExcelSeguros($arrayVendedores, $arraySorteos, $pdoConn, $accion){

    //Prepara el excel para creado y escrito
    require_once 'phpExcel/PHPExcel.php';
    require_once 'phpExcel/PHPExcel/Writer/Excel5.php';
    //Centra el texto de los cuadros
    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );

    $spreadsheet = new PHPExcel();
    $spreadsheet->setActiveSheetIndex(0);

    $worksheet = $spreadsheet->getActiveSheet();
    $worksheet->getDefaultStyle()->applyFromArray($style);
    $worksheet->getColumnDimensionByColumn(0)->setAutoSize(true);
    $spreadsheet->getActiveSheet()->getStyle("A4:BL4")->getFont()->setBold(true);
    $spreadsheet->getActiveSheet()->getStyle("B1:B110")->getFont()->setBold(true);
    $spreadsheet->getActiveSheet()->getStyle("A1:A3")->getFont()->setBold(true);


    $idSorteo = 0;

    //ESCRIBE EL NOMBRE DEL SORTEO
    foreach($arraySorteos as $sorteo){

        $worksheet->SetCellValueByColumnAndRow(0, 1, $sorteo['nombre_sorteo']);
        $idSorteo = $sorteo['sorteo_prog_id'];
        $nombreSorteo = $sorteo['nombre_sorteo'];
    }

    //ESCRIBE LA FECHA DEL DIA
    $worksheet->SetCellValueByColumnAndRow(0, 2, date("d/m/Y"));

    //ESCRIBE EL ENCABEZADO
    $worksheet->SetCellValueByColumnAndRow(0, 5, "Numeros");
    $worksheet->SetCellValueByColumnAndRow(1, 4, "Total");

    //ESCRIBE LOS NUMEROS DE 0-99
    for($i = 6; $i < 106; $i++){
        $worksheet->SetCellValueByColumnAndRow(0, $i, $i-6);
    }

    $columna = 2;
    $totalesVendedor = array();//PARA IR GUARDANDO EL TOTAL DE CADA VENDEDOR PARA SER ESCRITOS LUEGO
    $totalesNumeros = array_fill(0, 100, 0);//PARA IR GUARDANDO EL TOTAL DE CADA NUMERO PARA SER ESCRITOS LUEGO


    foreach($arrayVendedores as $vendedor){
        $worksheet->getColumnDimensionByColumn($columna)->setAutoSize(true);
        if (strpos($vendedor['nombre_usuario'],'IMPORT') === false){
            $worksheet->SetCellValueByColumnAndRow($columna, 5,$vendedor['nombre_usuario']);
        }else{
            $worksheet->SetCellValueByColumnAndRow($columna, 5, substr($vendedor['nombre_usuario'], 7));
        }


        for($i = 6; $i < 106; $i++){
            $worksheet->SetCellValueByColumnAndRow($columna, $i, 0);
        }//FIN FOR

        /*******************SEGUROS COMPRADOS POR VENDEDOR******************************/
        $sqlSeguros = "SELECT numero, monto
                           FROM seg_lista_seguros
                           WHERE sorteo_programacion_id = :sorteo_id and user_id = :user_id";

        $stmtSeguros = $pdoConn->prepare($sqlSeguros);
        $stmtSeguros->execute(array(':sorteo_id' => $idSorteo, ':user_id' => $vendedor['usuario_id']));
        $seguros = $stmtSeguros->fetchAll(PDO::FETCH_ASSOC);
        /********************************************************************/

        $totalesNumerosVendedor = array_fill(0, 100, 0);//PARA IR GUARDANDO EL TOTAL DE CADA NUMERO x VENDEDOR
        $total = 0;//Total acumulado por vendedor
        foreach($seguros as $seguro){

            $pos = $seguro['numero'];
            $totalesNumeros[$pos] = $totalesNumeros[$pos] + $seguro['monto'];
            $total = $total + $seguro['monto'];
            $totalesNumerosVendedor[$pos] = $totalesNumerosVendedor[$pos] + $seguro['monto'];

        }//FIN FOREACH

        array_push($totalesVendedor, $total);

        //ESCRIBE LOS EL TOTAL DE CADA NUMERO POR VENDEDOR
        for($i = 0; $i < sizeof($totalesNumerosVendedor); $i++){
            $worksheet->SetCellValueByColumnAndRow($columna, $i+6, $totalesNumerosVendedor[$i]);
        }//FIN FOR


        $columna++;

    }//FIN FOREACH

    $totalGeneral = 0;
    //ESCRIBE LOS EL TOTAL  VENDEDOR
    for($i = 0; $i < sizeof($totalesVendedor); $i++){
        $worksheet->SetCellValueByColumnAndRow($i + 2, 4, $totalesVendedor[$i]);
        $totalGeneral = $totalGeneral + $totalesVendedor[$i];
    }//FIN FOR

    //ESCRIBE LOS EL TOTAL  NUMERO
    for($i = 0; $i < sizeof($totalesNumeros); $i++){
        $worksheet->SetCellValueByColumnAndRow(1, $i+6, $totalesNumeros[$i]);
    }//FIN FOR

    //ESCRIBE EL TOTAL GENERAL DE LA LISTA
    $worksheet->SetCellValueByColumnAndRow(0, 3, 'TOTAL LISTA: ');
    $worksheet->SetCellValueByColumnAndRow(1, 3,  $totalGeneral);

    //AGREGA LOS PADRINOS AL EXCEL
    $columna++;
    $empiezaPadrino = $columna; //COLUMNA PARA GAURDAR DONDE EMPIEZA LOS PADRINOS
    $sqlBancas = "SELECT U.ID, U.NombreUsuario, U.Reparticion
              FROM Usuarios U JOIN Usuarios P ON U.IDPadre = P.ID
              WHERE U.NivelUsuario
              IN (SELECT id from UsuariosNivel where jerarquia = 'Bank')";
    $stmtBancas = $pdoConn->prepare($sqlBancas);
    $stmtBancas->execute();
    $bancas = $stmtBancas->fetchAll(PDO::FETCH_ASSOC);

    //SQL PARA RECUPERAR LOS NUMEROS Y MONTOS POR USUARIO
    $sqlSeguros = "SELECT numero, monto FROM seg_lista_seguros_banca
        WHERE sorteo_id = :sorteo_id and user_id = :id_usuario
        ORDER BY numero ASC";
    $stmtSeguros = $pdoConn->prepare($sqlSeguros);

    foreach($bancas as $banca){
        $stmtSeguros->execute(array(':sorteo_id' => $idSorteo, ':id_usuario' => $banca['ID']));
        $numerosSeguros = $stmtSeguros->fetchAll(PDO::FETCH_ASSOC);

        $worksheet->SetCellValueByColumnAndRow($columna, 5, $banca['NombreUsuario']);

        for($i = 6; $i < 106; $i++){
            $worksheet->SetCellValueByColumnAndRow($columna, $i, 0);
        }//FIN FOR

        $totalPadrino = 0;
        $totalesNumerosPadrino = array_fill(0, 100, 0);//PARA IR GUARDANDO EL TOTAL DE CADA NUMERO x VENDEDOR
        foreach($numerosSeguros as $seguro){

            $pos = $seguro['numero'];
            $totalesNumerosPadrino[$pos] = $totalesNumerosPadrino[$pos] + $seguro['monto'];
            $totalPadrino += $seguro['monto'];

        }//FIN FOREACH

        $worksheet->SetCellValueByColumnAndRow($columna, 4 , $totalPadrino);
        for($i = 0; $i < sizeof($totalesNumerosPadrino); $i++){
            $worksheet->SetCellValueByColumnAndRow($columna, $i+6, $totalesNumerosPadrino[$i]);
        }//FIN FO

        $columna++;
    }

    if($accion == 'Descargar'){
        $nombreArchivo = 'SEGUROS'.trim($nombreSorteo, " ").'.xls';//Nombre del excel
        $writer = new PHPExcel_Writer_Excel5($spreadsheet);
        $writer->save(IMPORTACION_DIR . '/' . $nombreArchivo);
        $writer->save('./Descargas/'. $nombreArchivo);

        return  $nombreArchivo;
    }else{
        return  "Guardado";
    }







}//FIN writeExcelSeguros


/*$arrayVendedores = arreglo con la lista de vendedores
 *$arraySorteos = arreglo con los sorteos del dia
 *$pdoConn = conexion a la BD
*/
//Metodo para crear el excel de la lista de jugadas
function writeExcelJugadas($arrayVendedores, $arraySorteos, $pdoConn, $accion){

    //Prepara el excel para creado y escrito
    require_once 'phpExcel/PHPExcel.php';
    require_once 'phpExcel/PHPExcel/Writer/Excel5.php';
    //Centra el texto de los cuadros
    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );

    $spreadsheet = new PHPExcel();
    $spreadsheet->setActiveSheetIndex(0);

    $worksheet = $spreadsheet->getActiveSheet();
    $worksheet->getDefaultStyle()->applyFromArray($style);
    $worksheet->getColumnDimensionByColumn(0)->setAutoSize(true);
    $spreadsheet->getActiveSheet()->getStyle("A1:A100")->getFont()->setBold(true);
    $spreadsheet->getActiveSheet()->getStyle("A4:Z4")->getFont()->setBold(true);

    $idSorteo = 0;

    //ESCRIBE EL NOMBRE DEL SORTEO
    foreach($arraySorteos as $sorteo){

        $worksheet->SetCellValueByColumnAndRow(0, 1, $sorteo['nombre_sorteo']);
        $idSorteo = $sorteo['sorteo_prog_id'];
        $nombreSorteo = $sorteo['nombre_sorteo'];
    }

    //ESCRIBE LA FECHA DEL DIA
    $worksheet->SetCellValueByColumnAndRow(0, 2, date("d/m/Y"));

    //ESCRIBE EL ENCABEZADO
    $worksheet->SetCellValueByColumnAndRow(0, 5, "Numeros");
    $worksheet->SetCellValueByColumnAndRow(1, 4, "Total");

    //ESCRIBE LOS NUMEROS DE 0-99
    for($i = 6; $i < 106; $i++){
        $worksheet->SetCellValueByColumnAndRow(0, $i, $i-6);
    }

    $columna = 2;
    $totalesVendedor = array();//PARA IR GUARDANDO EL TOTAL DE CADA VENDEDOR PARA SER ESCRITOS LUEGO
    $totalesNumeros = array_fill(0, 100, 0);//PARA IR GUARDANDO EL TOTAL DE CADA NUMERO PARA SER ESCRITOS LUEGO
    /*****************ARMADO DEL EXCEL***********************/

    $fila = 5;//FILA EN DONDE SE COMIENZA A ESCRIBIR LOS NOMBRES DE LOS VENDDEDORES
    foreach($arrayVendedores as $vendedor){

        $worksheet->getColumnDimensionByColumn($columna)->setAutoSize(true);
        //PARA QUITAR LA PALABRA IMPORT DEL NOMBRE DEL VENDEDOR
        if (strpos($vendedor['nombre_usuario'],'IMPORT') === false){
            $worksheet->SetCellValueByColumnAndRow($columna, $fila,$vendedor['nombre_usuario']);
        }else{
            $worksheet->SetCellValueByColumnAndRow($columna, $fila, substr($vendedor['nombre_usuario'], 7));
        }

        for($i = 6; $i < 106; $i++){
            $worksheet->SetCellValueByColumnAndRow($columna, $i, 0);
        }//FIN FOR

        /*******************JUGADAS ENVIADAS POR VENDEDOR******************************/
        $sqlJugadas = "SELECT jugada1, jugada2, revision_monto, monto_terminal, monto_recarga
                       FROM COF_Revision_Log
                       WHERE cof_revision_id =
                          (SELECT id FROM COF_Revision WHERE sorteo_programacion_id = :sorteo_id
                                                        and usuario_vendedor_calle_id = :user_id)  AND cof_new_status_id = 3";

        $stmtJugadas = $pdoConn->prepare($sqlJugadas);
        $stmtJugadas->execute(array(':sorteo_id' => $idSorteo, ':user_id' => $vendedor['usuario_id']));
        $jugadas = $stmtJugadas->fetchAll(PDO::FETCH_ASSOC);
        /********************************************************************/

        $pagaPorcentaje = $vendedor['paga_jugada'];

        $totalesNumerosVendedor = array_fill(0, 100, 0);//PARA IR GUARDANDO EL TOTAL DE CADA NUMERO x VENDEDOR
        $total = 0;//Total acumulado por vendedor

        foreach($jugadas as $jugada){
            if($jugada['jugada1'] != -1){

                $compra = ($jugada['revision_monto'] + $jugada['monto_terminal']) / $pagaPorcentaje;
                $compra = round($compra, 0, PHP_ROUND_HALF_UP);//ELIMINA DECIMALES
                $compra = redondeo5($compra);
                if($vendedor['recargaLocura'] == 1){
                    $recargaLocura = $jugada['monto_recarga'] / $pagaPorcentaje;
                    $recargaLocura = round($recargaLocura, 0, PHP_ROUND_HALF_UP);//ELIMINA DECIMALES
                    $recargaLocura = redondeo5($recargaLocura);

                    $compra = $compra + $recargaLocura;
                }
               /* $redondear = substr($compra, sizeof($compra)-3);//CAPTURA LOS 2 ULTIMOS DIGITOS PARA APLICAR EL REDONDEO
                $redondear = 100 - $redondear;//PARA SABER SI SE RENDODEA 100
                if($redondear < 50){//SI LA DIFENCIA ES MENOS DE 50 SE REDONDEA AL 100 PROXIMO
                    $compra = $compra + $redondear;//
                }elseif($redondear > 50 && $redondear < 100){//SI LA DIFENCIA ES MAYOR A DE 50 Y MENOR A 99 SE REDONDEA AL 50 PROXIMO
                    $redondear = substr($compra, sizeof($compra)-3);//CAPTURA LOS 2 ULTIMOS DIGITOS PARA APLICAR EL REDONDEO
                    $redondear = 50 - $redondear;//PARA SABER LA DIFERECIA ENTRE LOS 2 ULTI DIGITOS HASTA 50
                    $compra = $compra + $redondear;
                }*/
                $pos = $jugada['jugada1'];
                $totalesNumeros[$pos] = $totalesNumeros[$pos] + $compra;
                $total = $total + $compra;
                $totalesNumerosVendedor[$pos] = $totalesNumerosVendedor[$pos] + $compra;



            }//FIN IF

            if($jugada['jugada2'] != -1){

                $compra = ($jugada['revision_monto'] + $jugada['monto_terminal']) / $pagaPorcentaje;
                $compra = round($compra, 0, PHP_ROUND_HALF_UP);//ELIMINA DECIMALES
                $compra = redondeo5($compra);

                if($vendedor['recargaLocura'] == 1){
                    $recargaLocura = $jugada['monto_recarga'] / $pagaPorcentaje;
                    $recargaLocura = round($recargaLocura, 0, PHP_ROUND_HALF_UP);//ELIMINA DECIMALES
                    $recargaLocura = redondeo5($recargaLocura);

                    $compra = $compra + $recargaLocura;
                }

                $pos = $jugada['jugada2'];
                $totalesNumeros[$pos] = $totalesNumeros[$pos] + $compra;
                $total = $total + $compra;
                $totalesNumerosVendedor[$pos] = $totalesNumerosVendedor[$pos] + $compra;


            }//FIN IF
        }

        array_push($totalesVendedor, $total);

        //ESCRIBE LOS EL TOTAL DE CADA NUMERO POR VENDEDOR
        for($i = 0; $i < sizeof($totalesNumerosVendedor); $i++){
            $worksheet->SetCellValueByColumnAndRow($columna, $i+6, $totalesNumerosVendedor[$i]);
        }//FIN FOR

    $columna++;//AUMENTA LA COLUMNA
    }//Fin foreach

    //ESCRIBE LA JUGADA DE LAS TABLETS
    $worksheet->SetCellValueByColumnAndRow($columna, $fila, "TABLETS");

    for($i = 6; $i < 106; $i++){
        $worksheet->SetCellValueByColumnAndRow($columna, $i, 0);
    }//FIN FOR

    /*******************NUMERO DE LA SUERTE DEL SORTEO******************************/
    //query for get sorteos of day
    $sqlSorteosDelDia = "SELECT SP.ID, SP.NumeroDeSuerte
     FROM SorteosProgramacion SP
     WHERE SP.ID = ?
";

    try
    {
        $stmtSorteosDelDia = $pdoConn->prepare($sqlSorteosDelDia);
        $stmtSorteosDelDia->execute(array($idSorteo));
        $sorteoDelDia = $stmtSorteosDelDia->fetch();
    } catch (PDOException $e)
    {
        $sorteoDelDia = array();
        echo $e->getMessage();
    }
    /********************************************************************/

    /*******************TOTAL VENDIDO EN TABLET******************************/
    //stmt to obtain total for sorteo and user
    $sqlTotalSorteo = "SELECT sp.ID, sd.NombreSorteo, SUM(sa.Cantidad) as total
                            FROM SorteosProgramacion sp
                              JOIN SorteosDefinicion sd on sd.ID = sp.IDSorteoDefinicion
                              JOIN SorteoApuesta_Validas sa on sa.IDSorteoProgramacion = sp.ID
                              WHERE sp.ID = ?

                            ";
    $stmtTotalSorteo = $pdoConn->prepare($sqlTotalSorteo);
    $stmtTotalSorteo->execute(array($idSorteo));
    $totalSorteo = $stmtTotalSorteo->fetch();

    $compra =  $totalSorteo['total']/ 75;
    $compra = round($compra, 0, PHP_ROUND_HALF_UP);//ELIMINA DECIMALES
    $compra = redondeo5($compra);

    $worksheet->SetCellValueByColumnAndRow($columna, $sorteoDelDia['NumeroDeSuerte']+6, $compra);
  //  $worksheet->SetCellValueByColumnAndRow($columna, 4, $compra);
    $totalTablet = $compra;
    $totalesNumeros[$sorteoDelDia['NumeroDeSuerte']] = $totalesNumeros[$sorteoDelDia['NumeroDeSuerte']] + $compra;

    /*SEGUNDA JUGADA TRES RIOS*/
    $sqlSegundaJugada = "SELECT CRL.jugada2 FROM COF_Revision_Log CRL
                         JOIN COF_Revision CR ON CR.id = CRL.cof_revision_id
                         WHERE CR.usuario_vendedor_calle_id = 228 AND CR.sorteo_programacion_id = ?
                         ORDER BY CRL.id DESC LIMIT 1";
    $stmtSegundaJugada = $pdoConn->prepare($sqlSegundaJugada);
    $stmtSegundaJugada->execute(array($idSorteo));
    $segundaJugada = $stmtSegundaJugada->fetch();

    $sqlTotalTresRios = "SELECT SUM(Cantidad) as 'total' FROM SorteoApuesta_Validas
                         WHERE IDUsuario = 181 AND IDSorteoProgramacion = ?";
    $stmtTotalTresRios = $pdoConn->prepare($sqlTotalTresRios);
    $stmtTotalTresRios->execute(array($idSorteo));
    $totalTresRios = $stmtTotalTresRios->fetch();

    $compra =  $totalTresRios['total']/ 75;
    $compra = round($compra, 0, PHP_ROUND_HALF_UP);//ELIMINA DECIMALES
    $compra = redondeo5($compra);
    $totalTablet = $totalTablet + $compra;

    $worksheet->SetCellValueByColumnAndRow($columna, $segundaJugada['jugada2']+6, $compra);
    $worksheet->SetCellValueByColumnAndRow($columna, 4, $totalTablet);
    $totalesNumeros[$segundaJugada['jugada2']] = $totalesNumeros[$segundaJugada['jugada2']] + $compra;

    array_push($totalesVendedor, $totalTablet);


    $totalGeneral = 0;
    //ESCRIBE LOS EL TOTAL  VENDEDOR
    for($i = 0; $i < sizeof($totalesVendedor); $i++){
        $worksheet->SetCellValueByColumnAndRow($i + 2, 4, $totalesVendedor[$i]);
        $totalGeneral = $totalGeneral + $totalesVendedor[$i];
    }//FIN FOR

    //ESCRIBE LOS EL TOTAL  NUMERO
    for($i = 0; $i < sizeof($totalesNumeros); $i++){
        $worksheet->SetCellValueByColumnAndRow(1, $i+6, $totalesNumeros[$i]);
    }//FIN FOR

    //ESCRIBE EL TOTAL GENERAL DE LA LISTA
    $worksheet->SetCellValueByColumnAndRow(0, 3, 'TOTAL LISTA: ');
    $worksheet->SetCellValueByColumnAndRow(1, 3,  $totalGeneral);


    /********************************************************/

    if($accion === 'Descargar'){
        $nombreArchivo = 'JUGADAS'.trim($nombreSorteo, " ").'.xls';//Nombre del excel
        $writer = new PHPExcel_Writer_Excel5($spreadsheet);
        $writer->save(IMPORTACION_DIR . '/' . $nombreArchivo);
        $writer->save('./Descargas/'. $nombreArchivo);

        return $nombreArchivo;
    }else{
        return  "Guardado";
    }



}//writeExcelJugadas


/*$arrayVendedores = arreglo con la lista de vendedores
 *$arraySorteos = arreglo con los sorteos del dia
 *$pdoConn = conexion a la BD
*/
//Metodo para crear el excel de la lista de numeros vendidos
function writeExcelNumeros($arrayVendedores, $arraySorteos, $pdoConn){

    //Prepara el excel para creado y escrito
    require_once 'phpExcel/PHPExcel.php';
    require_once 'phpExcel/PHPExcel/Writer/Excel5.php';
    //Centra el texto de los cuadros
    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );

    $spreadsheet = new PHPExcel();
    $spreadsheet->setActiveSheetIndex(0);

    $worksheet = $spreadsheet->getActiveSheet();
    $worksheet->getDefaultStyle()->applyFromArray($style);
    $worksheet->getColumnDimensionByColumn(0)->setAutoSize(true);
    $spreadsheet->getActiveSheet()->getStyle("A4:Z4")->getFont()->setBold(true);
    $spreadsheet->getActiveSheet()->getStyle("B1:B105")->getFont()->setBold(true);
    $spreadsheet->getActiveSheet()->getStyle("A1:A3")->getFont()->setBold(true);


    $idSorteo = 0;

    //ESCRIBE EL NOMBRE DEL SORTEO
    foreach($arraySorteos as $sorteo){

        $worksheet->SetCellValueByColumnAndRow(0, 1, $sorteo['nombre_sorteo']);
        $idSorteo = $sorteo['sorteo_prog_id'];
        $nombreSorteo = $sorteo['nombre_sorteo'];
    }

    //ESCRIBE LA FECHA DEL DIA
    $worksheet->SetCellValueByColumnAndRow(0, 2, date("d/m/Y"));

    //ESCRIBE EL ENCABEZADO
    $worksheet->SetCellValueByColumnAndRow(0, 5, "Numeros");
    $worksheet->SetCellValueByColumnAndRow(1, 4, "Total");

    //ESCRIBE LOS NUMEROS DE 0-99
    for($i = 6; $i < 106; $i++){
        $worksheet->SetCellValueByColumnAndRow(0, $i, $i-6);
    }

    $columna = 2;
    $totalesVendedor = array();//PARA IR GUARDANDO EL TOTAL DE CADA VENDEDOR PARA SER ESCRITOS LUEGO
    $totalesNumeros = array_fill(0, 100, 0);//PARA IR GUARDANDO EL TOTAL DE CADA NUMERO PARA SER ESCRITOS LUEGO

    foreach($arrayVendedores as $vendedor){
        $worksheet->getColumnDimensionByColumn($columna)->setAutoSize(true);
        if (strpos($vendedor['nombre_usuario'],'IMPORT') === false){
            $worksheet->SetCellValueByColumnAndRow($columna, 5,$vendedor['nombre_usuario']);
        }else{
            $worksheet->SetCellValueByColumnAndRow($columna, 5, substr($vendedor['nombre_usuario'], 7));
        }


        for($i = 6; $i < 106; $i++){
            $worksheet->SetCellValueByColumnAndRow($columna, $i, 0);
        }//FIN FOR

        /*******************NUMEROS VENDIDOS POR VENDEDOR******************************/
        $sqlNumeros = "SELECT Cantidad, Numero
                           FROM sorteoapuesta_validas_puestos
                           WHERE IDSorteoProgramacion = :sorteo_id and IDUsuario = :user_id";

        $stmtNumeros = $pdoConn->prepare($sqlNumeros);
        $stmtNumeros->execute(array(':sorteo_id' => $idSorteo, ':user_id' => $vendedor['usuario_id']));
        $Numeros = $stmtNumeros->fetchAll(PDO::FETCH_ASSOC);
        /********************************************************************/

        $totalesNumerosVendedor = array_fill(0, 100, 0);//PARA IR GUARDANDO EL TOTAL DE CADA NUMERO x VENDEDOR
        $total = 0;//Total acumulado por vendedor
        foreach($Numeros as $numero){

            $pos = $numero['Numero'];
            $totalesNumeros[$pos] = $totalesNumeros[$pos] + $numero['Cantidad'];
            $total = $total + $numero['Cantidad'];
            $totalesNumerosVendedor[$pos] = $totalesNumerosVendedor[$pos] + $numero['Cantidad'];

        }//FIN FOREACH

        array_push($totalesVendedor, $total);

        //ESCRIBE EL TOTAL DE CADA NUMERO POR VENDEDOR
        for($i = 0; $i < sizeof($totalesNumerosVendedor); $i++){
            $worksheet->SetCellValueByColumnAndRow($columna, $i+6, $totalesNumerosVendedor[$i]);
        }//FIN FOR


        $columna++;

    }//FIN FOREACH

    $totalGeneral = 0;
    //ESCRIBE LOS EL TOTAL  VENDEDOR
    for($i = 0; $i < sizeof($totalesVendedor); $i++){
        $worksheet->SetCellValueByColumnAndRow($i + 2, 4, $totalesVendedor[$i]);
        $totalGeneral = $totalGeneral + $totalesVendedor[$i];
    }//FIN FOR

    //ESCRIBE LOS EL TOTAL  NUMERO
    for($i = 0; $i < sizeof($totalesNumeros); $i++){
        $worksheet->SetCellValueByColumnAndRow(1, $i+6, $totalesNumeros[$i]);
    }//FIN FOR

    //ESCRIBE EL TOTAL GENERAL DE LA LISTA
    $worksheet->SetCellValueByColumnAndRow(0, 3, 'TOTAL LISTA: ');
    $worksheet->SetCellValueByColumnAndRow(1, 3,  $totalGeneral);




    $nombreArchivo = 'NUMEROS'.trim($nombreSorteo, " ").'.xls';//Nombre del excel
    $writer = new PHPExcel_Writer_Excel5($spreadsheet);
    $writer->save(IMPORTACION_DIR . '/' . $nombreArchivo);
    $writer->save('./Descargas/'. $nombreArchivo);

    return $nombreArchivo;
}//FIN writeExcelNumeros




function validarDevolucion($filename, $userID, $pdoConn, $sorteoID, $devolucion, $accion){

    try{

        //Prepara el excel para ser leido
        require_once 'phpExcel/PHPExcel.php';
        $reader      = new PHPExcel_Reader_Excel5();
        $Path =      IMPORTACION_DIR . '/' . $filename  . '.xls';//Ruta del excel
        $obj         = $reader->load($Path);
        $activeSheet = $obj->getActiveSheet();

        $spreadsheet = new PHPExcel();
        $spreadsheet->setActiveSheetIndex(0);

        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->getCell("A1")->setValue("Numero");
        $worksheet->getCell("B1")->setValue("Monto");
        $worksheet->getCell("D9")->setValue("Codigo");


        //Captura los restringidos del dia
        $sqlNumRestringidos = "SELECT SR.Numero as numero_restringido
                           FROM SorteosNumerosRestringidos SR
                           WHERE SR.IDSorteoProgramacion = $sorteoID
";

        try
        {
            $stmtNumRestringidos = $pdoConn->prepare($sqlNumRestringidos);
            $stmtNumRestringidos->execute();
            $numRestringidos = $stmtNumRestringidos->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e)
        {
            $numRestringidos = array();
            echo $e->getMessage();
        }

        //getOldCalculatedValue

        //Captura TOTAL de la lista
        if($activeSheet->getCell("C1")->getOldCalculatedValue() == NULL){
            $totalLista =      $activeSheet->getCell("C1")->getValue();
        }else{
            $totalLista =      $activeSheet->getCell("C1")->getOldCalculatedValue();
        }

        $sqlCheckDevo = "SELECT * FROM SorteoDevolucion WHERE sorteo_prog_id = ? AND usuario_id = ?";
        $stmtCheckDevo = $pdoConn->prepare($sqlCheckDevo);
        $stmtCheckDevo->execute(array($sorteoID, $userID));

        if($stmtCheckDevo->rowcount() == 0){

            $sqlInsertDevo = "INSERT INTO SorteoDevolucion(sorteo_prog_id, usuario_id, monto_lista, fechayhora)
                              VALUES(?,?,?, NOW())";
            $stmtInsertDevo = $pdoConn->prepare($sqlInsertDevo);
            $stmtInsertDevo->execute(array($sorteoID, $userID, $totalLista));
            $devoID = $pdoConn->lastInsertId();

        }else{

            $devolucionSQL = $stmtCheckDevo->fetch();
            $devoID = $devolucionSQL['id'];

            $sqlUpdateDevo = "UPDATE SorteoDevolucion
                              SET monto_lista = ?
                              WHERE id = $$devoID";
            $stmtUpdateDevo = $pdoConn->prepare($sqlUpdateDevo);
            $stmtUpdateDevo->execute(array($totalLista));


        }//FIN if/else

        /*CALCULA LOS MONTOS TOPE*/
        $montoARebajar = 0;
        $montoTope = round($totalLista * ($devolucion/100));
        $montoTope = redondeo12($montoTope);
        $huboDevo = false;

        $respuesta = "</br>"."Lista Bruta = " . system_number_money_format($totalLista) . "</br>";
       // $respuesta .= "Monto Tope = " . system_number_money_format($montoTope). "</br>";
      //  $respuesta .= "Se devuelve cada numero el siguiente monto" . "</br>";

       /* if($activeSheet->getCell("E9")->getOldCalculatedValue() == NULL){
            $codigo =      $activeSheet->getCell("E9")->getValue();
        }else{
            $codigo =      $activeSheet->getCell("E9")->getOldCalculatedValue();
        }
        $worksheet->getCell("E9")->setValue($codigo);*/

     /*   for($i = 1; $i <=100; $i++){
            $posicion = $i + 1;
            if($activeSheet->getCell("B" . $posicion)->getOldCalculatedValue() == NULL){
                $monto =      $activeSheet->getCell("B" . $posicion)->getValue();
            }else{
                $monto =      $activeSheet->getCell("B" . $posicion)->getOldCalculatedValue();
            }

            if($monto == 0){
                $worksheet->getCell("A" .$posicion)->setValue($i);
                $worksheet->getCell("B" .$posicion)->setValue(0);
            }else{
                $worksheet->getCell("A" .$posicion)->setValue($i);
                $worksheet->getCell("B" .$posicion)->setValue($monto);
            }


        }*/

        $sqlDeleteDetalleDevo = "DELETE FROM SorteoDevolucion_Detalle WHERE sorteodevolucion_id = ?";
        $stmtDeleteDetalleDevo = $pdoConn->prepare($sqlDeleteDetalleDevo);
        $stmtDeleteDetalleDevo->execute(array($devoID));

        $sqlInsertDetalle = "INSERT INTO SorteoDevolucion_Detalle(sorteodevolucion_id, numero, monto_devuelto, fechayhora)
                             VALUES(?,?,?, NOW())";
        $stmtInsertDetalle = $pdoConn->prepare($sqlInsertDetalle);



        foreach($numRestringidos as $restringido){
            $numero = $restringido['numero_restringido'];
            $posicion = $numero + 1;
            if($activeSheet->getCell("B" . $posicion)->getOldCalculatedValue() == NULL){
                $monto =      $activeSheet->getCell("B" . $posicion)->getValue();
            }else{
                $monto =      $activeSheet->getCell("B" . $posicion)->getOldCalculatedValue();
            }


            if($monto > $montoTope){
                $huboDevo = true;
                $aRebajar = $monto - $montoTope;
             //   $worksheet->getCell("B" . $posicion)->setValue($montoTope);
                $montoARebajar = $montoARebajar + $aRebajar;
                $respuesta = $respuesta . $numero . " = " . system_number_money_format($aRebajar) . "</br>";
                $stmtInsertDetalle->execute(array($devoID, $numero, $aRebajar));

            }


        }//FIN foreach

        $respuesta = $respuesta . "TOTAL Excesos = " . system_number_money_format($montoARebajar) . "</br>";
        $respuesta = $respuesta . "PERMITIDO= " . system_number_money_format($montoTope) . "</br>";
        if($huboDevo == true ){
            $respuesta = $respuesta . "</br></br>LISTA NETA RECIBIDA = " . system_number_money_format($totalLista -  $montoARebajar) . "</br>";
            $respuesta = $respuesta . ";" . ($totalLista - $montoARebajar);
        }else{
            $respuesta = $respuesta . "</br></br>LISTA NETA RECIBIDA = " . system_number_money_format($totalLista) . "</br>";
            $respuesta = $respuesta . ";" . $totalLista;

        }

        $respuesta = $respuesta . ";" . $totalLista;

        $worksheet->getCell('C1')->setValue($totalLista -  $montoARebajar);

        if($accion == 'Guardar'){
            try{
                $writer = new PHPExcel_Writer_Excel5($spreadsheet);
                unlink(IMPORTACION_DIR . '/' . $filename .'.xls');
                $writer->save(IMPORTACION_DIR . '/' . $filename .'.xls');
                $writer->save('./Descargas/'. $filename .'.xls');

            }catch (Exception $e){
                echo("No guarda;0");
            }

        }

        if($accion != 'Guardar'){
           echo($respuesta);
        }



    }catch (Exception $e){
        echo("No han subido el excel;0");
    }



}//FIN validarDevolucion


function crearExcelSpectra($filename, $arrayNumeros, $arrayMontos){

    //Prepara el excel para creado y escrito
    require_once 'phpExcel/PHPExcel.php';
    require_once 'phpExcel/PHPExcel/Writer/Excel5.php';
    //Centra el texto de los cuadros
    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );

    $spreadsheet = new PHPExcel();
    $spreadsheet->setActiveSheetIndex(0);

    $worksheet = $spreadsheet->getActiveSheet();
    $worksheet->getDefaultStyle()->applyFromArray($style);
    $worksheet->getColumnDimensionByColumn(0)->setAutoSize(true);
    $spreadsheet->getActiveSheet()->getStyle("A4:Z4")->getFont()->setBold(true);
    $spreadsheet->getActiveSheet()->getStyle("B1:B105")->getFont()->setBold(true);
    $spreadsheet->getActiveSheet()->getStyle("A1:A3")->getFont()->setBold(true);

    //ESCRIBE EL ENCABEZADO
    $worksheet->SetCellValueByColumnAndRow(0, 1, "Numero");
    $worksheet->SetCellValueByColumnAndRow(1, 1, "Monto");


    //ESCRIBE LOS NUMEROS DE 0-99 Y MONTOS
    for($i = 2; $i < 102; $i++){
        $worksheet->SetCellValueByColumnAndRow(0, $i, $i-1);
        $worksheet->SetCellValueByColumnAndRow(1, $i, 0);
    }//FIN for

    $total = 0;
    for($m = 0; $m < sizeof($arrayNumeros); $m++){
        if($arrayNumeros[$m] == 0){
            $worksheet->SetCellValueByColumnAndRow(1, 101, $arrayMontos[$m]);
        }else{
            $worksheet->SetCellValueByColumnAndRow(1, $arrayNumeros[$m]+1, $arrayMontos[$m]);
        }

        $total = $total + $arrayMontos[$m];

    }//Fin for

    $worksheet->SetCellValueByColumnAndRow(2, 1,  $total);

    $nombreArchivo = trim($filename, " ").'.xls';//Nombre del excel
    $writer = new PHPExcel_Writer_Excel5($spreadsheet);
    unlink(IMPORTACION_DIR . '/' . $nombreArchivo .'.xls');
    $writer->save(IMPORTACION_DIR . '/' . $nombreArchivo);
    $writer->save('./Descargas/'. $nombreArchivo);

    return $total;



}// FIN crearExcelSpectra

function crearExcelLista($filename, $arrayMontos, $pdoConn, $IDUsuario, $sorteoID){

    //Prepara el excel para creado y escrito
    require_once 'phpExcel/PHPExcel.php';
    require_once 'phpExcel/PHPExcel/Writer/Excel5.php';
    //Centra el texto de los cuadros
    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );

    $spreadsheet = new PHPExcel();
    $spreadsheet->setActiveSheetIndex(0);

    $worksheet = $spreadsheet->getActiveSheet();
    $worksheet->getDefaultStyle()->applyFromArray($style);
    $worksheet->getColumnDimensionByColumn(0)->setAutoSize(true);

    //ESCRIBE LA FECHA DEL DIA
    //$worksheet->SetCellValueByColumnAndRow(0, 2, date("d/m/Y"));

    //ESCRIBE EL ENCABEZADO
    $worksheet->SetCellValueByColumnAndRow(0, 1, "Numeros");
    $worksheet->SetCellValueByColumnAndRow(1, 1, "Total");

    //ESCRIBE LOS NUMEROS DE 0-99
    for($i = 1; $i < 101; $i++){
        $worksheet->SetCellValueByColumnAndRow(0, $i+1, $i);
    }

    $columna = 2;
    $totalesVendedor = array();//PARA IR GUARDANDO EL TOTAL DE CADA VENDEDOR PARA SER ESCRITOS LUEGO
    $totalesNumeros = array_fill(0, 100, 0);//PARA IR GUARDANDO EL TOTAL DE CADA NUMERO PARA SER ESCRITOS LUEGO

    //Select para comprobar si el ticket y los numeros se ha ingresado en la tabla sorteoapuesta_puestos
    $sqlComprobarTicket = "SELECT * FROM sorteoapuesta_puestos
                           WHERE IDUsuario = :id_user and IDSorteoProgramacion = :id_sorteo";
    $stmtComprobarTicket = $pdoConn->prepare($sqlComprobarTicket);
    $stmtComprobarTicket->execute(array(':id_user' => $IDUsuario, ':id_sorteo' => $sorteoID));
    $Tiquetes = $stmtComprobarTicket->fetchAll(PDO::FETCH_ASSOC);

    $total = 0;
    foreach($Tiquetes as $tiquete){
        if($tiquete['Numero'] == 0){
            $worksheet->SetCellValueByColumnAndRow(1, 101, $tiquete['Cantidad']);
        }else{
            $worksheet->SetCellValueByColumnAndRow(1, $tiquete['Numero']+1, $tiquete['Cantidad']);
        }

        $total = $total + $tiquete['Cantidad'];

    }//Fin for

    $worksheet->SetCellValueByColumnAndRow(2, 1,  $total);

    $nombreArchivo = trim($filename, " ").'.xls';//Nombre del excel
    $writer = new PHPExcel_Writer_Excel5($spreadsheet);
    $writer->save(IMPORTACION_DIR . '/' . $nombreArchivo);
   // $writer->save('./Descargas/'. $nombreArchivo);

   // return $total;



}// FIN crearExcelSpectra

function crearExcelPapelito($IDUsuario, $sorteoID, $pdoConn){

    //Prepara el excel para creado y escrito
    require_once 'phpExcel/PHPExcel.php';
    require_once 'phpExcel/PHPExcel/Writer/Excel5.php';
    //Centra el texto de los cuadros
    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );

    $spreadsheet = new PHPExcel();
    $spreadsheet->setActiveSheetIndex(0);

    $worksheet = $spreadsheet->getActiveSheet();
    $worksheet->getDefaultStyle()->applyFromArray($style);
    $worksheet->getColumnDimensionByColumn(0)->setAutoSize(true);

    //ESCRIBE LA FECHA DEL DIA
    //$worksheet->SetCellValueByColumnAndRow(0, 2, date("d/m/Y"));

    //ESCRIBE EL ENCABEZADO
    $worksheet->SetCellValueByColumnAndRow(0, 1, "Numeros");
    $worksheet->SetCellValueByColumnAndRow(1, 1, "Total");

    //ESCRIBE LOS NUMEROS DE 0-99
    for($i = 1; $i < 101; $i++){
        $worksheet->SetCellValueByColumnAndRow(0, $i+1, $i);
    }


    //Select para comprobar si el ticket y los numeros se ha ingresado en la tabla sorteoapuesta_puestos
    $sqlNumeroTicket = "SELECT SUM(Cantidad) as 'total' FROM sorteoapuesta_validas_puestos
                        WHERE IDUsuario = :id_user and IDSorteoProgramacion = :id_sorteo and Numero = :numero";
    $stmtNumeroTicket = $pdoConn->prepare($sqlNumeroTicket);
    /*
    $stmtComprobarTicket->execute(array(':id_user' => $IDUsuario, ':id_sorteo' => $sorteoID));
    $Tiquetes = $stmtComprobarTicket->fetchAll(PDO::FETCH_ASSOC);*/

    $total = 0;
    for($i = 0; $i < 100; $i++){
        $stmtNumeroTicket->execute(array(':id_user' => $IDUsuario, ':id_sorteo' => $sorteoID, 'numero' => $i));
        $numero = $stmtNumeroTicket->fetch();
        if($i == 0){
            $worksheet->SetCellValueByColumnAndRow(1, 101, $numero['total']);
        }else{
            $worksheet->SetCellValueByColumnAndRow(1, $i+1, $numero['total']);
        }

        $total = $total +  $numero['total'];

    }//Fin for

    $sqlGetFilename = "SELECT NombreUsuario FROM Usuarios WHERE ID = ?";
    $stmtGetFilename = $pdoConn->prepare($sqlGetFilename);
    $stmtGetFilename->execute(array($IDUsuario));
    $Nombre = $stmtGetFilename->fetch();


    $worksheet->SetCellValueByColumnAndRow(2, 1,  $total);

    if (strpos($Nombre['NombreUsuario'],'IMPORT') === false){
        $filename = $Nombre['NombreUsuario'];
    }else{
        $filename = substr($Nombre['NombreUsuario'], 7);
    }

    $nombreArchivo = trim($filename, " ").'.xls';//Nombre del excel
    $writer = new PHPExcel_Writer_Excel5($spreadsheet);
    $writer->save(IMPORTACION_DIR . '/' . $nombreArchivo);
    // $writer->save('./Descargas/'. $nombreArchivo);

    // return $total;



}// FIN crearExcelSpectra


function excelPapelitosBruto($pdoConn, $arrayVendedores, $arraySorteos){

    //Prepara el excel para creado y escrito
    require_once 'phpExcel/PHPExcel.php';
    require_once 'phpExcel/PHPExcel/Writer/Excel5.php';
    //Centra el texto de los cuadros
    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );

    $spreadsheet = new PHPExcel();
    $spreadsheet->setActiveSheetIndex(0);

    $worksheet = $spreadsheet->getActiveSheet();
    $worksheet->getDefaultStyle()->applyFromArray($style);
    $worksheet->getColumnDimensionByColumn(0)->setAutoSize(true);
    $spreadsheet->getActiveSheet()->getStyle("A4:Z4")->getFont()->setBold(true);
    $spreadsheet->getActiveSheet()->getStyle("B1:B105")->getFont()->setBold(true);
    $spreadsheet->getActiveSheet()->getStyle("A1:A3")->getFont()->setBold(true);


    $idSorteo = 0;

    //ESCRIBE EL NOMBRE DEL SORTEO
    foreach($arraySorteos as $sorteo){

        $worksheet->SetCellValueByColumnAndRow(0, 1, $sorteo['nombre_sorteo']);
        $idSorteo = $sorteo['sorteo_prog_id'];
        $nombreSorteo = $sorteo['nombre_sorteo'];
    }

    //ESCRIBE LA FECHA DEL DIA
    $worksheet->SetCellValueByColumnAndRow(0, 2, date("d/m/Y H:i:s"));

    //ESCRIBE EL ENCABEZADO
    $worksheet->SetCellValueByColumnAndRow(0, 5, "Numeros");
    $worksheet->SetCellValueByColumnAndRow(1, 4, "Total");

    //ESCRIBE LOS NUMEROS DE 0-99
    for($i = 6; $i < 106; $i++){
        $worksheet->SetCellValueByColumnAndRow(0, $i, $i-6);
    }

    $columna = 2;
    $totalesVendedor = array();//PARA IR GUARDANDO EL TOTAL DE CADA VENDEDOR PARA SER ESCRITOS LUEGO
    $totalesNumeros = array_fill(0, 100, 0);//PARA IR GUARDANDO EL TOTAL DE CADA NUMERO PARA SER ESCRITOS LUEGO

    foreach($arrayVendedores as $vendedor){
        $worksheet->getColumnDimensionByColumn($columna)->setAutoSize(true);
        if (strpos($vendedor['NombreUsuario'],'IMPORT') === false){
            $worksheet->SetCellValueByColumnAndRow($columna, 5,$vendedor['NombreUsuario']);
        }else{
            $worksheet->SetCellValueByColumnAndRow($columna, 5, substr($vendedor['NombreUsuario'], 7));
        }


        for($i = 6; $i < 106; $i++){
            $worksheet->SetCellValueByColumnAndRow($columna, $i, 0);
        }//FIN FOR

        /*******************NUMEROS VENDIDOS POR VENDEDOR******************************/
        $sqlNumeros = "SELECT Cantidad, Numero
                           FROM sorteoapuesta_puestos
                           WHERE IDSorteoProgramacion = :sorteo_id and IDUsuario = :user_id";

        $stmtNumeros = $pdoConn->prepare($sqlNumeros);
        $stmtNumeros->execute(array(':sorteo_id' => $idSorteo, ':user_id' => $vendedor['ID']));
        $Numeros = $stmtNumeros->fetchAll(PDO::FETCH_ASSOC);
        /********************************************************************/

        $totalesNumerosVendedor = array_fill(0, 100, 0);//PARA IR GUARDANDO EL TOTAL DE CADA NUMERO x VENDEDOR
        $total = 0;//Total acumulado por vendedor
        foreach($Numeros as $numero){

            $pos = $numero['Numero'];
            $totalesNumeros[$pos] = $totalesNumeros[$pos] + $numero['Cantidad'];
            $total = $total + $numero['Cantidad'];
            $totalesNumerosVendedor[$pos] = $totalesNumerosVendedor[$pos] + $numero['Cantidad'];

        }//FIN FOREACH

        array_push($totalesVendedor, $total);

        //ESCRIBE EL TOTAL DE CADA NUMERO POR VENDEDOR
        for($i = 0; $i < sizeof($totalesNumerosVendedor); $i++){
            $worksheet->SetCellValueByColumnAndRow($columna, $i+6, $totalesNumerosVendedor[$i]);
        }//FIN FOR


        $columna++;

    }//FIN FOREACH

    $totalGeneral = 0;
    //ESCRIBE LOS EL TOTAL  VENDEDOR
    for($i = 0; $i < sizeof($totalesVendedor); $i++){
        $worksheet->SetCellValueByColumnAndRow($i + 2, 4, $totalesVendedor[$i]);
        $totalGeneral = $totalGeneral + $totalesVendedor[$i];
    }//FIN FOR

    //ESCRIBE LOS EL TOTAL  NUMERO
    for($i = 0; $i < sizeof($totalesNumeros); $i++){
        $worksheet->SetCellValueByColumnAndRow(1, $i+6, $totalesNumeros[$i]);
    }//FIN FOR

    //ESCRIBE EL TOTAL GENERAL DE LA LISTA
    $worksheet->SetCellValueByColumnAndRow(0, 3, 'TOTAL LISTA: ');
    $worksheet->SetCellValueByColumnAndRow(1, 3,  $totalGeneral);




    $nombreArchivo = 'LISTABRUTAPELUQUERO'.trim($nombreSorteo, " ").'.xls';//Nombre del excel
    $writer = new PHPExcel_Writer_Excel5($spreadsheet);
    $writer->save(IMPORTACION_DIR . '/' . $nombreArchivo);
    $writer->save('./Descargas/'. $nombreArchivo);

    return $nombreArchivo;

   /* $sqlGetRevision = "SELECT RS.id, S.name, S.display_name, RS.monto
                       FROM COF_RevisionStatus_View RS
                       JOIN COF_Status S ON RS.status_id = S.id
                       WHERE RS.sorteo_programacion_id = ? AND RS.usuario_vendedor_calle_id = ?";

    $stmtGetRevision = $pdoConn->prepare($sqlGetRevision);

    $sqlGetSorteoTotal = "SELECT Numero, Cantidad
                          FROM sorteoapuesta_puestos
                          WHERE IDSorteoProgramacion = ? AND IDUsuario = ?";
    $stmtGetSorteoTotal = $pdoConn->prepare($sqlGetSorteoTotal);

    $columna = 2;
    foreach($usuarios as $usuario){



    }//Fin foreach*/





}//Fin excelPapelitosBruto

function excelPapelitosNeto($pdoConn, $arrayVendedores, $arraySorteos){

    //Prepara el excel para creado y escrito
    require_once 'phpExcel/PHPExcel.php';
    require_once 'phpExcel/PHPExcel/Writer/Excel5.php';
    //Centra el texto de los cuadros
    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );

    $spreadsheet = new PHPExcel();
    $spreadsheet->setActiveSheetIndex(0);

    $worksheet = $spreadsheet->getActiveSheet();
    $worksheet->getDefaultStyle()->applyFromArray($style);
    $worksheet->getColumnDimensionByColumn(0)->setAutoSize(true);
    $spreadsheet->getActiveSheet()->getStyle("A4:Z4")->getFont()->setBold(true);
    $spreadsheet->getActiveSheet()->getStyle("B1:B105")->getFont()->setBold(true);
    $spreadsheet->getActiveSheet()->getStyle("A1:A3")->getFont()->setBold(true);


    $idSorteo = 0;

    //ESCRIBE EL NOMBRE DEL SORTEO
    foreach($arraySorteos as $sorteo){

        $worksheet->SetCellValueByColumnAndRow(0, 1, $sorteo['nombre_sorteo']);
        $idSorteo = $sorteo['sorteo_prog_id'];
        $nombreSorteo = $sorteo['nombre_sorteo'];
    }

    //ESCRIBE LA FECHA DEL DIA
    $worksheet->SetCellValueByColumnAndRow(0, 2, date("d/m/Y H:i:s"));

    //ESCRIBE EL ENCABEZADO
    $worksheet->SetCellValueByColumnAndRow(0, 5, "Numeros");
    $worksheet->SetCellValueByColumnAndRow(1, 4, "Total");

    //ESCRIBE LOS NUMEROS DE 0-99
    for($i = 6; $i < 106; $i++){
        $worksheet->SetCellValueByColumnAndRow(0, $i, $i-6);
    }

    $columna = 2;
    $totalesVendedor = array();//PARA IR GUARDANDO EL TOTAL DE CADA VENDEDOR PARA SER ESCRITOS LUEGO
    $totalesNumeros = array_fill(0, 100, 0);//PARA IR GUARDANDO EL TOTAL DE CADA NUMERO PARA SER ESCRITOS LUEGO

    /*************************************************/
    /*GET EXCESOS*/
    $sqlListaEnviada = "SELECT * FROM SorteoDevolucion
                    WHERE sorteo_prog_id = ? AND usuario_id = ?";
    $stmtListaEnviada = $pdoConn->prepare($sqlListaEnviada);

    $sqlNumerosExcesos = "SELECT numero, monto_devuelto FROM SorteoDevolucion_Detalle
                          WHERE sorteodevolucion_id = ?";
    $stmtNumerosExcesos = $pdoConn->prepare($sqlNumerosExcesos);
    /*************************************************/

    foreach($arrayVendedores as $vendedor){
        $worksheet->getColumnDimensionByColumn($columna)->setAutoSize(true);
        if (strpos($vendedor['NombreUsuario'],'IMPORT') === false){
            $worksheet->SetCellValueByColumnAndRow($columna, 5,$vendedor['NombreUsuario']);
        }else{
            $worksheet->SetCellValueByColumnAndRow($columna, 5, substr($vendedor['NombreUsuario'], 7));
        }


        for($i = 6; $i < 106; $i++){
            $worksheet->SetCellValueByColumnAndRow($columna, $i, 0);
        }//FIN FOR

        /*******************NUMEROS VENDIDOS POR VENDEDOR******************************/
        $sqlNumeros = "SELECT Cantidad, Numero
                           FROM sorteoapuesta_puestos
                           WHERE IDSorteoProgramacion = :sorteo_id and IDUsuario = :user_id";

        $stmtNumeros = $pdoConn->prepare($sqlNumeros);
        $stmtNumeros->execute(array(':sorteo_id' => $idSorteo, ':user_id' => $vendedor['ID']));
        $Numeros = $stmtNumeros->fetchAll(PDO::FETCH_ASSOC);
        /********************************************************************/

        /*EXCESOS*/
        $stmtListaEnviada->execute(array($idSorteo, $vendedor['ID']));
        $listaEnviada = $stmtListaEnviada->fetch();
        $devoID = $listaEnviada['id'];

        $stmtNumerosExcesos->execute(array($devoID));
        $Excesos = $stmtNumerosExcesos->fetchAll(PDO::FETCH_ASSOC);

        $totalesExcesosVendedor = array_fill(0, 100, 0);//PARA IR GUARDANDO LOS EXCESOS DE CADA VENDEDOR
        foreach($Excesos as $exceso){
            $totalesExcesosVendedor[$exceso['numero']] = $exceso['monto_devuelto'];
        }

        /********/



        $totalesNumerosVendedor = array_fill(0, 100, 0);//PARA IR GUARDANDO EL TOTAL DE CADA NUMERO x VENDEDOR
        $total = 0;//Total acumulado por vendedor
        foreach($Numeros as $numero){

            $pos = $numero['Numero'];
            $numNeto = $numero['Cantidad'] - $totalesExcesosVendedor[$pos];
            $totalesNumeros[$pos] = $totalesNumeros[$pos] + $numNeto;
            $total = $total + $numNeto;
            $totalesNumerosVendedor[$pos] = $totalesNumerosVendedor[$pos] + $numNeto;

        }//FIN FOREACH

        array_push($totalesVendedor, $total);

        //ESCRIBE EL TOTAL DE CADA NUMERO POR VENDEDOR
        for($i = 0; $i < sizeof($totalesNumerosVendedor); $i++){
            $worksheet->SetCellValueByColumnAndRow($columna, $i+6, $totalesNumerosVendedor[$i]);
        }//FIN FOR


        $columna++;

    }//FIN FOREACH

    $totalGeneral = 0;
    //ESCRIBE LOS EL TOTAL  VENDEDOR
    for($i = 0; $i < sizeof($totalesVendedor); $i++){
        $worksheet->SetCellValueByColumnAndRow($i + 2, 4, $totalesVendedor[$i]);
        $totalGeneral = $totalGeneral + $totalesVendedor[$i];
    }//FIN FOR

    //ESCRIBE LOS EL TOTAL  NUMERO
    for($i = 0; $i < sizeof($totalesNumeros); $i++){
        $worksheet->SetCellValueByColumnAndRow(1, $i+6, $totalesNumeros[$i]);
    }//FIN FOR

    //ESCRIBE EL TOTAL GENERAL DE LA LISTA
    $worksheet->SetCellValueByColumnAndRow(0, 3, 'TOTAL LISTA: ');
    $worksheet->SetCellValueByColumnAndRow(1, 3,  $totalGeneral);




    $nombreArchivo = 'LISTANETAPELUQUERO'.trim($nombreSorteo, " ").'.xls';//Nombre del excel
    $writer = new PHPExcel_Writer_Excel5($spreadsheet);
    $writer->save(IMPORTACION_DIR . '/' . $nombreArchivo);
    $writer->save('./Descargas/'. $nombreArchivo);

    return $nombreArchivo;


}//Fin excelPapelitosNeto


//Redeondeo al 50
function redondeo($numeroAredondear){
    $numero = substr($numeroAredondear, sizeof($numeroAredondear)-3);
    $redondear = 100 - $numero;
    if($redondear < 50){
            return $numeroAredondear + $redondear;

    }else if($redondear > 50 && $redondear < 100){
           $redondear = 50 - $numero;
            return $numeroAredondear + $redondear;
    }else{
        return $numeroAredondear;
    }

}//Fin redondeo

//Redondeo al 25
function redondeo25($numeroAredondear){
    $numero = substr($numeroAredondear, sizeof($numeroAredondear)-3);
    $redondear = 100 - $numero;
    if($redondear < 50 && $redondear > 0){
        if($redondear <= 25){
            return $numeroAredondear + $redondear;
        }else{
            $redondear = 50 - $redondear;
            return $numeroAredondear - $redondear;
        }

    }else if($redondear > 50 && $redondear < 100){
        $redondear = 50 - $numero;
        if($redondear <= 25){
            return $numeroAredondear + $redondear;
        }else{
            $redondear = 50 - $redondear;
            return $numeroAredondear - $redondear;
        }
    }else{
        return $numeroAredondear;
    }

}//Fin redondeo25


//Redondeo al 5
function redondeo5($numeroAredondear){
    $numero = substr($numeroAredondear, sizeof($numeroAredondear)-2);
    $redondear = 10 - $numero;
    if($redondear < 5 && $redondear > 0){
        if($redondear <= 2.5){
            return $numeroAredondear + $redondear;
        }else{
            $redondear = 5 - $redondear;
            return $numeroAredondear - $redondear;
        }

    }else if($redondear > 5 && $redondear < 10){
        $redondear = 5 - $numero;
        if($redondear <= 2.5){
            return $numeroAredondear + $redondear;
        }else{
            $redondear = 5 - $redondear;
            return $numeroAredondear - $redondear;
        }
    }else{
        return $numeroAredondear;
    }

}//Fin redondeo5

//Redondeo al 0,25,50,75
function redondeo12($numeroAredondear){
    $numero = substr($numeroAredondear, sizeof($numeroAredondear)-3);
    $redondear = 100 - $numero;
    if($redondear < 50 && $redondear > 0){
        if($redondear <= 25){
            $redondear = 25 - $redondear;
            //Entre 88-99
            if($redondear <= 12){
                return $numeroAredondear - $redondear;
            }else{//Entre 75-87
                $redondear = 25 - $redondear;
                return $numeroAredondear + $redondear;
            }
        }else{
            $redondear = $redondear - 25;
            //Entre 63-74
            if($redondear <= 12){
                return $numeroAredondear + $redondear;
            }else{//Entre 51-62
                $redondear = 25 - $redondear;
                return $numeroAredondear - $redondear;
            }
        }

    }else if($redondear > 50 && $redondear < 100){
        $redondear = 50 - $numero;
        if($redondear <= 25){
            $redondear = 25 - $redondear;
                //Entre 38-49
                if($redondear <= 12){
                    return $numeroAredondear - $redondear;
                }else{//Entre 26-37
                    $redondear = 25 - $redondear;
                    return $numeroAredondear + $redondear;
                }
        }else{
            $redondear = $redondear - 25;
            //Entre 13-24
            if($redondear <= 12){
                return $numeroAredondear + $redondear;
            }else{//Entre 1-12
                $redondear = 25 - $redondear;
                return $numeroAredondear - $redondear;
            }
        }
    }else{
        return $numeroAredondear;
    }

}//Fin redondeo12

?>



