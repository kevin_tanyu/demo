<?php


/**
 * Sql for no scored events
 */
function getSqlNoScoredEvents()
{
    $sql = "SELECT e.id, sd.NombreSorteo, sp.FechayHora
          , IF( IFNULL(er.id,-1) = -1, 0, 1 )
            as has_winner_number
            FROM PAR_Event e
                JOIN PAR_Event_Part ep ON ep.par_event_id = e.id
                LEFT JOIN PAR_Event_Result er ON er.par_event_part_id = ep.id
                JOIN SorteosProgramacion sp ON sp.ID = ep.sorteo_prog_id
                JOIN SorteosDefinicion sd ON sd.ID = sp.IDSorteoDefinicion
            WHERE e.scored = 0
            ";

    return $sql;
}

/**
 * Agrupa los eventos que vienen de la bd y le agregan los sorteos
 * que contiene cada uno
 */
function groupEvents($eventsRaw)
{
    $events = array();

    foreach ($eventsRaw as $eventInfo)
    {
        $eventId = $eventInfo['id'];
        if (!array_key_exists($eventId, $events))
        {
            $events[$eventId]                = array();
            $events[$eventId]['event_parts'] = array();
            $events[$eventId]['event_id']    = $eventId;
            $events[$eventId]['can_score']   = true;
        }

        $eventPart                     = array();
        $eventPart['nombre_sorteo']    = $eventInfo['NombreSorteo'];
        $eventPart['fecha_hora']       = $eventInfo['FechayHora'];
        $eventPart['has_winner']       = ($eventInfo['has_winner_number'] > 0);
        $events[$eventId]['can_score'] = $events[$eventId]['can_score'] && $eventPart['has_winner'];

        $events[$eventId]['event_parts'][] = $eventPart;

    }

    return $events;
}