<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 3/9/15
 * Time: 3:07 PM
 */

require_once 'config.ini.php';
require_once 'conectadb.php';

$response = array('success' => true, 'msg' => 'Se actualizaron correctamente los números ganadores', 'error_msg' => '');

$spid               = $_POST['IDSorteoProgramacion'];
$numerosGanadores   = $_POST['numerosganadores'];
$nuevoNumeroGanador = $numerosGanadores[0];

try
{


// obtener el registro del SorteoProgramacion
    $sqlSorteoProgramacion  = "SELECT ID, IDSorteoDefinicion FROM SorteosProgramacion SP WHERE SP.ID  = ?";
    $stmtSorteoProgramacion = $pdoConn->prepare($sqlSorteoProgramacion);
    $stmtSorteoProgramacion->execute(array($spid));
    $sorteo = $stmtSorteoProgramacion->fetch(PDO::FETCH_ASSOC);

// obtener el registro con el ID NumeroGanador, y el numero ganador como tal
    $sqlNumeroGanador  = "SELECT id, Numero FROM SorteosNumerosGanadores SN WHERE SN.IDSorteoProgramacion = :sorteo_prog_id";
    $stmtNumeroGanador = $pdoConn->prepare($sqlNumeroGanador);
    $stmtNumeroGanador->execute(array(':sorteo_prog_id' => $spid));
    $numeroGanadorOriginal = $stmtNumeroGanador->fetchAll(PDO::FETCH_ASSOC);


// Actualizar/Limpiar las apuestas, quitarles el flag ganador y el IDNumeroGanador,

    $sqlLimpiaGanadores  = "UPDATE SorteoApuesta SA
                        SET SA.FlagGanador = 0, SA.IDNumeroGanador = 0
                        WHERE SA.IDSorteoProgramacion = :sorteo_prog_id
                              AND SA.FlagGanador = 1";
    $stmtLimpiaGanadores = $pdoConn->prepare($sqlLimpiaGanadores);
    $stmtLimpiaGanadores->execute(array(':sorteo_prog_id' => $spid));

// actualizar row numero ganador actual
    $sqlActualizaNumeroGanador  = "UPDATE SorteosNumerosGanadores SET Numero = :numero_nuevo WHERE ID = :id_ng";
    $stmtActualizaNumeroGanador = $pdoConn->prepare($sqlActualizaNumeroGanador);
    $stmtActualizaNumeroGanador->execute(array(':numero_nuevo' => $nuevoNumeroGanador, ':id_ng' => $numeroGanadorOriginal[0]['id']));

    // actualizar las apuestas poniendo el numero ganador nuevo
    $sqlActualizaApuestasGanadoras  = "UPDATE SorteoApuesta SET FlagGanador = 1, IDNumeroGanador = :id_ng WHERE Numero = :numero";
    $stmtActualizaApuestasGanadoras = $pdoConn->prepare($sqlActualizaApuestasGanadoras);
    $stmtActualizaApuestasGanadoras->execute(array(':id_ng' => $numeroGanadorOriginal[0]['id'], ':numero' => $nuevoNumeroGanador));

    if($sorteo['IDSorteoDefinicion'] == 28){
        $nuevoNumeroGanador = $numerosGanadores[1];
        $sqlActualizaNumeroGanador  = "UPDATE SorteosNumerosGanadores SET Numero = :numero_nuevo WHERE ID = :id_ng";
        $stmtActualizaNumeroGanador = $pdoConn->prepare($sqlActualizaNumeroGanador);
        $stmtActualizaNumeroGanador->execute(array(':numero_nuevo' => $nuevoNumeroGanador, ':id_ng' => $numeroGanadorOriginal[1]['id']));

        $nuevoNumeroGanador = $numerosGanadores[2];
        $sqlActualizaNumeroGanador  = "UPDATE SorteosNumerosGanadores SET Numero = :numero_nuevo WHERE ID = :id_ng";
        $stmtActualizaNumeroGanador = $pdoConn->prepare($sqlActualizaNumeroGanador);
        $stmtActualizaNumeroGanador->execute(array(':numero_nuevo' => $nuevoNumeroGanador, ':id_ng' => $numeroGanadorOriginal[2]['id']));

    }

    //SQL PARA LOS USUARIOS BANCA
    $sqlBancas = "SELECT U.ID, U.PagaPorcentaje, U.Recepcion, U.Reparticion, U.Comision
                           FROM Usuarios U JOIN Usuarios P ON U.IDPadre = P.ID
                           WHERE U.NivelUsuario
                           IN (SELECT id from UsuariosNivel where jerarquia = 'Bank') and U.FlagActivo=1 and U.ActivoFlag=1";
    $stmtBancas = $pdoConn->prepare($sqlBancas);
    $stmtBancas->execute();
    $bancas = $stmtBancas->fetchAll(PDO::FETCH_ASSOC);

//SQL PARA CAPTURAR EL VALOR ANTERIOR
    $sqlBalancesAnterior = "SELECT balance FROM Balances_Bancas
                            WHERE user_id = ? AND sorteo_id = ?";
    $stmtBalancesAnterior = $pdoConn->prepare($sqlBalancesAnterior);

//SQL DELETE BALNACE
    $sqlDeleteBalancesAnterior = "DELETE FROM Balances_Bancas
                                  WHERE user_id = ? AND sorteo_id = ?";
    $stmtDeleteBalancesAnterior = $pdoConn->prepare($sqlDeleteBalancesAnterior);

//SQL INSERCCION
    $sqlBalances = "INSERT INTO Balances_Bancas(sorteo_id, user_id, segurosTotal, comision, premio, balance, fechayhora)
                    VALUES (:sorteoID,:userID,:segurosTotal,:comision,:premio,:balance, NOW())";
    $stmtBalances = $pdoConn->prepare($sqlBalances);

//SQL CAPTURA BALANCES TOTALES
    $sqlCaptura = "SELECT * FROM Balances_Total_Banca
                   WHERE user_id = ?";
    $stmtCapturaTotal = $pdoConn->prepare($sqlCaptura);

//SQL ACTUALIZA BALANCES TOTALES
    $sqlActualiza = "UPDATE Balances_Total_Banca
                 SET balanceTotal = ?
                 WHERE user_id = ?";
    $stmtActualiza = $pdoConn->prepare($sqlActualiza);

//SQL PARA RECUPERAR LOS NUMEROS Y MONTOS POR USUARIO
    $sqlSeguros = "SELECT numero, monto FROM seg_lista_seguros_banca
        WHERE sorteo_id = :sorteo_id and user_id = :id_usuario
        ORDER BY numero ASC";
    $stmtSeguros = $pdoConn->prepare($sqlSeguros);



    foreach ($bancas as $banca){
        $totalSeguros = 0;
        $comision = 0;
        $premio = 0;
        $balance = 0;

        /*ELIMINA BALANCES ANTERIORES*/
        $stmtBalancesAnterior->execute(array($banca['ID'], $spid));
        $balanceAnterior = $stmtBalancesAnterior->fetch();

        $stmtCapturaTotal->execute(array($banca['ID']));
        $capturaTotal = $stmtCapturaTotal->fetch();

        $nuevoBalanceAnterior = $capturaTotal['balanceTotal'] - $balanceAnterior['balance'];

        //ACTUALIZA EL TOTAL ANTERIOR
        $stmtActualiza->execute(array($nuevoBalanceAnterior ,$banca['ID']));

        $stmtDeleteBalancesAnterior->execute(array($banca['ID'], $spid));




        /*CAPTURA TOTAL ACUMULADO**/
        $stmtCapturaTotal->execute(array($banca['ID']));
        $capturaTotal = $stmtCapturaTotal->fetch();
        $balanceTotal = $capturaTotal['balanceTotal'];

        $stmtSeguros->execute(array(':sorteo_id' => $spid, ':id_usuario' => $banca['ID']));
        $numerosSeguros = $stmtSeguros->fetchAll(PDO::FETCH_ASSOC);

        foreach($numerosSeguros as $numero){
            $totalSeguros = $totalSeguros + $numero['monto'];

            if($numero['numero'] == $numerosGanadores[0]){
                $premio = $numero['monto'] * 75;
            }


        }//FIN FOREACH NUMERO

        $porcen = $banca['Comision'] / 100;
        $comision = $porcen * $totalSeguros;

        $balance = $totalSeguros - $comision - $premio;

        //INSERTA LOS BALANCES DE CADA SORTEO
        $stmtBalances->execute(array(':sorteoID' => $spid, ':userID' => $banca['ID'], ':segurosTotal' => $totalSeguros,
            ':comision' => $comision,':premio' => $premio, ':balance' => $balance));

        $balanceTotal = $balanceTotal + $balance;

        //ACTUALIZA EL TOTAL ACUMULADO
        $stmtActualiza->execute(array($balanceTotal,$banca['ID']));

    }//FIN FOREACH BANCA




} catch (PDOException $e)
{
    $response['success']   = false;
    $response['error_msg'] = "Hubo un error en la bd, trate de nuevo";
}

echo json_encode($response);