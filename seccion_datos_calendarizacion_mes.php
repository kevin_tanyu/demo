<?php
session_start();
if (!isset($_SESSION['ActivoFlag']))
{
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

function DiaMes($x)
{
    $dias  = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
    $meses = array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

    $dia  = date("w", mktime(0, 0, 0, date("m"), date("d") + $x, date("Y")));
    $dia2 = date("d", mktime(0, 0, 0, date("m"), date("d") + $x, date("Y")));
    $dia3 = date("m", mktime(0, 0, 0, date("m"), date("d") + $x, date("Y")));
    $dia4 = date("Y", mktime(0, 0, 0, date("m"), date("d") + $x, date("Y")));

    $dia3 = $dia3 + 0;

    $dia  = $dias[$dia];
    $dia3 = $meses[$dia3];

    return $dia . " " . $dia2 . " de " . $dia3 . " de " . $dia4;
}


function DiaMes2($x)
{

    $dia = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + $x, date("Y")));

    return $dia;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");
?>
<h3 style="line-height:1px;">Calendarizacion de Sorteos</h3>
<h6 style="line-height:1px;">Administrar las fechas de los siguientes sorteos</h6>
<div class="divTable">
    <div class="divRowCalendario">
        <?php
        $h = 1;
        $Flag = 0;
        for ($x = 0;
        $x <= 15;
        $x++) {
        if ($h == 8) {
        ?>
    </div>
    <div class="divRowCalendario">
        <?php
        $h = 1;
        }
        ?>
        <div class="divCellCalendario">
            <div class="FechaCalendario"><?= DiaMes($x); ?>
                <div style="float:right; padding-right:10px;" class="letra"><a class="various  fancybox.ajax"
                                                                               style="color:#333333"
                                                                               href="seccion_datos_calendarizacion_add_form.php?dia=<?= DiaMes2($x); ?>">AGREGAR</a>
                </div>
            </div>
            <div class="SorteosCalendarizados">
                <?php
                $QUERY = "SELECT 	sp.FechayHora,
																		sp.NumeroDeSuerte,
																		sd.NombreSorteo, 
																		sd.PagaPorcentaje,
																		sp.id,
																		sp.NumeroDeSuerte2
														FROM 		SorteosProgramacion sp, 
																		SorteosDefinicion sd
														WHERE 	sp.IDSorteoDefinicion=sd.ID 
															  AND 	sd.FlagActivo<>2
															  AND 	sp.FechayHora BETWEEN '" . DiaMes2($x) . " 00:00:00'
															  AND    '" . DiaMes2($x) . " 23:59:59'
															  ORDER BY sp.FechayHora ASC";
                $rs = mysql_query($QUERY);

                if (mysql_affected_rows() > 0)
                {
                    ?>
                    <div class="divTable" style="width:100%;">
                        <div class="divRowHeader">
                            <div class="divCellHeader letra2" style="width:150px;">Fecha y Hora</div>
                            <div class="divCellHeader letra2" style="width:150px;">Loteria</div>
                            <div class="divCellHeader letra2">NS</div>
                            <div class="divCellHeader letra2" style="width:150px;">NR</div>
                            <div class="divCellHeader letra2">PA</div>
                            <div class="divCellHeader letra2">Accion</div>
                        </div>
                        <?php
                        while ($row = mysql_fetch_row($rs))
                        {
                            ?>
                            <div class="divRow">
                                <div class="divCellCen letra" style="width:150px;"><?= $row[0]; ?></div>
                                <div class="divCellCen letra" style="width:150px;"><?= $row[2]; ?></div>
                                <div class="divCellCen letra"><?= $row[1]. ', ' . $row[5]; ?></div>
                                <?php
                                $QUERY2 = "SELECT 	Numero
																	FROM 		SorteosNumerosRestringidos
																	WHERE	IDSorteoProgramacion = " . $row[4];
                                $rs2 = mysql_query($QUERY2);

                                if (mysql_affected_rows() > 0)
                                {
                                    $CADENA = "";
                                    while ($row2 = mysql_fetch_row($rs2))
                                    {
                                        if ($CADENA != "") $CADENA = $CADENA . ", ";
                                        $CADENA = $CADENA . $row2[0];
                                    }
                                } else  $CADENA = " - ";
                                ?>
                                <div class="divCellCen letra" style="width:150px;"><?= $CADENA; ?></div>
                                <div class="divCellCen letra"><?= $row[3]; ?></div>
                                <div class="divCellCen letra">
                                    <a class="various fancybox.ajax"
                                       href="seccion_datos_calendarizacion_edit_form.php?SORTEO_PROG_ID=<?php echo $row[4] ?>">Editar</a>
                                    |
                                    <a href="#"
                                       onclick="DeleteSorteoProgramacion(<?php echo $row[4] ?>)">Eliminar</a>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                <?php
                } else
                {
                    ?>
                    <div class="SorteosCalendarizadosVacio">No hay sorteos calendarizados para esta fecha. <a
                            class="various  fancybox.ajax"
                            href="seccion_datos_calendarizacion_add_form.php?dia=<?= DiaMes2($x); ?>">Desea agregar
                            uno?</a></div>
                <?php
                }
                ?>
            </div>
        </div>
        <?php
        $h++;
        }
        ?>
    </div>
</div>
