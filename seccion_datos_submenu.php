<?php
$viewNumerosGanadores = array(NIVEL_PERMISO_USUARIO_ADMIN);
$viewCandelarizacion = array(NIVEL_PERMISO_USUARIO_ADMIN);
$viewHistorialSorteos = array(NIVEL_PERMISO_USUARIO_ADMIN);
$viewMaestroLoterias = array(NIVEL_PERMISO_USUARIO_ADMIN);
$viewParametros = array(NIVEL_PERMISO_USUARIO_ADMIN);
$viewConfiguracionListas = array(NIVEL_PERMISO_USUARIO_ADMIN);
$viewMassiveMails = array(NIVEL_PERMISO_USUARIO_ADMIN);

?>

<aside_submenu>
    <h3>Opciones</h3>

    <div id="list4">

        <ul>
            <?php if (in_array($_SESSION['TipoUsuarioInt'], $viewNumerosGanadores)): ?>
                <li><a href="seccion_datos_numerosganadores.php">Numeros Ganadores</a></li>
            <?php endif; ?>

            <?php if (in_array($_SESSION['TipoUsuarioInt'], $viewCandelarizacion)): ?>
                <li><a href="seccion_datos_calendarizacion.php">Calendarizacion de Sorteos</a></li>
            <?php endif; ?>

            <?php if (in_array($_SESSION['TipoUsuarioInt'], $viewHistorialSorteos)): ?>
                <!-- <li><a href="seccion_datos_historial.php">Historial de Sorteos</a></li>-->
            <?php endif; ?>

            <?php if (in_array($_SESSION['TipoUsuarioInt'], $viewMaestroLoterias)): ?>
                <!-- <li><a href="seccion_datos_loterias.php">Maestro de Loteria</a></li>-->
            <?php endif; ?>

            <?php if (in_array($_SESSION['TipoUsuarioInt'], $viewParametros)): ?>
                <!--  <li><a href="seccion_datos_parametros.php">Parametros Generales</a></li>-->
            <?php endif; ?>
            <?php if (in_array($_SESSION['TipoUsuarioInt'], $viewConfiguracionListas)): ?>
                <!--  <li><a href="seccion_datos_configuracion_listas.php">Configuracion Listas</a></li>-->
            <?php endif; ?>
            <?php if (in_array($_SESSION['TipoUsuarioInt'], $viewConfiguracionListas)): ?>
                <!--  <li><a href="seccion_datos_configuracion_listas_revision.php">Configuracion Listas Puestos</a></li>-->
            <?php endif; ?>
            <?php if (in_array($_SESSION['TipoUsuarioInt'], $viewMassiveMails)): ?>
                <!--  <li><a href="seccion_datos_correo_masivo.php">Envio de correo masivo</a></li>
                 <li><a href="seccion_datos_importacion_listas_main.php">Importacion de listas</a></li>-->
            <?php endif; ?>
        </ul>
    </div>
</aside_submenu>
