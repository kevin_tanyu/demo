<?php

session_start();

require_once 'config.ini.php';
require_once 'conectadb.php';

$parametros = $_POST['loteria'];

$sqlUpdate = "insert
into USUARIOS_CALLE_venta_parametros (IDSorteoDefinicion,IDUsuario,porcentaje_restriccion,tope_inicial)
values ( :sorteo_definicion_id, :id_usuario, :porcentaje, :tope)
on duplicate key update porcentaje_restriccion = VALUES(porcentaje_restriccion),
  tope_inicial = VALUES(tope_inicial);";

$stmtUpdate = $pdoConn->prepare($sqlUpdate);

$sqlUpdateSeguroUsuario = "UPDATE Usuarios SET compra_seguros = :estado
                           WHERE ID = :id_usuario";
$stmtUpdateUsuario = $pdoConn->prepare($sqlUpdateSeguroUsuario);


$userID = 0;

$error = false;

foreach ($parametros as $loteriaId => $parametro)
{
    $params = array(':sorteo_definicion_id' => $loteriaId,
        ':id_usuario' => $parametro['user_id'],
        ':porcentaje' => $parametro['porcentaje_restringido'],
        ':tope' => $parametro['tope_inicial'],
    );
    try
    {
        $stmtUpdate->execute($params);
    } catch (Exception $e)
    {
        $error = true;
    }

    $userID = $parametro['user_id'];

}//FIN FOREACH


try
{
    $stmtUpdateUsuario->execute(array(':estado' => $_POST['slcSeguro'], ':id_usuario' => $userID));
} catch (Exception $e)
{
    $error = true;
}

?>


<?php if ($error)
{
    ?>
    <div class="error">Hubo un error en la actualizacion revise los parametros, e intente de nuevo</div>
<?php
} else
{
    ?>
    <div class="success">Actualizacion con exito</div>
<?php } ?>