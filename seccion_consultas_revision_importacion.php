<?php

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

/*VENDEDORES*/
$sqlVendedores = "SELECT * FROM Usuarios WHERE TipoUsuario=5 and importacion = 1
                  ORDER BY NombreUsuario"; // VENDEDORES
$stmtVendedores = $pdoConn->prepare($sqlVendedores);
$stmtVendedores->execute();
$Vendedores = $stmtVendedores->fetchAll(PDO::FETCH_ASSOC);

$sqlSorteos = "SELECT SP.ID, SD.NombreSorteo, SP.FechayHora
               FROM SorteosProgramacion SP
               JOIN SorteosDefinicion SD
               ON SP.IDSorteoDefinicion = SD.ID
               WHERE SP.ID = ?";
$stmtSorteo = $pdoConn->prepare($sqlSorteos);
$stmtSorteo->execute(array($_GET['SID']));
$sorteo = $stmtSorteo->fetch();
?>

<style type="text/css">
    .fancybox-custom .fancybox-skin {
        box-shadow: 0 0 50px #222;
    }

    input[type=checkbox] {
        visibility: visible;
        height: 15px;
    }


</style>

<h3>IMPORTAR LISTA PARA SORTEO <?php echo $sorteo['NombreSorteo'] . ' DEL ' . $sorteo['FechayHora']?></h3>
<div class="divTable">
    <div class="divRow" style="width: 250px">
        <div class="divCellHeader2" style="width: 180px">Lista</div>
        <div class="divCellHeader2" style="width: 150px">Estado</div>
        <div class="divCellHeader2" style="width: 350px">Enviado</div>
        <div class="divCellHeader2" style="width: 150px">Importar</div>
    </div>
    <?php foreach ($Vendedores as $vendedor): ?>
        <div class="divRow2">
            <?php
            if (strpos($vendedor['NombreUsuario'],'IMPORT') === false){
                $nombreUsuario = $vendedor['NombreUsuario'];
            }else{
                $nombreUsuario = substr($vendedor['NombreUsuario'], 7);
            }
            ?>
            <div class="divCellCen2" style="width: 180px; text-align: center; font-weight: bold; font-size: 15px">
                <?php echo $nombreUsuario ?>
            </div>
            <?php
              $directorio = IMPORTACION_DIR ;
              $disponible = false;
              if(file_exists($directorio . '/' . $nombreUsuario . '.xls')){
                  $disponible = true;
              }//Fin if

            ?>
            <?php if($disponible == true){?>
                <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px">
                    <?php echo 'Disponible '?>
                </div>
            <?php }else{?>
                <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px">
                    <?php echo 'No disponible' ?>
                </div>
            <?php }?>

            <?php if($disponible == true){?>
                <div class="divCellCen2" style="width: 350px; text-align: center; font-weight: bold; font-size: 15px">
                    <?php echo system_date_format(date ("Y-m-d H:i:s", filemtime($directorio . '/' . $nombreUsuario . '.xls')))?>
                </div>
            <?php }else{?>
                <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px">
                    <?php echo '-------' ?>
                </div>
            <?php }?>

            <?php if($disponible == true){?>
                <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px">
                    <input name="importacion[]" type="checkbox" value="<?php echo $nombreUsuario . '/' . $vendedor['ID']?>" checked/>
                </div>
            <?php }else{?>
                <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px">

                </div>
            <?php }?>
        </div>
    <?php endforeach; ?>
    <div class="divCellCen2" style="width: 300px; text-align: center; font-weight: bold; font-size: 15px">
        <input type="button" id="btnImportar" class="button"
               value="Importar Lista(s)" onclick="importarListas()"><div id="result"></div>
    </div>
</div>


<script>
    function importarListas(){

        var importacion = [];
        $(':checkbox:checked').each(function() {
            importacion.push($(this).val());
        });
        if(importacion.length == 0){
            $("#result").html('NO hay listas para importar');
        }else{
            var parametros = {
                "importacion" : importacion,
                "sorteoID" : <?php echo $_GET['SID'];?>,
                "accion" : 'Importar'
            };
            $.ajax({
                data : parametros,
                url: 'seccion_consultas_revision_importacion_data.php',
                type: 'post',
                beforeSend: function(){
                    $("#result").html("Procesando");
                },
                success: function(response){
                    $("#result").html(response);
                }
            });
        }


    }//Fin importarListas

</script>