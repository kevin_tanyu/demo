<?php
session_start();

if (!isset($_SESSION['ActivoFlag']))
{
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");
?>
<script type="text/javascript">
    function doChosen() {
        $(".chzn-select").chosen();
        $(".chzn-select-deselect").chosen({allow_single_deselect: true});
    }

    doChosen();
</script>
<?php

$revendedorString = "";
if ($_SESSION['TipoUsuarioInt'] == NIVEL_PERMISO_USUARIO_WEB)
{
    $revendedorString = " AND ID = " . $_SESSION['IDUsuario'] . " ";
}

$QUERY = "SELECT id, NombreUsuario FROM Usuarios WHERE TipoUsuario=10 and FlagActivo=1 and ActivoFlag=1 $revendedorString "; // Seleccionar usuarios recolectores
$rs = mysql_query($QUERY);
?>
<h3 style="line-height:1px;">Agregar Usuario</h3>
<div style="text-align:center; width:450px; height:200px; background-color:#ffffff;">
    <form name="formulario1" id="formulario1">
        <div class="divTable">
            <div class="divRow">
                <div class="divCellIzq letra" style="width:150px;">Nombre de Usuario</div>
                <div class="divCellDer" style="width:150px;"><input type="text" name="NombreUsuario"
                                                                    class="campotexto letra" style="width:150px;"
                                                                    required placeholder="Usuario"></div>
            </div>
            <div class="divRow">
                <div class="divCellIzq letra" style="width:150px;">Contrase&ntilde;a</div>
                <div class="divCellDer" style="width:150px;"><input type="text" name="Contrasena"
                                                                    class="campotexto letra" style="width:150px;"
                                                                    required placeholder="Contrasena"></div>
            </div>
            <div class="divRow">
                <div class="divCellIzq letra" style="width:150px;">Email (login)</div>
                <div class="divCellCen" style="width:150px;"><input type="text" name="Email" class="campotexto letra"
                                                                    style="width:150px;" required
                                                                    placeholder="email@dominio.com"></div>
            </div>
            <div class="divRow">
                <div class="divCellIzq letra" style="width:150px;">Cedula</div>
                <div class="divCellCen" style="width:150px;"><input type="number" name="cedula" class="campotexto letra"
                                                                    style="width:150px;" required></div>
            </div>
            <div class="divRow">
                <div class="divCellIzq letra" style="width:150px;">Pregunta Secreta</div>
                <div class="divCellCen" style="width:150px;"><input type="text" name="pregunta_secreta"
                                                                    class="campotexto letra" style="width:150px;"
                                                                    required></div>
            </div>
            <div class="divRow">
                <div class="divCellIzq letra" style="width:150px;">Respuesta</div>
                <div class="divCellCen" style="width:150px;"><input type="text" name="respuesta_secreta"
                                                                    class="campotexto letra" style="width:150px;"
                                                                    required></div>
            </div>
            <div class="divRow">
                <div class="divCellIzq letra" style="width:150px;">Revendedor</div>
                <div class="divCellCen" style="width:150px;"><select name="IDRevendedor" data-placeholder="Recolector"
                                                                     class="chzn-select" style="width:150px;"
                                                                     id="chosen" required><?php
                        while ($row = mysql_fetch_row($rs))
                        {
                            ?>
                            <option value="<?= $row[0]; ?>"><?= ucwords($row[1]); ?></option><?php
                        }
                        ?></select></div>
            </div>
        </div>
        <input type="button" value="Agregar" class="button" id="BotonFormulario"
               onclick="javascript:FormularioAddUsuario();">
    </form>
</div>
