<?php
session_start();
require_once 'config.ini.php';
require_once 'conectadb.php';
require_once 'lib/seccion_datos_importacion_listas_lib.php';

try{

    $accion = $_POST['accion'];

    if($accion == 'Importar'){

        $aImportar = $_POST['importacion'];


        for($i = 0; $i < sizeof($aImportar); $i++){
            $file = explode("/", $aImportar[$i]);
            $valido = importarATablet($pdoConn, $file[0], $file[1], $_POST['sorteoID']);
        }//Fin for

        echo 'Ingresado con exito';
    }//Fin if

    if($accion == 'Desimportar'){

        $aDesimportar = $_POST['desimportacion'];

        /*GET TICKET ID*/
        $sqlGetTicketID= "SELECT * FROM SorteoApuesta
                             WHERE IDUsuario = ? AND IDSorteoProgramacion = ?
                             LIMIT 1";
        $stmtGetTicketID = $pdoConn->prepare($sqlGetTicketID);

        /*DELETE TICKET*/
        $sqlDeleteTicket = "DELETE FROM Ticket WHERE id = ?";
        $stmtDeleteTicekt = $pdoConn->prepare($sqlDeleteTicket);

        for($i = 0; $i < sizeof($aDesimportar); $i++){

            $stmtGetTicketID->execute(array($aDesimportar[$i], $_POST['sorteoID']));
            $ticketID = $stmtGetTicketID->fetch();

            $stmtDeleteTicekt->execute(array($ticketID['IDTicket']));

            echo 'Removido con exito';
        }//Fin for

    }//Fin if


}catch (Exception $e){
    echo 'Error, intente de nuevo';
}

?>