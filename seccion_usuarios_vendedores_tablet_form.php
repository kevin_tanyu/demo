<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 1/4/15
 * Time: 12:47 AM
 */

session_start();

require_once 'config.ini.php';
require_once 'conectadb.php';
require_once 'lib/seccion_usuarios_vendedores_parametros_lib.php';

$vendedorCalleId = $_GET['id'];

$sqlInfoUsuario = "SELECT ID, printer_type FROM Usuarios WHERE ID = :id_usuario";
$infoUsuarioStmt = $pdoConn->prepare($sqlInfoUsuario);
$infoUsuarioStmt->execute(array(':id_usuario' => $vendedorCalleId));
$infoUsuario = $infoUsuarioStmt->fetch(PDO::FETCH_ASSOC);

$tiposImpresora = array("CHINA", "ZEBRA", "BM9000");

?>

<form method="post" id="form_modifica_vendedor_tablet" action="seccion_usuarios_vendedores_tablet_action.php">
    <legend><h3>Modificacion tablet para vendedor calle</h3></legend>
    <input type="hidden" value="<?php echo $vendedorCalleId ?>" name="uid" >
    <fieldset>
        <legend>Tipo de impresora</legend>
        <div class="divTable" >
            <div class="divRow">
                <div class="divCellIzq" style="width: 15em;" >
                    <select name="tipo_impresora" class="chzn-select" style="width: 100%;" >
                        <?php foreach ($tiposImpresora as $tipoImpresora): ?>
                            <option value="<?php echo $tipoImpresora ?>" <?php echo $tipoImpresora == $infoUsuario['printer_type']?"SELECTED":""; ?> ><?php echo  $tipoImpresora ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
    </fieldset>

    <input type="submit" value="Guardar" class="button">

    <div id="params_result"></div>
</form>
<script>
    $('#form_modifica_vendedor_tablet').ajaxForm({
        target: '#params_result',
        beforeSubmit: function () {
            $('#form_modifica_vendedor_tablet').block();
        },
        success: function () {
            $('#form_modifica_vendedor_tablet').unblock();
        }
    });
    $('.chzn-select').chosen();
</script>