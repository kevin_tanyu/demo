<?php include_once("../config.ini.php"); ?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>SigueTuSuerte.com</title>
    <link rel="stylesheet" type="text/css" href="../css/<?php
    if($isMobile) echo "handheld.css";
    else echo "main.css";
    ?>" />
    <?php  if($isMobile)  { ?>
        <meta name = "viewport" content = "user-scalable=no,initial-scale=1.0, maximum-scale=1.0, width=device-width" />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/canvas.js"></script>
    <?php
    }
    ?>
</head>
<body>
<div id="container" class="container">
    <section id="content">
        <form action="login_seguros.php" method="post">
            <h1> Ingresar al Sitio </h1>
            <h1>COMPRA DE SEGUROS</h1>
            <div>
                <input type="text" placeholder="Usuario" required="" id="username" name="username" />
            </div>
            <div>
                <input type="password" placeholder="Contrase&ntilde;a" required="" id="password" name="password" />
            </div>
            <div>
                <input type="submit" value="Ingresar" />
                <a href="#">Perdi&oacute; la Contrase&ntilde;a?</a>
            </div>
        </form><!-- form -->
    </section><!-- content -->
</div><!-- container -->
<canvas id="canvas">
    Canvas de HTML5 no esta soportado por este navegador.
</canvas>
<div id="fix"></div>
</body>
</html>