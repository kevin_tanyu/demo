<?php
session_start();

if (!isset($_SESSION['ActivoFlag'])) {
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index_seguros.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("../config.ini.php");
include("../conectadb.php");

// Incluye Header
include("header_seguros.php");
?>

    <div
        style="width:100%; height: 90%; vertical-align:middle; text-align:center;padding-top:200px;padding-bottom:200px; font-family:Arial, Helvetica, Sans-serif; font-size:18px;">
        Seguros de numeros restringidos y otros
    </div>

<?php
// Incluye Footer
include("../footer.php");
?>