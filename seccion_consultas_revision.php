<?php
session_start();
if (!isset($_SESSION['ActivoFlag']))
{
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");
// Incluye Header
include("header.php");

include("seccion_consultas_submenu.php");

?>
    <div id="ContenedorGeneral">
        <div style="float: right;">
            <input type="button" class="flotante" value="Refrescar" onclick="updateList();">


        </div>
        <h3 style="line-height:1px;">Listas del Dia</h3>
        <h6 style="line-height:1px;">Estas son las listas para lo que va del dia para revision</h6>

        <div id="list_revision_content">
        </div>
    </div>

    <script>
        var enableAutoRefresh = false;
        var refreshFrequency = 30000;
        var timeOutReference;
        $(function () {
            updateList();

            $('#list_revision_content').on('click', '.action_trigger', handleChangeStatus);

            $('#myonoffswitch').on('change', function (e) {
                enableAutoRefresh = $(this).is(':checked');
                if (enableAutoRefresh) {
                    timeOutReference = setTimeout(updateList, refreshFrequency);
                } else {
                    clearTimeout(timeOutReference);
                }
            });

        });
        function updateList() {
            if(window.location.hash) {
                var id = window.location.hash.substring(1); //Puts hash in variable, and removes the # character

                var url = 'seccion_consultas_revision_list.php?ID=' + id;
                // hash found
            } else {
                // No hash found
                var url = 'seccion_consultas_revision_list.php';
            }
            $('#list_revision_content').block();
            $('#list_revision_content').load(url, function () {
                $('#list_revision_content').unblock();
                if (enableAutoRefresh) {
                    timeOutReference = setTimeout(updateList, refreshFrequency);
                }
            });
        }

        function handleChangeStatus(e) {
            e.preventDefault();

            var sorteo_prog_id = $(this).data('sorteo_prog_id');
            var vendedor_id = $(this).data('vendedor_id');
            var action = $(this).data('next_action');

            if (typeof action !== "undefined") {
                var conf = confirm("Desea cambiar el estado?");
                if (conf) {
                    $('#list_revision_content').block();
                    var data = { sorteo_prog_id: sorteo_prog_id, vendedor_id: vendedor_id, action: action };
                    $.get('seccion_consultas_revision_action.php', data, function () {
                    })
                        .done(function (data) {
                            var message = "Cambio de estado correcto";
                            if (data.error)
                                message = data.error_message

                            alert(message);
                            updateList();
                        })
                        .always($('#list_revision_content').unblock());
                }
            }
        }

    </script>

    <style>
        .lista-solicita-revision {
            background-color: #ffff00;
        }

        .lista-revisado {
            background-color: #AAD2F5;
        }
    </style>
    <style>

        .flotante {
            display:scroll;
            position:fixed;
            width:120px;
            z-index: 2;
            height: 60px;
            right:5px;
            top: 330px;
            font-weight: bold;
            font-family: 'segoe ui', arial, sans-serif;
            border-style: dotted;
            text-transform: uppercase;
        }

        .onoffswitch {
            position: relative;
            width: 90px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
        }

        .onoffswitch-checkbox {
            display: none;
        }

        .onoffswitch-label {
            display: block;
            overflow: hidden;
            cursor: pointer;
            border: 2px solid #999999;
            border-radius: 20px;
        }

        .onoffswitch-inner {
            display: block;
            width: 200%;
            margin-left: -100%;
            -moz-transition: margin 0.3s ease-in 0s;
            -webkit-transition: margin 0.3s ease-in 0s;
            -o-transition: margin 0.3s ease-in 0s;
            transition: margin 0.3s ease-in 0s;
        }

        .onoffswitch-inner:before, .onoffswitch-inner:after {
            display: block;
            float: left;
            width: 50%;
            height: 30px;
            padding: 0;
            line-height: 30px;
            font-size: 14px;
            color: white;
            font-family: Trebuchet, Arial, sans-serif;
            font-weight: bold;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        .onoffswitch-inner:before {
            content: "ON";
            padding-left: 10px;
            background-color: #34A7C1;
            color: #FFFFFF;
        }

        .onoffswitch-inner:after {
            content: "OFF";
            padding-right: 10px;
            background-color: #EEEEEE;
            color: #999999;
            text-align: right;
        }

        .onoffswitch-switch {
            display: block;
            width: 18px;
            margin: 6px;
            background: #FFFFFF;
            border: 2px solid #999999;
            border-radius: 20px;
            position: absolute;
            top: 0;
            bottom: 0;
            right: 56px;
            -moz-transition: all 0.3s ease-in 0s;
            -webkit-transition: all 0.3s ease-in 0s;
            -o-transition: all 0.3s ease-in 0s;
            transition: all 0.3s ease-in 0s;
        }

        .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
            margin-left: 0;
        }

        .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
            right: 0px;
        }
    </style>
<?php require_once 'footer.php' ?>