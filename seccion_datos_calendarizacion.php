<?php
session_start();

if (!isset($_SESSION['ActivoFlag']))
{
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

// Incluye Header
include("header.php");

include("seccion_datos_submenu.php");
?>
<script type="text/javascript">
    $(document).ready(function () {
        $(".various").fancybox({
            fitToView: true,
            width: '650px',
            height: '70%',
            autoSize: false,
            closeClick: false,
            openEffect: 'fade',
            closeEffect: 'elastic'
        });
        TablaLoterias();

    });

    function TablaLoterias() {
        $.ajax({
            type: "GET",
            url: 'seccion_datos_calendarizacion_mes.php',
            DataType: 'html',
            success: function (response) {
                document.getElementById('TablaLoterias').innerHTML = response;
            }
        });

        return false;
    }


    function FormularioAddLoteria() {
        $.ajax({
            type: "POST",
            processData: false,
            traditional: true,
            url: 'seccion_datos_calendarizacion_add.php',
            data: $('#formulario1').serialize(),
            success: function (response) {
                TablaLoterias();
                //MasFechas.innerHTML=response;
                $.fancybox.close();
            }
        });

        return false;
    }

    function FormularioEditLoteria() {
        $.ajax({
            type: "POST",
            processData: false,
            traditional: true,
            url: 'seccion_datos_calendarizacion_edit.php',
            data: $('#formulario2').serialize(),
            success: function (response) {
                TablaLoterias();
                //MasFechas.innerHTML=response;
                $.fancybox.close();
            }
        });

        return false;
    }

    function FormularioDeleteLoteria(id) {

        if (confirm("Esta seguro de querer deshabilitar esta Loteria?")) {
            $.ajax({
                type: "POST",
                url: 'seccion_datos_loterias_delete_accion.php?id=' + id,
                success: function (response) {
                    TablaLoterias();
                    $.fancybox.close();
                }
            });
        }
        return false;
    }

    function DeleteSorteoProgramacion(id) {
        if (confirm("Esta seguro de eliminar el sorteo?")) {
            var url = 'seccion_datos_calendarizacion_delete_sorteo.php';
            var data = { spid: id };
            $('#TablaLoterias').block();
            $.post(url, data)
                .done(function (response_data) {
                    response_data = $.parseJSON(response_data);
                    if (response_data.success)
                        TablaLoterias();
                    alert(response_data.msg);
                })
                .always(function () {
                    $('#TablaLoterias').unblock();
                });
        }
    }

</script>
<style type="text/css">
    .fancybox-custom .fancybox-skin {
        box-shadow: 0 0 50px #222;
    }
</style>

<div id="ContenedorGeneral">
    <div id="TablaLoterias"></div>


    <br><br>
    <a class="various  fancybox.ajax button" href="seccion_datos_calendarizacion_add_form.php"
       style="font-size:10px; float:right;">Agregar Sorteo</a>

</div>
<?php
// Incluye Footer
include("footer.php");
?>
