<?php
session_start();

if (!isset($_SESSION['ActivoFlag'])) {
	// Verifica si hay session creada, de lo contrario redirige al index
	header("Location: index.php?IDM=TO");
	exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

if ($_SERVER['REQUEST_METHOD']=="POST") {
	// Modifica archivo de parametros
	
	unlink("parametros.ini.php");
	
	$fp = fopen('parametros.ini.php', 'w');
	
	$ARCHIVOCONF="<?php
	//este archivo es generado por el sistema, no lo edite manualmente
		\$REGLA_PORCENTAJE=".$_POST['REGLA_PORCENTAJE']."; // Porcentaje de apuesta restringida
		\$REGLA_PAGO_DEFAULT=".$_POST['REGLA_PAGO_DEFAULT']."; // Veces que paga por default
		\$CORREO_ADMINISTRADOR=\"".$_POST['CORREO_ADMINISTRADOR']."\";
		\$CORREO_SALIENTE=\"".$_POST['CORREO_SALIENTE']."\";
		\$CORREO_SALIENTE_NOMBRE=\"".$_POST['CORREO_SALIENTE_NOMBRE']."\";
		\$MONTO_TOPE_INICIAL=\"".$_POST['MONTO_TOPE_INICIAL']."\";
	?>";
	
	fwrite($fp, trim($ARCHIVOCONF));
	fclose($fp);
	
	header("Location: ".$_SERVER['PHP_SELF']."?R=S");
	
}

// Incluye Header
include("header.php");

include("seccion_datos_submenu.php");
?>
			<div id="ContenedorGeneral">
				<h3 style="line-height:1px;">Parametros Generales</h3>
				<h6 style="line-height:1px;">Los parametros generales son validos para todo el sistema en todos los sorteos para todas las loterias</h6>
				<?php
				if ( isset($_GET['R']) && $_GET['R']=="S") {
					echo "<h6 style=\"color:green\">Archivo de configuracion escrito con exito.</h6>";
				}
				?>
				<form name="formulario1" method="POST" action="seccion_datos_parametros.php">
					<div class="divTable">
						<div class="divRow">
							<div class="divCellIzq letra" style="width:170px;">Porcentaje Numeros Restringidos</div>
							<div class="divCellIzq letra" style="width:150px;"><input type="text" class="campotexto" name="REGLA_PORCENTAJE" value="<?=$REGLA_PORCENTAJE; ?>" required style="width:60px;"></div>
							<div class="divCellIzq letra" style="margin-left:20px; color:#999999; line-height:12px; width:150px;">Indica el porcentaje maximo que puede representar un numero restringido en una apuesta.</div>
						</div>
						<div class="divRow">
							<div class="divCellIzq letra" style="width:170px;">Pago default para nuevas loterias</div>
							<div class="divCellIzq letra" style="width:150px;"><input type="text" class="campotexto" name="REGLA_PAGO_DEFAULT" value="<?=$REGLA_PAGO_DEFAULT; ?>" required style="width:60px;"></div>
							<div class="divCellIzq letra"  style="margin-left:20px; color:#999999; line-height:12px; width:150px;">Al crear una loteria, este es el numero que aparecera como relacion de pago por defecto.</div>
						</div>
						<div class="divRow">
							<div class="divCellIzq letra" style="width:170px;">Correo del Administrador</div>
							<div class="divCellIzq letra" style="width:150px;"><input type="text" class="campotexto" name="CORREO_ADMINISTRADOR" value="<?=$CORREO_ADMINISTRADOR; ?>" required style="width:120px;"></div>
							<div class="divCellIzq letra"  style="margin-left:20px; color:#999999; line-height:12px; width:150px;">Al generar una alerta de sistema, se enviara la notificacion a este correo electronico.</div>
						</div>
						<div class="divRow">
							<div class="divCellIzq letra" style="width:170px;">Correo del Saliente</div>
							<div class="divCellIzq letra" style="width:150px;"><input type="text" class="campotexto" name="CORREO_SALIENTE" value="<?=$CORREO_SALIENTE; ?>" required style="width:120px;"></div>
							<div class="divCellIzq letra"  style="margin-left:20px; color:#999999; line-height:12px; width:150px;">Al generar notificaciones se utilizara este correo como remitente.</div>
						</div>
						<div class="divRow">
							<div class="divCellIzq letra" style="width:170px;">Nombre del Correo Saliente</div>
							<div class="divCellIzq letra" style="width:150px;"><input type="text" class="campotexto" name="CORREO_SALIENTE_NOMBRE" value="<?=$CORREO_SALIENTE_NOMBRE; ?>" required style="width:120px;"></div>
							<div class="divCellIzq letra"  style="margin-left:20px; color:#999999; line-height:12px; width:150px;">Este es el nombre que aparece como remitente del correo para que no aparezca solamente la direccion.</div>
						</div>
                        <div class="divRow">
							<div class="divCellIzq letra" style="width:170px;">Monto tope inical</div>
							<div class="divCellIzq letra" style="width:150px;"><input type="number" class="campotexto" name="MONTO_TOPE_INICIAL" placeholder="&cent;" value="<?=$MONTO_TOPE_INICIAL; ?>" required style="width:120px;"></div>
							<div class="divCellIzq letra"  style="margin-left:20px; color:#999999; line-height:12px; width:150px;">Este monto aplica para todos los sorteos, numeros, y los tickets que jueguen los clientes. Indica el monto maximo que se le puede jugar a los numeros antes de que hayan restringidos.</div>
						</div>
					</div>
					<div class="divTable">
						<div class="divRow">
							<div class="divCellCen" style="width:500px;"><input type="submit" name="submit" value="Modificar" class="button" style="height:27px;"></div>
						</div>
					</div>
				</div>
				
			</div>
<?php
		
// Incluye Footer
include("footer.php");
?>