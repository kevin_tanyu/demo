<?php
session_start();

if (!isset($_SESSION['ActivoFlag']))
{
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

$sql = "SELECT 	sp.FechayHora,
                    LPAD(sp.NumeroDeSuerte,2,0) as numero_suerte,
                    sd.NombreSorteo,
                    sd.PagaPorcentaje,
                    sp.id as sp_id,
                    sd.NumerosGanadores,
                    sd.id as sd_id,
                    sd.FlagMultipleMinimoGana,
                    sng.Numero as NumeroGanador
    FROM 		SorteosProgramacion sp,
                    SorteosDefinicion sd,
                    SorteosNumerosGanadores sng
    WHERE 	sp.IDSorteoDefinicion=sd.ID
          AND 	sp.id=:sorteo_prog_id
          AND sng.IDSorteoProgramacion = sp.ID
    ORDER BY sp.FechayHora DESC
          ";
$statement = $pdoConn->prepare($sql);
$statement->execute(array(':sorteo_prog_id' => $_GET['id']));
if ($statement->rowCount() > 0)
{
    $row = $statement->fetchAll(PDO::FETCH_ASSOC);
    ?>
    <h3 style="line-height:1px;">Numeros Ganadores</h3>
    <h6 style="line-height:1px;">Para Sorteo de loteria <span style="color:red"><?= $row[0]['NombreSorteo']; ?></span>
        efectuado fecha y hora <span style="color:red"><?= $row[0]['FechayHora']; ?></span></h6>
    <div style="text-align:center;" id="form_add_container">
        <form name="formulario1" id="formulario1">
            <input type="hidden" name="IDSorteoProgramacion" value="<?= $_GET['id']; ?>">

            <div class="divTable" style="width:100%">
                <div class="divRow">
                    <div class="divCellIzq letra">Numero de la suerte en Juego</div>
                    <div class="divCellCen letra"><?= $row[0]['numero_suerte']; ?></div>
                </div>
                <?php if($row[0]['sd_id'] == 28){?>
                  <div class="divRow">
                      <div class="divCellIzq letra">Cantidad numeros Ganadores Ingresados</div>
                      <div class="divCellCen letra"><?= $row[0]['NumerosGanadores']+2; ?></div>
                  </div>
                 <?php }else{?>
                    <div class="divCellIzq letra">Cantidad numeros Ganadores Ingresados</div>
                    <div class="divCellCen letra"><?= $row[0]['NumerosGanadores']; ?></div>
                 <?php }?>
                <?php
                for ($x = 1; $x <= $row[0]['NumerosGanadores']; $x++)
                {
                    ?>
                    <div class="divRow">
                        <div class="divCellIzq letra">Numero Ganador #<?= $x; ?></div>
                        <div class="divCellcen"><input value="<?php echo $row[0]['NumeroGanador'] ?>"
                                                       name="numerosganadores[]" class="campotexto letra"/></div>
                    </div>
                    <?php if($row[0]['sd_id'] == 28):?>
                    <div class="divRow">
                        <div class="divCellIzq letra">Numero Ganador #<?= $x+1; ?></div>
                        <div class="divCellcen"><input value="<?php echo $row[1]['NumeroGanador'] ?>"
                                                       name="numerosganadores[]" class="campotexto letra"/></div>
                    </div>
                    <div class="divRow">
                        <div class="divCellIzq letra">Numero Ganador #<?= $x+2; ?></div>
                        <div class="divCellcen"><input value="<?php echo $row[2]['NumeroGanador'] ?>"
                                                       name="numerosganadores[]" class="campotexto letra"/></div>
                    </div>
                    <?php endif?>
                <?php
                }
                ?>
            </div>

            <input type="button" value="Cambiar numero ganador" class="button" id="BotonNumeroGanador"
                   onclick="javascript:FormularioCambiarNumeroGanador();">

            <input type="button" value="Reenviar Correos" class="button" id="BotonFormulario"
                   onclick="javascript:FormularioReenviarGanadores();">
        </form>
    </div>
    <div id="Resultado"></div>
    <br><br>
<?php
} else
{
    ?>
    <h3>Atencion! : Sorteo indicado no existe en base de datos</h3>
<?php } ?>