<?php
session_start();

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

//query for get sorteos of day
$sqlSorteosDelDia = "SELECT SP.ID AS sorteo_prog_id, SD.NombreSorteo as nombre_sorteo, SP.NumeroDeSuerte
FROM SorteosProgramacion SP
JOIN SorteosDefinicion SD
    ON SP.IDSorteoDefinicion = SD.ID
WHERE DATEDIFF(NOW(), SP.FechayHora) = 0
 ORDER BY SD.NombreSorteo ASC
";

try
{
    $stmtSorteosDelDia = $pdoConn->prepare($sqlSorteosDelDia);
    $stmtSorteosDelDia->execute();
    $sorteosDelDia = $stmtSorteosDelDia->fetchAll(PDO::FETCH_ASSOC);
} catch (PDOException $e)
{
    $sorteosDelDia = array();
    echo $e->getMessage();
}

//stmnt to obtain users "vendedores callle"
$sqlVendedores = "SELECT U.ID as usuario_id, U.NombreUsuario as nombre_usuario
FROM Usuarios_View U
WHERE U.ID IN (SELECT S.IDUsuario FROM SorteoApuesta_Validas S WHERE S.IDSorteoProgramacion = :sorteo_prog_id)
order by U.NombreUsuario";

$stmtVendedores = $pdoConn->prepare($sqlVendedores);

//stmt to obtain total for sorteo and user
$sqlTotalSorteo = "SELECT sp.ID, sd.NombreSorteo, SUM(sa.Cantidad) as total
                            FROM SorteosProgramacion sp
                              JOIN SorteosDefinicion sd on sd.ID = sp.IDSorteoDefinicion and DATEDIFF(sp.FechayHora,NOW()) = 0
                              JOIN SorteoApuesta_Validas sa on sa.IDSorteoProgramacion = sp.ID and sa.IDUsuario = :id_usuario
                              WHERE sp.ID = :sorteo_prog_id
                            group by sp.ID, sd.NombreSorteo
                            order by sp.FechayHora
                            ";
$stmtTotalSorteo = $pdoConn->prepare($sqlTotalSorteo);

//stmt to obtain status from user-sorteo
$sqlStatusSorteo = "SELECT RS.id, S.name, S.display_name
FROM COF_RevisionStatus_View RS
JOIN COF_Status S ON RS.status_id = S.id
WHERE RS.sorteo_programacion_id = :sorteo_prog_id AND RS.usuario_vendedor_calle_id = :user_id";
$stmtStatusSorteo = $pdoConn->prepare($sqlStatusSorteo);
?>

<ul class="tabs" style="height: 1000px">
<?php foreach ($sorteosDelDia as $sorteoDelDia): ?>
    <?php
    $sorteoDelDiaId = $sorteoDelDia['sorteo_prog_id'];
    $stmtVendedores->execute(array(':sorteo_prog_id' => $sorteoDelDiaId));
    $vendedores = $stmtVendedores->fetchAll(PDO::FETCH_ASSOC);
    ?>

    <li>
        <?php
        $id = 0;
        if(isset($_GET['ID'])){
            $id = $_GET['ID'];
        }
        ?>

        <?php if($id ==$sorteoDelDiaId){?>
           <input type="radio" checked name="tabs" id="tab<?php echo $sorteoDelDiaId?>" >
        <?php }else{?>
            <input type="radio" name="tabs" id="tab<?php echo $sorteoDelDiaId?>" >
        <?php }?>


            <label for="tab<?php echo $sorteoDelDiaId?>" class="labeltabs" onclick="msg(<?php echo $sorteoDelDiaId?>);"><?php echo $sorteoDelDia['nombre_sorteo'] ?></label>


    <div id="tab-content<?php echo $sorteoDelDiaId?>" class="tab-content animated fadeIn">
    <div class="divTable">
        <h3><?php echo $sorteoDelDia['nombre_sorteo'] . "   " . " - Jugada: " . $sorteoDelDia['NumeroDeSuerte'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="various fancybox.ajax"
              href="seccion_consultas_revision_importacion.php?SID=<?php echo $sorteoDelDiaId ?>" >
             Importacion</a>
        </h3>
        <div class="divCellCalendario">
            <div class="SorteosCalendarizados">
                <div class="divTable" style="width:100%;">
                    <div class="divRowHeader">
                        <div class="divCellHeader letra2" style="width:15em;">Vendedor</div>
                        <div class="divCellHeader letra2" style="width:15em;"
                             id="total_div_<?php echo $sorteoDelDiaId ?>">Total
                        </div>
                        <div class="divCellHeader letra2" style="width:10em;">Estado</div>
                        <div class="divCellHeader letra2" style="width:10em;">Accion</div>
                    </div>
                    <?php $allTotal = 0; ?>
                    <?php foreach ($vendedores as $vendedor): ?>
                        <?php
                        $vendedorId = $vendedor['usuario_id'];
                        $stmtTotalSorteo->execute(array(':id_usuario' => $vendedorId, ':sorteo_prog_id' => $sorteoDelDiaId));
                        $total = $stmtTotalSorteo->fetch(PDO::FETCH_ASSOC);
                        $allTotal += $total['total'];
                        $stmtStatusSorteo->execute(array(':sorteo_prog_id' => $sorteoDelDiaId, ':user_id' => $vendedorId));
                        $statusRow         = $stmtStatusSorteo->fetch(PDO::FETCH_ASSOC);
                        $status            = "";
                        $cssClass          = "";
                        $actionDisplayText = "--";
                        $actionName        = "";
                        switch ($statusRow['name'])
                        {
                            case 'sent':
                                $status            = "Solicita Revision";
                                $cssClass          = "lista-solicita-revision";
                                $actionDisplayText = "Marcar Revisado";
                                $actionName        = "check_correct";
                                break;
                            case 'check_correct':
                                $status            = "Revisado";
                                $cssClass          = "lista-revisado";
                                $actionDisplayText = "--";
                                break;
                            default:
                                $status = "Sin solicitud";
                                break;
                        }
                        ?>
                        <div class="divRow <?php echo $cssClass; ?> ">
                            <div class="divCellCen letra"
                                 style="width:15em;"><?php echo $vendedor['nombre_usuario']; ?></div>
                            <div class="divCellCen letra"
                                 style="width:15em;"><?php echo system_number_money_format($total['total']); ?></div>

                            <div class="divCellCen letra" style="width:10em;">
                                <?php echo $status; ?>
                            </div>

                            <div class="divCellCen letra" style="width:10em;"><a
                                    class="action_trigger" href="#"
                                    data-sorteo_prog_id="<?php echo $sorteoDelDiaId ?>"
                                    data-vendedor_id="<?php echo $vendedorId ?>"
                                    data-next_action="<?php echo $actionName ?>">
                                    <?php echo $actionDisplayText ?> </a></div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>

        </div>
        <?php if ($allTotal > 0): ?>
            <script>
                $('#total_div_<?php echo $sorteoDelDiaId ?>').html(" <?php echo "Total <span><br/>" . system_number_money_format($allTotal) . "</span>" ?> ");
            </script>
            <?php $allTotal = 0; ?>
        <?php endif; ?>
    </div>
    </div>
    </li>
<?php endforeach; ?>
</ul>

<script>
    function msg(id){
        location.hash = id;
    }

    $(document).ready(function () {
        $(".various").fancybox({
            fitToView: true,
            width: '100%',
            height: '70%',
            autoSize: true,
            closeClick: false,
            openEffect: 'fade',
            closeEffect: 'elastic'
        });
    });



</script>

<style type="text/css">
    .fancybox-custom .fancybox-skin {
        box-shadow: 0 0 50px #222;
    }

    input[type=checkbox] {
        visibility: visible;
        height: 15px;
    }
</style>




