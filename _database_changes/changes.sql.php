<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 10/30/14
 * Time: 5:22 PM
 */

$changes = array();

$changes['create_table_usuario_calle_params'] =
    "CREATE  TABLE `Luck`.`USUARIOS_CALLE_venta_parametros` (
  `IDUsuario` INT NOT NULL ,
  `IDSorteoDefinicion` INT NOT NULL ,
  `porcentaje_restriccion` INT NOT NULL ,
  `tope_inicial` INT NOT NULL ,
  PRIMARY KEY (`IDUsuario`, `IDSorteoDefinicion`) );";

$changes['importacion_listas_archivos_a_subir'] = "CREATE TABLE Luck.IL_ArchivosASubir
(
    id int PRIMARY KEY NOT NULL,
    name nchar (30) NOT NULL,
    full_name nchar (30) NOT NULL,
    full_path nchar (75) NOT NULL,
    IDUsuario int NOT NULL,
    import_id nchar (15) NOT NULL
);
ALTER TABLE Luck.IL_ArchivosASubir ADD CONSTRAINT unique_id UNIQUE (id);";

$changes['importacion_listas_archivo'] = "CREATE TABLE Luck.IL_Archivo
(
    file_id int NOT NULL,
    number int NOT NULL,
    amount int NOT NULL
);";

$changes['importacion_listas_archivos_subir_autoincrement'] = "ALTER TABLE IL_ArchivosASubir MODIFY COLUMN id int(11) NOT NULL AUTO_INCREMENT;";

$changes['importacion_listas_archivos_subir_index_archivo_archivosubir'] = "ALTER TABLE IL_Archivo ADD FOREIGN KEY
IL_Archivo_ArchivosASubir (file_id)
REFERENCES IL_ArchivosASubir (id)
ON DELETE CASCADE;";

$changes['printer_type'] = "ALTER TABLE Luck.Usuarios ADD printer_type varchar(5) NULL;";


$changes['confirmation_lists_prep'] = "ALTER TABLE `Luck`.`SorteosDefinicion` ENGINE = InnoDB ;";

$changes['confirmation_lists_revision'] = "CREATE  TABLE `Luck`.`COF_Revision` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `usuario_vendedor_calle_id` INT(11) NOT NULL ,
  `sorteo_programacion_id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_COF_Status_SorteoProgramacion_idx` (`sorteo_programacion_id` ASC) ,
  INDEX `fk_COF_Status_SorteoDefinicion_idx` (`sorteo_definicion_id` ASC) ,
  INDEX `fk_COF_Revision_Usuarios_idx` (`usuario_vendedor_calle_id` ASC) ,
  CONSTRAINT `fk_COF_Revision_Usuarios`
    FOREIGN KEY (`usuario_vendedor_calle_id` )
    REFERENCES `Luck`.`Usuarios` (`ID` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_COF_Revision_SorteoProgramacion`
    FOREIGN KEY (`sorteo_programacion_id` )
    REFERENCES `Luck`.`SorteosProgramacion` (`ID` )
    ON DELETE CASCADE
    ON UPDATE CASCADE
    )
ENGINE = InnoDB;
";

$changes['confirmation_lists_status'] = "CREATE TABLE `COF_Status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  `display_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1";

$changes['confirmation_lists_revision_log'] = "CREATE TABLE `COF_Revision_Log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cof_revision_id` int(11) NOT NULL,
  `cof_new_status_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_COF_Revision_Log_Revision_idx` (`cof_revision_id`),
  KEY `fk_COF_Revision_Log_Cof_Status_idx` (`cof_new_status_id`),
  CONSTRAINT `fk_COF_Revision_Log_Cof_Revision` FOREIGN KEY (`cof_revision_id`) REFERENCES `COF_Revision` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_COF_Revision_Log_Cof_Status` FOREIGN KEY (`cof_new_status_id`) REFERENCES `COF_Status` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1";

$changes['confirmation_lists_revision_log_add_user'] = "ALTER TABLE `Luck`.`COF_Revision_Log` ADD COLUMN `user_id` INT(11) NOT NULL  AFTER `created_at` ;";

$changes['confirmation_lists_revision_log_add_fk'] = "ALTER TABLE `Luck`.`COF_Revision_Log`
  ADD CONSTRAINT `fk_COF_Revision_Log_Usuarios`
  FOREIGN KEY (`user_id` )
  REFERENCES `Luck`.`Usuarios` (`ID` )
  ON DELETE CASCADE
  ON UPDATE CASCADE
, ADD INDEX `fk_COF_Revision_Log_Usuarios_idx` (`user_id` ASC) ;
";

$changes['confirmation_lists_revision_status_values'] = "INSERT INTO COF_Status (name, display_name)
VALUES ('sent','Enviado'),
  ('on_revision','En Revision'),
  ('check_correct','Revisado');";

$changes['confirmation_lists_revision_log_view'] = "CREATE  OR REPLACE VIEW `Luck`.`COF_Last_RevisionLog_View` AS
select cof_revision_id ,max(id) as last_id  from COF_Revision_Log rl group by cof_revision_id;
";

$changes['confirmation_lists_revision_status_view'] = "
CREATE  OR REPLACE VIEW `Luck`.`COF_RevisionStatus_View` AS
SELECT r.id, r.sorteo_programacion_id, r.usuario_vendedor_calle_id
  , rl.cof_new_status_id as status_id
  , rl.created_at as changed_date
  , rl.user_id as changed_by_user_id
from COF_Revision r
  join COF_Last_RevisionLog_View last_rev_log
    on last_rev_log.cof_revision_id = r.id
  join COF_Revision_Log rl on rl.id = last_rev_log.last_id;
";

$changes['vendedor_totales'] = "CREATE  TABLE `Luck`.`Vendedor_Calle_Totales` (
  `IDUsuario` INT NOT NULL ,
  `IDSorteoProgramacion` INT NOT NULL ,
  `numero` INT NOT NULL ,
  `monto` INT NOT NULL ,
  PRIMARY KEY (`IDUsuario`, `IDSorteoProgramacion`, `numero`) ,
  INDEX `fk_Vendedor_Calle_Totales_Usuarios_idx` (`IDUsuario` ASC) ,
  INDEX `fk_Vendedor_Calle_Totales_SorteoProgramacion_idx` (`IDSorteoProgramacion` ASC) ,
  CONSTRAINT `fk_Vendedor_Calle_Totales_Usuarios`
    FOREIGN KEY (`IDUsuario` )
    REFERENCES `Luck`.`Usuarios` (`ID` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Vendedor_Calle_Totales_SorteoProgramacion`
    FOREIGN KEY (`IDSorteoProgramacion` )
    REFERENCES `Luck`.`SorteosProgramacion` (`ID` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
";

$changes['par_event'] = "CREATE  TABLE `Luck`.`PAR_Event` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `display_until` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) );";

$changes['par_event_part'] = "CREATE  TABLE `Luck`.`PAR_Event_Part` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `sorteo_prog_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_PAR_Event_Part_Sorteo_Programacion_Id_idx` (`sorteo_prog_id` ASC) ,
  CONSTRAINT `fk_PAR_Event_Part_Sorteo_Programacion_Id`
    FOREIGN KEY (`sorteo_prog_id` )
    REFERENCES `Luck`.`SorteosProgramacion` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
";

$changes['par_event_part_add_event_id'] = "ALTER TABLE `Luck`.`PAR_Event_Part` ADD COLUMN `par_event_id` INT NOT NULL  AFTER `id` ,
  ADD CONSTRAINT `fk_PAR_Event_Part_Par_Event`
  FOREIGN KEY (`par_event_id` )
  REFERENCES `Luck`.`PAR_Event` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE
, ADD INDEX `fk_PAR_Event_Part_Par_Event_idx` (`par_event_id` ASC) ;
";

$changes['par_ticket'] = "CREATE  TABLE `Luck`.`PAR_Ticket` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `par_event_id` INT NOT NULL ,
  `usuario_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_PAR_Ticket_PAR_Event_idx` (`par_event_id` ASC) ,
  INDEX `fk_PAR_Ticket_Usuarios_idx` (`usuario_id` ASC) ,
  CONSTRAINT `fk_PAR_Ticket_PAR_Event`
    FOREIGN KEY (`par_event_id` )
    REFERENCES `Luck`.`PAR_Event` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PAR_Ticket_Usuarios`
    FOREIGN KEY (`usuario_id` )
    REFERENCES `Luck`.`Usuarios` (`ID` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
";

$changes['par_ticket_add_total'] = "ALTER TABLE `Luck`.`PAR_Ticket` ADD COLUMN `total` INT NOT NULL  AFTER `usuario_id` ;";

$changes['par_ticket_add_created_at'] = "ALTER TABLE `Luck`.`PAR_Ticket` ADD COLUMN `created_at` DATETIME NOT NULL  AFTER `total`;";

$changes['par_ticket_detail'] = "CREATE TABLE `PAR_Ticket_Detail` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `par_bet_type_id` int(11) NOT NULL,
                  `amount` int(11) NOT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `id_UNIQUE` (`id`),
                  KEY `fk_PAR_Ticket_Detail_Par_Bet_Types_idx` (`par_bet_type_id`),
                  CONSTRAINT `fk_PAR_Ticket_Detail_Par_Bet_Types` FOREIGN KEY (`par_bet_type_id`) REFERENCES `PAR_Bet_Types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

$changes['par_ticket_detail_add_ticket_reference'] = "ALTER TABLE `Luck`.`PAR_Ticket_Detail` ADD COLUMN `par_ticket_id` INT NOT NULL  AFTER `id` ,
  ADD CONSTRAINT `fk_PAR_Ticket_Detail_Par_Ticket`
  FOREIGN KEY (`par_ticket_id` )
  REFERENCES `Luck`.`PAR_Ticket` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE
, ADD INDEX `fk_PAR_Ticket_Detail_Par_Ticket_idx` (`par_ticket_id` ASC) ;
";

$changes['par_bet_types'] = "CREATE  TABLE `Luck`.`PAR_Bet_Types` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(15) NOT NULL COMMENT 'Esta columna debe reflejar un nombre unico' ,
  `display_name` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;
";

$changes['par_ticket_bet'] = "CREATE  TABLE `Luck`.`PAR_Ticket_Bet` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `par_ticket_id` INT NOT NULL ,
  `par_bet_type_id` INT NOT NULL ,
  `amount` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_PAR_Ticket_Bet_Par_Ticket_idx` (`par_ticket_id` ASC) ,
  INDEX `fk_PAR_Ticket_Bet_Par_Bet_Type_idx` (`par_bet_type_id` ASC) ,
  CONSTRAINT `fk_PAR_Ticket_Bet_Par_Ticket`
    FOREIGN KEY (`par_ticket_id` )
    REFERENCES `Luck`.`PAR_Ticket` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PAR_Ticket_Bet_Par_Bet_Type`
    FOREIGN KEY (`par_bet_type_id` )
    REFERENCES `Luck`.`PAR_Bet_Types` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
";

$changes['par_ticket_bet_part'] = "CREATE  TABLE `Luck`.`PAR_Ticket_Bet_Part` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `par_ticket_bet_id` INT NOT NULL ,
  `par_event_part_id` INT NOT NULL ,
  `number` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_PAR_Ticket_Bet_Part_PAR_Ticket_Bet_idx` (`par_ticket_bet_id` ASC) ,
  INDEX `fk_PAR_Ticket_Bet_Part_PAR_Event_Part_idx` (`par_event_part_id` ASC) ,
  CONSTRAINT `fk_PAR_Ticket_Bet_Part_PAR_Ticket_Bet`
    FOREIGN KEY (`par_ticket_bet_id` )
    REFERENCES `Luck`.`PAR_Ticket_Bet` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PAR_Ticket_Bet_Part_PAR_Event_Part`
    FOREIGN KEY (`par_event_part_id` )
    REFERENCES `Luck`.`PAR_Event_Part` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
";

$changes['par_seed_bet_types'] = "INSERT INTO `Luck`.`PAR_Bet_Types` (`name`, `display_name`) VALUES ('straight', 'Directo');
INSERT INTO `Luck`.`PAR_Bet_Types` (`name`, `display_name`) VALUES ('reverse', 'Reverso');
INSERT INTO `Luck`.`PAR_Bet_Types` (`name`, `display_name`) VALUES ('double_reverse', 'Doble Reverso');
";

$changes['par_scoring_hit_type'] = "CREATE  TABLE `Luck`.`PAR_scoring_hit_type` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(25) NOT NULL ,
  `hits_qty` INT NOT NULL COMMENT 'describe la cantidad de aciertos que una tiene apuesta' ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB;
";

$changes['par_scoring_hit_type_add_display_name'] = "ALTER TABLE `Luck`.`PAR_scoring_hit_type` ADD COLUMN `display_name` VARCHAR(45) NOT NULL  AFTER `name` ;";

$changes['par_scoring_hit_type_seed'] = "INSERT INTO `Luck`.`PAR_scoring_hit_type` (`name`,  `display_name`, `hits_qty`) VALUES ('single', 'Solo uno', '1');
INSERT INTO `Luck`.`PAR_scoring_hit_type` (`name`, `display_name`, `hits_qty`) VALUES ('complete', 'Completo', '2');";


$changes['par_scoring_parameters'] = "CREATE  TABLE `Luck`.`PAR_scoring_parameters` (
  `par_bet_type_id` INT NOT NULL ,
  `par_scoring_hit_type` INT NOT NULL ,
  `pays` INT NOT NULL ,
  PRIMARY KEY (`par_bet_type_id`, `par_scoring_hit_type`) )
ENGINE = InnoDB;
";

$changes['par_scoring_paremeters_add_fk'] = "ALTER TABLE `Luck`.`PAR_scoring_parameters`
  ADD CONSTRAINT `fk_PAR_scoring_parameters_par_bet_type`
  FOREIGN KEY (`par_bet_type_id` )
  REFERENCES `Luck`.`PAR_Bet_Types` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_PAR_scoring_parameters_par_scoring_hit_type`
  FOREIGN KEY (`par_scoring_hit_type` )
  REFERENCES `Luck`.`PAR_scoring_hit_type` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `fk_PAR_scoring_parameters_par_bet_type_idx` (`par_bet_type_id` ASC)
, ADD INDEX `fk_PAR_scoring_parameters_par_scoring_hit_type_idx` (`par_scoring_hit_type` ASC) ;
";

$changes['par_ticket_bet_parameters'] = "CREATE  TABLE `Luck`.`PAR_Ticket_Bet_Parameters` (
  `id` INT NOT NULL ,
  `min` INT NOT NULL ,
  `max` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB;
";

$changes['par_ticket_bet_parameters_seed'] = "INSERT INTO `Luck`.`PAR_Ticket_Bet_Parameters` (`id`,`min`, `max`) VALUES (0,'2000', '5000')";

//para el scoring
$changes['par_event_result'] = "CREATE  TABLE `Luck`.`PAR_Event_Result` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `par_event_id` INT NOT NULL ,
  `par_event_part_id` INT NOT NULL ,
  `number` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_PAR_Event_Result_Event_idx` (`par_event_id` ASC) ,
  INDEX `fk_PAR_Event_Result_Event_Part_idx` (`par_event_part_id` ASC) ,
  CONSTRAINT `fk_PAR_Event_Result_Event`
    FOREIGN KEY (`par_event_id` )
    REFERENCES `Luck`.`PAR_Event` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PAR_Event_Result_Event_Part`
    FOREIGN KEY (`par_event_part_id` )
    REFERENCES `Luck`.`PAR_Event_Part` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
";

$changes['par_event_prize'] = "CREATE  TABLE `Luck`.`PAR_Event_Prize` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `par_event_id` INT NOT NULL ,
  `par_ticket_id` INT NOT NULL ,
  `par_ticket_bet_id` INT NOT NULL ,
  `par_scoring_hit_type_id` INT NOT NULL ,
  `par_bet_amount` INT NOT NULL ,
  `pays` INT NOT NULL COMMENT 'Este es el parametro de cuanto se le paga segun el tipo de apuesta y la cantidad de hits' ,
  `final_prize` INT NOT NULL COMMENT 'Esto contiene la multiplicacion de par_bet_amount y pays, esto da el precio final' ,
  `created_at` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_PAR_Event_Result_Par_Event_idx` (`par_event_id` ASC) ,
  INDEX `fk_PAR_Event_Prize_Par_Ticket_idx` (`par_ticket_id` ASC) ,
  INDEX `fk_PAR_Event_Prize_Par_Ticket_Bet_idx` (`par_ticket_bet_id` ASC) ,
  INDEX `fk_PAR_Event_Prize_Par_Scoring_Hit_Type_idx` (`par_scoring_hit_type_id` ASC) ,
  CONSTRAINT `fk_PAR_Event_Result_Par_Event`
    FOREIGN KEY (`par_event_id` )
    REFERENCES `Luck`.`PAR_Event` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PAR_Event_Prize_Par_Ticket`
    FOREIGN KEY (`par_ticket_id` )
    REFERENCES `Luck`.`PAR_Ticket` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PAR_Event_Prize_Par_Ticket_Bet`
    FOREIGN KEY (`par_ticket_bet_id` )
    REFERENCES `Luck`.`PAR_Ticket_Bet` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PAR_Event_Prize_Par_Scoring_Hit_Type`
    FOREIGN KEY (`par_scoring_hit_type_id` )
    REFERENCES `Luck`.`PAR_scoring_hit_type` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);
";

$changes['par_event_add_scored'] = "ALTER TABLE `Luck`.`PAR_Event` ADD COLUMN `scored` BIT NULL DEFAULT 0  AFTER `display_until` ;";

$changes['par_ticket_bet_add_scoring_hit'] = "ALTER TABLE `Luck`.`PAR_Ticket_Bet` ADD COLUMN `hits` INT NULL COMMENT 'Hits qty in bet parts'  AFTER `amount` , ADD COLUMN `scoring_hit_type_id` INT NULL  AFTER `hits` ,
  ADD CONSTRAINT `fk_PAR_Ticket_Bet_Par_Scoring_Hit_Type`
  FOREIGN KEY (`scoring_hit_type_id` )
  REFERENCES `Luck`.`PAR_scoring_hit_type` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE
, ADD INDEX `fk_PAR_Ticket_Bet_Par_Scoring_Hit_Type_idx` (`scoring_hit_type_id` ASC) ;
";

//changes for set scoring one hit with the first event only
$changes['par_event_part_add_hitOrder'] = "ALTER TABLE `Luck`.`PAR_Event_Part` ADD COLUMN `hit_order` TINYINT NULL  AFTER `sorteo_prog_id` ;";

$changes['par_event_part_add_has_hit_first_part|hits_valid|'] = "ALTER TABLE `Luck`.`PAR_Ticket_Bet` CHANGE COLUMN `hits` `hits` TINYINT NULL DEFAULT NULL COMMENT 'Hits qty in bet parts'  , ADD COLUMN `has_hit_first_part` BIT NULL DEFAULT 0  AFTER `hits` , ADD COLUMN `valid_hits` TINYINT NULL  AFTER `has_hit_first_part` ;";

//validation of limits in number combinations for event
$changes['par_val_validation'] = "CREATE  TABLE `Luck`.`PAR_VAL_Validation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL ,
  `created_at` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_PAR_VAL_Validation_User_idx` (`user_id` ASC) ,
  CONSTRAINT `fk_PAR_VAL_Validation_User`
    FOREIGN KEY (`user_id` )
    REFERENCES `Luck`.`Usuarios` (`ID` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
";


$changes['par_val_combination'] = "CREATE  TABLE `Luck`.`PAR_VAL_Combination` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `validation_id` INT NOT NULL ,
  `par_event_id` INT NOT NULL ,
  `par_bet_type_id` INT NOT NULL ,
  `amount` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_PAR_VAL_Combination_PAR_VAL_Validation_idx` (`validation_id` ASC) ,
  INDEX `fk_PAR_VAL_Combination_PAR_Event_idx` (`par_event_id` ASC) ,
  INDEX `fk_PAR_VAL_Combination_PAR_Bet_Type_idx` (`par_bet_type_id` ASC) ,
  CONSTRAINT `fk_PAR_VAL_Combination_PAR_VAL_Validation`
    FOREIGN KEY (`validation_id` )
    REFERENCES `Luck`.`PAR_VAL_Validation` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PAR_VAL_Combination_PAR_Event`
    FOREIGN KEY (`par_event_id` )
    REFERENCES `Luck`.`PAR_Event` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PAR_VAL_Combination_PAR_Bet_Type`
    FOREIGN KEY (`par_bet_type_id` )
    REFERENCES `Luck`.`PAR_Bet_Types` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
";

$changes['par_val_combination_part'] = "CREATE  TABLE `Luck`.`PAR_VAL_Combination_Part` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `val_combination_id` INT NOT NULL ,
  `number` INT NOT NULL ,
  `par_event_part_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_PAR_VAL_Combination_Part_PAR_Event_Part_idx` (`par_event_part_id` ASC) ,
  INDEX `fk_PAR_VAL_Combination_Part_PAR_VAL_Combination_idx` (`val_combination_id` ASC) ,
  CONSTRAINT `fk_PAR_VAL_Combination_Part_PAR_Event_Part`
    FOREIGN KEY (`par_event_part_id` )
    REFERENCES `Luck`.`PAR_Event_Part` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PAR_VAL_Combination_Part_PAR_VAL_Combination`
    FOREIGN KEY (`val_combination_id` )
    REFERENCES `Luck`.`PAR_VAL_Combination` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
";

$changes['par_lim_parameters'] = "CREATE  TABLE `Luck`.`PAR_LIM_Parameters` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `par_bet_type_id` INT NOT NULL ,
  `limit_amount` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_PAR_LIM_Parameters_PAR_Bet_Type_idx` (`par_bet_type_id` ASC) ,
  CONSTRAINT `fk_PAR_LIM_Parameters_PAR_Bet_Type`
    FOREIGN KEY (`par_bet_type_id` )
    REFERENCES `Luck`.`PAR_Bet_Types` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);
";

//second part of validation db structure
$changes['par_event_combination'] = "CREATE  TABLE `Luck`.`PAR_Event_Combination` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `par_event_id` INT NOT NULL ,
  `par_bet_type_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_PAR_Event_Combination_PAR_Event_idx` (`par_event_id` ASC) ,
  INDEX `fk_PAR_Event_Combination_PAR_Bet_Type_idx` (`par_bet_type_id` ASC) ,
  CONSTRAINT `fk_PAR_Event_Combination_PAR_Event`
    FOREIGN KEY (`par_event_id` )
    REFERENCES `Luck`.`PAR_Event` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PAR_Event_Combination_PAR_Bet_Type`
    FOREIGN KEY (`par_bet_type_id` )
    REFERENCES `Luck`.`PAR_Bet_Types` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
";

$changes['par_event_combination_part'] = "CREATE  TABLE `Luck`.`PAR_Event_Combination_Part` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `par_event_combination_id` INT NOT NULL ,
  `number` INT NOT NULL ,
  `par_event_part_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_PAR_Event_Combination_Part_PAR_Event_Combination_idx` (`par_event_combination_id` ASC) ,
  INDEX `fk_PAR_Event_Combination_Part_PAR_Event_Part_idx` (`par_event_part_id` ASC) ,
  CONSTRAINT `fk_PAR_Event_Combination_Part_PAR_Event_Combination`
    FOREIGN KEY (`par_event_combination_id` )
    REFERENCES `Luck`.`PAR_Event_Combination` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PAR_Event_Combination_Part_PAR_Event_Part`
    FOREIGN KEY (`par_event_part_id` )
    REFERENCES `Luck`.`PAR_Event_Part` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);
";

$changes['par_event_total_combinations'] = "CREATE  TABLE `Luck`.`PAR_Event_Total_Combinations` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `par_event_id` INT NOT NULL ,
  `par_event_combination_id` INT NOT NULL ,
  `amount` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_PAR_Event_Total_Combinations_PAR_Event_idx` (`par_event_id` ASC) ,
  INDEX `fk_PAR_Event_Total_Combinations_PAR_Event_Combination_idx` (`par_event_combination_id` ASC) ,
  CONSTRAINT `fk_PAR_Event_Total_Combinations_PAR_Event`
    FOREIGN KEY (`par_event_id` )
    REFERENCES `Luck`.`PAR_Event` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PAR_Event_Total_Combinations_PAR_Event_Combination`
    FOREIGN KEY (`par_event_combination_id` )
    REFERENCES `Luck`.`PAR_Event_Combination` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);
";

$changes['par_val_combinations_match'] = "CREATE  TABLE `Luck`.`PAR_VAL_Combinations_Match` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `val_combination_id` INT NOT NULL ,
  `par_event_combination_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_PAR_VAL_Combinations_Match_VAL_Combination_idx` (`val_combination_id` ASC) ,
  INDEX `fk_PAR_VAL_Combinations_Match_PAR_Event_Combination_idx` (`par_event_combination_id` ASC) ,
  CONSTRAINT `fk_PAR_VAL_Combinations_Match_VAL_Combination`
    FOREIGN KEY (`val_combination_id` )
    REFERENCES `Luck`.`PAR_VAL_Combination` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PAR_VAL_Combinations_Match_PAR_Event_Combination`
    FOREIGN KEY (`par_event_combination_id` )
    REFERENCES `Luck`.`PAR_Event_Combination` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);
";

$changes['par_val_combinations_match_add_validation_id'] = "
ALTER TABLE `Luck`.`PAR_VAL_Combinations_Match` ADD COLUMN `val_validation_id` INT NOT NULL  AFTER `par_event_combination_id` ,
  ADD CONSTRAINT `fk_PAR_VAL_Combinations_Match_PAR_VAL_Validation`
  FOREIGN KEY (`val_validation_id` )
  REFERENCES `Luck`.`PAR_VAL_Validation` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE
, ADD INDEX `fk_PAR_VAL_Combinations_Match_PAR_VAL_Validation_idx` (`val_validation_id` ASC) ;
";

$changes['par_lim_parameters_set_pk_bet_type_id'] = "ALTER TABLE `Luck`.`PAR_LIM_Parameters` CHANGE COLUMN `id` `id` INT(11) NOT NULL
, DROP PRIMARY KEY
, ADD PRIMARY KEY (`par_bet_type_id`) ;
";

$changes['par_lim_parameters_set_remove_id'] = "ALTER TABLE `Luck`.`PAR_LIM_Parameters` DROP COLUMN `id` ;";