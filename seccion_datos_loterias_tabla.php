<?php
session_start();

if (!isset($_SESSION['ActivoFlag'])) {
	// Verifica si hay session creada, de lo contrario redirige al index
	header("Location: index.php?IDM=TO");
	exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");
?>
				<h3 style="line-height:1px;">Maestro de Loterias</h3>
				<h6 style="line-height:1px;">Administrar los distintos tipos de loteria disponible</h6>
				<?php
					$QUERY="SELECT * FROM SorteosDefinicion WHERE FlagActivo=1 ORDER BY Orden ASC";
					$rs=mysql_query($QUERY);
					
					if (mysql_affected_rows()>0) {
						?>
						<div class="divTable">
							<div class="divRow">
								<div class="divCellHeader2" style="width:120px;">Loteria</div>
								<div class="divCellHeader2">Num</div>
								<div class="divCellHeader2">L</div>
								<div class="divCellHeader2">M</div>
								<div class="divCellHeader2">M</div>
								<div class="divCellHeader2">J</div>
								<div class="divCellHeader2">V</div>
								<div class="divCellHeader2">S</div>
								<div class="divCellHeader2">D</div>
								<div class="divCellHeader2">P</div>
							</div>
						<?php
						while ($row=mysql_fetch_array($rs)) {
							?>
							<div class="divRow2 various2 fancybox.ajax" href="seccion_datos_loterias_delete.php?id=<?=$row['ID']; ?>" onclick="AbreVentanaLoterias();">
								<div class="divCellCen2" style="width:120px; text-align:left;"><?=$row['NombreSorteo']; ?></div>
								<div class="divCellCen2"><?=$row['NumerosGanadores']; ?></div>
								<div class="divCell<?php if ($row['LunesFlag']==1) 		echo "Marca"; else echo "NoMarca"; ?>"></div>
								<div class="divCell<?php if ($row['MartesFlag']==1) 		echo "Marca"; else echo "NoMarca";  ?>"></div>
								<div class="divCell<?php if ($row['MiercolesFlag']==1) 	echo "Marca"; else echo "NoMarca";  ?>"></div>
								<div class="divCell<?php if ($row['JuevesFlag']==1) 		echo "Marca"; else echo "NoMarca";  ?>"></div>
								<div class="divCell<?php if ($row['ViernesFlag']==1) 		echo "Marca"; else echo "NoMarca";  ?>"></div>
								<div class="divCell<?php if ($row['SabadoFlag']==1) 		echo "Marca"; else echo "NoMarca";  ?>"></div>
								<div class="divCell<?php if ($row['DomingoFlag']==1) 	echo "Marca"; else echo "NoMarca";  ?>"></div>
								<div class="divCellCen2"><?=$row['PagaPorcentaje']; ?></div>
							</div>							
							<?php
						}
						?>
						</div>
						<?php
					}
					else {
						?>
						<div style="text-align:center; height:80px; vertical-align:bottom;"><h6>No existen Loterias activas. <a class="various  fancybox.ajax" href="seccion_datos_loterias_add_form.php">Desea Crear una Loteria?</a></h6></div>
						<?php					
					}
				?>
