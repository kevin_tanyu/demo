<?php
session_start();
if (!isset($_SESSION['ActivoFlag']))
{
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");
// Incluye Header
include("header.php");

include("seccion_consultas_submenu.php");

//query for get sorteos of day
$sqlSorteosDelDia = "SELECT SP.ID AS sorteo_prog_id, SD.NombreSorteo as nombre_sorteo
FROM SorteosProgramacion SP
JOIN SorteosDefinicion SD
    ON SP.IDSorteoDefinicion = SD.ID
WHERE DATEDIFF(NOW(), SP.FechayHora) = 0
 ORDER BY SD.NombreSorteo ASC
";

try
{
    $stmtSorteosDelDia = $pdoConn->prepare($sqlSorteosDelDia);
    $stmtSorteosDelDia->execute();
    $sorteosDelDia = $stmtSorteosDelDia->fetchAll(PDO::FETCH_ASSOC);
} catch (PDOException $e)
{
    $sorteosDelDia = array();
    echo $e->getMessage();
}

?>

<div id="ContenedorGeneral">

    <h3 style="line-height:1px;">Lista de Numeros General</h3>
    <h6 style="line-height:1px;">Descargar la lista de numeros vendidos el dia de hoy.</h6>

    <?php foreach ($sorteosDelDia as $sorteoDelDia): ?>
        <h3 style="line-height:1px;">

            <h3> <?php echo $sorteoDelDia['nombre_sorteo']?></h3>


            <input type="button" id="btnDescargar" class="button"
                   value=" Guardar y Descargar" onclick="descargarExcel(<?php echo $sorteoDelDia['sorteo_prog_id']?>)">

        </h3>



    <?php endforeach?>


</div>

<script>
    function descargarExcel(idSorteo){


        var ajaxRequest = loadAjaxRequest();  // Carga el AJAX para que se pueda ejecutar
        //

        //Recibimos la varibale que nos envian del server y la mostramos en el lado del cliente
        ajaxRequest.onreadystatechange = function() {
            if (ajaxRequest.readyState == 4) {//Verfica si el estado del AJAX es correcto

                var url = ajaxRequest.responseText;
                //document.getElementById('btnDescargar').src = url;
                window.location = url;

            }
        }

        ajaxRequest.open("GET", "seccion_consultas_listas_numeros_download.php?SID=" + idSorteo, true);
        ajaxRequest.send(null);


    }//FIN descargarExcel

    //Funcion para ejecutar el AJAX
    function loadAjaxRequest(){
        var ajaxRequest;  // The variable that makes Ajax possible!

        try {
            // Opera 8.0+, Firefox, Safari
            ajaxRequest = new XMLHttpRequest();
        } catch (e) {
            // Internet Explorer Browsers
            try {
                ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {
                    // Something went wrong
                    alert("Your browser broke!");
                    return null;
                }
            }
        }
        return ajaxRequest;
    }
</script>

<?php require_once 'footer.php' ?>