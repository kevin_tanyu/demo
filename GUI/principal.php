<?php
session_start();

if (!isset($_SESSION['ActivoFlag'])) {
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: ../index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("../config.ini.php");
include("../conectadb.php");

// Incluye Header
include("header.php");


//PARA CAPTURAR EL BALANCE DE CADA BANCA
$sqlTotalLista = "SELECT SUM(monto) as 'total' FROM seg_lista_seguros_banca WHERE user_id = ? and sorteo_id = ?";
$stmtTotalLista = $pdoConn->prepare($sqlTotalLista);

//PARA CAPTURAR EL BALANCE DE CADA BANCA
$sqlBalances = "SELECT * FROM Balances_Bancas WHERE user_id = ? and sorteo_id = ?";
$stmtBalances = $pdoConn->prepare($sqlBalances);

//PARA CAPTURAR EL BALANCE ACUMULADO DE CADA BANCA
$sqlBalancesTotal = "SELECT * FROM Balances_Total_Banca WHERE user_id = ?";
$stmtBalancesTotal = $pdoConn->prepare($sqlBalancesTotal);
$stmtBalancesTotal->execute(array($_SESSION['IDUsuario']));
$balanceTotal = $stmtBalancesTotal->fetch();


$sqlNumeroGanador = "SELECT Numero FROM SorteosNumerosGanadores WHERE IDSorteoProgramacion = ? LIMIT 1";
$stmtNumeroGanador = $pdoConn->prepare($sqlNumeroGanador);

//Consulta para recuperar los sorteos del dia
$sqlSorteosDelDia = "SELECT SP.ID AS sorteo_prog_id, SD.NombreSorteo as nombre_sorteo, SP.NumeroDeSuerte
FROM SorteosProgramacion SP
JOIN SorteosDefinicion SD
    ON SP.IDSorteoDefinicion = SD.ID
WHERE DATEDIFF(NOW(), SP.FechayHora) = 0
 ORDER BY SD.NombreSorteo DESC
";

try
{
    $stmtSorteosDelDia = $pdoConn->prepare($sqlSorteosDelDia);
    $stmtSorteosDelDia->execute();
    $sorteosDelDia = $stmtSorteosDelDia->fetchAll(PDO::FETCH_ASSOC);
} catch (PDOException $e)
{
    $sorteosDelDia = array();
    echo $e->getMessage();
}



?>

<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> <?php echo $lang['BIENVENIDO']; ?></h3>
        <div class="row mt">
            <div class="col-lg-12">
                <p>El Dia de la Suerte  - 2015</p>
            </div>
        </div>


        <?php if($_SESSION['NivelUsuario'] == 5)://5 real?>
        <div class="row" >

            <div class="col-lg-4 col-md-4 col-sm-4 mb">
                <div class="weather-3 pn centered" style="height: 55px; width: 450px; background: #C9D3E6">
                    <a href="seccion_reportes_balances.php" style="color: darkblue">
                        <label class="centered" style="padding: 5px; font-size: 34px; color: #000000"><?php echo $lang['BALANCETOTAL']; ?>
                        <?php if($balanceTotal['balanceTotal'] >= 0){?>
                            <span style="color: #0B610B"><?php echo system_number_money_format($balanceTotal['balanceTotal'])?></span>
                        <?php }else{?>
                            <span style="color: #BF0000"><?php echo system_number_money_format($balanceTotal['balanceTotal'])?></span>
                        <?php }?>
                    </label>
                    </a>
                </div>
            </div>

        </div>

        <div class="row">
            <?php foreach ($sorteosDelDia as $sorteoDelDia): ?>
                <?php  $stmtBalances->execute(array($_SESSION['IDUsuario'], $sorteoDelDia['sorteo_prog_id']));
                       $balance = $stmtBalances->fetch();
                       $stmtNumeroGanador->execute(array( $sorteoDelDia['sorteo_prog_id']));
                       $ganador = $stmtNumeroGanador->fetch();
                       $stmtTotalLista->execute(array($_SESSION['IDUsuario'], $sorteoDelDia['sorteo_prog_id']));
                       $totalLista = $stmtTotalLista->fetch();
                       $porcen = $_SESSION['Comision'] / 100;
                       $comision = $porcen * $totalLista['total'];

                ?>
            <div class="col-md-4 col-sm-4 mb">
                <div class="stock card">
                    <div class="stock current-price">
                        <div class="row">
                            <div class="info col-sm-6 col-xs-6"><abbr><?php echo $lang['NUMEROGANADOR']; ?></abbr>
                                <time></time>
                            </div>
                            <div class="changes col-sm-6 col-xs-6">
                                <div class="value up" style="color: #000000"><i class="fa fa-check hidden-sm hidden-xs"></i> <?php echo $ganador['Numero'] ?></div>
                                <div class="change hidden-sm hidden-xs"></div>
                            </div>
                        </div>
                    </div>
                    <div class="stock current-price">
                        <div class="row">
                            <div class="info col-sm-6 col-xs-6"><abbr><?php echo $lang['TOTALLISTA']; ?></abbr>
                                <time></time>
                            </div>
                            <div class="changes col-sm-6 col-xs-6">
                                <div class="value up"><i class="fa fa-caret-up hidden-sm hidden-xs"></i> <?php echo system_number_money_format($totalLista['total']) ?></div>
                                <div class="change hidden-sm hidden-xs"></div>
                            </div>
                        </div>
                    </div>
                    <div class="stock current-price">
                        <div class="row">
                            <div class="info col-sm-6 col-xs-6"><abbr><?php echo $lang['COMISION']; ?></abbr>
                                <time></time>
                            </div>
                            <div class="changes col-sm-6 col-xs-6">
                                <div class="value down" style="color: red"><i class="fa fa-caret-down hidden-sm hidden-xs"></i> <?php echo system_number_money_format($comision) ?></div>
                                <div class="change hidden-sm hidden-xs"></div>
                            </div>
                        </div>
                    </div>
                    <div class="stock current-price">
                        <div class="row">
                            <div class="info col-sm-6 col-xs-6"><abbr><?php echo $lang['PREMIO']; ?></abbr>
                                <time></time>
                            </div>
                            <div class="changes col-sm-6 col-xs-6">
                                <?php if($balance['premio'] == 0){?>
                                    <div class="value up"><i class="fa fa-caret-up hidden-sm hidden-xs"></i> <?php echo system_number_money_format($balance['premio']) ?></div>
                                    <div class="change hidden-sm hidden-xs"></div>
                                <?php }else{?>
                                    <div class="value down" style="color: red"><i class="fa fa-caret-down hidden-sm hidden-xs"></i> <?php echo system_number_money_format($balance['premio']) ?></div>
                                    <div class="change hidden-sm hidden-xs"></div>
                                <?php }?>
                            </div>
                        </div>
                    </div>

                    <div class="stock current-price">
                        <div class="row">
                            <div class="info col-sm-6 col-xs-6"><abbr><?php echo $lang['BALANCE']; ?></abbr>
                                <time></time>
                            </div>
                            <div class="changes col-sm-6 col-xs-6">
                                <?php if( $balance['balance'] >= 0){?>
                                    <div class="value up"><i class="fa fa-caret-up hidden-sm hidden-xs"></i> <?php echo system_number_money_format($balance['balance']) ?></div>
                                    <div class="change hidden-sm hidden-xs"></div>
                                <?php }else{?>
                                    <div class="value down" style="color: red"><i class="fa fa-caret-down hidden-sm hidden-xs"></i> <?php echo system_number_money_format($balance['balance']) ?></div>
                                    <div class="change hidden-sm hidden-xs"></div>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <div class="summary">
                        <strong style="font-size: 20px"><?php echo $sorteoDelDia['nombre_sorteo'];?></strong> <span></span>
                    </div>
                </div>
            </div><! -- /col-md-4 -->
            <?php endforeach ?>
        </div>
        <?php endif ?>



    </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
<!--footer start-->
<?php include("footer.php");?>
<!--footer end-->