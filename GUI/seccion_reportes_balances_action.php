<?php
session_start();

// Incluye datos generales y conexion a DB
include("../config.ini.php");
include("../conectadb.php");

//CAPTURA LA FECHAS DEL FORM
$dateFrom = isset($_POST['fromDate']) ? $_POST['fromDate'] : date('Y-m-d');
$dateTo = isset($_POST['toDate']) ? $_POST['toDate'] . ' 23:59' : date('Y-m-d 23:59');
$dateToTran = date('Y-m-d H:i:s');
$transaccion = $_POST['transaccion'];

//SQL PARA RECUPERAR LOS SORTEOS ENTRE LAS FECHAS
if($transaccion == '1'){
    $sql = "SELECT SP.ID,SP.FechayHora,SD.NombreSorteo
            FROM SorteosProgramacion SP
            JOIN SorteosDefinicion SD ON SD.ID = SP.IDSorteoDefinicion
            WHERE SP.FechayHora BETWEEN ? AND ?
            ORDER BY SP.FechayHora DESC, SD.NombreSorteo";

    $query = $pdoConn->prepare($sql);
    $query->execute(array($dateFrom, $dateTo));

    $sqlTran = "SELECT TB.monto, TB.fechayhora, TT.Nombre
            FROM Transaccion_Bancas TB
            JOIN Transaccion_Tipo TT ON TB.tipo_trans_id = TT.id
            WHERE TB.fechayhora BETWEEN ? AND ? AND TB.id_banca = ?
            ORDER BY TB.fechayhora DESC";

    $queryTran = $pdoConn->prepare($sqlTran);
    /*$queryTran->execute(array($dateFrom, $dateTo, $_SESSION['IDUsuario']));
    $transacciones = $queryTran->fetchAll(PDO::FETCH_ASSOC);*/

}elseif($transaccion == 'DEBITO'){

    $sqlTran = "SELECT TB.monto, TB.fechayhora, TT.Nombre
            FROM Transaccion_Bancas TB
            JOIN Transaccion_Tipo TT ON TB.tipo_trans_id = TT.id
            WHERE TB.fechayhora BETWEEN ? AND ? AND TB.tipo_trans_id = 1 AND TB.id_banca = ?
            ORDER BY TB.fechayhora DESC";

    $queryTran = $pdoConn->prepare($sqlTran);
    $queryTran->execute(array($dateFrom, $dateTo, $_SESSION['IDUsuario']));
    $transacciones = $queryTran->fetchAll(PDO::FETCH_ASSOC);



}elseif($transaccion == 'RETIRO'){

    $sqlTran = "SELECT TB.monto, TB.fechayhora, TT.Nombre
            FROM Transaccion_Bancas TB
            JOIN Transaccion_Tipo TT ON TB.tipo_trans_id = TT.id
            WHERE TB.fechayhora BETWEEN ? AND ? AND TB.tipo_trans_id = 2 AND TB.id_banca = ?
            ORDER BY TB.fechayhora DESC";

    $queryTran = $pdoConn->prepare($sqlTran);
    $queryTran->execute(array($dateFrom, $dateTo, $_SESSION['IDUsuario']));
    $transacciones = $queryTran->fetchAll(PDO::FETCH_ASSOC);

}else{
    $sql = "SELECT SP.ID,SP.FechayHora,SD.NombreSorteo
            FROM SorteosProgramacion SP
            JOIN SorteosDefinicion SD ON SD.ID = SP.IDSorteoDefinicion
            WHERE SP.FechayHora BETWEEN ? AND ? AND NombreSorteo = ?
            ORDER BY SP.FechayHora DESC, SD.NombreSorteo";

    $query= $pdoConn->prepare($sql);
    $query->execute(array($dateFrom, $dateTo, $transaccion));

}

//PARA CAPTURAR EL BALANCE DE CADA BANCA
$sqlBalances = "SELECT * FROM Balances_Bancas WHERE user_id = ? and sorteo_id = ?";
$stmtBalances = $pdoConn->prepare($sqlBalances);
$totalLista = 0;
if(isSet($_GET['lang']))
{
    $lang = $_GET['lang'];

// register the session and set the cookie
    $_SESSION['lang'] = $lang;

    setcookie('lang', $lang, time() + (3600 * 24 * 30));
}
else if(isSet($_SESSION['lang']))
{
    $lang = $_SESSION['lang'];
}
else if(isSet($_COOKIE['lang']))
{
    $lang = $_COOKIE['lang'];
}
else
{
    $lang = 'en';
}

switch ($lang) {
    case 'en':
        $lang_file = 'lang.en.php';
        break;

    case 'es':
        $lang_file = 'lang.es.php';
        break;

    default:
        $lang_file = 'lang.en.php';

}

include_once './lang/'.$lang_file;
?>




<div class="content-panel">
    <!--<h4><i class="fa fa-angle-right"></i> <?php //echo $lang['LISTATRANSACCION']; ?>   </h4>
    <h3>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <label id="lblTotal" style="padding-left: 630px"></label></h3>-->
    <hr>
    <table class="table">
        <thead>
        <tr>
            <th><?php echo $lang['DESCRIPCION']; ?></th>
            <th><?php echo $lang['FECHA']; ?></th>
            <th><?php echo $lang['MONTO']; ?></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>

        </tr>
        </thead>
        <tbody>
        <?php if ($transaccion == 'DEBITO' || $transaccion == 'RETIRO'): ?>
            <?php foreach ($transacciones as $trans): ?>
                <tr>
                    <?php if ($transaccion == 'DEBITO'){
                        $totalLista = $totalLista + $trans['monto'];?>
                        <td><?php echo $lang[$trans['Nombre']]; ?></td>
                    <?php }else{
                        $totalLista = $totalLista - $trans['monto'];?>
                        <td><?php echo $lang[$trans['Nombre']]; ?></td>
                    <?php } ?>
                    <td><?php echo system_date_formatENES($trans['fechayhora'], $lang['IDIO']); ?> </td>
                    <td><?php echo system_number_money_format($trans['monto']); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>

        <?php if ($transaccion != 'DEBITO' && $transaccion != 'RETIRO'): ?>
        <?php while ($row = $query->fetch(PDO::FETCH_ASSOC)): ?>

                <?php if ($transaccion == '1'){
                    $queryTran->execute(array($row['FechayHora'], $dateToTran, $_SESSION['IDUsuario']));
                    $transacciones = $queryTran->fetchAll(PDO::FETCH_ASSOC);
                    $dateToTran = $row['FechayHora'];?>

                    <?php foreach ($transacciones as $trans): ?>
                        <tr>
                            <?php if ($trans['Nombre'] == 'DEBITO'){
                                $totalLista = $totalLista + $trans['monto'];?>
                                <td><?php echo $lang[$trans['Nombre']]; ?></td>
                            <?php }else{
                                $totalLista = $totalLista - $trans['monto'];?>
                                <td><?php echo $lang[$trans['Nombre']]; ?></td>
                            <?php } ?>
                            <td><?php echo system_date_formatENES($trans['fechayhora'], $lang['IDIO']); ?> </td>
                            <td><?php echo system_number_money_format($trans['monto']); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php } ?>
            <?php $stmtBalances->execute(array($_SESSION['IDUsuario'], $row['ID']));
                  $balance = $stmtBalances->fetch();
                  $totalLista = $totalLista + $balance['balance'];
                  ?>
            <tr>
                <td><?php echo $row['NombreSorteo']; ?></td>
                <td><?php echo system_date_formatENES($row['FechayHora'], $lang['IDIO']); ?> </td>
                <td><?php echo system_number_money_format($balance['balance']); ?>
                </td>
            </tr>
        <?php endwhile; ?>
        <?php if ($transaccion == '1'){
                $dateFrom = date("Y-m-d 00:00", strtotime($dateFrom));;
                $queryTran->execute(array($dateFrom, $dateToTran, $_SESSION['IDUsuario']));
                $transacciones = $queryTran->fetchAll(PDO::FETCH_ASSOC); ?>
                <?php foreach ($transacciones as $trans): ?>
                    <tr>
                        <?php if ($trans['Nombre'] == 'DEBITO'){
                            $totalLista = $totalLista + $trans['monto'];?>
                            <td><?php echo $lang[$trans['Nombre']]; ?></td>
                        <?php }else{
                            $totalLista = $totalLista - $trans['monto'];?>
                            <td><?php echo $lang[$trans['Nombre']]; ?></td>
                        <?php } ?>
                        <td><?php echo system_date_formatENES($trans['fechayhora'], $lang['IDIO']); ?> </td>
                        <td><?php echo system_number_money_format($trans['monto']); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
        <?php } ?>

        <?php endif; ?>

        </tbody>
    </table>
</div><! --/content-panel -->

<script type="text/javascript">
    //$('#lblTotal').html(" <?php //echo "Total: " . system_number_money_format($totalLista)  ?> ");
    $(document).ready(function() {

        $('.fancybox').fancybox({
            fitToView: true,
            width: '24%',
            height: '30%',
            autoSize: true,
            closeClick: false,
            openEffect: 'fade',
            closeEffect: 'elastic'
        });


    });
</script>
<style type="text/css">
    .fancybox-custom .fancybox-skin {
        box-shadow: 0 0 50px #222;
    }

</style>