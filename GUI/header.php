<?php

$nivelPermisoHouse = array(1);
$nivelPermisoAgente = array(2);
$nivelPermisoStore = array(3);
$nivelPermisoMachine = array(4);
$nivelPermisoBank = array(5);


if (!isset($_SESSION['IDUsuario']))
{
    header('Location: ../index.php');
    exit;
}

if(isSet($_GET['lang']))
{
    $lang = $_GET['lang'];

// register the session and set the cookie
    $_SESSION['lang'] = $lang;

    setcookie('lang', $lang, time() + (3600 * 24 * 30));
}
else if(isSet($_SESSION['lang']))
{
    $lang = $_SESSION['lang'];
}
else if(isSet($_COOKIE['lang']))
{
    $lang = $_COOKIE['lang'];
}
else
{
    $lang = 'en';
}

switch ($lang) {
    case 'en':
        $lang_file = 'lang.en.php';
        break;

    case 'es':
        $lang_file = 'lang.es.php';
        break;

    default:
        $lang_file = 'lang.en.php';

}

include_once './lang/'.$lang_file;
?>



<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>--ElDiaDeSuerte.net--</title>
    <script type="text/javascript" src="../js/jquery-2.1.3.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="../assets/css/style.css" rel="stylesheet">
    <link href="../assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <!--[DATEPICKER]>
    <script type="text/javascript" src="./datepicker/js/bootstrap-datepicker.js"></script>

    <![endif]-->


</head>

<body onload="getTime()">

<section id="container" >
    <!-- **********************************************************************************************************************************************************
    TOP BAR CONTENT & NOTIFICATIONS
    *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>
        <!--logo start-->
        <a href="principal.php" class="logo"><b>ElDiaDeSuerte.net</b></a>
        <!--logo end-->
        <div class="nav notify-row" id="top_menu">
            <!--  notification start -->
            <ul class="nav top-menu">
                <label style="color: #ffffff"><?php echo $lang['HORA']; ?></label>
                <div id="showtime2" style="position: relative"></div>
                <!-- inbox dropdown end -->

                <label style="color: #ffffff">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<?php echo $lang['FECHA']; ?></label>
                <div id="showtime2" style="position: relative; width: 150px"><?php echo date('d-m-Y');?></div>
            </ul>



            <!--  notification end -->
        </div>
        <div class="top-menu">
            <ul class="nav pull-right top-menu">
                <li><a class="logout" href="../index.php"><?php echo $lang['SALIR']; ?></a></li>
            </ul>
        </div>
    </header>
    <!--header end-->

    <!-- **********************************************************************************************************************************************************
    MAIN SIDEBAR MENU
    *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
        <div id="sidebar"  class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">

                <p class="centered"><a href=""><img src="../assets/img/user.png" class="img-circle" width="60"></a></p>
                <h5 class="centered"><?php if (strpos($_SESSION['NombreUsuario'],'IMPORT') === false) {
                                                 echo $_SESSION['NombreUsuario'];
                                               }else{echo substr($_SESSION['NombreUsuario'], 7);} ?>
                </h5>

                <li class="mt">
                    <a href="principal.php">
                        <i class="fa fa-dashboard"></i>
                        <span><?php echo $lang['PRINCIPAL']; ?></span>
                    </a>
                </li>

                <li class="sub-menu">
                    <a href="javascript:;" >
                        <i class="fa fa-file-text"></i>
                        <span><?php echo $lang['REPORTES']; ?></span>
                    </a>
                    <ul class="sub">
                        <?php if (in_array($_SESSION['NivelUsuario'], $nivelPermisoBank)
                            || in_array($_SESSION['NivelUsuario'], $nivelPermisoHouse)): ?>
                            <li><a href="seccion_reportes_lista_seguros.php"><?php echo $lang['SEGUROS']; ?></a></li>
                        <?php endif; ?>
                        <?php if (in_array($_SESSION['NivelUsuario'], $nivelPermisoBank)): ?>
                            <li><a href="seccion_reportes_balances.php"><?php echo $lang['BALANCE']; ?></a></li>
                        <?php endif; ?>

                    </ul>
                </li>

                <li class="sub-menu">
                    <a href="javascript:;" >
                        <i class="fa fa-comments-o"></i>
                        <span><?php echo $lang['IDIOMA']; ?></span>
                    </a>
                    <ul class="sub">
                        <li><a href="principal.php?lang=en"><?php echo $lang['INGLES']; ?></a></li>
                        <li><a href="principal.php?lang=es"><?php echo $lang['ESPANOL']; ?></a></li>
                    </ul>
                </li>

            </ul>
            <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->



<!-- js placed at the end of the document so the pages load faster -->
<script src="../assets/js/jquery.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="../assets/js/jquery.ui.touch-punch.min.js"></script>
<script class="include" type="text/javascript" src="../assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="../assets/js/jquery.scrollTo.min.js"></script>
<script src="../assets/js/jquery.nicescroll.js" type="text/javascript"></script>


<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="./fancybox/source/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="./fancybox/source/jquery.fancybox.css?v=2.1.4" media="screen" />


<!--common script for all pages-->
<script src="../assets/js/common-scripts.js"></script>

<!--script for this page-->

<script>

    function getTime()
    {
        var today=new Date();
        var h=today.getHours();
        var m=today.getMinutes();
        var s=today.getSeconds();
        // add a zero in front of numbers<10
        m=checkTime(m);
        s=checkTime(s);
        document.getElementById('showtime2').innerHTML=h+":"+m+":"+s;
        t=setTimeout(function(){getTime()},500);
    }

    function checkTime(i)
    {
        if (i<10)
        {
            i="0" + i;
        }
        return i;
    }

</script>

</body>
</html>