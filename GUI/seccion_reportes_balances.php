<?php
session_start();

if (!isset($_SESSION['ActivoFlag'])) {
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: ../index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("../config.ini.php");
include("../conectadb.php");

// Incluye Header
include("header.php");

$dateFrom = date('Y-m-d');
$dateTo = date('Y-m-d');
?>
<head>
    <link href="./datepicker/css/datepicker.css" rel="stylesheet"  type="text/css">

</head>

<!-- **********************************************************************************************************************************************************
     MAIN CONTENT
     *********************************************************************************************************************************************************** -->
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> <?php echo $lang['BALANCE']; ?></h3>
        <div class="row mt">
            <div class="col-lg-12">
                <p>
                <form>
                    <label for="dateFrom"><?php echo $lang['DE']; ?></label> <input type="text" value="<?php echo $dateFrom ?>" id="fromDate" class="datepicker">
                    <label for="dateFrom"><?php echo $lang['HASTA']; ?></label> <input type="text" value="<?php echo $dateFrom ?>" id="toDate" class="datepicker">
                    <select id="slcLoteria" style="font-size: 16px">
                        <option value="1">----</option>
                        <option value="COSTA RICA">COSTA RICA</option>
                        <option value="DIGITALES">DIGITALES</option>
                        <option value="MEDIO DIA">MEDIO DIA</option>
                        <option value="PANAMA">PANAMA</option>
                        <option value="DEBITO"><?php echo $lang['DEBITO']; ?></option>
                        <option value="RETIRO"><?php echo $lang['RETIROS']; ?></option>
                    </select>
                    <input type="submit" value="<?php echo $lang['VER']; ?>" class="button" onclick="enviarFechas(); return false"/>
                </form>

                <div id="result" style="margin-top: 2em">

                    </p>
                </div>
            </div>

    </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->


<!--main content end-->
<!--footer start-->
<?php include("footer.php");?>
<!--footer end-->
</section>

<script>
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<-Ant  ',
        nextText: '  Sig->',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'yy-mm-dd',
        firstDay: 0

    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

    $('.datepicker').datepicker();


    function enviarFechas(){
        var parametros = {
            "fromDate" : $('#fromDate').val(),
            "toDate" : $('#toDate').val(),
            "transaccion" : $('#slcLoteria').val()
        };
        $.ajax({
            data : parametros,
            url: 'seccion_reportes_balances_action.php',
            type: 'post',
            beforeSend: function(){
                $("#result").html("<?php echo $lang['ESPERA']; ?>");
            },
            success: function(response){
                $("#result").html(response);
            }
        });
    }




</script>