<?php

if (session_id() == '') session_start();

// Incluye datos generales y conexion a DB
require_once("../config.ini.php");
require_once("../conectadb.php");

//CAPTURA VALORES
$sorteoID = $_GET['SID'];
$userID = $_SESSION['IDUsuario'];

//SQL PARA RECUPERAR LOS NUMEROS Y MONTOS POR USUARIO
$sqlSeguros = "SELECT numero, monto FROM seg_lista_seguros_banca
        WHERE sorteo_id = :sorteo_id and user_id = :id_usuario
        ORDER BY numero ASC";
$stmtSeguros = $pdoConn->prepare($sqlSeguros);
$stmtSeguros->execute(array(':sorteo_id' => $sorteoID, ':id_usuario' => $userID));
$numerosSeguros = $stmtSeguros->fetchAll(PDO::FETCH_ASSOC);

$total = 0;

//SQL PARA RECUPERAR LA FECHA EN LA QUE SE GENERO EL ULTIMO REPORTE
$sqlHora = "SELECT Fecha FROM seg_lista_seguros_banca
            WHERE sorteo_id = :sorteo_id and user_id = :id_usuario
            ORDER BY id DESC LIMIT 1;";
$stmtHora = $pdoConn->prepare($sqlHora);
$stmtHora->execute(array(':sorteo_id' => $sorteoID, ':id_usuario' => $userID));
$hora = $stmtHora->fetchAll(PDO::FETCH_ASSOC);

if(isSet($_GET['lang']))
{
    $lang = $_GET['lang'];

// register the session and set the cookie
    $_SESSION['lang'] = $lang;

    setcookie('lang', $lang, time() + (3600 * 24 * 30));
}
else if(isSet($_SESSION['lang']))
{
    $lang = $_SESSION['lang'];
}
else if(isSet($_COOKIE['lang']))
{
    $lang = $_COOKIE['lang'];
}
else
{
    $lang = 'en';
}

switch ($lang) {
    case 'en':
        $lang_file = 'lang.en.php';
        break;

    case 'es':
        $lang_file = 'lang.es.php';
        break;

    default:
        $lang_file = 'lang.en.php';

}

include_once './lang/'.$lang_file;


?>
<head>
    <!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="../assets/css/style.css" rel="stylesheet">
    <link href="../assets/css/style-responsive.css" rel="stylesheet">
</head>
<center>
<div class="content-panel">
    <h4><i class="fa fa-angle-right"></i> <?php echo $lang['LISTANUMEROS']; ?></h4>
    <?php if($stmtHora->rowCount() > 0):?>
        <div class="divCellCen2" style="width: 250px; text-align: center; font-weight: bold; font-size: 16px; color: red">
            <?php echo $lang['RECIBIDO']; ?> <br> <?php echo $hora[0]['Fecha']; ?>
        </div>
    <?php endif?>
    <table class="table">
        <thead>
        <tr>
            <th><?php echo $lang['NUMERO']; ?></th>
            <th><?php echo $lang['MONTO']; ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($numerosSeguros as $numero):?>
          <tr>
            <th> <?php echo $numero['numero']; ?></th>
            <th> <?php echo system_number_money_format($numero['monto']); ?></th>
              <?php $total = $total + $numero['monto']; ?>
          </tr>
        <?php endforeach?>
        </tbody>
        <tbody>
        <tr style="color: #1e9d0d">
            <th>TOTAL</th>
            <th><?php echo system_number_money_format($total) ?></th>
        </tr>
        </tbody>
    </table>
</div>
</center>


