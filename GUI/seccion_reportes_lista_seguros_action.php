<?php
session_start();

// Incluye datos generales y conexion a DB
include("../config.ini.php");
include("../conectadb.php");

//CAPTURA LA FECHAS DEL FORM
$dateFrom = isset($_POST['fromDate']) ? $_POST['fromDate'] : date('Y-m-d');
$dateTo = isset($_POST['toDate']) ? $_POST['toDate'] . ' 23:59' : date('Y-m-d 23:59');

//SQL PARA RECUPERAR LOS SORTEOS ENTRE LAS FECHAS
$sql = "SELECT SP.ID,SP.FechayHora,SD.NombreSorteo
        FROM SorteosProgramacion SP
          JOIN SorteosDefinicion SD ON SD.ID = SP.IDSorteoDefinicion
        WHERE SP.FechayHora BETWEEN ? AND ?
        ORDER BY SP.FechayHora DESC, SD.NombreSorteo";

$query = $pdoConn->prepare($sql);
$result = $query->execute(array($dateFrom, $dateTo));

if(isSet($_GET['lang']))
{
    $lang = $_GET['lang'];

// register the session and set the cookie
    $_SESSION['lang'] = $lang;

    setcookie('lang', $lang, time() + (3600 * 24 * 30));
}
else if(isSet($_SESSION['lang']))
{
    $lang = $_SESSION['lang'];
}
else if(isSet($_COOKIE['lang']))
{
    $lang = $_COOKIE['lang'];
}
else
{
    $lang = 'en';
}

switch ($lang) {
    case 'en':
        $lang_file = 'lang.en.php';
        break;

    case 'es':
        $lang_file = 'lang.es.php';
        break;

    default:
        $lang_file = 'lang.en.php';

}

include_once './lang/'.$lang_file;


?>




<div class="content-panel">
    <h4><i class="fa fa-angle-right"></i> <?php echo $lang['LISTALOTERIA']; ?></h4>
    <hr>
    <table class="table">
        <thead>
        <tr>
            <th><?php echo $lang['SORTEO']; ?></th>
            <th><?php echo $lang['FECHA']; ?></th>
            <th><?php echo $lang['ACCION']; ?></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>

        </tr>
        </thead>
        <tbody>

        <?php while ($row = $query->fetch(PDO::FETCH_ASSOC)): ?>
            <tr>
                <td><?php echo $row['NombreSorteo']; ?></td>
                <td><?php echo system_date_formatENES($row['FechayHora'], $lang['IDIO']); ?> </td>
                <td><a class="various fancybox.ajax"
                       href="seccion_reportes_lista_seguros_display.php?SID=<?php echo $row['ID'] ?>" >
                        <?php echo $lang['REGISTRO']; ?></a>
                </td>
            </tr>
        <?php endwhile; ?>
        </tbody>
    </table>
</div><! --/content-panel -->

<script type="text/javascript">
    $(document).ready(function() {

        $('.various').fancybox({
            fitToView: true,
            width: '18%',
            height: '60%',
            autoSize: false,
            closeClick: false,
            openEffect: 'fade',
            closeEffect: 'elastic'
        });


    });
</script>
<style type="text/css">
    .fancybox-custom .fancybox-skin {
        box-shadow: 0 0 50px #222;
    }

</style>








