<?php

$lang = array();
$lang['IDIO']  = 'es';

/*HEADER*/
$lang['HORA'] = "Hora:";
$lang['FECHA'] = "Fecha:";
$lang['SALIR'] = "Salir";

/*PRINCIPAL*/
$lang['BIENVENIDO'] = "Bienvenido(a)";
$lang['BALANCETOTAL'] = "Balance Total:";
$lang['NUMEROGANADOR'] = "Numero Ganador";
$lang['TOTALLISTA'] = "Total de la Lista";
$lang['COMISION'] = "Comision";
$lang['PREMIO'] = "Premio";


/*MENU*/
$lang['PRINCIPAL'] = "Principal";
$lang['REPORTES'] = "Reportes";
$lang['IDIOMA'] = "Idioma";

/*SUBMENU REPORTES*/
$lang['SEGUROS'] = "Lista Seguros";
$lang['BALANCE'] = "Balance";

/*SUBMENU IDIOMA*/
$lang['INGLES'] = "Ingles";
$lang['ESPANOL'] = "Espanol";

/*OTROS*/
$lang['DE'] = "De";
$lang['HASTA'] = "Hasta ";
$lang['VER'] = "Ver";
$lang['ESPERA'] = "Procesando, espere por favor...";
$lang['SORTEO'] = "Sorteo";
$lang['LISTALOTERIA'] = "Lista de sorteos";
$lang['LISTATRANSACCION'] = "Lista de transacciones";
$lang['ACCION'] = "Accion";
$lang['REGISTRO'] = "Ver Registro";
$lang['DEBITO'] = "Depositos";
$lang['RETIROS'] = "Retiros";
$lang['DESCRIPCION'] = "Descripcion";

/*VER REGISTRO*/
$lang['LISTANUMEROS'] = "Lista de numeros";
$lang['RECIBIDO'] = "Recibido:";
$lang['NUMERO'] = "Numero";
$lang['MONTO'] = "Monto";




?>
