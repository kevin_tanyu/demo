<?php

$lang = array();
$lang['IDIO']  = 'en';

/*HEADER*/
$lang['HORA'] = "Time:";
$lang['FECHA'] = "Date:";
$lang['SALIR'] = "Exit";

/*PRINCIPAL*/
$lang['BIENVENIDO'] = "Welcome";
$lang['BALANCETOTAL'] = "Main Balance:";
$lang['NUMEROGANADOR'] = "Winner Number";
$lang['TOTALLISTA'] = "Total List";
$lang['COMISION'] = "Commission";
$lang['PREMIO'] = "Prize";

/*MENU*/
$lang['PRINCIPAL'] = "Dashboard";
$lang['REPORTES'] = "Reports";
$lang['IDIOMA'] = "Language";

/*SUBMENU REPORTES*/
$lang['SEGUROS'] = "R. Numbers List";
$lang['BALANCE'] = "Balance";

/*SUBMENU IDIOMA*/
$lang['INGLES'] = "English";
$lang['ESPANOL'] = "Spanish";

/*OTROS*/
$lang['DE'] = "From";
$lang['HASTA'] = "To ";
$lang['VER'] = "Show";
$lang['ESPERA'] = "Processing, please wait...";
$lang['SORTEO'] = "Lottery";
$lang['LISTALOTERIA'] = "Lottery list";
$lang['LISTATRANSACCION'] = "Transaction list";
$lang['ACCION'] = "Action";
$lang['REGISTRO'] = "Show record";
$lang['DEBITO'] = "Deposit";
$lang['RETIROS'] = "Withdraw";
$lang['DESCRIPCION'] = "Description";


/*VER REGISTRO*/
$lang['LISTANUMEROS'] = "Numbers List";
$lang['RECIBIDO'] = "Received:";
$lang['NUMERO'] = "Number";
$lang['MONTO'] = "Amount";

?>