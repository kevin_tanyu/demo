<?php
session_start();

if (!isset($_SESSION['ActivoFlag'])) {
	// Verifica si hay session creada, de lo contrario redirige al index
	header("Location: index.php?IDM=TO");
	exit;
}


// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

if (isset($_POST['Lunes'])) 	$LunesFlag=1; else $LunesFlag=0;
if (isset($_POST['Martes']))  $MartesFlag=1; else $MartesFlag=0;
if (isset($_POST['Miercoles']))   $MiercolesFlag=1; else $MiercolesFlag=0;
if (isset($_POST['Jueves']))   $JuevesFlag=1; else $JuevesFlag=0;
if (isset($_POST['Viernes']))   $ViernesFlag=1; else $ViernesFlag=0;
if (isset($_POST['Sabado']))   $SabadoFlag=1; else $SabadoFlag=0;
if (isset($_POST['Domingo']))   $DomingoFlag=1; else $DomingoFlag=0;

if (!isset($_POST['PagaPorcentaje']))  $PagaPorcentaje=0; else $PagaPorcentaje=$_POST['PagaPorcentaje'];
if (!isset($_POST['FlagMultiple']))	$FlagMultiple=0; else  $FlagMultiple=1;
if (isset($_POST['FlagMultipleMinimoGana']) && $_POST['FlagMultipleMinimoGana']!="") $FlagMultipleMinimoGana=$_POST['FlagMultipleMinimoGana']; else $FlagMultipleMinimoGana=0;

$QUERY="INSERT INTO SorteosDefinicion (
																	NombreSorteo,
																	NumerosGanadores,
																	LunesFlag,
																	MartesFlag,
																	MiercolesFlag,
																	JuevesFlag,
																	ViernesFlag,
																	SabadoFlag,
																	DomingoFlag,
																	PagaPorcentaje,
																	FlagMultiple,
																	FlagMultipleMinimoGana, 
																	FlagActivo
																) VALUES (
																	'".$_POST['NombreSorteo']."',
																	'".$_POST['NumerosGanadores']."',
																	".$LunesFlag.",
																	".$MartesFlag.",
																	".$MiercolesFlag.",
																	".$JuevesFlag.",
																	".$ViernesFlag.",
																	".$SabadoFlag.",
																	".$DomingoFlag.",
																	".$PagaPorcentaje.",
																	".$FlagMultiple.",
																	".$FlagMultipleMinimoGana.",1
																)";
$rs=mysql_query($QUERY);

if ($rs) $T="SU";
else $T="NOSU";

//header("Location: http://suerte.macroweb-cr.com/seccion_datos_loterias.php?T=".$T);

?>