<ul data-role="listview" >
    <?php foreach($apuestas as $apuesta): ?>
    <li><a href="#" data-apuesta_id="<?php echo $apuesta->ID ?>" class="list_apuesta_item <?php if(!is_null($apuesta->ReglaPorcentaje)): echo "list_apuesta_item_error"; endif; ?>" >
            Sorteo del <?php echo $apuesta->fechaSorteo ?>
            <br/>
            <?php echo ' Numeros: ' . implode(',',$apuesta->numeros) ?>
            <br/>
            <?php echo 'Cantidad: ¢' . $apuesta->Cantidad ?>
            <?php if(!is_null($apuesta->ReglaPorcentaje)): ?>
            <br/>
            Excede en mas del <?php echo $apuesta->ReglaPorcentaje . '%' ?>
            <?php endif; ?>
        </a>
    </li>
    <?php endforeach; ?>
</ul>