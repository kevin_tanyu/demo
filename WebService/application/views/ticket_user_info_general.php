<?php

$cantidad = 0;
$totalDinero = 0;
$promedio = 0;
if(!is_array($userInfo))
{
    $cantidad = $userInfo->cantidadTickets;
    $totalDinero = $userInfo->totalDinero;
    $promedio = number_format( $userInfo->promedio, 2 );
}

?>


<div style="width:100%; height:100%; background-color:#ccc; margin:auto; text-align:center;" >
<div style="width:300px; height:100%; border:1px solid #666; background-color:#FFF; margin:auto;" >
    <!-- A partir de aqui -->
    <br><br>
    <p class="encabezado">Tickets de Hoy</p>
    <p class="monto"><?php echo $cantidad ?></p>
    <br><br>
    <p class="encabezado">Recolectado</p>
    <p class="monto">&cent;<?php echo $totalDinero ?></p>
    <p class="encabezado">Promedio &cent;<?php echo $promedio ?> / Ticket</p>
    <!-- Hasta aqui -->
</div>
</div>