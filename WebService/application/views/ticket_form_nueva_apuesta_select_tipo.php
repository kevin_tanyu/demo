<ul data-role="listview" data-theme="d" data-divider-theme="d" id="tipo_apuesta_list">
    <li data-role="list-divider">Normal<span class="ui-li-count">1</span></li>
    <li><a data-tipo_apuesta="normal" href="#" class="apuesta_item">
            <h3>Tipo apuesta Normal:</h3>
            <p>Esta apuesta un unico numero al cual apostar</p>
        </a>
    </li>

    <li data-role="list-divider">Parlay<span class="ui-li-count">2</span></li>
    <li><a data-tipo_apuesta="parlay" href="#" class="apuesta_item">
            <h3>Tipo apuesta Parlay</h3>
            <p>Apuesta que involucra  el orden de tres numeros</p>
        </a>
    </li>

</ul>
<script>
    //esto se ejecuta del lado del cliente
    $("#tipo_apuesta_list").listview();
</script>