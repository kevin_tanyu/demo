<?php

/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 3/21/15
 * Time: 1:58 PM
 */
class Parlay extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('parlay_lib');
        /** @var $this ->parlay_lib libraries/Parlay_lib */
    }

    /**
     * Despliega los eventos proximos
     * @return string un string con formato en json
     */
    public function listEvents()
    {
        $eventsReturn = array();
        $eventsResult = $this->parlay_lib->getAvailableFullEvents();

        if ($eventsResult['isValid'])
        {
            $data   = $eventsResult['data'];
            $events = array();
            //obtener los eventos
            foreach ($data as $event)
            {
                $events[$event->event_id] = array('id' => $event->event_id, 'event_parts' => array());
            }

            //obtener los event_part
            foreach ($data as $eventPartObj)
            {
                $eventPartArray                                   = array('id' => $eventPartObj->event_part_id,
                    'fechahora' => $eventPartObj->sorteo_fechahora,
                    'nombre' => $eventPartObj->sorteo_nombre
                );
                $events[$eventPartObj->event_id]['event_parts'][] = $eventPartArray;
            }
            $eventsReturn = array_values($events);

        }

        echo json_encode($eventsReturn);
    }

    /**
     * Retorna un objeto en formato json, que contiene los tipos de apuestas disponibles
     * el rango de montos que puede meterle a cada tipo de apuesta
     * @param $eventId int el id del evento para el cual obtener la info inicial
     * @param $userId int el id del usuario
     * @return
     */
    public function eventBetInitialInfo($eventId, $userId)
    {
        $finalData = array();
        //obtener las apuestas disponibles, inicialmente son todas
        $apuestasArr = $this->parlay_lib->getParlayBetTypes();
        foreach ($apuestasArr['data'] as $betType)
        {
            $finalData['bet_types'][] = array('id' => $betType->id, 'name' => $betType->name, 'display_name' => $betType->display_name);
        }
        //obtener la info del evento
        $eventArr  = $this->parlay_lib->getFullEventInfo($eventId);
        $eventData = $eventArr['data'];
        $events    = array();
        foreach ($eventData as $event)
        {
            $events[$event->event_id] = array('id' => $event->event_id, 'event_parts' => array());
        }

        //obtener los event_part
        foreach ($eventData as $eventPartObj)
        {
            $eventPartArray                                   = array('id' => $eventPartObj->event_part_id,
                'fechahora' => $eventPartObj->sorteo_fechahora,
                'nombre' => $eventPartObj->sorteo_nombre
            );
            $events[$eventPartObj->event_id]['event_parts'][] = $eventPartArray;
        }
        $eventsReturn            = array_values($events);
        $finalData['event_info'] = $eventsReturn;

        //limits for bets
        $limits                    = $this->parlay_lib->getLimits();
        $finalData['money_ranges'] = $limits;

        $finalData['event_parlay_total'] = $this->parlay_lib->getTotalEvent($eventId, $userId);

        //empaquetar la info e imprimirla
        $jsonReturn = json_encode($finalData);

        return $this->output
            ->set_content_type('application/json')
            ->set_output($jsonReturn);
    }

    /**
     * Salva un ticket con sus apuestas
     * @param $userId
     * @param $eventId
     * @param $eventPartIdsString sorteos programacion
     * @param $numbersString
     * @param $betTypesString
     * @param $betAmountsString
     */
    public function saveTicket($userId, $eventId, $eventPartIdsString, $numbersString, $betTypesString, $betAmountsString)
    {
        $jsonReturn = array('success' => false, 'ticket_id' => -1, 'error_msg' => '');
        //revisar si el evento todavia es valido segun la fechahora
        $eventAvailable = $this->parlay_lib->eventIsAvailable($eventId);

        if ($eventAvailable)
        {
            $eventPartIds = explode('_', $eventPartIdsString);
            $numbers      = explode('_', $numbersString);
            $betTypes     = explode('-', $betTypesString);
            $betAmounts   = explode('_', $betAmountsString);

            $ticketStructure = array('id' => -1,
                'event_id' => $eventId,
                'user_id' => $userId,
                'total' => 0,
                'date_time' => date('Y-m-d H:i:s'),
                'ticket_details' => array(),
                'bets_details' => array(),
            );

            //llenar el ticket detail
            for ($i = 0; $i < count($betTypes); ++$i)
            {
                $ticketStructure['ticket_details'][] = array(
                    'bet_type' => $betTypes[$i],
                    'amount' => $betAmounts[$i],
                );
                $ticketStructure['total'] += $betAmounts[$i];
            }

            $initialBet = $this->generate_initial_bet($numbers, $eventPartIds);

            //llenar las apuestas
            $numbersCombination = array();
            for ($i = 0; $i < count($betTypes); ++$i)
            {
                $numbersCombination[$betTypes[$i]]['amount'] = $betAmounts[$i];
                $numbersCombination[$betTypes[$i]]['bets']   = $this->generate_bets($betTypes[$i], $initialBet);
            }
            $ticketStructure['bets_details'] = $numbersCombination;

            $nowDate         = new DateTime();
            $canInsertTicket = true;
            $validationId    = $this->parlay_lib->createTicketValidation($userId, $nowDate);
            //create combinations
            $this->parlay_lib->createValCombinations($validationId, $ticketStructure);
            //limites totales por combinacion
            $areGoodLimitsByBetInfo = $this->parlay_lib->validateLimitByBet($validationId);

            $canInsertTicket = $canInsertTicket && $areGoodLimitsByBetInfo['valid'];

            $areGoogLimitsByEventInfo = array('valid' => false);
            try
            {
                //limites contra el total que lleva la combinacion
                $areGoogLimitsByEventInfo = $this->parlay_lib->validateLimitsByEvent($validationId);
                $canInsertTicket          = $canInsertTicket && $areGoogLimitsByEventInfo['valid'];
            } catch (Exception $e)
            {
                echo $e->getMessage();
                $canInsertTicket = false;
            }
//            $this->parlay_lib->cleanValidation($validationId);
            if ($canInsertTicket)
            {
                $ticketStructure = $this->parlay_lib->insertTicket($ticketStructure);
                $ticketStructure = $this->parlay_lib->insertTicketDetails($ticketStructure);
                $ticketStructure = $this->parlay_lib->insertBets($ticketStructure);

                $jsonReturn['success']     = true;
                $jsonReturn['ticket_id']   = $ticketStructure['id'];
                $jsonReturn['total']       = $ticketStructure['total'];
                $jsonReturn['date_time']   = $ticketStructure['date_time'];
                $jsonReturn['ticket_info'] = $ticketStructure['bets_details'];
            } else
            {
                if (!$areGoodLimitsByBetInfo['valid'])
                    $jsonReturn['error_msg'] = 'Se excede el tope para las combinaciones';
                if (!$areGoogLimitsByEventInfo['valid'])
                    $jsonReturn['error_msg'] = 'Una combinacion excede el monto para el evento';
            }

        } else
        {
            $jsonReturn['error_msg'] = 'Ya no se pueden meter apuestas para este evento.';
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($jsonReturn));

    }


    private function generate_bets($betType, $initialBet)
    {
        $bets = array();
        switch ($betType)
        {
            case 'straight':

                $initialBet['bet_type'] = $betType;
                $bets                   = array($initialBet);

                break;

            case 'reverse':
                $bets = $this->generate_reverse_bets($initialBet);
                break;

            case 'double_reverse':
                $bets = $this->generate_double_reverse_bets($initialBet);
                break;

        }

        return $bets;
    }

    /**
     * agrupa los numeros con los sorteos en
     * una estructura asi: ( (n1, s1) , (n2, s2), ..., (nI, sI) )
     */
    private function generate_initial_bet($numbers, $eventPartsId)
    {
        $bet = array('id' => -1, 'bet_type' => '', 'bet_parts' => array());

        for ($i = 0; $i < count($numbers); ++$i)
        {
            $bet['bet_parts'][] = array('number' => $numbers[$i], 'event_part_id' => $eventPartsId[$i]);
        }

        return $bet;
    }


    /**
     * genera los bets para reverse
     * @param $initialBet
     * @return array
     */
    private function generate_reverse_bets($initialBet)
    {
        $originalBet = $initialBet;
        $reverseBet  = $initialBet;

        $originalBetPartsCount = count($originalBet['bet_parts']);

        $i = 0;
        $j = $originalBetPartsCount - 1;
        while ($i < $originalBetPartsCount && $j >= 0)
        {

            $reverseBet['bet_parts'][$i]['number'] = $originalBet['bet_parts'][$j]['number'];

            ++$i;
            --$j;
        }

        return array($originalBet, $reverseBet);
    }

    private function generate_double_reverse_bets($initialBet)
    {
        $bets = array();

        //reverse bets
        $firstSetBets = $this->generate_reverse_bets($initialBet);

        foreach ($firstSetBets as $bet)
            $bets[] = $bet;

        //double reversed bets
        $tempNumberReversedBet = $initialBet;
        foreach ($initialBet['bet_parts'] as $key => $betPart)
        {
            $tmpNumber                                          = str_pad($betPart['number'], 2, "0", STR_PAD_LEFT);
            $tempNumberReversedBet['bet_parts'][$key]['number'] = strrev($tmpNumber);
        }
        $numberReversedBets = $this->generate_reverse_bets($tempNumberReversedBet);
        foreach ($numberReversedBets as $bet)
            $bets[] = $bet;

        return $bets;
    }

}