<?php


class Ticket extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('sorteos_lib');
        $this->load->library('ticket_lib');
        $this->load->library('authentication_lib');
    }

    public function formNuevaApuestaSelectSorteo()
    {
        $sorteos = $this->sorteos_lib->getNextWeekSorteos();
        $data['result'] = $sorteos['data'];
        $this->load->view('sorteos_proximos_sorteos',$data);
    }

    public function formNuevaApuestaSelectTipo()
    {
        $this->load->view('ticket_form_nueva_apuesta_select_tipo');
    }

    public function formDatosApuesta($sorteoId)
    {
        $result = $this->sorteos_lib->getSorteo($sorteoId);
        $data = array('sorteo'=>$result['data']);
        $this->load->view('ticket_form_crear_apuesta',$data);

    }

    public function crearApuestaNormal()
    {
        $numerosApuesta = $this->input->post('numero_apuesta');
        $cantidadApuesta = $this->input->post('cantidad_apuesta');
        $sorteoProgramacionId = $this->input->post('sorteo_id');
        $ticketId = $this->input->post('ticket_id');
        $userName = $this->input->post('username');
        $userId = $this->authentication_lib->getUsuarioId($userName);
        $fecha = new DateTime();
        $result = $this->ticket_lib->insertarApuesta($userId['id']
            ,$sorteoProgramacionId
            ,$fecha->format('Y-m-d H:i')
            ,$ticketId
            ,$cantidadApuesta
            ,$numerosApuesta
        );

        echo json_encode($result);

    }

    public function ticketsDiariosPorUsuario($username)
    {
        $user = $this->authentication_lib->getUsuarioId($username);
        $userId = $user['id'];
        $date = new DateTime();
        $startDate = $date->format('Y-m-d 00:00');
        $endDate = $date->format('Y-m-d 23:59');

        $tickets = $this->ticket_lib->getTicketsPorUsuario($userId,$startDate,$endDate);


    }

    public function apuestasPorTicket($ticketId)
    {
        $apuestas = $this->ticket_lib->getApuestasPorTicket($ticketId);
        $data = array('apuestas' => $apuestas['data']);
        $this->load->view('ticket_apuestas_por_ticket', $data );
    }

    public function showTicketResume($ticketId)
    {
        $apuestas = $this->ticket_lib->getApuestasPorTicket($ticketId);
        $data = array('apuestas' => $apuestas['data']);
        $this->load->view('ticket_apuestas_por_ticket', $data );
    }

    public function salvarTicket($ticketId)
    {
        $info = $this->checkTicketRestringidos($ticketId);
        $response = array('isValid' => $info['isValid'], 'message' => '');
        if($response['isValid'])
        {
            $this->ticket_lib->releaseTicket($ticketId);
        }

        echo json_encode($response);
    }

    public function isValidTicket($ticketId)
    {
        $isValid = $this->checkTicketRestringidos($ticketId);
        echo json_encode(array('isValid' => $isValid['isValid']));
    }

    public function getFormEditarApuesta($apuestaId)
    {
        $apuesta = $this->ticket_lib->getApuesta($apuestaId);
        $data = array('apuesta'=>$apuesta['data'][$apuestaId]);
        $this->load->view('ticket_form_editar_apuesta',$data);
    }

    private function checkTicketRestringidos($ticketId)
    {
        $numerosRestringidos = $this->ticket_lib->getNumerosSobrepasanRegla($ticketId);
        $valid = count($numerosRestringidos) > 0 ? false: true;
        $return = array('isValid' => $valid, 'numeros' => $numerosRestringidos);

        return $return;
    }

    public function ticketsUserInfo($username)
    {
        $userId = $this->authentication_lib->getUsuarioId($username);

        $userInfo = $this->ticket_lib->getUserTicketsInfo($userId['id']);

        $this->load->view('ticket_user_info_general',array('userInfo' => $userInfo));

    }

    public function revert($userId,$ticketId)
    {
        $resultDb = $this->ticket_lib->revertTicket($userId,$ticketId);
        $result = array('reverted' => $resultDb['reverted'], "message" => "");

        if( $resultDb['invalidUser'] )
            $result['message'] = "El ticket #". $ticketId ." no le pertenece";

        if( $resultDb['invalidTicket'] )
            $result['message'] = "El ticket #". $ticketId ." no existe";

        echo json_encode($result);
    }

    public function byUser($userId,$ticketId)
    {
        $return = array();
        $res = $this->ticket_lib->getAllInfo($userId,$ticketId);

        if($res['exists'])
        {
            $return =  $this->processTicketInfo($res['result']);
        }
        echo json_encode($return);
    }

    private function processTicketInfo($resultArray)
    {
        $ticket = array();
        $ticket['id'] = $resultArray[0]->id;
        $ticket['IDUsuario'] = $resultArray[0]->IDUsuario;
        $ticket['FechayHora'] = $resultArray[0]->ticketDate;
        $ticket['sorteo'] = array();

        $ticket['sorteo']['nombre'] = $resultArray[0]->NombreSorteo;
        $ticket['sorteo']['date'] = $resultArray[0]->sorteoDate;
        $ticket['sorteo']['NumeroDeSuerte'] = $resultArray[0]->NumeroDeSuerte;

        $ticket['apuestas'] = array();

        $amount = 0;

        foreach($resultArray as $res)
        {
            $apuesta = array();
            $apuesta['ID'] = $res->apuestaID;
            $amount += $apuesta['Cantidad'] = $res->Cantidad;
            $apuesta['Numero'] = $res->Numero;
            $ticket['apuestas'][] = $apuesta;
        }
        $ticket['amount'] = $amount;
        return $ticket;
    }

}