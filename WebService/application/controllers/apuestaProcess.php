<?php

/**
 * Este controlador se utiliza para darle la informacion necesaria inicial al app de la
 * tablet cuando comienza a crear un ticket, ademas de procesar la creacion de los tickets
 * User: jorge
 * Date: 7/8/13
 * Time: 10:55 AM
 */
class ApuestaProcess extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('sorteos_lib');
        $this->load->library('ticket_lib');
        $this->load->library('numerosRestringidos_lib');
        $this->load->library('authentication_lib');
    }

    /**
     * Informacion que se carga en la interfaz de ingreso de ticket
     * muestra la informacion de la fecha, el sorteo, el total del dia,
     * y los restringidos del dia, con su respectivo monto disponible
     * para el usuario actual
     * @param $userId
     * @param $sorteoId
     */
    public function initialInfo($userId, $sorteoId)
    {
        $info = array('fecha_hora' => null, 'sorteo' => null, 'totalDia' => 0, 'restringidosDia' => array());
        $info['fecha_hora']             = date("d/m/Y H:i:s");
        $info['sorteo']                 = $this->sorteos_lib->getSorteoProgramacion($sorteoId);
        $info['sorteo']['restringidos'] = $this->numerosrestringidos_lib->getDisponibles($userId, $sorteoId);
        $info['totalDia']               = $this->ticket_lib->getDiaryTotal($userId, $sorteoId);
       // $info['restringidosDia']        = $this->ticket_lib->restringidosDia($userId, $sorteoId);

        echo json_encode($info);
    }
    public function initialInfo2($userId, $sorteoId)
    {
        $info = array('fecha_hora' => null, 'sorteo' => null, 'totalDia' => 0, 'restringidosDia' => array());
        $info['fecha_hora']             = date("d/m/Y H:i:s");
        $info['sorteo']                 = $this->sorteos_lib->getSorteoProgramacion($sorteoId);
        $info['sorteo']['restringidos'] = $this->numerosrestringidos_lib->getDisponibles($userId, $sorteoId);
        $info['totalDia']               = $this->ticket_lib->getDiaryTotal($userId, $sorteoId);
        //$info['restringidosDia']        = $this->ticket_lib->restringidosDia($userId, $sorteoId);

        echo json_encode($info);
    }
    public function saveTicket($usuarioId, $sorteoProgramacionId, $numeros, $montos)
    {
        $infoInsertTicket = $this->ticket_lib->insertTicket($usuarioId);
        $ticketId         = $infoInsertTicket['newId'];
        $ticketNumber     = $infoInsertTicket['ticketNumber'];
        $date             = $infoInsertTicket['date'];

        $numerosArray = explode("_", $numeros);
        $montosArray  = explode("_", $montos);

        $insertedApuestas = $this->ticket_lib->insertApuestas($ticketId, $sorteoProgramacionId, $usuarioId, $numerosArray, $montosArray);
        echo json_encode(array(
            "ticketId" => $ticketId,
            "ticketNumber" => $ticketNumber,
            "apuestas_state" => $insertedApuestas,
            "date" => $date
        ));
    }

    public function saveTicketCasino($usuarioId, $sorteoProgramacionId, $numeros, $montos)
    {
        $response = array(
            "ticketId" => -1,
            "ticketNumber" => -1,
            "apuestas_state" => false,
            "date" => ""
        );

        $sorteo = $this->sorteos_lib->getSorteoProgramacion($sorteoProgramacionId);

        $now        = new DateTime("now");
        $sorteoDate = new DateTime($sorteo['fechaHora']);

        if ($now < $sorteoDate)
        {
            $numerosArray = explode("_", $numeros);
            $montosArray  = explode("_", $montos);

            //$this->ticket_lib->getLocksForTicketValidityCheck();
            $errorAmounts  = $this->ticket_lib->getAmountErrors($sorteoProgramacionId, $numerosArray, $montosArray);
            $errorInAmount = count($errorAmounts) > 0;

            if (!$errorInAmount)
            {
                $infoInsertTicket = $this->ticket_lib->insertTicket($usuarioId);
                $ticketId         = $infoInsertTicket['newId'];

                $insertedApuestas = $this->ticket_lib->insertApuestas($ticketId, $sorteoProgramacionId, $usuarioId, $numerosArray, $montosArray);

                $response["ticketId"]       = $ticketId;
                $response["ticketNumber"]   = $infoInsertTicket['ticketNumber'];
                $response["apuestas_state"] = $insertedApuestas;
                $response["date"]           = $infoInsertTicket['date'];
            } else
            {
                $errors = array();
                foreach ($errorAmounts as $error)
                {
                    $errors[] = array(
                        'numero' => $error->numero,
                        'disponible' => $error->disponible
                    );
                }
                $response["errors"] = $errors;
            }
            //$this->ticket_lib->realeaseLocksForTicketValidityCheck();
        } else
        {
            $response['errors'] = array(array('numero' => 000, 'disponible' => '-1'));
        }
        echo json_encode($response);
    }

}
