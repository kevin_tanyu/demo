<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by JetBrains PhpStorm.
 * User: jorge
 * Date: 5/21/13
 * Time: 12:01 PM
 * To change this template use File | Settings | File Templates.
 */

class Authentication extends CI_Controller
{

    public function __construct() {
        parent::__construct();
        $this->load->library('authentication_lib');
    }

    public function login($username,$password)
    {
        $username = strtolower( trim($username) );
        $isValid = $this->authentication_lib->checkUsernamePass($username,$password);
        echo json_encode($isValid);
    }

    public function loginWEB($username, $password){
        //PARA QUITAR LA PALABRA IMPORT DEL NOMBRE DEL VENDEDOR
        if (strpos($username,'IMPORT') === false){
            $username = trim($username);
        }else{
            $username =  substr($username, 7);
        }

        $isValid = $this->authentication_lib->login($username,$password);
        //return json_encode($isValid);

        echo json_encode($isValid);

        return 'LOGIN WEB STRING';

    }
}