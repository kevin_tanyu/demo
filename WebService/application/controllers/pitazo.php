<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 1/21/14
 * Time: 2:55 PM
 */

class Pitazo extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->library('pitazo_lib');
        $this->load->library('authentication_lib');
    }

    public function getProximasJornadas()
    {
        $fechas = array();
        $date = new DateTime();
        $results = $this->pitazo_lib->getFechas($date->format("Y-m-d"));
        if($results['isValid']){
            $fechas = $results['data'];
        }
        echo json_encode($fechas);
    }

    public function getPartidos($idJornada)
    {
        $info = array();
        $results = $this->pitazo_lib->getPartidos($idJornada);
        if($results['isValid'])
        {
            $info['partidos'] = $results['data'];
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output( json_encode($info) );
    }

} 