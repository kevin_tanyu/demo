<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jorge
 * Date: 7/1/13
 * Time: 1:00 PM
 * To change this template use File | Settings | File Templates.
 */

class Loterias extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->library('loterias_lib');
        $this->load->library('authentication_lib');
    }

    public function listAll()
    {
        $result = $this->loterias_lib->getLoterias();
        $return = array();
        if( true == $result['isValid'])
        {
            $return = json_encode( $result['data'] );
        }
        echo $return;
    }

}