<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jorge
 * Date: 5/23/13
 * Time: 11:36 AM
 * To change this template use File | Settings | File Templates.
 */

class Sorteos extends CI_Controller{

    public function __construct() {
        parent::__construct();
        $this->load->library('sorteos_lib');
        $this->load->library('authentication_lib');
    }
    
    public function listByLoteria($loteriaId)
    {
        $result     = $this->sorteos_lib->getTodaySorteosByLoteria($loteriaId);
        $data['result'] = $result['data'];
        $jsonReturn = json_encode($data['result']);

        return $this->output
            ->set_content_type('application/json')
            ->set_output($jsonReturn);
    }

    public function proximosSorteos()
    {
        $result = $this->sorteos_lib->getNextWeekSorteos();
        $data['result'] = $result['data'];
        $this->load->view('sorteos_proximos_sorteos',$data);

    }

    public function crearTicketTemporal($username)
    {
        $user = $this->authentication_lib->getUsuarioId($username);
        $result = $this->sorteos_lib->insertTicketTentativo($user['id']);
        echo json_encode($result);
    }

    public function getSorteosDefinicion()
    {
        $sorteos = $this->sorteos_lib->getSorteosDefinicion();
        $this->load->view('sorteos_list_sorteos_definicion',array('sorteos'=>$sorteos));
    }

    public function cerrarDia($userId)
    {
        $result = $this->sorteos_lib->cerrarDia($userId);
        if($result['isValid'])
            echo json_encode(array("RESULT"=>"OK"));
        else
            echo json_encode(array("RESULT"=>"INVALID"));
    }

    /**
     * verifica si el dia esta cerrado
     * @param $userId
     */
    public function diaEstaCerrado($userId)
    {
        $result = $this->sorteos_lib->diaCerrado($userId,date("Y-m-d"));
    }

    public function totalesUsuario($userId)
    {
        $startDate = date('Y-m-d 00:00');
        $endDate = date('Y-m-d 23:59');
        $result = $this->sorteos_lib->getSorteosTotales($userId, $startDate, $endDate);
        echo json_encode($result);
    }

    public function requestRevision($userId, $sorteoProgId)
    {
        $result = $this->sorteos_lib->requestRevision($userId, $sorteoProgId);
        echo json_encode($result);
    }

}
