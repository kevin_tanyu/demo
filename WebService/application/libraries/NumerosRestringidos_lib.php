<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 10/31/14
 * Time: 1:57 AM
 */
class NumerosRestringidos_lib
{
    private $ci;
    private $db;

    public function __construct()
    {
        $this->ci = & get_instance();
        $this->db = $this->ci->load->database('pdo', true);
    }

    public function getDisponibles($userId, $sorteoId)
    {
        $sql = "SELECT
                  SNR.Numero,
                  (tope_permitido - IFNULL(TOTAL, 0)) AS disponible
                FROM SorteosNumerosRestringidos SNR
                  JOIN (SELECT
                          ((IFNULL(MONTO_SORTEO.TOTAL_SORTEO,0) / 100) * PARAMS.porcentaje_restriccion) AS permitido_por_porcentaje,
                          PARAMS.tope_inicial,
                          SP.ID                                                               AS IDSorteoProgramacion,
                          CASE
                          WHEN PARAMS.tope_inicial < ((IFNULL(MONTO_SORTEO.TOTAL_SORTEO,0) / 100) * PARAMS.porcentaje_restriccion)
                          THEN ((IFNULL(MONTO_SORTEO.TOTAL_SORTEO,0) / 100) * PARAMS.porcentaje_restriccion)
                          WHEN PARAMS.tope_inicial >= ((IFNULL(MONTO_SORTEO.TOTAL_SORTEO,0) / 100) * PARAMS.porcentaje_restriccion)
                          THEN PARAMS.tope_inicial
                          END                                                                    tope_permitido
                        FROM SorteosProgramacion SP
                          JOIN USUARIOS_CALLE_venta_parametros PARAMS
                            ON PARAMS.IDSorteoDefinicion = SP.IDSorteoDefinicion
                               AND PARAMS.IDUsuario = :id_usuario
                          LEFT JOIN (SELECT
                                       SAV.IDSorteoProgramacion,
                                       SUM(IFNULL(SAV.monto,0)) AS TOTAL_SORTEO
                                     FROM Vendedor_Calle_Totales SAV
                                     WHERE SAV.IDSorteoProgramacion = :id_sorteo AND SAV.IDUsuario = :id_usuario
                                     ) AS MONTO_SORTEO
                            ON SP.ID = MONTO_SORTEO.IDSorteoProgramacion
                        WHERE SP.ID = :id_sorteo)
                    AS MONTOS_PERMITIDOS ON MONTOS_PERMITIDOS.IDSorteoProgramacion = SNR.IDSorteoProgramacion
                  LEFT JOIN (SELECT
                               SV.Numero,
                               SV.monto AS TOTAL
                             FROM Vendedor_Calle_Totales SV
                               JOIN SorteosNumerosRestringidos SNR ON SNR.IDSorteoProgramacion = SV.IDSorteoProgramacion
                                                                      AND SNR.Numero = SV.Numero
                             WHERE SV.IDUsuario = :id_usuario AND SV.IDSorteoProgramacion = :id_sorteo
                             )
                    AS RESTRINGIDOS_GASTADO ON RESTRINGIDOS_GASTADO.Numero = SNR.Numero

                WHERE SNR.IDSorteoProgramacion = :id_sorteo";


        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->execute(array(':id_sorteo' => $sorteoId, ':id_usuario' => $userId));
        $results = $stmt->fetchAll(PDO::FETCH_CLASS);
        $result = array();
        foreach ($results as $restringido)
        {
            $result[] = array('numero' => $restringido->Numero,
                'disponible' => $restringido->disponible);
        }

        return $result;
    }

} 