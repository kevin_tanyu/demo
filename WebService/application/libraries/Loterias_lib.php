<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jorge
 * Date: 7/1/13
 * Time: 1:01 PM
 * To change this template use File | Settings | File Templates.
 */

class Loterias_lib
{
    private $ci;
    private $db;

    private $_ACTIVE_LOTERIAS_VAL = 1;

    public function __construct()
    {
        $this->ci = & get_instance();
        $this->db = $this->ci->load->database('suerte',true);
    }

    public function getLoterias()
    {
        $sql = "SELECT ID, NombreSorteo as Nombre, NumerosGanadores, PagaPorcentaje FROM SorteosDefinicion WHERE FlagActivo = ?";
        $result = $this->getResult($sql,array($this->_ACTIVE_LOTERIAS_VAL));
        return $result;
    }

    private function getResult($sql,$params)
    {
        $result = array('isValid' => false, 'data' => array() );
        $query = $this->db->query($sql,$params);
        $rows = $query->result();
        if( count( $rows ) > 0 )
        {
            $result['isValid'] = true;
            $result['data'] = $rows;
        }
        return $result;
    }

}