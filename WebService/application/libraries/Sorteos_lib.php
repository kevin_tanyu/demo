<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by JetBrains PhpStorm.
 * User: jorge
 * Date: 5/23/13
 * Time: 11:25 AM
 * To change this template use File | Settings | File Templates.
 */
class Sorteos_lib
{
    private $ci;
    private $db;

    public function __construct()
    {
        $this->ci = & get_instance();
//        $this->db = $this->ci->load->database('suerte', true);
        $this->db = $this->ci->load->database('pdo', true);
    }

    public function getSorteosByLoteria($startDate, $endDate, $loteriaId)
    {
        $result = array('isValid' => false, 'data' => array());
        $sql    = "SELECT sp.ID ,FechaYHora, sd.NombreSorteo, sp.NumeroDeSuerte, sd.ID as SorteoDefinicionID
                FROM SorteosProgramacion sp
                JOIN SorteosDefinicion sd ON sp.IDSorteoDefinicion = sd.ID
                WHERE sp.FechaYHora
                BETWEEN  ?
                AND  ?
                AND sd.ID = ?";
        $query  = $this->db->query($sql, array($startDate, $endDate, $loteriaId));
        $rows   = $query->result();
        if (count($rows) > 0)
        {
            $result['isValid'] = true;
            $result['data']    = $rows;
        }

        return $result;
    }

    public function getSorteos($startDate, $endDate)
    {
        $result = array('isValid' => false, 'data' => array());
        $sql    = "SELECT sp.ID ,FechaYHora, sd.NombreSorteo, sp.NumeroDeSuerte, sd.ID as SorteoDefinicionID
                FROM SorteosProgramacion sp
                JOIN SorteosDefinicion sd ON sp.IDSorteoDefinicion = sd.ID
                WHERE sp.FechaYHora
                BETWEEN  ?
                AND  ?";
        $query  = $this->db->query($sql, array($startDate, $endDate));
        $rows   = $query->result();
        if (count($rows) > 0)
        {
            $result['isValid'] = true;
            $result['data']    = $rows;
        }

        return $result;
    }

    public function getNextWeekSorteos()
    {
        $date    = new DateTime();
        $today   = $date->format('Y-m-d H:i');
        $nexWeek = $date->add(new DateInterval('P8D'))->format('Y-m-d H:i');

        $result = $this->getSorteos($today, $nexWeek);

        return $result;
    }

    public function getNextWeekSorteosByLoteria($loteriaId)
    {
        $date     = new DateTime();
        $today    = $date->format('Y-m-d H:i');
        $nextWeek = $date->add(new DateInterval('P8D'))->format('Y-m-d H:i');

        $result = $this->getSorteosByLoteria($today, $nextWeek, $loteriaId);

        return $result;
    }

    public function getTodaySorteosByLoteria($loteriaId)
    {
        $date     = new DateTime();
        $today    = $date->format('Y-m-d H:i');
        $todayEnd = $date->format('Y-m-d 23:59');

        $result = $this->getSorteosByLoteria($today, $todayEnd, $loteriaId);

        return $result;
    }

    public function getSorteo($sorteoId)
    {
        $return = array('isValid' => false, 'data' => array());
        $sql    = "SELECT
                ID,
                NombreSorteo,
                Orden,
                NumerosGanadores,
                LunesFlag,
                MartesFlag,
                MiercolesFlag,
                JuevesFlag,
                ViernesFlag,
                SabadoFlag,
                DomingoFlag,
                PagaPorcentaje,
                FlagMultiple,
                FlagMultipleMinimoGana,
                FlagActivo
                FROM SorteosDefinicion
                WHERE ID = ?
                LIMIT 1;
                 ";

        $result = $this->db->query($sql, array($sorteoId))->row();

        if (0 < count($result))
        {
            $return['isValid'] = true;
            $return['data']    = $result;
        }

        return $return;

    }

    public function getSorteosDefinicion()
    {
        $sql = "select s.ID, s.NombreSorteo, s.NumerosGanadores
                  from SorteosDefinicion s
                  where s.FlagActivo = 1";

        $result = $this->db->query($sql)->result();

        return $result;

    }

    public function getSorteoProgramacion($id)
    {
        $sql       = "select sp.ID,sp.FechayHora,sp.NumeroDeSuerte,sd.NombreSorteo from SorteosProgramacion sp
                    join  SorteosDefinicion sd on sd.ID = sp.IDSorteoDefinicion where sp.ID = ?";
        $sorteoStd = $this->db->query($sql, array($id))->row();

        $sorteo                   = array('restringidos' => array());
        $sorteo['id']             = $sorteoStd->ID;
        $sorteo['fechaHora']      = $sorteoStd->FechayHora;
        $sorteo['NumeroDeSuerte'] = $sorteoStd->NumeroDeSuerte;
        $sorteo['NombreSorteo']   = $sorteoStd->NombreSorteo;

        return $sorteo;

    }

    public function cerrarDia($userId)
    {
        $sqlCantidad = "SELECT sum(Cantidad) as total FROM SorteoApuesta_Validas WHERE IDUsuario=" . $userId . " AND FechayHora>='" . date('Y-m-d 00:00:00') . "'";
        $result      = $this->db->query($sqlCantidad)->row();
        $cantidad    = $result->total ? : 0;

        $sqlCantidadRestringidos = "SELECT SUM(Cantidad) as total FROM SorteoApuesta_Validas WHERE Numero in (SELECT Numero FROM `SorteosNumerosRestringidos` WHERE IDSorteoProgramacion in (SELECT ID from SorteosProgramacion WHERE IDUsuario=" . $userId . ")) AND FechayHora>='" . date('Y-m-d 00:00:00') . "'";
        $result                  = $this->db->query($sqlCantidadRestringidos)->row();
        $cantidadRestringidos    = $result->total ? : 0;


        $sqlInsertCierre = "INSERT INTO CierreDelDia (FechayHora, FechaCierre, MontoTotal, MontoRestringidos, IDUsuario) VALUES
				(now(),'" . date('Y-m-d') . "', " . $cantidad . ", " . $cantidadRestringidos . ", " . $userId . ")";
        $this->db->query($sqlInsertCierre);

        $return = array('isValid' => false);
        if ($this->db->affected_rows() > 0)
        {
            $return['isValid'] = true;
        }

        return $return;
    }

    public function diaCerrado($userId, $date)
    {
        $sql    = "SELECT ID FROM CierreDelDia WHERE DATEDIFF(?,FechaCierre) = 0 AND IDUsuario = ? ";
        $result = $this->db->query($sql, array($date, $userId))->row();

        $diaCerrado = false;

        if (!is_array($result))
            $diaCerrado = true;

        echo json_encode(array("cerrado" => $diaCerrado));
    }

    /**
     * obtiene los totales para el vendedor calle para los sorteos dentro del rango de fechas
     * que viene por parametro
     * @param $userId int el id del usuario
     * @param $startDate string init date
     * @param $endDate string end date
     * @return array
     */
    public function getSorteosTotales($userId, $startDate, $endDate)
    {
        $sql = "SELECT
                  SP.ID,
                  SP.FechayHora AS FechaYHora ,
                  SD.NombreSorteo,
                  SP.NumeroDeSuerte,
                  SP.IDSorteoDefinicion AS SorteoDefinicionID,
                  IFNULL(TOTALES.TOTAL, 0) AS total,
                  IFNULL(S.name,'open') AS status
                FROM SorteosProgramacion SP
                  JOIN SorteosDefinicion SD ON SD.ID = SP.IDSorteoDefinicion
                  LEFT JOIN (SELECT
                               SP.ID            AS IDSorteoProgramacion,
                               SUM(SV.Cantidad) AS TOTAL
                             FROM SorteoApuesta_Validas SV
                               JOIN SorteosProgramacion SP ON SP.ID = SV.IDSorteoProgramacion
                             WHERE SV.IDUsuario = :user_id
                                   AND SP.FechayHora BETWEEN :start_date AND :end_date
                             GROUP BY SP.ID)
                    AS TOTALES ON TOTALES.IDSorteoProgramacion = SP.ID
                  LEFT JOIN COF_RevisionStatus_View RV ON RV.usuario_vendedor_calle_id = :user_id AND RV.sorteo_programacion_id = SP.ID
                  LEFT JOIN COF_Status S ON S.id = RV.status_id
                  WHERE SP.FechayHora BETWEEN :start_date AND :end_date";

        $params = array(':start_date' => $startDate, ':end_date' => $endDate, ':user_id' => $userId);
        $stmt   = $this->db->conn_id->prepare($sql);
        $stmt->execute($params);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    public function requestRevision($userId, $sorteoProgId)
    {
        $return = array('isValid' => true, 'error_msg' => '');

        $sqlParams = array(":user_id" => $userId, ":sorteo_id" => $sorteoProgId);

        $sqlCheckUnique  = "SELECT * FROM COF_Revision WHERE usuario_vendedor_calle_id = :user_id and sorteo_programacion_id = :sorteo_id";
        $stmtCheckUnique = $this->db->conn_id->prepare($sqlCheckUnique);
        $stmtCheckUnique->execute($sqlParams);

        if ($stmtCheckUnique->rowCount() == 0)
        {
            $sql = "INSERT INTO COF_Revision (usuario_vendedor_calle_id, sorteo_programacion_id)
        VALUES (:user_id,:sorteo_id)";
            try
            {
                $stmt              = $this->db->conn_id->prepare($sql);
                $return['isValid'] = $stmt->execute($sqlParams);
                $revisionId        = $this->db->conn_id->lastInsertId();
                $this->registerChangeRevisionStatusLog($revisionId, $userId, "sent");

            } catch (PDOException $e)
            {
                $return['isValid']   = false;
                $return['error_msg'] = 'Problema ingresando la solicitud';
            }
        } else
        {
            $return['isValid']   = false;
            $return['error_msg'] = 'Ya existe la solicitud';
        }

        return $return;
    }

    private function registerChangeRevisionStatusLog($revisionId, $userId, $newStatusName)
    {
        $statusId = $this->getStatusId($newStatusName);

        $sqlLog = "INSERT INTO COF_Revision_Log (cof_revision_id, cof_new_status_id, created_at, user_id)
        values (:revision_id, :status_id, NOW(), :user_id)";

        try
        {
            $stmt              = $this->db->conn_id->prepare($sqlLog);
            $return['isValid'] = $stmt->execute(array(":revision_id" => $revisionId, ":status_id" => 3, ":user_id" => $userId));

        } catch (PDOException $e)
        {
            $return['isValid'] = false;
        }

    }

    private function getStatusId($stateName)
    {
        $sql    = "SELECT id FROM COF_Status WHERE name = ?";
        $result = $this->db->query($sql, array($stateName))->row();

        return $result->id;
    }

}
