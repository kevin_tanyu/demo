<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by JetBrains PhpStorm.
 * User: jorge
 * Date: 5/21/13
 * Time: 12:05 PM
 * To change this template use File | Settings | File Templates.
 */

class Authentication_lib {
    private $ci;
    private $db;
    private $pdoConn;

    public function __construct()
    {
        $this->ci = & get_instance();
        $this->db = $this->ci->load->database('suerte',true);
        $this->pdoConn = $this->ci->load->database('pdo', true);
    }

    /**
     * Validar un usuario y password
     * @param $username string con el nombre del usuario
     * @param $pass string con el password del usuario
     * @return array de la forma array('isValid'=>true|false, 'message' => string)
     */
    public function checkUsernamePass($username,$pass)
    {
        $result = array('isValid'=>false,'message'=>'');

        $sql = "Select * from Usuarios where NombreUsuario = ? AND Contrasena = ? AND TipoUsuario = 5 AND FlagActivo = 1 AND ActivoFlag = 1";

        $query = $this->db->query($sql,array($username,$pass));
        $rs = $query->result();
        if( count( $rs ) > 0 )
        {
            $result['isValid'] = true;
            $result['id'] = $rs[0]->ID;
            $result['mac_printer'] = $rs[0]->printer_mac_address;
            $result['paga'] = $rs[0]->PagaPorcentaje;
            $result['printer_type'] = $rs[0]->printer_type;
        }else{
            $result['message'] = 'No se encuentra el usuario';
        }

        return $result;
    }

    public function getUsuarioId($username)
    {
        $return = array('isValid'=>false,'id'=>null);
        $sql = "SELECT ID FROM Usuarios_View WHERE NombreUsuario = ? ;";
        $result = $this->db->query($sql,array($username))->result();
        if(count($result) > 0 )
        {
            $return['id'] = $result[0]->ID;
            $return['isValid'] = true;
        }
        return $return;
    }

    /*METODO LOGIN PARA APP WEB*/
    public function login($username, $password){
        $result = array('isValid'=> false);
        $sqlLogin = "SELECT * FROM Usuarios WHERE NombreUsuario = ? AND Contrasena = ? AND TipoUsuario = 5";
        $stmtLogin = $this->pdoConn->conn_id->prepare($sqlLogin);
        $stmtLogin->execute(array($username, $password));

        if($stmtLogin->rowcount() == 0){
            $username = 'IMPORT_' . $username;
            $stmtLogin->execute(array($username, $password));

            if($stmtLogin->rowcount() == 0){
                $result['message'] = 'No se encuentra el usuario';
                return $result;
            }else{
                $usuario = $stmtLogin->fetch();
                $result['isValid'] = true;
                $result['message'] = '';
                $result['ID'] = $usuario['ID'];
                $result['PagaPorcentaje'] = $usuario['PagaPorcentaje'];
                $result['devolucion'] = $usuario['devolucion'];
                return $result;

            }//Fin if/else
        }else{
            $usuario = $stmtLogin->fetch();
            $result['isValid'] = true;
            $result['message'] = '';
            $result['ID'] = $usuario['ID'];
            $result['PagaPorcentaje'] = $usuario['PagaPorcentaje'];
            $result['devolucion'] = $usuario['devolucion'];
            return $result;
        }//Fin if/else

        $result['message'] = 'No se encuentra el usuario';
        return $result;


    }//Fin login

}