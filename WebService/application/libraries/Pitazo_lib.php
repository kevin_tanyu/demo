<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 1/21/14
 * Time: 2:57 PM
 */
class Pitazo_lib
{

    private $ci;
    private $db;

    public function __construct()
    {
        $this->ci = & get_instance();
        $this->db = $this->ci->load->database('luck', true);
    }

    public function getFechas($start)
    {
        $result = array('isValid' => false, 'data' => array());
        $sql    = "SELECT id,fecha FROM PITAZO_Jornadas_View WHERE fecha > ?";
        $query  = $this->db->query($sql, array($start));
        $rows   = $query->result();
        if (count($rows) > 0)
        {
            $result['isValid'] = true;
            $result['data']    = $rows;
        }

        return $result;
    }

    public function getPartidos($idJornada)
    {
        $result = array('isValid' => false, 'data' => array());
        $sql    = "SELECT * FROM PITAZO_Partidos_View where IDJornada = ?";
        $query  = $this->db->query($sql, array($idJornada));
        $rows   = $query->result();
        if (count($rows) > 0)
        {
            $result['isValid'] = true;
            $result['data']    = $rows;
        }

        return $result;
    }

} 