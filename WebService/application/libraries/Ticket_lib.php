<?php

/**
 * Created by JetBrains PhpStorm.
 * User: jorge
 * Date: 5/27/13
 * Time: 1:05 AM
 * To change this template use File | Settings | File Templates.
 */
class Ticket_lib
{
    private $ci;
    private $db;

    public function __construct()
    {
        $this->ci = & get_instance();
        $this->db = $this->ci->load->database('suerte', true);
    }

    public function insertarApuesta($userId, $sorteoProgramacionId, $date, $ticketId, $cantidadApuesta, $numeroApuesta)
    {
        $return = array('isValid' => false, 'newId' => null);
        $sql    = "INSERT INTO SorteoApuesta (
                IDUsuario
                ,IDSorteoProgramacion
                ,FechaYHora
                ,IDTicket
                ,Cantidad
                ,Numero
                ) VALUES (?,?,?,?,?,?)";

        $this->db
            ->query($sql, array($userId, $sorteoProgramacionId, $date, $ticketId, $cantidadApuesta, $numeroApuesta));

        return $return;

    }

    public function getTicketsPorUsuario($userId, $startDate, $endDate)
    {
        $sql = "";
    }

    public function getApuestasPorTicket($ticketId)
    {
        $sql = "SELECT
                sa.ID,
                IDUsuario,
                IDSorteoProgramacion,
                RestringidoFlag,
                sa.FechayHora,
                FlagGanador,
                IDNumeroGanador,
                IDTicket,
                sa.Cantidad,
                san.numero,
                NULL AS numeros,
                tnr.ReglaPorcentaje,
                sap.FechaYHora AS fechaSorteo
                FROM SorteoApuesta sa
                JOIN SorteoApuestaNumero san ON san.SorteoApuestaID = sa.ID
                JOIN SorteosProgramacion sap ON sap.ID = sa.IDSorteoProgramacion
                LEFT JOIN Ticket_NumerosRestringidosView tnr ON tnr.apuestaId = sa.ID AND (tnr.porcentaje - tnr.ReglaPorcentaje) > 0
                WHERE IDTicket = ?";

        $return = array('isValid' => false, 'data' => array());
        $result = $this->db->query($sql, array($ticketId))->result();
        if (0 < count($result)) {
            $return['data']    = $this->groupSorteoApuestaNumeros($result);
            $return['isValid'] = true;
        }

        return $return;

    }


    private function groupSorteoApuestaNumeros($apuestas, $withId = false)
    {
        $apuestasFinal = array();
        for ($i = 0; $i < count($apuestas); ++$i) {
            if (!array_key_exists($apuestas[$i]->ID, $apuestasFinal)) {
                $apuesta = $apuestas[$i];
//                $apuesta->numeros = array($apuestas[$i]->numero);
                $apuestasFinal[$apuesta->ID] = $apuesta;
            }
            for ($j = $i; $j < count($apuestas); ++$j) {
                if ($apuestas[$i]->ID == $apuestas[$j]->ID) {
                    if ($withId) {
                        $apuestasFinal[$apuestas[$i]->ID]->numeros[]
                            = array('id' => $apuestas[$j]->numeroId,
                            'numero' => $apuestas[$j]->numero);
                    } else {
                        $apuestasFinal[$apuestas[$i]->ID]->numeros[]
                            = $apuestas[$j]->numero;
                    }

                }
            }
        }

        return $apuestasFinal;
    }

    public function infoTicket($ticketId)
    {
        $sql = "";
    }

    public function releaseTicket($ticketId)
    {
        $sql = "UPDATE Ticket SET isReleased = 1 WHERE id = ?";
        $this->db->query($sql, array($ticketId));
    }

    public function getNumerosSobrepasanRegla($ticketId)
    {
        $sql = "SELECT t.TicketId,t.apuestaId,t.numero,t.monto,t.ReglaPorcentaje AS permitido
                  FROM Ticket_NumerosRestringidosView t
                  WHERE t.Ticketid = ? AND (t.porcentaje - t.ReglaPorcentaje) > 0";

        $result = $this->db->query($sql, array($ticketId))->result();

        return $result;

    }

    public function getApuesta($id)
    {
        $sql = "SELECT
                sa.ID,
                IDUsuario,
                IDSorteoProgramacion,
                RestringidoFlag,
                sa.FechayHora,
                FlagGanador,
                IDNumeroGanador,
                IDTicket,
                sa.Cantidad,
                san.id AS numeroId,
                san.numero,
                NULL AS numeros,
                tnr.ReglaPorcentaje,
                sap.FechaYHora AS fechaSorteo
                FROM SorteoApuesta sa
                JOIN SorteoApuestaNumero san ON san.SorteoApuestaID = sa.ID
                JOIN SorteosProgramacion sap ON sap.ID = sa.IDSorteoProgramacion
                LEFT JOIN Ticket_NumerosRestringidosView tnr ON tnr.apuestaId = sa.ID AND (tnr.porcentaje - tnr.ReglaPorcentaje) > 0
                WHERE sa.ID = ?";

        $return = array('isValid' => false, 'data' => array());
        $result = $this->db->query($sql, array($id))->result();
        if (0 < count($result)) {
            $return['data']    = $this->groupSorteoApuestaNumeros($result, true);
            $return['isValid'] = true;
        }

        return $return;
    }

    public function updateApuesta($apuestaId, $cantidad, $numeros)
    {
        $sqlUpdateApuesta = "UPDATE SorteoApuesta SET Cantidad = ? WHERE ID = ?";
        $this->db->query($sqlUpdateApuesta, array($cantidad, $apuestaId));

        //TODO averiguar como validar que devuelva affected rows si el valor no cambia
//        $affected = $this->db->affected_rows();
//        $isValidUpdateApuesta = $affected > 0;
        $sqlUpdateNumeros = "";

        foreach ($numeros as $id => $number) {
            $sqlUpdateNumeros = "UPDATE SorteoApuestaNumero SET numero = $number WHERE id = $id;";
            $this->db->query($sqlUpdateNumeros);
        }

//        $affected = $this->db->affected_rows();
//        $isValidUpdateNumeros = $affected > 0;

        return array('isValid' => true);

    }

    public function getUserTicketsInfo($userId)
    {
        $sql = "SELECT
              COUNT(t.id) AS cantidadTickets ,SUM(c.total) AS totalDinero ,AVG(c.total) AS promedio
              FROM Ticket t
                JOIN (SELECT IDTicket,SUM(Cantidad) AS total FROM SorteoApuesta sa GROUP BY sa.IDTicket) c
                  ON c.IDTicket = t.id

              WHERE t.IDUsuario = ? AND  DATE(t.FechayHora) = DATE(CURDATE())
              GROUP BY  DATE(t.FechayHora)";

        $result = $this->db->query($sql, array($userId))->row();

        return $result;
    }

    public function getDiaryTotal($userId, $sorteoId)
    {
        $sql = "SELECT SUM(monto) AS totalDiario
                FROM Vendedor_Calle_Totales
                  WHERE IDUsuario = ? AND IDSorteoProgramacion = ?";
        $result = $this->db->query($sql, array($userId, $sorteoId))->row();
        $total  = (is_array($result) || $result->totalDiario == null) ? 0 : $result->totalDiario;

        return $total;
    }

    public function restringidosDia($userId, $sorteoId)
    {
        $sql = "SELECT srt.IDUsuario,srt.IDSorteoProgramacion
                  ,srt.Numero
                  ,(3/100)*std.Total AS CantidadARespetar
                  ,((3/100)*std.Total) - srt.Total AS Balance
                  FROM SorteoApuesta_RestringidoTotalCantidad srt
                    JOIN SorteoApuesta_TotalDiario std
                      ON std.IDSorteoProgramacion = srt.idsorteoprogramacion
                      AND std.IDUsuario = srt.IDUsuario
                  WHERE srt.IDUsuario = ? AND srt.IDSorteoProgramacion = ?
                        AND ((3/100)*std.Total) - srt.Total < 0
                LIMIT 0,2";

        $result       = $this->db->query($sql, array($userId, $sorteoId))->result();
        $restringidos = array();

        foreach ($result as $res) {
            $restringidos[] = array(
                'numero' => $res->Numero
            , 'balance' => number_format($res->Balance * -1, 0, '.', ''));
        }

        return $restringidos;
    }

    public function insertTicket($usuarioId)
    {
        $return          = array();
        $date            = date("Y-m-d H:i:s");
        $sqlInsertTicket = "INSERT INTO Ticket (IDUsuario,FechayHora)
                    VALUES (?,?);";
        $this->db->query($sqlInsertTicket, array($usuarioId, $date));
        if ($this->db->affected_rows() > 0) {
            $return['isValid']      = true;
            $return['newId']        = $this->db->insert_id();
            $return['ticketNumber'] = $return['newId'];
            $return['date']         = $date;
        }

        return $return;
    }

    public function insertApuestas($ticketId, $sorteoProgramacionId, $usuarioId, $numeros, $montos)
    {
        $length            = count($numeros);
        $sqlInsertTemplate = "INSERT INTO SorteoApuesta (IDTicket,IDUsuario,IDSorteoProgramacion,Numero,Cantidad,FechayHora)
        VALUES ( ?,?,?,?,?, NOW() ) ";
        $sqlTotalTemplate = "INSERT INTO Vendedor_Calle_Totales(IDUsuario, IDSorteoProgramacion, numero, monto)
          VALUES (?, ?, ?, ?)
          ON DUPLICATE KEY UPDATE monto = monto + ?;
           ";
        $affectedRows      = 0;
        for ($i = 0; $i < $length; ++$i) {
            $numero = $numeros[$i];
            $monto  = $montos[$i];
            $this->db->query($sqlInsertTemplate, array($ticketId, $usuarioId, $sorteoProgramacionId, $numero, $monto));
            $affectedRows += $this->db->affected_rows();
            $this->db->query($sqlTotalTemplate, array($usuarioId, $sorteoProgramacionId, $numero, $monto, $monto));
        }

        return $affectedRows == $length;
    }

    public function revertTicket($userId, $ticketId)
    {
        $result = array('invalidUser' => false, 'invalidTicket' => false, 'reverted' => false);

        $sql       = "SELECT * FROM Ticket_View WHERE id = ?";
        $rowTicket = $this->db->query($sql, array($ticketId))->row();

        if (!is_array($rowTicket)) {
            if ($rowTicket->IDUsuario == $userId) {
                $sqlReverTicket = "UPDATE Ticket SET fechaRevertido = ? WHERE id = ?";
                $this->db->query($sqlReverTicket, array(date('Y-m-d h:i'), $ticketId));
                if ($this->db->affected_rows() > 0)
                {
                    $result['reverted'] = true;
                    $sqlRevertTotalesVendedor = "UPDATE Vendedor_Calle_Totales VCT
                                                  JOIN SorteoApuesta SA ON SA.Numero = VCT.numero
                                                    AND SA.IDUsuario = VCT.IDUsuario
                                                    AND SA.IDSorteoProgramacion = VCT.IDSorteoProgramacion
                                                    AND SA.IDTicket = ?
                                                SET VCT.monto = VCT.monto - SA.Cantidad";
                    $this->db->query($sqlRevertTotalesVendedor, array($ticketId));
                }
            } else {
                $result['invalidUser'] = true;
            }
        } else {
            $result['invalidTicket'] = true;
        }

        return $result;
    }

    public function getAllInfo($userId, $ticketId)
    {
        $sql = "SELECT tv.id, tv.IDUsuario, tv.FechayHora AS ticketDate
                  ,sp.FechayHora AS sorteoDate, sp.NumeroDeSuerte
                  , sd.NombreSorteo, sav.ID AS apuestaID, sav.Cantidad
                  , sav.Numero
                FROM Ticket_View tv
                  JOIN SorteoApuesta_Validas sav ON sav.IDTicket = tv.id
                  JOIN SorteosProgramacion sp ON sp.ID = sav.IDSorteoProgramacion
                  JOIN SorteosDefinicion sd ON sd.ID = sp.IDSorteoDefinicion
                  WHERE tv.IDUsuario = ? AND tv.id = ?";

        $result = $this->db->query($sql, array($userId, $ticketId))->result();

        $return = array('exists' => false, 'result' => null);
        if (count($result) > 0) {
            $return['exists'] = true;
            $return['result'] = $result;
        }

        return $return;
    }

    public function getAmountErrors($sorteoProgramacionId, $numeros, $montos)
    {
        $montosPartialSql = array();
        foreach ($numeros as $key => $numero) {
            $monto              = $montos[$key];
            $montosPartialSql[] = " SELECT $numero as numero, $monto as monto ";
        }
        $numerosForIn = implode(',',$numeros);
        $selectNumeros = implode(" UNION ", $montosPartialSql);
        $sql           = "SELECT *
                          FROM
                            ( $selectNumeros ) `TN`
                              JOIN
                            (SELECT
                                  `N`.`numero`                                                        AS `numero`,
                                  sum( ifnull(`SA`.`Cantidad`, 0) )                                     AS `gastado`
                                  ,ifnull(`SP`.`LimiteCasino`, 2000) - sum(ifnull(`SA`.`Cantidad`, 0)) AS disponible
                                FROM `Numeros` `N`
                                  LEFT JOIN `SorteosProgramacion` `SP` ON `SP`.ID = ?
                                  LEFT JOIN `SorteoApuesta` `SA` ON ((`SA`.`Numero` = `N`.`numero`)) AND SA.IDSorteoProgramacion = SP.ID
                                WHERE `N`.numero IN ($numerosForIn)
                                GROUP BY `N`.`numero`)
                               info ON info.numero = TN.numero
                        WHERE (info.disponible - TN.monto) < 0";//esto para que me de los errores
        $result        = $this->db->query($sql, array($sorteoProgramacionId))->result();

        return $result;

    }

    public function getLocksForTicketValidityCheck()
    {
        $sqlLock = "LOCK TABLES Ticket WRITE,  SorteoApuesta WRITE, Numeros as N READ, SorteoApuesta as SA READ, SorteosProgramacion as SP READ";
        $this->db->query($sqlLock);
    }

    public function realeaseLocksForTicketValidityCheck()
    {
        $this->db->query("UNLOCK TABLES");
    }

}