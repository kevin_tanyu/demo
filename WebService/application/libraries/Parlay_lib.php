<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Libreria para manejar la logica de las apuestas en parlay
 * User: jorge
 * Date: 3/21/15
 * Time: 1:59 PM
 */
class Parlay_lib
{
    private $ci;
    private $db;

    public function __construct()
    {
        $this->ci = & get_instance();
        $this->db = $this->ci->load->database('pdo', true);
    }

    /**
     * Retorna los eventos proximos, todos los que estan creados
     * de la fecha y hora actual hacia adelante
     * @return mixed un array con isValid indicando si se encontraron eventos
     */
    public function getAvailableFullEvents()
    {
        $result = array('isValid' => false, 'data' => array());
        $sql    = "SELECT e.id as event_id, ep.id as event_part_id, sp.FechayHora as sorteo_fechahora
                , sd.NombreSorteo as sorteo_nombre
                FROM PAR_Event e
                    JOIN PAR_Event_Part ep ON ep.par_event_id = e.id
                    JOIN SorteosProgramacion sp ON sp.ID = ep.sorteo_prog_id
                    JOIN SorteosDefinicion sd ON sd.ID = sp.IDSorteoDefinicion
                WHERE e.display_until >= NOW()";
        $rows   = $this->db->query($sql)->result();
        if (0 < count($rows))
        {
            $result['isValid'] = true;
            $result['data']    = $rows;
        }

        return $result;
    }

    /**
     * Obtiene la informacion de un evento
     */
    public function getFullEventInfo($eventId)
    {
        $result = array('isValid' => false, 'data' => array());
        $sql = "SELECT e.id as event_id, e.display_until as event_display_until, ep.id as event_part_id, sp.FechayHora as sorteo_fechahora
                , sd.NombreSorteo as sorteo_nombre
                FROM PAR_Event e
                    JOIN PAR_Event_Part ep ON ep.par_event_id = e.id
                    JOIN SorteosProgramacion sp ON sp.ID = ep.sorteo_prog_id
                    JOIN SorteosDefinicion sd ON sd.ID = sp.IDSorteoDefinicion
                WHERE e.id = ?";
        $rows   = $this->db->query($sql, array($eventId))->result();
        if (0 < count($rows))
        {
            $result['isValid'] = true;
            $result['data']    = $rows;
        }

        return $result;
    }

    /**
     * Obtener los tipos de apuestas para parlay disponibles
     */
    public function getParlayBetTypes()
    {
        $result = array('isValid' => false, 'data' => array());
        $sql    = "SELECT id, name, display_name FROM PAR_Bet_Types ORDER BY ID ASC";
        $rows   = $this->db->query($sql)->result();
        if (0 < count($rows))
        {
            $result['isValid'] = true;
            $result['data']    = $rows;
        }

        return $result;
    }

    /**
     * Consulta en la base de datos por la fecha hasta la que se le pueden hacer apuestas a un evento
     * @param $eventId
     * @return bool
     */
    public function eventIsAvailable($eventId)
    {
        $isAvailable = false;
        $date        = new DateTime();
        $sql         = "SELECT * FROM PAR_Event WHERE id = ? AND display_until >= ?";
        $rows        = $this->db->query($sql, array($eventId, $date->format('Y-m-d H:i')))->result();
        if (0 < count($rows))
            $isAvailable = true;

        return $isAvailable;
    }

    // funciones para persistir

    /**
     * inserta el registro en la tabla PAR_Ticket
     * @param $ticketStructure
     * @return array same $ticketStructure with the id of the ticket
     */
    public function insertTicket($ticketStructure)
    {
        $userId  = $ticketStructure['user_id'];
        $eventId = $ticketStructure['event_id'];
        $total   = $ticketStructure['total'];
        $dateTime = $ticketStructure['date_time'];
        $sql     = "INSERT INTO PAR_Ticket (par_event_id, usuario_id, total, created_at) VALUES (?, ?, ?, ?)";

        $this->db->query($sql, array($eventId, $userId, $total, $dateTime));

        $ticketStructure['id'] = $this->db->insert_id();

        return $ticketStructure;

    }

    /**
     * Inserta los registros en PAR_Ticket_Details
     * @param $ticketStructure mixed informacion del ticket
     * @return mixed
     */
    public function insertTicketDetails($ticketStructure)
    {

        $sql = "INSERT INTO PAR_Ticket_Detail (par_ticket_id, par_bet_type_id, amount) VALUES (?, ?, ?)";

        $betTypes = $this->groupBetTypesId($this->getParlayBetTypes());
        $ticketId = $ticketStructure['id'];
        foreach ($ticketStructure['ticket_details'] as $key => $ticketDetail)
        {
            $betTypeId = $betTypes[$ticketDetail['bet_type']];
            $amount    = $ticketDetail['amount'];
            $this->db->query($sql, array($ticketId, $betTypeId, $amount));
            $ticketStructure['ticket_details'][$key]['id'] = $this->db->insert_id();
        }

        return $ticketStructure;
    }

    /**
     * Crea los registros en la tabla de PAR_Ticket_Bet y PAR_Ticket_Bet_Part
     * @param $ticketStructure
     */
    public function insertBets($ticketStructure)
    {
        $sqlInsertBet     = "INSERT INTO PAR_Ticket_Bet (par_ticket_id, par_bet_type_id, amount) VALUES (?, ?, ?)";
        $sqlInsertBetPart = "INSERT INTO PAR_Ticket_Bet_Part (par_ticket_bet_id, par_event_part_id, number)
        VALUES (?, ?, ?)";

        $ticketId     = $ticketStructure['id'];
        $betTypesInfo = $this->groupBetTypesId($this->getParlayBetTypes());
        foreach ($ticketStructure['bets_details'] as $name => $betTypeBets)
        {
            $betTypeId = $betTypesInfo[$name];
            foreach ($betTypeBets['bets'] as $bet)
            {
                $this->db->query($sqlInsertBet, array($ticketId, $betTypeId, $betTypeBets['amount']));
                $bet['id'] = $betId = $this->db->insert_id();
                foreach ($bet['bet_parts'] as $betPart)
                {
                    $this->db->query($sqlInsertBetPart, array($betId, $betPart['event_part_id'], $betPart['number']));
                }
            }
        }

        return $ticketStructure;
    }

    /**
     * Crea un array de la forma array( 'nombre_tipo_apuesta' => id )
     * @param $getParlayBetTypes
     * @return array
     */
    private function groupBetTypesId($getParlayBetTypes)
    {
        $betTypes = array();
        $data     = $getParlayBetTypes['data'];
        foreach ($data as $betType)
        {
            $betTypes[$betType->name] = $betType->id;
        }

        return $betTypes;
    }

    /**
     * Devuelve los limites permitidos para las apuestas
     */
    public function getLimits()
    {
        $limits = array();

        $sql           = 'SELECT id,`min`,`max` FROM PAR_Ticket_Bet_Parameters LIMIT 1';
        $rows          = $this->db->query($sql)->result();
        $limits['min'] = $rows[0]->min;
        $limits['max'] = $rows[0]->max;

        return $limits;
    }

    /**
     * Devuelve el total para el sorteo actual
     */
    public function getTotalEvent($eventId, $userId)
    {
        $sql   = "SELECT SUM( IFNULL(T.total,0) ) AS TOT FROM PAR_Ticket T WHERE T.par_event_id = ? AND T.usuario_id = ?";
        $rows  = $this->db->query($sql, array($eventId, $userId))->result();
        $total = $rows[0]->TOT;

        return $total;
    }

    //funciones para validar un nuevo ticket

    /**
     * Crea un registro para validacion de un nuevo ticket
     * @param $userId
     * @param $dateTime
     */
    public function createTicketValidation($userId, $dateTime)
    {
        $sql = "INSERT INTO PAR_VAL_Validation (user_id, created_at) VALUES (?,?)";
        $this->db->query($sql, array($userId, $dateTime->format('Y-m-d H:i')));
        $id = $this->db->insert_id();

        return $id;
    }

    /**
     * Stores combinations in db associated
     */
    public function createValCombinations($validationId, $ticketStructure)
    {
        $sqlCombination           = "INSERT INTO PAR_VAL_Combination (validation_id, par_event_id, par_bet_type_id, amount)
                            VALUES (?,?,?,?)";
        $sqlInsertCombinationPart = "INSERT INTO PAR_VAL_Combination_Part (val_combination_id, number, par_event_part_id)
        VALUES (?, ?, ?)";
        $eventId                  = $ticketStructure['event_id'];
        $betTypesInfo             = $this->groupBetTypesId($this->getParlayBetTypes());
        foreach ($ticketStructure['bets_details'] as $name => $betTypeBets)
        {
            $betTypeId = $betTypesInfo[$name];
            foreach ($betTypeBets['bets'] as $combination)
            {
                $this->db->query($sqlCombination, array($validationId, $eventId, $betTypeId, $betTypeBets['amount']));
                $combinationId = $this->db->insert_id();
                foreach ($combination['bet_parts'] as $combinationPart)
                {
                    $this->db->query($sqlInsertCombinationPart, array($combinationId, $combinationPart['number'], $combinationPart['event_part_id']));
                }
            }
        }

        return $ticketStructure;
    }

    /**
     * Valida contra los limites globales,
     * esto para que las apuestas que no tienen aun
     * ningun ticket se validen adecuadamente.
     * Es muy dificil que una sola apuesta exceda el
     * monto global por evento, pero no se debe especular cuando
     * hay dinero de por medio.
     * @param $validationId
     * @return array
     */
    public function validateLimitByBet($validationId)
    {
        $sql = "SELECT C.id as combination_id, C.par_bet_type_id, C.amount - PAR.limit_amount AS excess
                FROM PAR_VAL_Validation V
                  JOIN PAR_VAL_Combination C ON C.validation_id = V.id
                  JOIN PAR_LIM_Parameters PAR ON PAR.par_bet_type_id = C.par_bet_type_id
                  WHERE V.id = ?
                  AND C.amount > PAR.limit_amount";

        $result = $this->db->query($sql, array($validationId))->result();

        $validationInfo = array('valid' => true);

        if (is_array($result) && count($result) > 0)
        {
            $validationInfo['valid'] = false;
            $sqlCombinationParts     = "SELECT CP.id, CP.number, SD.NombreSorteo as event_part_name, EP.id as event_part_id
                FROM PAR_VAL_Combination_Part CP
                    JOIN PAR_Event_Part EP ON CP.par_event_part_id = EP.id
                    JOIN SorteosProgramacion SP ON SP.ID = EP.sorteo_prog_id
                    JOIN SorteosDefinicion SD ON SD.ID = SP.IDSorteoDefinicion
                WHERE CP.val_combination_id = ?";
            foreach ($result as $overflows)
            {
                $combinationId                    = $overflows->combination_id;
                $combinations                     = $this->db->query($sqlCombinationParts, array($combinationId))->result();
                $combinationsInfo                 = array();
                $combinationsInfo[$combinationId] = array('error_complete_msg' => '');
                foreach ($combinations as $combination)
                {
                    $combinationsInfo[$combinationId]['error_complete_msg'] .= " Numero " . $combination->number . " para " . $combination->event_part_name;
                    $combinationsInfo[$combinationId]['num_event_parts'][] = array('number' => $combination->number,
                        'event_part_name' => $combination->event_part_name, 'event_part_id' => $combination->event_part_id);
                }
                $validationInfo['errors_info'][] = $combinationsInfo;
            }
        }

        return $validationInfo;
    }


    /**
     * Valida las combinaciones del posible ticket que viene
     * contra los totales que lleva cada combinacion por evento
     * @param $validationId
     * @return array
     */
    public function validateLimitsByEvent($validationId)
    {
        $result = array('valid' => true);
        //para manejar la concurrencia
        //y nadie cambie los totales mientras
        //se estan revisando
        $sqlLockTables = "
        LOCK TABLES
          PAR_VAL_Combinations_Match AS PVCM READ,
          PAR_VAL_Combinations_Match WRITE,
          PAR_VAL_Combination_Part AS PVCP READ,
          PAR_VAL_Combination AS C READ,
          PAR_VAL_Combination AS PVC READ,
          PAR_Event_Combination AS PEC READ,
          PAR_Event_Combination WRITE,
          PAR_Event_Combination_Part AS PECP READ,
          PAR_Event_Combination_Part WRITE,
          PAR_Event_Total_Combinations AS PETC READ,
          PAR_Event_Total_Combinations AS TC READ,
          PAR_Event_Total_Combinations WRITE,
          PAR_Event_Total_Combinations AS PEC WRITE,
          PAR_LIM_Parameters AS P READ";

        $sqlExceededCombinations = "SELECT VAL_COMBINATIONS.val_combination_id, (PETC.amount + VAL_COMBINATIONS.combination_amount) - P.limit_amount as excess
                FROM
                  (SELECT C.id as val_combination_id, PVCP.number as val_number
                    , PVCP.par_event_part_id as val_event_part_id
                    , C.par_bet_type_id as combination_bet_type_id
                    , C.par_event_id as combination_event_id
                    , C.amount as combination_amount
                    FROM PAR_VAL_Combination C
                    JOIN PAR_VAL_Combination_Part PVCP ON PVCP.val_combination_id = C.id
                    WHERE C.validation_id = :validation_id) AS VAL_COMBINATIONS
                JOIN
                  (SELECT PEC.id AS event_combination_id
                      , PECP.number AS event_combination_number
                      , PECP.par_event_part_id AS event_combination_event_part_id
                      FROM PAR_Event_Combination PEC
                      JOIN PAR_Event_Combination_Part PECP ON PECP.par_event_combination_id = PEC.id
                    ) AS EVT_COMBINATIONS
                    ON
                      EVT_COMBINATIONS.event_combination_event_part_id = VAL_COMBINATIONS.val_event_part_id
                    AND EVT_COMBINATIONS.event_combination_number = VAL_COMBINATIONS.val_number
                JOIN PAR_Event_Total_Combinations PETC ON PETC.par_event_combination_id = EVT_COMBINATIONS.event_combination_id
                JOIN PAR_LIM_Parameters P ON P.par_bet_type_id = VAL_COMBINATIONS.combination_bet_type_id
                  WHERE (PETC.amount + VAL_COMBINATIONS.combination_amount) > P.limit_amount
                  ;";

        $sqlInsertCombinationsMatch
            = "INSERT INTO PAR_VAL_Combinations_Match (val_combination_id, par_event_combination_id, val_validation_id)
               SELECT DISTINCT VAL_COMBINATIONS.val_combination_id, EVT_COMBINATIONS.event_combination_id, :validation_id
               FROM (SELECT C.id as val_combination_id, PVCP.number as val_number
                    , PVCP.par_event_part_id as val_event_part_id
                    , C.par_bet_type_id as combination_bet_type_id
                    , C.par_event_id as combination_event_id
                    , C.amount as combination_amount
                    FROM PAR_VAL_Combination C
                    JOIN PAR_VAL_Combination_Part PVCP ON PVCP.val_combination_id = C.id
                    WHERE C.validation_id = :validation_id) AS VAL_COMBINATIONS
                JOIN
                  (SELECT PEC.id AS event_combination_id
                      , PECP.number AS event_combination_number
                      , PECP.par_event_part_id AS event_combination_event_part_id
                      FROM PAR_Event_Combination PEC
                      JOIN PAR_Event_Combination_Part PECP ON PECP.par_event_combination_id = PEC.id
                    ) AS EVT_COMBINATIONS
                    ON
                      EVT_COMBINATIONS.event_combination_event_part_id = VAL_COMBINATIONS.val_event_part_id
                    AND EVT_COMBINATIONS.event_combination_number = VAL_COMBINATIONS.val_number";

        $sqlDeleteCombinationsMatch = "DELETE FROM PAR_VAL_Combinations_Match WHERE val_validation_id = ?";

        $sqlGetCombinationsToInsert = "SELECT PVC.id, PVC.par_event_id, PVC.par_bet_type_id
        FROM PAR_VAL_Combination PVC
        LEFT JOIN PAR_VAL_Combinations_Match PVCM ON PVCM.val_combination_id = PVC.id
        WHERE PVC.validation_id = :validation_id AND PVCM.id IS NULL";

        $sqlGetCombinationPart = "SELECT PVCP.number, PVCP.par_event_part_id FROM PAR_VAL_Combination_Part PVCP WHERE PVCP.val_combination_id = ?";

        //queries for insert new combinations into event_combinations
        $sqlInsertNewCombination = "INSERT INTO PAR_Event_Combination (par_event_id, par_bet_type_id)
        VALUES (?,?)";

        $sqlInsertNewCombinationPart
            = "INSERT INTO PAR_Event_Combination_Part (par_event_combination_id, number, par_event_part_id)
            VALUES (?,?,?)";


        $sqlInsertNewCombinationZeroTotals
            = "INSERT INTO PAR_Event_Total_Combinations (par_event_id, par_event_combination_id, amount)
                SELECT C.par_event_id, C.id, 0
                FROM PAR_Event_Combination C
                  LEFT JOIN PAR_Event_Total_Combinations TC ON TC.par_event_combination_id = C.id
                  WHERE TC.id IS NULL";


        $sqlUpdateCombinationTotal
            = "UPDATE PAR_Event_Total_Combinations PEC
                JOIN PAR_VAL_Combinations_Match PVCM
                    ON PEC.par_event_combination_id = PVCM.par_event_combination_id
                JOIN PAR_VAL_Combination PVC ON PVC.id = PVCM.val_combination_id
                    SET PEC.amount = PEC.amount + PVC.amount
                    WHERE PVCM.val_validation_id = :validation_id";

        $sqlUnlockTables = "UNLOCK TABLES";

        $this->db->query($sqlLockTables);

        $stmtExceeds = $this->db->conn_id->prepare($sqlExceededCombinations);

        $stmtExceeds->execute(array(':validation_id' => $validationId));

        //if zero, then is ok
        if ($stmtExceeds->rowCount() == 0)
        {
            try
            {
                //insert matchs
                $stmtInsertCombinationMatch = $this->db->conn_id->prepare($sqlInsertCombinationsMatch);
                $stmtInsertCombinationMatch->execute(array(':validation_id' => $validationId));

                //insert new combinations in
                $stmtCombinations = $this->db->conn_id->prepare($sqlGetCombinationsToInsert);
                $stmtCombinations->execute(array(':validation_id' => $validationId));
                $combinations = $stmtCombinations->fetchAll(PDO::FETCH_ASSOC);

                foreach ($combinations as $combination)
                {
                    $combParams = array($combination['par_event_id'], $combination['par_bet_type_id']);
                    $this->db->query($sqlInsertNewCombination, $combParams);
                    $newCombinationId = $this->db->insert_id();
                    $combinationParts = $this->db->query($sqlGetCombinationPart, array($combination['id']))->result();
                    foreach ($combinationParts as $combinationPart)
                    {
                        $combPartsParams = array($newCombinationId,
                            $combinationPart->number,
                            $combinationPart->par_event_part_id);

                        $this->db->query($sqlInsertNewCombinationPart, $combPartsParams);
                    }
                }

                //clear combinations match
                $this->db->query($sqlDeleteCombinationsMatch, array($validationId));
                //insert validation matchs, with correct info
                $stmtInsertCombinationMatch->execute(array(':validation_id' => $validationId));

                //insert new combinations with zero in totals table
                $this->db->query($sqlInsertNewCombinationZeroTotals);

                //update amounts
                $stmtUpdateTotals = $this->db->conn_id->prepare($sqlUpdateCombinationTotal);
                $stmtUpdateTotals->execute(array(':validation_id' => $validationId));
            } catch (Exception $e)
            {
                echo $e->getMessage();
            }
        } else
        {
            $exceeds           = $stmtExceeds->fetchAll(PDO::FETCH_ASSOC);
            $result['valid']   = false;
            $result['exceeds'] = $exceeds;
        }
        $this->db->query($sqlUnlockTables);

        return $result;
    }

    public function cleanValidation($validationId)
    {
        $sql = "delete from PAR_VAL_Validation WHERE id = ?";
        $this->db->query($sql, array($validationId));
    }

}