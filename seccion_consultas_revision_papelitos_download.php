<?php

session_start();

// Incluye datos generales y conexion a DB
require_once 'config.ini.php';
require_once 'conectadb.php';
require_once 'lib/seccion_datos_importacion_listas_lib.php';

try{

    $sorteoID = $_POST['sorteoID'];

    /*******************VENDEDORES******************************/
    $sqlVendedores = "SELECT U.ID, U.NombreUsuario
                      FROM Usuarios U
                      WHERE U.en_lista_revision = 1 and enPapelito = 1
                      order by U.NombreUsuario";

    $stmtVendedores = $pdoConn->prepare($sqlVendedores);
    $stmtVendedores->execute();
    $vendedores = $stmtVendedores->fetchAll(PDO::FETCH_ASSOC);
    /********************************************************************/

    /*******************SORTEO******************************/
    $sqlSorteosDelDia = "SELECT SP.ID AS sorteo_prog_id, SD.NombreSorteo as nombre_sorteo
                     FROM SorteosProgramacion SP
                     JOIN SorteosDefinicion SD
                     ON SP.IDSorteoDefinicion = SD.ID
                     WHERE SP.ID = ?";

    $stmtSorteosDelDia = $pdoConn->prepare($sqlSorteosDelDia);
    $stmtSorteosDelDia->execute(array($sorteoID));
    $sorteosDelDia = $stmtSorteosDelDia->fetchAll(PDO::FETCH_ASSOC);
    /****************************************************************/

    $accion = $_POST['Accion'];

    if($accion == 'Bruto'){

        $nombreArchivo = excelPapelitosBruto($pdoConn, $vendedores, $sorteosDelDia );

    }elseif($accion == 'Neto'){

        $nombreArchivo = excelPapelitosNeto($pdoConn, $vendedores, $sorteosDelDia);

    }

    echo('./Descargas/' . $nombreArchivo);

}catch (Exception $e){
    echo 'ERROR';
}



?>