<?php
$usuariosCanViewAdministradores = array(NIVEL_PERMISO_USUARIO_ADMIN);
$usuariosCanViewVendedores = array(NIVEL_PERMISO_USUARIO_ADMIN);
$usuariosCanViewRevendedores = array(NIVEL_PERMISO_USUARIO_ADMIN);
$usuariosCanViewUsuarios = array(NIVEL_PERMISO_USUARIO_ADMIN, NIVEL_PERMISO_USUARIO_WEB);
$usuariosCanViewVendidoDiaCerrado = array(NIVEL_PERMISO_USUARIO_ADMIN);

/*******************NuevaEstructuraPermisos******************************/

$nivelPermisoHouse = array(1);

/*******************NuevaEstructuraPermisos******************************/


?>
<link rel="stylesheet" href="jqwidgets/styles/jqx.base.css" type="text/css"/>
<script type="text/javascript" src="jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="jqwidgets/jqxcheckbox.js"></script>
<script type="text/javascript" src="jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.selection.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.filter.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.sort.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.edit.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.selection.js"></script>
<script type="text/javascript" src="jqwidgets/jqxcalendar.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdatetimeinput.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.pager.js"></script>
<script type="text/javascript" src="jqwidgets/jqxnumberinput.js"></script>
<script type="text/javascript" src="jqwidgets/globalization/globalize.js"></script>


<script type="text/javascript">
    function getDemoTheme() {
        var theme = $.data(document.body, 'theme');
        if (theme == null) {
            theme = '';
        }
        else {
            return theme;
        }
        var themestart = window.location.toString().indexOf('?');
        if (themestart == -1) {
            return '';
        }

        var theme = window.location.toString().substring(1 + themestart);
        var url = "../../jqwidgets/styles/jqx." + theme + '.css';

        if (document.createStyleSheet != undefined) {
            var hasStyle = false;
            $.each(document.styleSheets, function (index, value) {
                if (value.href != undefined && value.href.indexOf(theme) != -1) {
                    hasStyle = true;
                    return false;
                }
            });
            if (!hasStyle) {
                document.createStyleSheet(url);
            }
        }
        else $(document).find('head').append('<link rel="stylesheet" href="' + url + '" media="screen" />');

        return theme;
    }
    ;


</script>
<aside_submenu>
    <h3>Opciones</h3>

    <div id="list4">

        <ul>
            <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuariosCanViewAdministradores)): ?>
                <!--<li><a href="seccion_usuarios_administradores.php">Administradores</a></li>-->
            <?php endif; ?>
            <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuariosCanViewVendedores)): ?>
                <li><a href="seccion_usuarios_vendedores.php">Vendedores Calle</a></li>
            <?php endif; ?>
            <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuariosCanViewRevendedores)): ?>
                <!--<li><a href="seccion_usuarios_revendedores.php">Vendedores Web</a></li>-->
            <?php endif; ?>
            <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuariosCanViewUsuarios)): ?>
                <!--<li><a href="seccion_usuarios_usuarios.php">Clientes Web</a></li>-->
            <?php endif; ?>
            <?php if (in_array($_SESSION['TipoUsuarioInt'], $usuariosCanViewVendidoDiaCerrado)): ?>
                <!--<li><a href="seccion_usuarios_diacerrado.php">Vend. Dia cerrado</a></li>-->
            <?php endif; ?>
            <?php if (in_array($_SESSION['NivelUsuario'], $nivelPermisoHouse)): ?>
                <!-- <li><a href="seccion_usuarios_agentes.php">Agentes</a></li>-->
            <?php endif; ?>
            <?php if (in_array($_SESSION['NivelUsuario'], $nivelPermisoHouse)): ?>
                <!--<li><a href="seccion_usuarios_banca.php">Padrinos</a></li>-->
            <?php endif; ?>
            <?php if (in_array($_SESSION['NivelUsuario'], $nivelPermisoHouse)): ?>
                <!--<li><a href="seccion_usuarios_mensaje.php">Mensaje a Vendedores</a></li>-->
            <?php endif; ?>
            <?php if (in_array($_SESSION['NivelUsuario'], $nivelPermisoHouse)): ?>
                <!--<li><a href="seccion_usuarios_transacciones.php">Pagos Padrinos</a></li>-->
            <?php endif; ?>

        </ul>
    </div>
</aside_submenu>