<?php
/**
 * Displays and store information about the config for lista general per user
 * User: jorge
 * Date: 10/23/14
 * Time: 12:42 PM
 */
session_start();
require_once 'config.ini.php';
require_once 'conectadb.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
//    TODO save all info about users
    $updateSql = "UPDATE Usuarios SET en_lista_general = :en_lista_gnrl WHERE ID = :user_id";
    $info      = isset($_POST['setting_lista_general']) && is_array($_POST['setting_lista_general']) ? $_POST['setting_lista_general'] : array();

    $updateStatement = $pdoConn->prepare($updateSql);
    foreach ($info as $userId => $value)
    {
        $updateStatement->execute(array(':en_lista_gnrl' => $value, ':user_id' => $userId));
    }

}

$usuariosSql = "SELECT U.ID, U.NombreUsuario, U.en_lista_general FROM Usuarios_View U WHERE U.TipoUsuario = :tipo_usuario ORDER BY NombreUsuario";
$usuariosStatement = $pdoConn->prepare($usuariosSql);

$usuariosStatement->execute(array(':tipo_usuario' => NIVEL_PERMISO_USUARIO_CALLE));
$usuariosCalle = $usuariosStatement->fetchAll(PDO::FETCH_ASSOC);

$usuariosStatement->execute(array(':tipo_usuario' => NIVEL_PERMISO_USUARIO_WEB));
$usuariosWeb = $usuariosStatement->fetchAll(PDO::FETCH_ASSOC);

$usuariosGroup = array('Web' => $usuariosWeb, 'Calle' => $usuariosCalle);

?>

<h3 style="line-height:1px;">Configuración de listas</h3>
<h6 style="line-height:1px;">Administrar las distintas opciones para las listas por usuario</h6>
<form id="configuracion_listas_form" method="post" action="seccion_datos_configuracion_listas_form.php">
    <div class="divTable">
        <div class="divRow">
            <div class="divCellHeader2" style="width:120px;">Usuario</div>
            <div class="divCellHeader2" style="width:120px;">Lista General</div>
        </div>

        <?php foreach ($usuariosGroup as $name => $usuarios): ?>
            <div class="divRow">
                <div class="divCellHeader2" style="width: 100%"><?php echo $name ?></div>
            </div>
            <?php foreach ($usuarios as $usuario): ?>
                <div class="divRow2 various2 fancybox.ajax">
                    <div class="divCellCen2"
                         style="width:120px; text-align:left;"><?= $usuario['NombreUsuario']; ?></div>
                    <div class="divCellCen2" style="width:120px; text-align:center;">
                        <label for="usr_lst_gnral_si_<?php echo $usuario['ID'] ?>">Si</label>
                        <input type="radio" value="1" name="setting_lista_general[<?php echo $usuario['ID'] ?>]"
                               id="usr_lst_gnral_si_<?php echo $usuario['ID'] ?>"
                               style="visibility: visible; height: inherit"
                            <?php echo ($usuario['en_lista_general'] == 1) ? "checked" : ""; ?>
                            />

                        <label for="usr_lst_gnral_no_<?php echo $usuario['ID'] ?>">No</label>
                        <input type="radio" value="0" name="setting_lista_general[<?php echo $usuario['ID'] ?>]"
                               id="usr_lst_gnral_no_<?php echo $usuario['ID'] ?>"
                               style="visibility: visible; height: inherit"
                            <?php echo ($usuario['en_lista_general'] == 0) ? "checked" : ""; ?>
                            />
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endforeach; ?>

    </div>
    <input type="submit" value="Guardar Cambios" class="button">
</form>
<script>
    $('#configuracion_listas_form').ajaxForm({
        target: '#TablaUsuarios',
        success: function () {
            $('#TablaUsuarios').unblock();
        },
        beforeSubmit: function (formData, jqForm, options) {
            $('#TablaUsuarios').block();
            return true;
        }
    });
</script>