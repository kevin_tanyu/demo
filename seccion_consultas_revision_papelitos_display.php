<?php

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

$sorteoID = $_GET['SID'];

try{

    $sqlSorteos = "SELECT SP.ID, SD.NombreSorteo, SP.FechayHora
               FROM SorteosProgramacion SP
               JOIN SorteosDefinicion SD
               ON SP.IDSorteoDefinicion = SD.ID
               WHERE SP.ID = ?";
    $stmtSorteo = $pdoConn->prepare($sqlSorteos);
    $stmtSorteo->execute(array($sorteoID));
    $sorteo = $stmtSorteo->fetch();

    /*******************VENDEDORES******************************/
    $sqlVendedores = "SELECT U.ID, U.NombreUsuario
                      FROM Usuarios U
                      WHERE U.en_lista_revision = 1 and enPapelito = 1
                      order by U.NombreUsuario";

    $stmtVendedores = $pdoConn->prepare($sqlVendedores);
    $stmtVendedores->execute();
    $vendedores = $stmtVendedores->fetchAll(PDO::FETCH_ASSOC);
    /********************************************************************/

    /*GANADOR*/
    $sqlNumeroGanador = "SELECT Numero FROM SorteosNumerosGanadores
                         WHERE IDSorteoProgramacion = ? LIMIT 1";
    $stmtNumeroGanador = $pdoConn->prepare($sqlNumeroGanador);
    $stmtNumeroGanador->execute(array($sorteoID));

    if($stmtNumeroGanador->rowCount() > 0){
        $numGanador = $stmtNumeroGanador->fetch();
        $ganador = $numGanador['Numero'];
    }else{
        $ganador = -1;
    }

    /*************************************************/
    /*GET EXCESOS*/
    $sqlListaEnviada = "SELECT * FROM SorteoDevolucion
                        WHERE sorteo_prog_id = ? AND usuario_id = ?";
    $stmtListaEnviada = $pdoConn->prepare($sqlListaEnviada);

    $sqlNumerosExcesos = "SELECT numero, monto_devuelto FROM SorteoDevolucion_Detalle
                          WHERE sorteodevolucion_id = ?";
    $stmtNumerosExcesos = $pdoConn->prepare($sqlNumerosExcesos);
    /*************************************************/

    /*******************NUMEROS VENDIDOS POR VENDEDOR******************************/
    $sqlNumeros = "SELECT Cantidad, Numero
                   FROM sorteoapuesta_puestos
                   WHERE IDSorteoProgramacion = :sorteo_id and IDUsuario = :user_id";

    $stmtNumeros = $pdoConn->prepare($sqlNumeros);
    /********************************************************************/

    $totalesNumeros = array_fill(0, 100, 0);//PARA IR GUARDANDO EL TOTAL DE CADA NUMERO PARA SER ESCRITOS LUEGO

    foreach($vendedores as $vendedor){

        $stmtNumeros->execute(array(':sorteo_id' => $sorteoID, ':user_id' => $vendedor['ID']));
        $Numeros = $stmtNumeros->fetchAll(PDO::FETCH_ASSOC);

        /*EXCESOS*/
        $stmtListaEnviada->execute(array($sorteoID, $vendedor['ID']));
        $listaEnviada = $stmtListaEnviada->fetch();
        $devoID = $listaEnviada['id'];

        $stmtNumerosExcesos->execute(array($devoID));
        $Excesos = $stmtNumerosExcesos->fetchAll(PDO::FETCH_ASSOC);

        $totalesExcesosVendedor = array_fill(0, 100, 0);//PARA IR GUARDANDO LOS EXCESOS DE CADA VENDEDOR
        foreach($Excesos as $exceso){
            $totalesExcesosVendedor[$exceso['numero']] = $exceso['monto_devuelto'];
        }
        /********/

        foreach($Numeros as $numero){

            $pos = $numero['Numero'];
            $numNeto = $numero['Cantidad'] - $totalesExcesosVendedor[$pos];
            $totalesNumeros[$pos] = $totalesNumeros[$pos] + $numNeto;

        }//FIN FOREACH

    }//Fin foreach

    $totalSorteo = 0;
    $Subtotal1 = 0;
    $Subtotal2 = 0;
    $Subtotal3 = 0;

    for($i = 0; $i < sizeof($totalesNumeros); $i++){
        $totalSorteo = $totalSorteo + $totalesNumeros[$i];
    }

}catch (Exception $e){
    echo 'ERROR';
}


?>

<style type="text/css">
    .fancybox-custom .fancybox-skin {
        box-shadow: 0 0 50px #222;
    }

    input[type=checkbox] {
        visibility: visible;
        height: 15px;
    }

</style>

<h3>PELUQUERO SORTEO <?php echo $sorteo['NombreSorteo'] . ' DEL ' . $sorteo['FechayHora']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TOTAL: <?php echo system_number_money_format($totalSorteo)?>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <?php if($ganador != -1):?>
       GANADOR: <?php echo $ganador?>
    <?php endif?>
</h3>
<div class="divTable">
    <div class="divRow" style="width: 250px">
        <div class="divCellHeader2" style="width: 150px">Numero</div>
        <div class="divCellHeader2" style="width: 150px">Monto</div>
        <div class="divCellHeader2" style="width: 150px">Numero</div>
        <div class="divCellHeader2" style="width: 150px">Monto</div>
        <div class="divCellHeader2" style="width: 150px">Numero</div>
        <div class="divCellHeader2" style="width: 150px">Monto</div>
    </div>

   <?php for($a = 1; $a <= 34; $a++):?>
       <div class="divRow2">
           <?php if($a >= 10 && $a < 34){?>
               <div class="divCellCen2" style="width: 150px; text-align: center; font-size: 15px" id="divNum<?php echo $a?>">
                   <?php echo $a?>
               </div>
               <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px;" id="divMonto<?php echo $a?>">
                   <?php echo system_number_money_format($totalesNumeros[$a]);
                   $Subtotal1 = $Subtotal1 + $totalesNumeros[$a];?>
               </div>
           <?php }elseif($a < 10){ ?>
               <div class="divCellCen2" style="width: 150px; text-align: center; font-size: 15px" id="divNum<?php echo $a?>">
                   <?php echo '0' . $a?>
               </div>
               <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px" id="divMonto<?php echo $a?>">
                   <?php echo system_number_money_format($totalesNumeros[$a]);
                   $Subtotal1 = $Subtotal1 + $totalesNumeros[$a];?>
               </div>
           <?php }else{ ?>

           <?php }?>
           <?php if($a+33 != 67):?>
               <div class="divCellCen2" style="width: 150px; text-align: center; font-size: 15px" id="divNum<?php echo $a+33?>">
                   <?php echo $a+33?>
               </div>
               <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px" id="divMonto<?php echo $a+33?>">
                   <?php echo system_number_money_format($totalesNumeros[$a+33]);
                     $Subtotal2 = $Subtotal2 + $totalesNumeros[$a+33];?>
               </div>
           <?php endif ?>
           <?php if($a+66 != 101):?>
               <?php if($a+66 == 100){?>
                   <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px"></div>
                   <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px"></div>
                   <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px"></div>
                   <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px"></div>
                   <div class="divCellCen2" style="width: 150px; text-align: center; font-size: 15px" id="divNum<?php echo 0?>">
                       <?php echo $a+66?>
                   </div>
                   <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px" id="divMonto<?php echo 0?>">
                       <?php echo system_number_money_format($totalesNumeros[0]);
                       $Subtotal3 = $Subtotal3 + $totalesNumeros[0];?>
                   </div>
               <?php }else{ ?>
                   <div class="divCellCen2" style="width: 150px; text-align: center; font-size: 15px" id="divNum<?php echo $a+66?>">
                       <?php echo $a+66?>
                   </div>
                   <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px" id="divMonto<?php echo $a+66?>">
                       <?php echo system_number_money_format($totalesNumeros[$a+66]);
                       $Subtotal3 = $Subtotal3 + $totalesNumeros[$a+66];?>
                   </div>
               <?php } ?>
           <?php endif ?>
       </div>
   <?php endfor?>
    <div class="divRow2">
        <div class="divCellHeader2" style="width: 150px; text-align: center; font-size: 15px; font-weight: bold">Subtotal:</div>
        <div class="divCellHeader2" style="width: 150px; text-align: center; font-size: 15px; font-weight: bold"><?php echo system_number_money_format($Subtotal1)?></div>
        <div class="divCellHeader2" style="width: 150px; text-align: center; font-size: 15px; font-weight: bold">Subtotal:</div>
        <div class="divCellHeader2" style="width: 150px; text-align: center; font-size: 15px; font-weight: bold"><?php echo system_number_money_format($Subtotal2)?></div>
        <div class="divCellHeader2" style="width: 150px; text-align: center; font-size: 15px; font-weight: bold">Subtotal:</div>
        <div class="divCellHeader2" style="width: 150px; text-align: center; font-size: 15px; font-weight: bold"><?php echo system_number_money_format($Subtotal3)?></div>
    </div>

    <h3 style="line-height:1px;">
        <input type="button" id="btnImprimir" class="button"
               value="Imprimir" onclick="window.print();">

</div>

<?php if($ganador != -1):?>
    <script>
        $('#divNum<?php echo $ganador ?>').css('background-color', '#B5AFAB');
        $('#divMonto<?php echo $ganador ?>').css('background-color', '#B5AFAB');
    </script>
<?php endif?>



