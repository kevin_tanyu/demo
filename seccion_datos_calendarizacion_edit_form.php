<?php
include("config.ini.php");
include("conectadb.php");

$sorteoProgId = $_GET['SORTEO_PROG_ID'];

$sorteoSql = "SELECT S.*, SD.NombreSorteo FROM SorteosProgramacion S JOIN SorteosDefinicion SD ON SD.ID = S.IDSorteoDefinicion WHERE S.ID = ?";

$sorteoStmt = $pdoConn->prepare($sorteoSql);
$sorteoStmt->execute(array($sorteoProgId));
$sorteo = $sorteoStmt->fetch(PDO::FETCH_ASSOC);

$sorteoFechaHora = DateTime::createFromFormat('Y-m-d H:i:s', $sorteo['FechayHora']);

$restringidosSql = "SELECT * FROM SorteosNumerosRestringidos WHERE IDSorteoProgramacion = ?";
$restringidosStmt = $pdoConn->prepare($restringidosSql);
$restringidosStmt->execute(array($sorteo['ID']));
$restringidosRs = $restringidosStmt->fetchAll(PDO::FETCH_ASSOC);

$restringidos = array();
foreach ($restringidosRs as $restringido)
    $restringidos[] = $restringido['Numero'];

?>
<script type="text/javascript">

	function doChosen() {
        $(".chzn-select").chosen();
        $(".chzn-select-deselect").chosen({allow_single_deselect:true});
    }
	

</script>
<h3 style="line-height:1px;">Calendarizacion de Loteria</h3>
					<div style="text-align:center;">
						<form name="formulario2" id="formulario2">
                        <input name="sorteoProgId" type="hidden" value="<?php echo $sorteoProgId ?>" />
						<div class="divTable" style="width:650px;">
							<div class="divRow">
								<div class="divCellIzq">Sorteo a Editar</div>
								<div class="divCellDer">
                                    <?php echo $sorteo['NombreSorteo'] ?>
                                </div>
							</div>
							<div class="divRow">
								<div class="divCellIzq">Fecha y Hora</div>
                                <div class="divCellDer">
                                    <input type="text" name="Fecha" value="<?= $sorteoFechaHora->format('Y-m-d'); ?>"
                                           class="campotexto"
                                           id="datepicker"
                                           style="width:70px; font-size:12px; margin-right:5px; margin-top:1px;"
                                           required>
                                    <script>
                                        $(function () {
                                            $("#datepicker").datepicker({ constrainInput: true, dateFormat: "yy-mm-dd" });
                                        });
                                    </script>
                                    <select name="Hora" data-placeholder="Hora" class="chzn-select" style="width:60px;"
                                            tabindex="1"
                                            id="Hora"><?php
                                        for ($h = 0; $h <= 23; $h++)
                                        {
                                            ?>
                                            <option value="<?= $h; ?>"<?php if ($h == $sorteoFechaHora->format('H')) echo " SELECTED"; ?>><?php
                                            echo str_pad($h, 2, '0', STR_PAD_LEFT); ?></option><?php
                                        } ?></select>
                                    <select name="Minutos" data-placeholder="Min" class="chzn-select"
                                            style="width:60px;" tabindex="1"
                                            id="Minutos"><?php
                                        for ($h = 0; $h <= 59; $h = $h + 5)
                                        {
                                            ?>
                                            <option value="<?= $h; ?>"<?php if ($h == $sorteoFechaHora->format('i')) echo " SELECTED"; ?>><?php
                                            echo str_pad($h, 2, '0', STR_PAD_LEFT); ?></option><?php
                                        } ?></select>

                                </div>
							</div>
							<div class="divRow">
								<div class="divCellIzq">Numero de la Suerte</div>
								<div class="divCellDer"><select name="NumeroSuerte[]" data-placeholder="Numeros de la Suerte"  class="chzn-select" multiple style="width:200px;" >
									<?php
										for ($x=0;$x<=99;$x++) {
										?><option value="<?php echo $x; ?>" <?php if($x == $sorteo['NumeroDeSuerte']) echo "selected";?> <?php
                                           if($sorteo['NumeroDeSuerte2'] !== ''){
                                            if($x == $sorteo['NumeroDeSuerte2'])
                                                echo "selected";}?>><?php echo $x; ?></option><?php
										}
									?>
								</select>
								</div>
							</div>
							<div class="divRow">
								<div class="divCellIzq">Numeros Restringidos</div>
								<div class="divCellDer"><select name="NumeroRestringido[]" data-placeholder="Numeros Restringidos"  class="chzn-select" multiple="multiple" style="width:200px;" >
									<?php
										for ($x=0;$x<=99;$x++) {
										?><option value="<?php echo $x; ?>" <?php if(in_array($x,$restringidos)) echo "selected"; ?> ><?php echo $x; ?></option><?php
										}
									?>
								</select>
								</div>
							</div>

						</div>
                        <input type="button" value="Salvar" class="button" id="BotonFormulario" onclick="javascript:FormularioEditLoteria();">
						</form>
							<script type="text/javascript">
								 $('.chzn-select').chosen();
							</script>

					</div>
					<div id="Resultado"></div>
					<br><br>
