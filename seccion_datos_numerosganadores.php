<?php
session_start();

if (!isset($_SESSION['ActivoFlag']))
{
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

// Incluye Header
include("header.php");

?>
<script type="text/javascript">

    function isFunction(functionToCheck) {
        var getType = {};
        return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
    }

    //				function AbreVentanaNumerosGanadores() {
    $(".various2").fancybox({
        fitToView: true,
        width: '70%',
        height: '70%',
        autoSize: true,
        closeClick: false,
        openEffect: 'fade',
        closeEffect: 'elastic'

    });
    //						return false;
    //				}
    function FormularioAddNumerosGanadores() {

        if (confirm("¿Esta seguro de agregar este numero ganador?")) {
            $('#form_add_container').block();
            $.ajax({
                type: "POST",
                url: 'seccion_datos_numerosganadores_add.php',
                data: $('#formulario1').serialize(),
                success: function (response) {
                    //TablaLoterias();

                    loadSorteosFinalizados('sorteosFinalizadosContainer');
                    alert('Se enviaron los correos correspondientes');
                    $.fancybox.close();
                }
            }).always(function (data_jqXHR, textStatus, jqXHR_errorThrown) {
                $('#form_add_container').unblock();
            });
        }
        return false;
    }

    function FormularioReenviarGanadores() {
        $('#form_add_container').block();
        $.ajax({
            type: "POST",
            url: 'seccion_datos_numerosganadores_mail_add.php',
            data: $('#formulario1').serialize(),
            success: function (response) {
                loadSorteosFinalizados('sorteosFinalizadosContainer');
                alert('Se enviaron los correos correspondientes');
                $.fancybox.close();
            }
        }).always(function (data_jqXHR, textStatus, jqXHR_errorThrown) {
            $('#form_add_container').unblock();
        });
        return false;
    }

    function FormularioCambiarNumeroGanador() {
        $('#formulario1').block();
        var url = 'seccion_datos_numerosganadores_ganador_cambia.php';
        var data = $('#formulario1').serialize();
        $.post(url, data)
            .done(function (response_data) {
                response_data = $.parseJSON(response_data);
                var message = response_data.msg;
                if (!response_data.success)
                    message = response_data.error_msg;

                alert(message);

            })
            .always(function () {
                $('#formulario1').unblock();
            });
    }

    function loadSorteosFinalizados(divId, callback) {
        if (!isFunction(callback)) callback = function () {
        };
        var jqueryDivId = "#" + divId.toString();
        $(jqueryDivId.toString()).load("seccion_datos_numerosganadores_sorteosfinalizados.php", callback);
    }
</script>

<script type="text/javascript" src="js/countdown.js"></script>
<style type="text/css">
    br {
        clear: both;
    }

    .cntSeparator {
        font-size: 54px;
        margin: 10px 7px;
        color: #000;
    }

    .desc {
        margin: 7px 3px;
    }

    .desc div {
        float: left;
        font-family: Arial;
        width: 70px;
        margin-right: 65px;
        font-size: 13px;
        font-weight: bold;
        color: #000;
    }
</style>

<?php


include("seccion_datos_submenu.php");
?>
<div id="ContenedorGeneral">
    <h3 style="line-height:1px;">Numeros Ganadores</h3>
    <h6 style="line-height:1px;">Desde esta funcionalidad podra agregar numeros ganadores para las loterias y sorteos
        existentes en la base de datos.</h6>
    <?php
    $whereParaCasino = "";

    $QUERY = "SELECT sp.FechayHora,
													sp.NumeroDeSuerte, 
													sd.NombreSorteo, 
													sd.PagaPorcentaje,
													sp.id,

													CONCAT(
					LPAD(FLOOR(HOUR(TIMEDIFF(sp.FechayHora, NOW())) / 24),2,'0'), ':',
					LPAD(MOD(HOUR(TIMEDIFF(sp.FechayHora, NOW())), 24),2,'0'), ':',
					LPAD(MINUTE(TIMEDIFF(sp.FechayHora, NOW())),2,'0'), ':',
					LPAD(SECOND(TIMEDIFF(sp.FechayHora, NOW())),2,'0'))
					,sp.FechayHora > NOW() AS esProximo
									FROM 		SorteosProgramacion sp, 
													SorteosDefinicion sd
									WHERE 	sp.IDSorteoDefinicion=sd.ID 
										  AND 	sd.FlagActivo<>2
										  AND sp.ID NOT IN (SELECT idsorteoprogramacion FROM SorteosNumerosGanadores)
										  AND sp.FechayHora <= NOW()
										  $whereParaCasino
									ORDER BY sp.FechayHora DESC
									LIMIT 0,10
										  ";
    $rs = mysql_query($QUERY);
    if (mysql_affected_rows() > 0)
    {
        // Hay Sorteos programados
        $F = 0;
        while ($row = mysql_fetch_row($rs))
        {
            if ($F == 0)
            {
                $F = 1;
                ?>
                <?php if ($row['6'] == 1): //si es proximo el sorteo ?>
                <script type="text/javascript">
                    $(function () {
                        $('#counter').countdown({
                            image: 'images/digits.png',
                            startTime: '<?php echo $row[5]; ?>'
                        });
                    });
                </script>
                <div style="height:200px; padding:10px;" class="Fondo">
                    <div style="line-height:10px; margin-left:100px; margin-bottom:20px; "><h4>Cuenta regresiva para
                            el proximo sorteo</h4><h5
                            style="line-height:10px; margin-top:-10px; color:green;"><?= $row[0]; ?> |
                            Loteria <?= $row[2]; ?></h5></div>
                    <div id="counter" style="margin-left:100px; width:550px;  text-align:center;"></div>
                    <div class="desc" style="margin-left:100px;">
                        <div>Días</div>
                        <div>Horas</div>
                        <div>Minutos</div>
                        <div>Segundos</div>
                    </div>
                </div>
            <?php endif; ?>
                <div style="margin-top:30px; display:block;">
                <div class="ColumnaIzquierda" id="sorteosFinalizadosContainer" >
                <h4 style="line-height:1px;">Sorteos Finalizados</h4>
                <h6 style="line-height:1px;">Estos sorteos necesitan reportar numeros ganadores</h6>

                <div class="divTable">
                <div class="divRow">
                    <div class="divCellHeader" style="width:100px; font-size:10px;">Fecha</div>
                    <div class="divCellHeader" style="width:160px; font-size:10px; text-align:left;">Sorteo</div>
                    <div class="divCellHeader" style="width:30px; font-size:10px; text-align:center;">NS</div>
                </div>

                <div class="divRow2 various2 fancybox.ajax"
                     href="seccion_datos_numerosganadores_add_form.php?id=<?= $row[4]; ?>">
                    <div class="divCellCen letra"
                         style="width:100px;"><?= system_date_format(substr($row[0], 0, 10), false); ?></div>
                    <div class="divCellIzq letra" style="width:160px;">Loteria <?= $row[2]; ?></div>
                    <div class="divCellCen letra" style="width:30px;"><?= $row[1]; ?></div>
                </div>
            <?php

            } else
            {
                // Todo el Resto para la tabla de la izquierda
                ?>
                <div class="divRow2 various2 fancybox.ajax"
                     href="seccion_datos_numerosganadores_add_form.php?id=<?= $row[4]; ?>">
                    <div class="divCellCen letra"
                         style="width:100px;"><?= system_date_format(substr($row[0], 0, 10), false); ?></div>
                    <div class="divCellIzq letra" style="width:160px;">Loteria <?= $row[2]; ?></div>
                    <div class="divCellCen letra" style="width:30px;"><?= $row[1]; ?></div>
                </div>
            <?php

            }
        }
        ?>
        </div>

        </div>


    <?php
    }
    ?>

                    <div class="ColumnaDerecha">
                        <h4 style="line-height:1px;">Sorteos Terminados</h4>
                        <h6 style="line-height:1px;">Lista para reenviar el correo con ganadores</h6>

                        <div class="divTable">
                            <div class="divRow">
                                <div class="divCellHeader"
                                     style="width:160px; font-size:10px; text-align:left; padding-left:5px;">
                                    Fecha
                                </div>
                                <div class="divCellHeader"
                                     style="width:120px; font-size:10px; text-align:right;  padding-right:5px;">Sorteo
                                </div>
                            </div>
                            <?php

                            $sorteosGanadosSql = "SELECT SP.ID, SD.NombreSorteo, SP.FechayHora
                        FROM SorteosProgramacion SP
                            JOIN SorteosDefinicion SD ON SD.ID = SP.IDSorteoDefinicion
                        WHERE SP.ID IN (SELECT IDSorteoProgramacion FROM SorteosNumerosGanadores GROUP BY IDSorteoProgramacion)
                        ORDER BY SP.ID DESC
                        LIMIT 0,10";

                            $sorteosGanadosStatement = $pdoConn->prepare($sorteosGanadosSql);
                            $sorteosGanadosStatement->execute();
                            if ($sorteosGanadosStatement->rowCount() > 0)
                            {
                                while ($row = $sorteosGanadosStatement->fetch(PDO::FETCH_ASSOC))
                                {
                                    ?>
                                    <div class="divRow2 various2 fancybox.ajax"
                                         href="seccion_datos_numerosganadores_mail_form.php?id=<?php echo $row['ID']; ?>">
                                        <div class="divCellCen letra"
                                             style="width:160px; text-align:left; padding-left:5px;"><?php echo system_date_format($row['FechayHora']); ?></div>
                                        <div class="divCellIzq letra"
                                             style="width:120px; text-align:right;  padding-right:5px;"><?php echo $row['NombreSorteo']; ?></div>
                                    </div>
                                <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
</div>
<?php
// Incluye Footer
include("footer.php");
?>
