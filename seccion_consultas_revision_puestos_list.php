<?php

//

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");
require_once 'lib/seccion_datos_importacion_listas_lib.php';
include 'lib/PHPExcel/PHPExcel/IOFactory.php';


//Consulta para recuperar los sorteos del dia
$sqlSorteosDelDia = "SELECT SP.ID AS sorteo_prog_id, SD.NombreSorteo as nombre_sorteo, SP.NumeroDeSuerte
FROM SorteosProgramacion SP
JOIN SorteosDefinicion SD
    ON SP.IDSorteoDefinicion = SD.ID
WHERE DATEDIFF(NOW(), SP.FechayHora) = 0
 ORDER BY SD.NombreSorteo ASC
";

try
{
    $stmtSorteosDelDia = $pdoConn->prepare($sqlSorteosDelDia);
    $stmtSorteosDelDia->execute();
    $sorteosDelDia = $stmtSorteosDelDia->fetchAll(PDO::FETCH_ASSOC);
} catch (PDOException $e)
{
    $sorteosDelDia = array();
    echo $e->getMessage();
}

//Query para obterner los usuarios(vendedores)
/*$sqlVendedores = "SELECT U.ID as usuario_id, U.NombreUsuario as nombre_usuario
FROM Usuarios_View U
WHERE U.ID IN (SELECT S.IDUsuario FROM SorteoApuesta_Validas S WHERE S.IDSorteoProgramacion = :sorteo_prog_id) and U.en_lista_revision = 1 and U.NombreUsuario LIKE '%IMPORT_%'
order by U.NombreUsuario";*/

$sqlVendedores = "SELECT U.ID as usuario_id, U.NombreUsuario as nombre_usuario
FROM Usuarios U
WHERE U.en_lista_revision = 1
order by U.NombreUsuario";


$stmtVendedores = $pdoConn->prepare($sqlVendedores);

//Para obtener el total del sorteo por usuario DATEDIFF(sp.FechayHora,NOW()) = 0
$sqlTotalSorteo = "SELECT sp.ID, sd.NombreSorteo, SUM(sa.Cantidad) as total
                            FROM SorteosProgramacion sp
                              JOIN SorteosDefinicion sd on sd.ID = sp.IDSorteoDefinicion and DATEDIFF(sp.FechayHora,NOW()) = 0
                              JOIN sorteoapuesta_validas_puestos sa on sa.IDSorteoProgramacion = sp.ID and sa.IDUsuario = :id_usuario
                              WHERE sp.ID = :sorteo_prog_id
                            group by sp.ID, sd.NombreSorteo
                            order by sp.FechayHora
                            ";
$stmtTotalSorteo = $pdoConn->prepare($sqlTotalSorteo);

//PAra obtener el estado de revision del sorteo
$sqlStatusSorteo = "SELECT RS.id, RS.monto, RS.terminal, RS.jugada1, RS.jugada2,RS.devolucion,  S.name, S.display_name, RS.recarga
FROM COF_RevisionStatus_View RS
JOIN COF_Status S ON RS.status_id = S.id
WHERE RS.sorteo_programacion_id = :sorteo_prog_id AND RS.usuario_vendedor_calle_id = :user_id";
$stmtStatusSorteo = $pdoConn->prepare($sqlStatusSorteo);

//PARA OBTENER EL TOTAL DEL EXCEL
$sqlTotalExcel = "";
$stmtTotalExcel = $pdoConn->prepare($sqlTotalExcel);

//PARA ASOCIAR EL VENDEDOR CON EL SORTEO
$sqlVendedorSorteo = "INSERT INTO COF_Revision(usuario_vendedor_calle_id, sorteo_programacion_id)
                      VALUES(:user_id, :sorteo_prog_id)";
$stmtVendedorSorteo = $pdoConn->prepare($sqlVendedorSorteo);

//PARA COMPROBAR SI EL VENDEDOR YA ESTA ASOCIADO CON EL SORTEO
$sqlComprobarVendedor = "SELECT * FROM COF_Revision
                         WHERE usuario_vendedor_calle_id = :user_id and sorteo_programacion_id = :sorteo_prog_id";
$stmtComprobarVendedor = $pdoConn->prepare($sqlComprobarVendedor);

/*<input type="button" value="Refrescar" id="btnRefresh<?php echo $vendedorId.$sorteoDelDiaId?>"
                                       onclick="getMonto(<?php echo $sorteoDelDiaId ?> , '<?php echo $nombreUsuario ?>', <?php echo $vendedorId?>)">*/



?>

    <!--FIN DE LOS QUERYS DE CONSULTA-->
<ul class="tabs" style="height: 1000px">
<?php foreach ($sorteosDelDia as $sorteoDelDia): ?>
    <?php
    $sorteoDelDiaId = $sorteoDelDia['sorteo_prog_id'];
    $stmtVendedores->execute(array(':sorteo_prog_id' => $sorteoDelDiaId));
    $vendedores = $stmtVendedores->fetchAll(PDO::FETCH_ASSOC);
    ?>
<li>
     <?php
     $id = 0;
     if(isset($_GET['ID'])){
       $id = $_GET['ID'];
      }
     ?>

     <?php if($id == $sorteoDelDiaId){?>
        <input type="radio" checked name="tabs" id="tab<?php echo $sorteoDelDiaId?>" >
     <?php }else{?>
        <input type="radio" name="tabs" id="tab<?php echo $sorteoDelDiaId?>" >
     <?php }?>
<label for="tab<?php echo $sorteoDelDiaId?>" class="labeltabs" onclick="hash(<?php echo $sorteoDelDiaId?>);"><?php echo $sorteoDelDia['nombre_sorteo'] ?></label>
<div id="tab-content<?php echo $sorteoDelDiaId?>" class="tab-content animated fadeIn">
    <div class="divTable">
        <h3><?php echo $sorteoDelDia['nombre_sorteo']. "   " ." - Jugada"?>
            <input type="button" id="btnGuardar" class="button"
                   value="Guardar" onclick="descargarExcelJugadas(<?php echo $sorteoDelDiaId;?>, 'Guardar')">
            <input type="button" id="btnDescargar" class="button"
                    value="Descargar" onclick="descargarExcelJugadas(<?php echo $sorteoDelDiaId;?>, 'Descargar')"></h3>

        <div class="divCellCalendario">
            <div class="SorteosCalendarizados">
                <div class="divTable" style="width:100%;">
                    <div class="divRowHeader">
                        <div class="divCellHeader letra2" style="width:13em;">Vendedor</div>
                        <div class="divCellHeader letra2" style="width:13em;"
                             id="total_div_<?php echo $sorteoDelDiaId ?>">Total
                        </div>
                        <div class="divCellHeader letra2" style="width:13em;">Estado</div>
                        <div class="divCellHeader letra2" style="width:13em;">Monto enviado</div>
                        <div class="divCellHeader letra2" style="width:13em;">Jugada</div>
                        <div class="divCellHeader letra2" style="width:13em;">Accion</div>
                    </div>
                    <?php $allTotal = 0; ?>
                    <?php foreach ($vendedores as $vendedor): ?>
                        <?php
                        $vendedorId = $vendedor['usuario_id'];


                        $stmtStatusSorteo->execute(array(':sorteo_prog_id' => $sorteoDelDiaId, ':user_id' => $vendedorId));
                        $statusRow         = $stmtStatusSorteo->fetch(PDO::FETCH_ASSOC);

                        $stmtComprobarVendedor->execute(array(':user_id' => $vendedorId, ':sorteo_prog_id' => $sorteoDelDiaId));

                        if ($stmtComprobarVendedor->rowCount() == 0){
                            try{
                                $stmtVendedorSorteo->execute(array(':user_id' => $vendedorId, ':sorteo_prog_id' => $sorteoDelDiaId));
                            }catch (Exception $e){
                               echo($e->getMessage());
                            }

                        }



                        //$total['total'] = 0;
                        $stmtTotalSorteo->execute(array(':id_usuario' => $vendedorId, ':sorteo_prog_id' => $sorteoDelDiaId));
                        $total = $stmtTotalSorteo->fetch(PDO::FETCH_ASSOC);

                        $allTotal += $total['total'];

                        $status            = "";
                        $cssClass          = "";
                        $actionDisplayText = "--";
                        $actionDisplayText2 = "--";
                        $actionDisplayText3 = "--";
                        $actionName        = "";
                        $montoEnviado = 0;
                        $terminal = 0;
                        $recarga = 0;
                        $jugada1 = "";
                        $jugada2 = "";
                        $displayButton = true;
                        $displayButtonRefresh = false;
                        $showAutoButton = false;
                        $nombreUsuario = substr($vendedor['nombre_usuario'], 7);
                        switch ($statusRow['name'])
                        {
                            case 'sent':
                                $status            = "Solicita Revision";
                                if($statusRow['devolucion'] == 0){
                                    $cssClass          = "lista-solicita-revision";
                                }else{
                                    $cssClass          = "lista-devolucion";
                                }

                                $actionDisplayText = "Revisado. Ok";
                                $actionDisplayText2 = "Guarde y reenvie";
                                $actionDisplayText3 = "Whatsapp";
                                // $actionName        = "whatsapp";
                                $actionName        = "check_correct";
                                $montoEnviado = $statusRow['monto'];

                                if($statusRow['jugada1'] != -1){
                                    $jugada1 = $statusRow['jugada1'];
                                }
                                if($statusRow['jugada2'] != -1){
                                    $jugada2 = $statusRow['jugada2'];
                                }

                                $displayButtonRefresh = true;
                                break;
                            case 'check_correct':
                                $status            = "Revisado";
                                $cssClass          = "lista-revisado";
                                $montoEnviado = $statusRow['monto'];

                                if($statusRow['jugada1'] != -1){
                                    $jugada1 = $statusRow['jugada1'];
                                }
                                if($statusRow['jugada2'] != -1){
                                    $jugada2 = $statusRow['jugada2'];
                                }
                                $actionDisplayText = "--";
                                $displayButton = false;
                                break;
                            case 'check_return':
                                $status            = "Revisado con devolucion";
                                $cssClass          = "lista-revisado";
                                $montoEnviado = $statusRow['monto'];

                                if($statusRow['jugada1'] != -1){
                                    $jugada1 = $statusRow['jugada1'];
                                }
                                if($statusRow['jugada2'] != -1){
                                    $jugada2 = $statusRow['jugada2'];
                                }
                                $actionDisplayText = "--";
                                $displayButton = false;
                                break;
                            case 'whatsapp':
                                $status            = "WhatsApp";
                                $cssClass          = "lista-whatsapp";
                                $montoEnviado = $statusRow['monto'];
                                if($statusRow['jugada1'] != -1){
                                    $jugada1 = $statusRow['jugada1'];
                                }
                                if($statusRow['jugada2'] != -1){
                                    $jugada2 = $statusRow['jugada2'];
                                }
                                $actionDisplayText = "--";
                                $displayButton = false;
                                break;
                            case 'phone':
                                $status            = "Telefono";
                                $cssClass          = "lista-whatsapp";
                                $montoEnviado = $statusRow['monto'];
                                if($statusRow['jugada1'] != -1){
                                    $jugada1 = $statusRow['jugada1'];
                                }
                                if($statusRow['jugada2'] != -1){
                                    $jugada2 = $statusRow['jugada2'];
                                }
                                $actionDisplayText = "--";
                                $displayButton = false;
                                break;
                            case 'auto':
                                $status            = "Automatico";
                                $cssClass          = "lista-auto";

                                $actionDisplayText = "Revisado. Ok";
                                // $actionName        = "whatsapp";
                                $actionName        = "check_correct";
                                $montoEnviado = $statusRow['monto'];

                                if($statusRow['jugada1'] != -1){
                                    $jugada1 = $statusRow['jugada1'];
                                }
                                if($statusRow['jugada2'] != -1){
                                    $jugada2 = $statusRow['jugada2'];
                                }
                                $showAutoButton = true;
                                break;
                            default:

                                $status = "Sin solicitud";
                                $actionDisplayText = "--";
                                $displayButton = true;

                                break;
                        }
                        ?>
                        <div class="divRow <?php echo $cssClass; ?> ">
                            <div class="divCellCen letra"
                                 style="width:13em; font-weight: bold;"><?php echo $vendedor['nombre_usuario']; ?></div>
                            <div class="divCellCen letra"
                                 style="width:13em;">
                             <label id="lbltotalVendedor<?php echo $nombreUsuario.$sorteoDelDiaId?>"><?php echo system_number_money_format($total['total']); ?></label>
                               <?php if($displayButtonRefresh == true){ ?>
                                 <input type="image" src="./css/images/refresh.png" id="btnRefresh<?php echo $vendedorId.$sorteoDelDiaId?>"
                                    onclick="getMonto(<?php echo $sorteoDelDiaId ?> , '<?php echo $nombreUsuario ?>', <?php echo $vendedorId?>)">
                               <?php } ?>

                            </div>

                            <div class="divCellCen letra" style="width:13em;">
                                <?php echo $status; ?>
                            </div>

                            <div class="divCellCen letra" style="width:13em;">
                                <?php echo system_number_money_format($montoEnviado); ?>
                            </div>


                            <div class="divCellCen letra" style="width:13em;">
                                <?php echo $jugada1 . ' - ' . $jugada2; ?>
                            </div>

                            <div class="divCellCen letra" style="width:13em;">

                            <?php if($displayButton == true){?>
                                <?php if($showAutoButton == false){?>
                                  <select name="accion_select<?php echo $vendedorId.$sorteoDelDiaId?>" id="accion_select<?php echo $vendedorId.$sorteoDelDiaId?>" >
                                      <option value="phone"><?php echo 'Telefono' ?></option>
                                      <option value="sent"><?php echo $actionDisplayText2 ?></option>
                                      <option value="whatsapp"><?php echo $actionDisplayText3 ?></option>
                                   </select>
                                <?php }else{?>
                                    <select name="accion_select<?php echo $vendedorId.$sorteoDelDiaId?>" id="accion_select<?php echo $vendedorId.$sorteoDelDiaId?>" >
                                        <option value="check_correct"><?php echo $actionDisplayText ?></option>
                                    </select>
                                <?php }?>

                                <input type="button" id="btn<?php echo $nombreUsuario.$sorteoDelDiaId ?>" value="Enviar" class="action_trigger" href="#"
                                       data-sorteo_prog_id="<?php echo $sorteoDelDiaId ?>"
                                       data-vendedor_id="<?php echo $vendedorId ?>"
                                       data-next_action="<?php echo $actionName ?>"
                                       data-jugada_1="<?php echo $jugada1 ?>"
                                       data-jugada_2="<?php echo $jugada2 ?>"
                                       data-monto_revision="<?php echo $montoEnviado ?>"
                                       data-monto_terminal="<?php echo $terminal ?>"
                                       data-monto_recarga="<?php echo $recarga ?>">
                                <?php }else{
                                echo $actionDisplayText;
                            }?>

                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>



        </div>
         <?php if ($allTotal >= 0): ?>
            <script>
                $('#total_div_<?php echo $sorteoDelDiaId ?>').html(" <?php echo "Total <br/>" . system_number_money_format($allTotal)  ?> ");
            </script>
         <?php $allTotal = 0; ?>
        <?php endif; ?>
    </div>

</div>
</li>
<?php endforeach; ?>
</ul>





<script>
    function hash(id){
        location.hash = id;
    }
    function getMonto(sorteoDiaID, nombreArchivo, IdUsuario)
    {


        //PARA CAPTURAR EL TOTAL DEL SORTEO
        var capturaTotalSorteo = document.getElementById('total_div_' + sorteoDiaID).innerHTML.split('¢');

        var totalSorteo = 0;

        if(capturaTotalSorteo[1] != 0 && capturaTotalSorteo[1].length <= 7){
            var montoSorteo = capturaTotalSorteo[1].split('.')
            totalSorteo = montoSorteo[0] + montoSorteo[1];
        }else{
            var montoSorteo = capturaTotalSorteo[1].split('.')
            totalSorteo = montoSorteo[0] + montoSorteo[1] + montoSorteo[2] ;
        }

        //PARA CAPTURAR EL TOTAL DEL VENDEDOR
        var capturaTotalVendedor = document.getElementById('lbltotalVendedor' + nombreArchivo + sorteoDiaID).innerHTML.split('¢');

        var totalVendedor = 0;

        if(capturaTotalVendedor[1] != 0 && capturaTotalVendedor[1].length <= 7){
            var montoVendedor = capturaTotalVendedor[1].split('.')
            totalVendedor = montoVendedor[0] + montoVendedor[1]
        }else{
            var montoVendedor = capturaTotalVendedor[1].split('.')
            totalVendedor = montoVendedor[0] + montoVendedor[1] + montoVendedor[2] ;
        }

        //var data = {sorteo_prog_id : sorteoDiaID, nombre_archivo : nombreArchivo, id_usuario : IdUsuario};
        var data = [sorteoDiaID,  nombreArchivo, IdUsuario, totalVendedor, totalSorteo];

        var ajaxRequest = loadAjaxRequest();  // Carga el AJAX para que se pueda ejecutar



        //Recibimos la varibale que nos envian del server y la mostramos en el lado del cliente
        ajaxRequest.onreadystatechange = function() {
            if (ajaxRequest.readyState == 4) {//Verfica si el estado del AJAX es correcto

                var response = ajaxRequest.responseText.split("/");
                if(response[0] == 'Error'){
                    alert("No han subido el excel al FTP");
                    document.getElementById("accion_select" + IdUsuario + sorteoDiaID).style.visibility = "visible";
                    document.getElementById("btn" + nombreArchivo + sorteoDiaID).style.visibility = "visible";
                    document.getElementById("total_div_" + sorteoDiaID).innerHTML = 'TOTAL <br>' + response[1];
                    document.getElementById('lbltotalVendedor' + nombreArchivo + sorteoDiaID).innerHTML = response[2];
                }else{
                    var allTotal = ajaxRequest.responseText.split("/");

                    var ajaxDisplay = document.getElementById('lbltotalVendedor' + nombreArchivo + sorteoDiaID);
                    ajaxDisplay.innerHTML = (allTotal[0]);

                    document.getElementById("total_div_" + sorteoDiaID).innerHTML = 'TOTAL <br>' + allTotal[1];

                    document.getElementById("btn" + nombreArchivo + sorteoDiaID).style.visibility = "visible";
                    document.getElementById("accion_select" + IdUsuario + sorteoDiaID).style.visibility = "visible";
                    var select = document.getElementById("accion_select" + IdUsuario + sorteoDiaID);
                    var option = document.createElement("option");
                    option.value = "check_correct";
                    option.text = "Revisado. Ok";
                    select.add(option, select[0]);
                    var option2 = document.createElement("option");
                    option2.value = "check_return";
                    option2.text = "Revisado. Con devolucion";
                    select.add(option2, select[1]);
                    select.selectedIndex = 0;
                    document.getElementById("btnRefresh" + IdUsuario + sorteoDiaID).style.visibility = "hidden";
                }


            }
        }

        ajaxRequest.open("POST", "seccion_consultas_revision_puestos_refresh.php", true);
        ajaxRequest.setRequestHeader("Content-type", "application/json")
        ajaxRequest.send(JSON.stringify(data));



    }//Fin getMonto

    function descargarExcelJugadas(idSorteo, accion){


        var ajaxRequest = loadAjaxRequest();  // Carga el AJAX para que se pueda ejecutar
        //

        //Recibimos la varibale que nos envian del server y la mostramos en el lado del cliente
        ajaxRequest.onreadystatechange = function() {
            if (ajaxRequest.readyState == 4) {//Verfica si el estado del AJAX es correcto

                var url = ajaxRequest.responseText;
                //document.getElementById('btnDescargar').src = url;
                if(url.trim() == 'Guardado'){
                    alert('Guardado con exito');
                }else{
                    window.location = url;
                }

            }
        }

        ajaxRequest.open("GET", "seccion_consultas_jugadas_download.php?SID=" + idSorteo + "&accion=" + accion, true);
        ajaxRequest.send(null);


    }//FIN descargarExcel

    function descargarExcelSeguros(idSorteo, accion){

        var ajaxRequest = loadAjaxRequest();  // Carga el AJAX para que se pueda ejecutar
        //

        //Recibimos la varibale que nos envian del server y la mostramos en el lado del cliente
        ajaxRequest.onreadystatechange = function() {
            if (ajaxRequest.readyState == 4) {//Verfica si el estado del AJAX es correcto


                var url = ajaxRequest.responseText;

                //document.getElementById('btnDescargar').src = url;
                if(url.trim() == "Guardado"){
                    alert('Guardado con exito');
                }else{
                    window.location = url;
                }


            }
        }

        ajaxRequest.open("GET", "seccion_consultas_seguros_download.php?SID=" + idSorteo + "&accion=" + accion, true);
        ajaxRequest.send(null);


    }//FIN descargarExcel






    //Funcion para ejecutar el AJAX
    function loadAjaxRequest(){
        var ajaxRequest;  // The variable that makes Ajax possible!

        try {
            // Opera 8.0+, Firefox, Safari
            ajaxRequest = new XMLHttpRequest();
        } catch (e) {
            // Internet Explorer Browsers
            try {
                ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {
                    // Something went wrong
                    alert("Your browser broke!");
                    return null;
                }
            }
        }
        return ajaxRequest;
    }

</script>

