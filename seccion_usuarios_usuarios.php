<?php
session_start();

if (!isset($_SESSION['ActivoFlag'])) {
	// Verifica si hay session creada, de lo contrario redirige al index
	header("Location: index.php?IDM=TO");
	exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

// Incluye Header
include("header.php");

include("seccion_usuarios_submenu.php");
?>
		<script type="text/javascript">
			$(document).ready(function() {
				$(".various2").fancybox({
					fitToView	: true,
					width		: '100%',
					height		: '100%',
					autoSize	: true,
					closeClick	: false,
					openEffect	: 'fade',
					closeEffect	: 'elastic'
				});

	


			});
			
				function FormularioAddUsuario() {
					$.ajax({
						type: "POST",
						url: 'seccion_usuarios_usuarios_add.php',
						data: $('#formulario1').serialize(),
						success: function(response) {
								$("#jqxgrid").jqxGrid("updatebounddata");
								$.fancybox.close();
						}
					});

					return false;
				}


			</script>		
			<style type="text/css">
				.fancybox-custom .fancybox-skin {
					box-shadow: 0 0 50px #222;
				}
			</style>
	
			<div id="ContenedorGeneral">

				<h3 style="line-height:1px;">Usuarios</h3>
				<h6 style="line-height:1px;">Agregar, Modificar o Borrar los usuarios con permisos de jugador dentro del sistema.</h6>
			
				<script type="text/javascript">

				$(document).ready(function () {
						// prepare the data
						var data = {};
						var theme = 'classic';
						var source =
						{
							datatype: "json",
							datafields: [
								 { name: 'NombreUsuario' },
//								 { name: 'Contrasena' },
								 { name: 'Email' },
                                { name: 'NumeroTelCasa' },
                                { name: 'NumeroTelCelular' },
                                { name: 'Pregunta' },
								 { name: 'Respuesta' },
								 { name: 'ID' }
							],
							id: 'ID',
							url: 'seccion_usuarios_usuarios_getdata.php',
							updaterow: function (rowid, rowdata, commit) {
								// synchronize with the server - send update command
								var data = "update=true&NombreUsuario=" + rowdata.NombreUsuario + "&Contrasena=" + rowdata.Contrasena + "&Email=" + rowdata.Email;
								data = data + "&Respuesta=" + rowdata.Respuesta + "&Pregunta=" + rowdata.Pregunta + "&ID=" + rowdata.ID;
								$.ajax({
									dataType: 'json',
									url: 'seccion_usuarios_usuarios_getdata.php',
									data: data,
									success: function (data, status, xhr) {
										// update command is executed.
										commit(true);
									},
									error: function () {
										// cancel changes.
										commit(false);
									}
								});
							},
							deleterow: function (rowid, commit) {
								// synchronize with the server - send update command
								var data = "delete=true&ID="+rowid;
								$.ajax({
									dataType: 'json',
									url: 'seccion_usuarios_usuarios_getdata.php',
									data: data,
									success: function (data, status, xhr) {
										// update command is executed.
										commit(true);
									},
									error: function () {
										// cancel changes.
										commit(false);
									}
								});										
								
							}

						};
						// initialize jqxGrid
						$("#jqxgrid").jqxGrid(
						{
							width: 700,
							height: 350,
							source: source,
							theme: theme,
							editable: true,
							selectionmode: 'row',
							columns: [
								  { text: 'NombreUsuario', editable: false, datafield: 'NombreUsuario', width: 180 },
								  { text: 'Contrasena', datafield: 'Contrasena', width: 100 },
								  { text: 'Email',  datafield: 'Email', width: 180 },
                                { text: 'Telefono Casa', datafield: 'NumeroTelCasa', width: 100 },
                                { text: 'Telefono Celular', datafield: 'NumeroTelCelular', width: 100 },
                                { text: 'Pregunta', datafield: 'Pregunta', width: 180 },
								  { text: 'Respuesta', datafield: 'Respuesta', width: 180 },
                                { text: 'ID', datafield: 'ID', width: 60, hidden: true }
                            ]
						});
							//$("#deleterowbutton").jqxButton({ theme: theme });
							$("#deleterowbutton").on('click', function () {
								var selectedrowindex = $("#jqxgrid").jqxGrid('getselectedrowindex');
								var rowscount = $("#jqxgrid").jqxGrid('getdatainformation').rowscount;
								if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
									if (confirm("Esta seguro de querer borrar el usuario?")) {
										var id = $("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
										var commit = $("#jqxgrid").jqxGrid('deleterow', id);
									}
								} else {
									alert('Debe seleccionar una fila para borrarla');
								}
							});
						});							

			
					</script>
			
					<div id='jqxWidget' style="font-size: 13px; font-family: Verdana; float: left;">
						<a class="various2  fancybox.ajax button" href="seccion_usuarios_usuarios_form.php" style="padding-top:3px; padding-bottom:3px; font-size:10px;  float:left; margin-bottom:10px;">Agregar Usuario</a>
						<input type="button" id="deleterowbutton" class="button" style="color:#ff0000; font-size:10px; padding-top:6px; padding-bottom:6px; float:left; margin-bottom:10px;" value="Borrar Usuario Seleccionado">
						<div style="float: left;" id="jqxgrid">
						</div>
					</div>			
			</div>
<?php
// Incluye Footer
include("footer.php");
?>
