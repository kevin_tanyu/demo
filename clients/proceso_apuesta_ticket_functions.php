<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 7/23/14
 * Time: 8:57 PM
 */

require_once 'paths.php';
require_once $global_system_path . '/config.ini.php';
require_once $global_system_path . '/conectadb.php';

//get balance of user
function getBalanceUsuario($userId)
{
    $balanceSql = "SELECT monto_disponible as disponible FROM CLIENTE_balance_bolsas_total WHERE IDUsuario = " . $userId;
    $balanceRs = mysql_query($balanceSql);
    $balance = mysql_fetch_assoc($balanceRs);
    return $balance['disponible'];
}

function saveTicket($ticketJson,$userId)
{
    //save ticket
    $sorteoId = $ticketJson->sorteoId;
    $date = date('Y-m-d H:i');
    $sqlInsertTicket = "INSERT INTO CLIENTE_Ticket (IDUsuario,FechaYHora) VALUES ( " . $userId . ", '". $date ."' )";
    mysql_query($sqlInsertTicket);
    $ticketId = mysql_insert_id();
    $ticketNumber = $ticketId;

//save apuestas
    foreach( $ticketJson->apuestas as $apuesta )
    {
        $sqlApuesta = "INSERT INTO CLIENTE_SorteoApuesta (IDTicket,IDUsuario,IDSorteoProgramacion,Numero,Cantidad,FechayHora)"
            . " VALUES($ticketId,$userId,$sorteoId,". $apuesta->number .",". $apuesta->amount .",'".$date."') ";
        mysql_query($sqlApuesta);
    }
    //libero los locks, pues ya no es necesario su uso
    liberarLocks();
    if($ticketId)
    {
        decrementUserBalance($userId,$ticketJson->totalAmount,$ticketId);
    }

    return array('ticket_id' => $ticketId, 'ticket_number' => $ticketNumber);
}

//decides from where extract money
function decrementUserBalance($userId, $amount, $ticketId)
{

    //obtener monto disponible en bolsa depositos

    $disponiblesSql = "SELECT monto_disponible AS disponible,disponible_bolsa_depositos,disponible_bolsa_premios FROM CLIENTE_balance_bolsas_total WHERE IDUsuario = $userId";
    $disponiblesRs = mysql_query($disponiblesSql);
    $disponibles = mysql_fetch_assoc($disponiblesRs);
    $debitoTipo = getDebitoTipo('ticket');

    $tomarDePremios = ($disponibles['disponible_bolsa_depositos'] - $amount) < 0;

    //si tiene que quitar de los premios
    $montoDebitoBolsaDeposito = ($tomarDePremios)? $disponibles['disponible_bolsa_depositos'] : $amount;
    $timeStamp = date('Y-m-d H:i');
    processDecrementBolsaDepositos($montoDebitoBolsaDeposito,$userId,$ticketId,$debitoTipo['id'],$timeStamp);
    if($tomarDePremios )
    {
        $montoDebitoBolsaPremios = $amount - $disponibles['disponible_bolsa_depositos'];
        processDecrementBolsaPremios($montoDebitoBolsaPremios,$userId,$ticketId,$debitoTipo['id'],$timeStamp);
    }

}


function processDecrementBolsaDepositos($amount,$userId,$ticketId,$tipoDebitoId,$timeStamp)
{
    $sql = "UPDATE CLIENTE_balance_bolsa_depositos SET disponible = disponible - $amount WHERE IDUsuario = $userId";
    mysql_query($sql);

    $sqlInsert = "INSERT INTO CLIENTE_balance_debito ( IDUsuario,IDTicket,IDTipoDebito,monto,created_at ) VALUES ( $userId, $ticketId,$tipoDebitoId ,$amount, '$timeStamp' ) ";
    mysql_query($sqlInsert);
}

function processDecrementBolsaPremios($amount,$userId,$ticketId,$tipoDebitoId,$timeStamp)
{
    $sql = "UPDATE CLIENTE_balance_bolsa_premios SET disponible = disponible - $amount WHERE IDUsuario = $userId";
    mysql_query($sql);

    $sqlInsert = "INSERT INTO CLIENTE_balance_debito ( IDUsuario,IDTicket,IDTipoDebito,monto, created_at ) VALUES ( $userId, $ticketId,$tipoDebitoId ,$amount, '$timeStamp' ) ";
    mysql_query($sqlInsert);
}

function getDebitoTipo($nombre)
{
    $debitoTipo = array();
    $sql = "SELECT id,nombre,nombreMostrar FROM CLIENTE_balance_debito_tipo WHERE nombre = '".$nombre."'";
    $rs = mysql_query($sql);

    if($rs)
        $debitoTipo = mysql_fetch_assoc($rs);

    return $debitoTipo;
}


function getRestrictedNumbers($sorteoProgramacionId)
{
    $retorno = array();
    $sql = "SELECT Numero,reglaporcentaje FROM CLIENTE_SorteosNumerosRestringidos WHERE IDSorteoProgramacion = $sorteoProgramacionId";
    $rs = mysql_query($sql);
    while($numero = mysql_fetch_assoc($rs))
    {
        $retorno[$numero['Numero']] = array( 'numero' => $numero['Numero'], 'porcentaje' => $numero['reglaporcentaje'] );
    }
    return $retorno;
}

function checkTicketValidityRestrictedNumbers($ticketJSON,$restrictedNumbers)
{
    $errors = array();
    foreach($ticketJSON->apuestas as $apuesta)
    {
        if( array_key_exists( $apuesta->number, $restrictedNumbers ) )
        {
            $restrictedLimitValue = ($restrictedNumbers[$apuesta->number]['porcentaje'] * $ticketJSON->totalAmount) / 100;
            if( ($restrictedLimitValue - $apuesta->amount) < 0 )
            {
                $errors[$apuesta->number] = array(
                    'codigo' => ErrorsCodeNumbers::$_ERR_NUMBER_RESTRICTED_NUMBERS,
                    'numero' => $apuesta->number,
                    'texto'  => "Disponible solo: " . $restrictedLimitValue,
                    'disponible' => $restrictedLimitValue
                );
            }
        }
    }
    return $errors;
}


//funciones para validacion de los topes en ticket

//valida los topes de los numeros, libera los locks en caso de que encuentre algun error
function validarDisponibilidadTotal($ticketJSON)
{
    $sorteoId = $ticketJSON->sorteoId;
    $numeros = array();
    foreach($ticketJSON->apuestas as $apuesta )
    {
        $numeros[] = " SELECT " . $apuesta->number . " AS numero, " . $apuesta->amount . " AS monto_apuesta ";
    }
    $numerosString = implode(" UNION ",$numeros);

//    importante notar que en el where pregunta si la diferencia es menor que cero, esto indica que no se debe
    $sqlGetMontos = "SELECT CLIENTE_SorteosNumerosEstado.numero
          , CASE
            WHEN CLIENTE_SorteosNumerosEstado.estado = 'abierto' THEN CLIENTE_SorteosMontosTope.TopeNoRestringidos - IFNULL(CLIENTE_SorteosNumerosGastado.gastado,0)
            WHEN CLIENTE_SorteosNumerosEstado.estado = 'restringido' THEN CLIENTE_SorteosMontosTope.TopeRestringidos - IFNULL(CLIENTE_SorteosNumerosGastado.gastado,0)
            WHEN CLIENTE_SorteosNumerosEstado.estado = 'restringido_mixto' THEN CLIENTE_SorteosMontosTope.TopeRestringidosMixtos - IFNULL(CLIENTE_SorteosNumerosGastado.gastado,0)
          END
          AS disponible,
          CASE
           WHEN CLIENTE_SorteosNumerosEstado.estado = 'abierto' THEN (CLIENTE_SorteosMontosTope.TopeNoRestringidos - IFNULL(CLIENTE_SorteosNumerosGastado.gastado,0) ) - IFNULL(TEMP_APUESTAS.monto_apuesta,0)
           WHEN CLIENTE_SorteosNumerosEstado.estado = 'restringido' THEN (CLIENTE_SorteosMontosTope.TopeRestringidos - IFNULL(CLIENTE_SorteosNumerosGastado.gastado,0) ) - IFNULL(TEMP_APUESTAS.monto_apuesta,0)
           WHEN CLIENTE_SorteosNumerosEstado.estado = 'restringido_mixto' THEN ( CLIENTE_SorteosMontosTope.TopeRestringidosMixtos - IFNULL(CLIENTE_SorteosNumerosGastado.gastado,0) ) - IFNULL(TEMP_APUESTAS.monto_apuesta,0)
           END
        AS diferencia
        FROM CLIENTE_SorteosNumerosEstado
          JOIN CLIENTE_SorteosMontosTope ON CLIENTE_SorteosMontosTope.IDSorteoProgramacion = CLIENTE_SorteosNumerosEstado.IDSorteoProgramacion
          LEFT JOIN CLIENTE_SorteosNumerosGastado ON CLIENTE_SorteosNumerosGastado.IDSorteoProgramacion = CLIENTE_SorteosNumerosEstado.IDSorteoProgramacion AND CLIENTE_SorteosNumerosGastado.numero = CLIENTE_SorteosNumerosEstado.numero
          LEFT JOIN ( $numerosString ) AS TEMP_APUESTAS ON TEMP_APUESTAS.numero = CLIENTE_SorteosNumerosEstado.numero
        WHERE CLIENTE_SorteosNumerosEstado.IDSorteoProgramacion = $sorteoId
          AND CASE
              WHEN CLIENTE_SorteosNumerosEstado.estado = 'abierto' THEN (CLIENTE_SorteosMontosTope.TopeNoRestringidos - IFNULL(CLIENTE_SorteosNumerosGastado.gastado,0) ) - IFNULL(TEMP_APUESTAS.monto_apuesta,0)
              WHEN CLIENTE_SorteosNumerosEstado.estado = 'restringido' THEN (CLIENTE_SorteosMontosTope.TopeRestringidos - IFNULL(CLIENTE_SorteosNumerosGastado.gastado,0) ) - IFNULL(TEMP_APUESTAS.monto_apuesta,0)
              WHEN CLIENTE_SorteosNumerosEstado.estado = 'restringido_mixto' THEN ( CLIENTE_SorteosMontosTope.TopeRestringidosMixtos - IFNULL(CLIENTE_SorteosNumerosGastado.gastado,0) ) - IFNULL(TEMP_APUESTAS.monto_apuesta,0)
              END < 0
        ORDER BY numero";

    $rs = mysql_query($sqlGetMontos);

    //en caso de error en el query, o que retorne filas que tienen diferencia negativa( lo que diria que el ticket tiene numeros no disponibles )
    //en tal caso se deben liberar los locks para que otros tickets puedan acceder a la validacion sin ningun problema
    $rowsQty = mysql_num_rows($rs);
    if( $rowsQty > 0 || $rowsQty === false ) liberarLocks();

    $errors = array();
    while($err = mysql_fetch_assoc($rs))
    {
        $codigoError = ( $err['disponible'] > 0 ) ? ErrorsCodeNumbers::$_ERR_NUMBER_OVER_TOP_MONEY : ErrorsCodeNumbers::$_ERR_NUMBERR_NOT_AVAILABLE ;
        $errors[$err['numero']] = array( 'numero' => $err['numero'], 'codigo' => $codigoError, 'disponible' => $err['disponible'] );
    }

    return $errors;

}


//funcion que une los errores de restringidos y de validacion de disponibilidad, ademas de darle un formato uniforme a los errores
function mergeTicketErrors($errorsValidacionRestringidos,$errorsValidacionDisponibilidad,$ticketJSON)
{
    $finalErrors = array();
    $errors = count($errorsValidacionRestringidos) + count($errorsValidacionDisponibilidad);

    if($errors > 0)
    {

        foreach($ticketJSON->apuestas as $apuesta)
        {
            $numero = $apuesta->number;
            if( array_key_exists($numero,$errorsValidacionRestringidos) && array_key_exists($numero,$errorsValidacionDisponibilidad) )
            {
                //esta funcion tambien debe darle formato a los errores de ser necesario
                $finalErrors[$numero] = decidePrecedenceError($errorsValidacionRestringidos[$numero],$errorsValidacionDisponibilidad[$numero]);
            }elseif(array_key_exists($numero,$errorsValidacionRestringidos)){
                $finalErrors[$numero] = $errorsValidacionRestringidos[$numero];
            }elseif(array_key_exists($numero,$errorsValidacionDisponibilidad)){
                $finalErrors[$numero] = $errorsValidacionDisponibilidad[$numero];
            }
        }

    }

    return $finalErrors;
}


//decide cual error retornar dependiendo del valor en el disponible de cada error
// y le da formato( si se necesitara )
function decidePrecedenceError($errorValidacionRestringido, $errorValidacionDisponibilidad)
{
    $error = array('numero' => $errorValidacionRestringido['numero'] );
    $codigo = 0;
    $disponible = 0;

    $diferenciaValidaciones = $errorValidacionRestringido['disponible'] - $errorValidacionDisponibilidad['disponible'];

    //si es mayor que cero, indica que hay menos disponibilidad del numero globalmente
    if( $diferenciaValidaciones > 0 )
    {
        $codigo     = $errorValidacionDisponibilidad['codigo'];
        $disponible = $errorValidacionDisponibilidad['disponible'];

    }elseif( $diferenciaValidaciones <= 0 )
    {
        $codigo     = $errorValidacionRestringido['codigo'];
        $disponible = $errorValidacionRestringido['disponible'];
    }

    $error['codigo'] = $codigo;
    $error['disponible'] = $disponible;

    return $error;
}

function obtenerLocks()
{
    $sqlObtenerLocks = "LOCK TABLES CLIENTE_SorteosNumerosEstado READ, CLIENTE_SorteosMontosTope READ, CLIENTE_SorteosNumerosGastado READ, CLIENTE_SorteosNumerosRestringidos READ,CLIENTE_Ticket WRITE, CLIENTE_SorteoApuesta WRITE";
    mysql_query($sqlObtenerLocks);
}

function liberarLocks()
{
    $sqlLiberarLocks = "UNLOCK TABLES";
    mysql_query($sqlLiberarLocks);
}

function isValidSorteo($sorteoId)
{
    $now       = date("Y-m-d H:i:s");
    $sorteoSql = "SELECT ID FROM SorteosProgramacion WHERE ID = $sorteoId AND FechaYHora >= '$now'";
    mysql_query($sorteoSql);

    return mysql_affected_rows() > 0;
}