<?php
require_once 'paths.php';
require_once $global_system_path . '/config.ini.php';
require_once $global_system_path . '/conectadb.php';

$sqlLoteriasDia = "SELECT NombreSorteo AS nombre,id FROM SorteosDefinicion WHERE flagactivo = 1 AND id IN (SELECT IDSorteoDefinicion FROM SorteosProgramacion SP WHERE SP.FechayHora>=DATE(NOW())+' 00:00:00' AND NOW() <= SP.FechaYHora )";
$loteriasRs = mysql_query($sqlLoteriasDia);

?>
<style>
    #loteria-list li {
        cursor: default;
    }

    .divTableNew {
        width: 50%;
        height: 50%;
        display: table;
    }

    .divTableRowNew {
        width: 100%;
        height: 100%;
        display: table-row;
    }

    .divTableCellNew {
        width: 40%;
        height: 100%;
        display: table-cell;
        min-width: 7em;
        padding: 1em;
    }


    .divTableCellNew:hover {
        background-color: #868D96;
        cursor: pointer;
    }

</style>
<form action="proceso_apuesta_sorteos.php" id="loterias_form" method="post">
    <h3>Seleccione Tipo de Sorteo</h3>

    <div id="loteria-list" class="divTableNew">
        <?php $counter = 0; ?>
        <?php $rowLimit = 3; ?>
        <?php $rowQtyGrp = mysql_num_rows($loteriasRs) / $rowLimit; ?>
        <?php for ($i = 0; $i < $rowQtyGrp; ++$i): ?>
            <?php $counter = 0; ?>
            <div class="divTableRowNew">
                <?php while ( $counter < $rowLimit && ( $row = mysql_fetch_assoc($loteriasRs) ) ): ?>
                    <div class="divTableCellNew" data-loteria_id="<?php echo $row['id'] ?>"
                         data-nombre="<?php echo $row['nombre'] ?>">
                        <?php $imageName = strtolower(str_replace(' ','_',$row['nombre'])) . ".png"; ?>
                        <img src="images/<?php echo $imageName; ?>">
                    </div>
                    <?php ++$counter; ?>
                <?php endwhile; ?>
            </div>
        <?php endfor; ?>
    </div>
    <div><h4>Sorteo seleccionado <span id="loteria_seleccionada" style="color: orangered;"></span></h4></div>
    <div>
        <input type="hidden" value="" id="loteria_id" name="loteria_id"/>
    </div>
</form>

<script>
    $(function () {
        var options = {
            target: '#process'   // target element(s) to be updated with server response

            // other available options:
            //url:       url         // override for form's 'action' attribute
            //type:      type        // 'get' or 'post', override for form's 'method' attribute
            //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)
            //clearForm: true        // clear all form fields after successful submit
            //resetForm: true        // reset the form after successful submit

            // $.ajax options can be used here too, for example:
            //timeout:   3000
        };

        // bind form using 'ajaxForm'
        $('#loterias_form').ajaxForm(options);
        var $loterias_list = $("#loteria-list");
        $loterias_list.on('click', 'div', function () {
            var value = $(this).data('loteria_id');
            $('#loteria_id').val(value);
            $('#loteria_seleccionada').html($(this).data('nombre'));
            $('#loterias_form').submit();
            $('#process').block();
        });

        $loterias_list.on('mouseenter', 'div', function () {
            var value = $(this).data('loteria_id');
            $('#loteria_id').val(value);
            $('#loteria_seleccionada').html($(this).data('nombre'));
        });

    });
</script>