<?php
session_start();
require_once 'paths.php';
require_once $global_system_path . '/config.ini.php';
require_once $global_system_path . '/conectadb.php';

$nombreUsuario   = mysql_real_escape_string($_POST['username']);
$email           = mysql_real_escape_string($_POST['email']);
$telefonoCasa    = mysql_real_escape_string($_POST['telefono_casa']);
$telefonoCelular = mysql_real_escape_string($_POST['telefono_celular']);
$domicilio       = mysql_real_escape_string($_POST['domicilio']);
$cedula          = mysql_real_escape_string($_POST['cedula']);
$password        = mysql_real_escape_string($_POST['password']);
$passwordConf    = mysql_real_escape_string($_POST['password_conf']);
$pregunta        = mysql_real_escape_string($_POST['pregunta']);
$respuesta       = mysql_real_escape_string($_POST['respuesta']);

$cuponCreacionTextId = mysql_real_escape_string($_POST['cupon']);


$cuponSql      = "SELECT * FROM CLIENTE_CuponCreacion where activated_at is null and text_id = '" . $cuponCreacionTextId . "'";
$cuponRs       = mysql_query($cuponSql);
$cuponCreacion = mysql_fetch_assoc($cuponRs);
$cupoCreacionCount = mysql_affected_rows();

$validaEmailSql = "SELECT * FROM Usuarios_View where email = '" . $email . "'";
$validaEmailRs  = mysql_query($validaEmailSql);
if (mysql_affected_rows() > 0)
{
    header("Location: signup.php?IDM=EMEX");
    session_destroy();
    exit;
}

if ($cuponCreacion && $cupoCreacionCount > 0)
{
    $sqlCreaUsuario = "INSERT INTO Usuarios ( NombreUsuario,Contrasena,TipoUsuario,"
        . "ActivoFlag,FlagActivo,IDPadre,Email,jugador_cedula,jugador_pregunta,"
        . "jugador_respuesta, jugador_tel_casa, jugador_tel_celular, jugador_domicilio ) "
        . " VALUES ( "
        . "'" . $nombreUsuario . "'"
        . ",'" . $password . "'"
        . ",'" . NIVEL_PERMISO_USUARIO_WEB_CLIENTE . "'"
        . ",1"
        . ",1"
        . ",'" . $cuponCreacion['IDUsuarioRevendedor'] . "'"
        . ",'" . $email . "'"
        . ",'" . $cedula . "'"
        . ",'" . $pregunta . "'"
        . ",'" . $respuesta . "'"
        . ",'" . $telefonoCasa . "'"
        . ",'" . $telefonoCelular . "'"
        . ",'" . $domicilio . "'"
        . ")";
    mysql_query($sqlCreaUsuario);
    $idUser = mysql_insert_id();
    if ($idUser == 0)
    {
        header("Location: signup.php?IDM=INTERR");
        session_destroy();
    } else
    {
        $balanceDepositosSql = "INSERT INTO CLIENTE_balance_bolsa_depositos ( disponible,IDUsuario ) VALUES "
            . " ("
            . $cuponCreacion['monto']
            . "," . $idUser
            . " ) ";
        mysql_query($balanceDepositosSql);

        $balancePremiosSql = "INSERT INTO CLIENTE_balance_bolsa_premios (IDUsuario) VALUES (" . $idUser . ")";
        mysql_query($balancePremiosSql);

        $cuponRegistroSql = "UPDATE CLIENTE_CuponCreacion SET activated_at = now(), IDUsuarioCliente = " . $idUser . " WHERE id = " . $cuponCreacion['id'];
        mysql_query($cuponRegistroSql);

        $_SESSION['IDUsuario_client']     = $idUser;
        $_SESSION['NombreUsuario_client'] = $nombreUsuario;
        $_SESSION['TipoUsuarioInt']       = NIVEL_PERMISO_USUARIO_WEB_CLIENTE;
        $_SESSION['TipoUsuario']          = "Jugador";
        $_SESSION['ActivoFlag']           = 1;
        $_SESSION['Email']                = $email;

        header("Location: primera.php");
        exit;

    };
} else
{
    header("Location: signup.php?IDM=CPINV");
    session_destroy();
}

?>