<?php
session_start();

if (!isset($_SESSION['ActivoFlag'])) {
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
require_once 'paths.php';
require_once $global_system_path . '/config.ini.php';
require_once $global_system_path . '/conectadb.php';


$userId = $_SESSION['IDUsuario_client'];

$jugadorSql = "SELECT * FROM Usuarios_View WHERE ID = $userId";
$jugadorRs = mysql_query($jugadorSql);
$jugador = mysql_fetch_assoc($jugadorRs);
$depositosSql = "SELECT d.created_at,d.monto,dt.nombreMostrar FROM CLIENTE_balance_deposito d JOIN CLIENTE_balance_deposito_tipo dt ON d.IDTipoDeposito = dt.id WHERE IDUsuarioCliente = $userId";
$depositosRs = mysql_query($depositosSql);

$debitosSql = "SELECT d.created_at,d.monto,dt.nombreMostrar FROM CLIENTE_balance_debito d JOIN CLIENTE_balance_debito_tipo dt ON d.IDTipoDebito = dt.id WHERE IDUsuario = $userId ";
$debitosRs = mysql_query($debitosSql);


// Incluye Header
include("header.php");

include("seccion_balance_submenu.php");
?>
<script type="text/javascript">
    $(document).ready(function() {
        $(".various").fancybox({
            fitToView	: true,
            width		: '100%',
            height		: '100%',
            autoSize	: true,
            closeClick	: false,
            openEffect	: 'fade',
            closeEffect	: 'elastic'
        });
    });
</script>
<style type="text/css">
    .fancybox-custom .fancybox-skin {
        box-shadow: 0 0 50px #222;
    }
</style>
<div id="ContenedorGeneral">

    <div style="padding-bottom: 1em;" >
        <h4 style="margin-bottom: 0.5em">Usuario: <?php echo $jugador['NombreUsuario'] ?> </h4>
        <h6 style="margin-top: 0.5em;margin-bottom: 0.5em">Email: <?php echo $jugador['Email'] ?> </h6>
        <h6 style="margin-top: 0.5em;margin-bottom: 0.5em;">
            Disponible Premios: <?php echo '&cent;' . system_number_format($jugador['balance_premios']); ?> </h6>
        <h6 style="margin-top: 0.5em;margin-bottom: 0.5em;">
            Disponible Depositos: <?php echo '&cent;' . system_number_format($jugador['balance_depositos']); ?> </h6>
        <h6 style="margin-top: 0.5em">
            Total Disponible: <?php echo '&cent;' . system_number_format($jugador['balance_disponible']); ?> </h6>
    </div>
    <div style="margin-top:30px; display:block;">

        <div class="ColumnaIzquierda" id="sorteosFinalizadosContainer">
            <h4 style="line-height:1px;">Depósitos</h4>

            <div class="divTable">
                <div class="divRow">
                    <div class="divCellHeader" style="width:100px; font-size:10px;">Fecha</div>
                    <div class="divCellHeader" style="width:90px; font-size:10px; text-align:center;">Cantidad</div>
                    <div class="divCellHeader" style="width:90px; font-size:10px; text-align:center;">Tipo</div>
                </div>
                <?php while ($deposito = mysql_fetch_assoc($depositosRs)): ?>
                    <div class="divRow2">
                        <div class="divCellCen letra"
                             style="width:100px; font-size:10px;"><?php echo $deposito['created_at'] ?></div>
                        <div class="divCellCen letra"
                             style="width:90px; font-size:10px;"><?php echo '&cent;' . system_number_format($deposito['monto']) ?></div>
                        <div class="divCellCen letra"
                             style="width:90px; font-size:10px;"><?php echo $deposito['nombreMostrar'] ?></div>
                    </div>
                <?php endwhile; ?>
            </div>

        </div>

        <div class="ColumnaDerecha">
            <h4 style="line-height:1px;">Débitos</h4>

            <div class="divTable">
                <div class="divRow">
                    <div class="divCellHeader" style="width:100px; font-size:10px;">Fecha</div>
                    <div class="divCellHeader" style="width:90px; font-size:10px; text-align:center;">Cantidad</div>
                    <div class="divCellHeader" style="width:90px; font-size:10px; text-align:center;">Tipo</div>
                </div>
                <?php while ($debito = mysql_fetch_assoc($debitosRs)): ?>
                    <div class="divRow2">
                        <div class="divCellCen letra"
                             style="width:100px; font-size:10px;"><?php echo $debito['created_at'] ?></div>
                        <div class="divCellCen letra"
                             style="width:90px; font-size:10px;"><?php echo '&cent;' . system_number_format($debito['monto']) ?></div>
                        <div class="divCellCen letra"
                             style="width:90px; font-size:10px;"><?php echo $debito['nombreMostrar'] ?></div>
                    </div>
                <?php endwhile; ?>
            </div>

        </div>
    </div>
</div>
<?php
// Incluye Footer
include("footer.php");
?>
