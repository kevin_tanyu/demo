<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 7/23/14
 * Time: 3:12 PM
 */
session_start();
require_once 'paths.php';
require_once $global_system_path . '/config.ini.php';
require_once $global_system_path . '/conectadb.php';
require_once $global_system_path . '/parametros.ini.php';
require_once 'ErrorsCodeNumbers.php';
require_once 'proceso_apuesta_ticket_functions.php';


$userId = $_SESSION['IDUsuario_client'];

//response initial values
$response                   = array();
$response['success']        = true;
$response['errors_ticket']  = array();
$response['errors_general'] = array();

$ticketJSON = json_decode($_POST['ticket']);

//validacion de sorteo
$sorteoValido = isValidSorteo($ticketJSON->sorteoId);

//validacion de balances
$balance         = getBalanceUsuario($userId);
$errorEnBalances = ($balance - ($ticketJSON->totalAmount)) < 0;

//obtengo los locks sobre las tablas para que la validacion sea consecuente
if ($sorteoValido && !$errorEnBalances) obtenerLocks();

//validacion de la regla del % en ticket
$restrictedNumbers             = getRestrictedNumbers($ticketJSON->sorteoId);
$errorsValidacionRestringidos  = checkTicketValidityRestrictedNumbers($ticketJSON, $restrictedNumbers);
$errorValidacionRestringidos   = count($errorsValidacionRestringidos) > 0;
if ($errorValidacionRestringidos) liberarLocks(); //esto para que se descongestione si hubo algun llamado a obtenerLocks


//verificar que los numeros que vienen no sobrepasen el monto de proteccion
$errorsValidacionDisponibilidad = validarDisponibilidadTotal($ticketJSON);
$errorValidacionDisponibilidad = (count($errorsValidacionDisponibilidad) > 0);


$ticketSaveInfo                  = array();
$ticketSaveInfo['ticket_id']     = -1;
$ticketSaveInfo['ticket_number'] = -1;

if ($sorteoValido && !$errorEnBalances && !$errorsValidacionRestringidos && !$errorValidacionDisponibilidad) {
    $ticketSaveInfo = saveTicket($ticketJSON, $userId);
    liberarLocks();
} else {
    $response['success'] = false;
    if ($errorEnBalances) {
        $errorString = 'Su cuenta no tiene los fondos suficientes para realizar esta accion';
        $errorString .= ' Fondos actuales &cent;' . system_number_format($balance);
        $response['errors_general'][] = array('err_number' => ErrorsCodeNumbers::$_ERR_NUMBER_INSUFICIENT_MONEY, 'text' => $errorString);
    }

    if (!$sorteoValido) {
        $errorString                  = 'El sorteo ya se cerro';
        $response['errors_general'][] = array('err_number' => ErrorsCodeNumbers::$_ERR_NUMBER_CLOSED_SORTEO, 'text' => $errorString);
    }

    $allErrorsTicket   = mergeTicketErrors($errorsValidacionRestringidos, $errorsValidacionDisponibilidad, $ticketJSON);
    $response['errors_ticket'] = $allErrorsTicket;
    $response['debug'] = json_encode($allErrorsTicket);

}

$ticketId     = $ticketSaveInfo['ticket_id'];
$ticketNumber = $ticketSaveInfo['ticket_number'];

//TODO handle properly errors
$response['ticket_id']     = $ticketId;
$response['ticket_number'] = $ticketNumber;

header('Content-type: application/json');
echo json_encode($response);