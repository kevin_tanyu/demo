<?php
session_start();

require_once 'paths.php';
require_once $global_system_path . '/config.ini.php';
require_once $global_system_path . '/conectadb.php';
require_once 'ErrorsCodeNumbers.php';

$loteriaId = $_POST['loteria_id'];
$sorteoId = $_POST['sorteo_id'];


//get sorteos info
$sqlSorteoInfo = "select sp.ID,sp.FechayHora,sp.NumeroDeSuerte,sd.NombreSorteo from SorteosProgramacion sp
                    join  SorteosDefinicion sd on sd.ID = sp.IDSorteoDefinicion where sp.ID = " . $sorteoId;
$rs = mysql_query($sqlSorteoInfo);
$sorteoInfo = mysql_fetch_assoc($rs);

//get restricted numbers
$restrictedNumbers = array();
$restrictedPercent = 30;
$SQL = "SELECT Numero, ReglaPorcentaje
FROM CLIENTE_SorteosNumerosRestringidos
WHERE IDSorteoProgramacion=" . $sorteoId;
$rs = mysql_query($SQL);
if (mysql_affected_rows() > 0) {
    while ($row = mysql_fetch_assoc($rs)) {
        $restrictedNumbers[] = $row['Numero'];
        $restrictedPercent   = $row['ReglaPorcentaje'];
    }
}
$restrictedNumbers = array();

$javascriptRestrictedNumbers = "[" . implode(',', $restrictedNumbers) . "]";
$textViewRestrictedNumbers = implode(',', $restrictedNumbers) ? : "--";

?>
<style>
    .input_number {
        padding: 8px;
        border: solid 1px #E5E5E5;
        font: normal 70px Verdana, Tahoma, sans-serif;
        width: 40%;
        height: 100%;
        background: #FFFFFF url('form_background.png') left top repeat-x;
        background: -webkit-gradient(linear, left top, left 25, from(#FFFFFF), color-stop(4%, #EEEEEE), to(#FFFFFF));
        background: -moz-linear-gradient(top, #FFFFFF, #EEEEEE 1px, #FFFFFF 25px);
        box-shadow: rgba(0, 0, 0, 0.1) 0 0 8px;
        -moz-box-shadow: rgba(0, 0, 0, 0.1) 0 0 8px;
        -webkit-box-shadow: rgba(0, 0, 0, 0.1) 0 0 8px;
    }

</style>
<script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.min.js"></script>
<script src="js/apuesta_process.js"></script>
<script>
    <?php $reflection = new ReflectionClass('ErrorsCodeNumbers'); ?>
    <?php $properties = $reflection->getStaticProperties(); ?>
    <?php foreach( $properties as $name => $value ):  ?>
    ErrorCodes.<?php echo $name ?> = <?php echo $value ?>;
    <?php endforeach; ?>

    var errorCssClass = [];
    errorCssClass[ErrorCodes._ERR_NUMBER_OVER_TOP_MONEY] = "ticket_error_over_top";
    errorCssClass[ErrorCodes._ERR_NUMBER_RESTRICTED_NUMBERS] = "ticket_error_restricted_number";
    errorCssClass[ErrorCodes._ERR_NUMBERR_NOT_AVAILABLE] = "ticket_error_not_available";


</script>

<div id="num_monto_box">
    <form id="form_apuestas">
        <label for="number">N&uacute;mero</label>
        <input class="input_number" type="number" min="00" max="99" id="number" required="" style="width: 20%;"/>
        <label for="amount">Monto</label>
        <input class="input_number" id="amount" required="" min="1"
               data-inputmask="'alias': 'numeric', 'groupSeparator': '.', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '¢', 'placeholder': '0'"/>
        <input type="submit" value="Agregar Apuesta" class="fancy_button" style="width: auto"/>
    </form>
</div>

<div id="BarraIzquierda" style="float: none; display: inline">
    <div id="Titulo" style="background-color: #f16529">RESUMEN DE TICKET</div>
    <div id="Encabezado">
        <div id="EncabezadoRow">
            <div id="EncabezadoCellIzq">Fecha y Hora</div>
            <div id="EncabezadoCell"><?= date('d/m/Y H:i:s'); ?></div>
        </div>
        <div id="EncabezadoRow">
            <div id="EncabezadoCellIzq">Evento</div>
            <div
                id="EncabezadoCell"><?php echo $sorteoInfo['NombreSorteo'] . " para " . system_date_format($sorteoInfo['FechayHora']); ?></div>
        </div>
        <div id="EncabezadoRow">
            <div id="EncabezadoCellIzq">Numero Suerte</div>
            <div id="EncabezadoCell"><?php echo $sorteoInfo['NumeroDeSuerte']; ?></div>
        </div>
        <!-- <div id="EncabezadoRow">
            <div id="EncabezadoCellIzq">Restringidos</div>
            <div id="EncabezadoCell"><?php /*?><?php echo $textViewRestrictedNumbers; ?> (<?php echo $restrictedPercent; ?><?php */ ?>%)
            </div>-->
    </div>
</div>
<div id="ticket_preview">
    <div id="ticket_state" style="height: auto; min-height: 2em; position: relative; right: -32em;"></div>
</div>
<div id="Resumen"></div>
<div style=" margin-top: 1em; ">
    <input id="finalizar_button" type="button" value="Finalizar" class="fancy_button" style="width: auto"/>
</div>
</div>


<!--<div id="ticket_preview">-->
<!--    <div id="ticket_state" style="height: auto" ></div>-->
<!--</div>-->
<div id="dialog-confirm" title="Borrar Apuesta?" style="visibility:hidden">
    Esta seguro que desea borrar la apuesta seleccionada?
</div>
<div id="dialog-buena-suerte" title="Ticket Finalizado" style="visibility:hidden">
    Buena Suerte!!
</div>
<script>

//init values

var restrictedNumbers = <?php echo $javascriptRestrictedNumbers ?>;
var restrictedPercent = <?php echo $restrictedPercent ?>;
var sorteoId = <?php echo $sorteoId ?>;
var ticket = new Ticket(restrictedNumbers, restrictedPercent);
ticket.sorteoId = sorteoId;

var number_view = $('#number');
var amount_view = $('#amount');

amount_view.inputmask();
number_view.focus();

$('#agregar_apuesta_view').click(function () {
    handleAddApuesta();
});

$('#form_apuestas').on('submit', function (e) {
    e.preventDefault();
    handleAddApuesta();
});

//bind action for delete apuesta
$('#Resumen').on('click', '.remove-x', function (e) {
    e.preventDefault();
    element = this;
    var $dialog = $("#dialog-confirm");
    $dialog.css("visibility", "visible");
    $dialog.dialog({
        resizable: false,
        height: 230,
        modal: true,
        buttons: {
            "Borrar": function () {
                handleDeleteApuesta(element);
                $(this).dialog("close");
                $("#dialog-confirm").css("visibility", "hidden");
            },
            "Cancelar": function () {
                $(this).dialog("close");
                $("#dialog-confirm").css("visibility", "hidden");
            }
        }
    });

});

//bind action to finalize button
$('#finalizar_button').click(handleFinalizeTicket);
hideFinalizeButton();

function handleAddApuesta() {
    tempApuesta = new Apuesta(number_view.val(), amount_view.val());
    ticket.addApuesta(tempApuesta);
    errors = ticket.checkTicketValidity();
    refreshApuestaList(ticket);
    cleanApuestaFields();

    element = $('#ticket_state');
    if (errors.length > 0) {
//            element.removeClass('ApuestaValida').addClass('ApuestaInvalida');
//            element.html('La apuesta tiene numeros restringidos que exceden el valor permitido');
    } else {
//            element.removeClass('ApuestaInvalida').addClass('ApuestaValida');
//            element.html('Apuesta Valida');
        showFinalizeButton();
    }

}

function handleDeleteApuesta(element) {
    number = parseInt($(element).data('num'));
    ticket.deleteApuesta(number);
    refreshApuestaList(ticket);
    if (ticket.getApuestas().length == 0) {
        hideFinalizeButton();
    }
}

function afterFinalizeProcessAnimation() {
    var $dialog = $("#dialog-buena-suerte");
    $dialog.css("visibility", "visible");
    $dialog.dialog({
        resizable: false,
        height: 230,
        modal: true
    });
    setTimeout(function () {
        window.location.replace("seccion_usuario_apuesta.php");
    }, 5000);
}

function handleFinalizeTicket() {
    //check if ticket is ok for submission
    if (ticket.checkTicketValidity().length == 0 && ticket.totalAmount > 0) {
        $('#process').block();
        //if is ok then serialize and send to script for add it
        element = $('#ticket_state');
        $.post("proceso_apuesta_ticket_action.php", { ticket: JSON.stringify(ticket) })
            .done(function (data) {
                ticket.cleanApuestaErrors();
                if (data.success) {
                    element.html("Número de ticket " + data.ticket_number);
                    element.removeClass('ApuestaInvalida').addClass('ApuestaValida');
                    hideFinalizeButton();
                    $('#agregar_apuesta_view').hide();
                    afterFinalizeProcessAnimation();
                } else {
                    errMsg = "No se pudo crear el tiquete por alguna de las siguientes razones: <ul>";
                    data.errors_general.forEach(function (error) {
                        errMsg += "<li>" + error.text + "</li>"
                    });
                    errMsg += "</ul>";
                    element.html(errMsg);
                    element.removeClass('ApuestaValida').addClass('ApuestaInvalida');
                    var ticketErrors = data.errors_ticket;
                    if (typeof ticketErrors.length === "undefined")//esto indica que es el objeto que contiene los errores y no un array(que de acuerdo a la salida es vacio)
                    {
                        ticket.getApuestas().forEach(function (apuesta) {
                            errorTicket = ticketErrors[ apuesta.getNumber() ];
                            if (typeof errorTicket !== "undefined") {
                                apuestaErrTemp = new ApuestaError(errorTicket.codigo, errorTicket.numero, errorTicket.disponible);
                                apuesta.addError(apuestaErrTemp);
                            }
                        });
                    }
                }
                refreshApuestaList(ticket);
            })
            .fail(function (x, e) {
                if (x.status == 0) {
                    alert('You are offline!!\n Please Check Your Network.');
                } else if (x.status == 404) {
                    alert('Requested URL not found.');
                } else if (x.status == 500) {
                    alert('Internel Server Error.');
                } else if (e == 'parsererror') {
                    alert('Error.\nParsing JSON Request failed.');
                } else if (e == 'timeout') {
                    alert('Request Time out.');
                } else {
                    alert('Unknow Error.\n' + x.responseText);
                }
            })
            .always(function () {
                $('#process').unblock();
            });

    } else {
        //else display error and continue process
    }


}

function cleanApuestaFields() {
    number_view.val("");
    amount_view.val("");
    number_view.focus();
}

function refreshApuestaList(ticket) {
    var html = "";
    ticket.getApuestas().forEach(function (apuesta) {
        highlightClass = (apuesta.getNumber() == ticket.lastAddedApuesta.getNumber()) ? " class='highlight-temp' " : "";
        var errorString = "";
        var errorHtml = "";
        if (apuesta.hasError()) {
            var errorTmp = apuesta.getError();
            errorString = errorTmp.getErrorString();
            errorCssClassString = errorCssClass[errorTmp.codigo];
            errorHtml = "<span class='" + errorCssClassString + "' >" + errorString + "</span>";
        }

        html +=
            "<div " + highlightClass + " >"
                + "<div class='Numero' >" + apuesta.getStringNumber() + "</div>"
                + "<div class='Monto' >&cent;"
                + apuesta.getAmount().systemFormatMoney()
                + "</div>"

                + "<div class='ColumnTicketRes' >"
                + errorHtml
                + "<button class='remove-x' data-num='" + apuesta.getNumber() + "'/>"
                + "</div>"

                + "</div>";
    });
    html += "<div>"
        + "<div class='total_resumen_ticket_jay_cambiele_color Numero' >" + "Total" + "</div>"
        + "<div class='total_resumen_ticket_jay_cambiele_color Monto' >&cent;" + ticket.totalAmount.systemFormatMoney() + "</div>"
        + "<div class='total_resumen_ticket_jay_cambiele_color Monto' ></div>"
        + "</div>";
    $('#Resumen').html(html);
    $('.highlight-temp').each(function () {
        blinkTemp(this);
    });
}


//aux functions
function blinkTemp(element) {
//        element.toggle( "pulsate");
//      $(element).effect( "highlight" );
    $(element).modernBlink({
        // Duration specified in milliseconds (integer)
        duration: 1000,
        // Number of times the element should blink ("infinite" or integer)
        iterationCount: 5,
        // Whether to start automatically or not (boolean)
        auto: true
    });
}

function showFinalizeButton() {
    $('#finalizar_button').show();
}

function hideFinalizeButton() {
    $('#finalizar_button').hide();
}

</script>