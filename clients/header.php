<?php
if (!isset($_SESSION['IDUsuario_client'])) {
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}
$_userId = $_SESSION['IDUsuario_client'];
$jugadorSql = "SELECT balance_premios,balance_depositos,balance_disponible FROM Usuarios_View WHERE ID = $_userId";
$jugadorRs = mysql_query($jugadorSql);
$jugador = mysql_fetch_assoc($jugadorRs);

?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>- - followyourluck.net - -</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main_main.css">
		<link rel="stylesheet" href="css/colorbox.css">
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
		
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.4"></script>
		<script type="text/javascript" src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
        <script type="text/javascript" src="js/plugins.js?_v=1"></script>

        <link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.4" media="screen"/>

        <script src="js/chosen.jquery.js" type="text/javascript"></script>
		  <script src="js/printThis.js" type="text/javascript"></script>
		  <script type="text/javascript">
			var config = {
			  '.chzn-select'           : {},
			  '.chzn-select-deselect'  : {allow_single_deselect:true},
			  '.chzn-select-no-single' : {disable_search_threshold:10},
			  '.chzn-select-no-results': {no_results_text:'Sin Resultados!'},
			  '.chzn-select-width'     : {width:"95%"}
            };
            for (var selector in config) {
			  $(selector).chosen(config[selector]);
			}
		  </script>
		  <link rel="stylesheet" href="js/chosen.css" />
		  <style type="text/css" media="all">
			/* fix rtl for demo */
			.chzn-rtl .chzn-search { left: -9000px; }
			.chzn-rtl .chzn-drop { left: -9000px; }

              .box_user_info{
                  position: relative;
                  right: -96%;
                  color: white;
                  font-size: 15px;
              }

              .box_user_info ul{
                  padding-left: 20px;
              }

            .box_user_info ul li{
                display: block;
                margin-bottom: -4px;
            }

		  </style>
		
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <div class="header-container">
            <header class="wrapper clearfix">
                <div class="title"><img src="images/logo.png"></div>
                <nav>
                    <ul>
                        <li><a href="logout.php"><img alt="Salir" src="images/salir.png" /></a></li>
                        <li><a href="seccion_consulta.php"><img src="images/consulta.png" /></a></li>
                        <li><a href="seccion_balance.php"><img src="images/balance.png" /></a></li>
                        <li><a href="seccion_usuario_apuesta.php"><img src="images/apueste.png" /></a></li>
                    </ul>
                    <div class="box_user_info"  >
                        <ul>
                            <li>Bienvenido: <?php echo $_SESSION['NombreUsuario_client'] ?></li>
                            <li>Premios: <?php echo "&cent;" . system_number_format($jugador['balance_premios']) ; ?></li>
                            <li>Depositos: <?php echo "&cent;" . system_number_format($jugador['balance_depositos']) ; ?></li>
                            <li style="font-size:18px">Total: <?php echo "&cent;" . system_number_format($jugador['balance_disponible']) ; ?></li>
                        </ul>
                    </div>
                </nav>
            </header>
        </div>
<!--        <div style="width: 100%; text-align: right;" >Bienvenido: --><?php //echo $_SESSION['NombreUsuario_client'] ?><!--. <a href="logout.php" style=" padding-right: 0.5em; " >Salir</a></div>-->
               
        <div class="main-container">
			<div class="main wrapper clearfix">