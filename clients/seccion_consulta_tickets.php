<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 7/24/14
 * Time: 8:08 PM
 */
session_start();

if (!isset($_SESSION['ActivoFlag'])) {
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

require_once 'paths.php';

// Incluye datos generales y conexion a DB
require_once $global_system_path . '/config.ini.php';
require_once $global_system_path . '/conectadb.php';

$userId = $_SESSION['IDUsuario_client'];

$ticketsSql = "SELECT * FROM CLIENTE_TicketsInfo_View WHERE IDUsuario =  $userId";
$ticketsRs = mysql_query($ticketsSql);


// Incluye Header
include("header.php");

include("seccion_consulta_submenu.php");
?>
    <style type="text/css">
        .fancybox-custom .fancybox-skin {
            box-shadow: 0 0 50px #222;
        }
    </style>

    <script>
        $(function () {
            $(".various").fancybox({
                fitToView: true,
                width: '200px',
                height: '60%',
                autoSize: false,
                closeClick: false,
                openEffect: 'fade',
                closeEffect: 'elastic'
            });
        });
    </script>

    <div id="ContenedorGeneral">
        <div class="divTable">
            <div class="divRow">
                <div class="divCellHeader2" style="width:170px;">Fecha</div>
                <div class="divCellHeader2" style="width:120px;">Monto</div>
                <div class="divCellHeader2" style="width:120px;">#</div>
                <div class="divCellHeader2" style="width:120px;"></div>
            </div>
            <?php while ($ticket = mysql_fetch_assoc($ticketsRs)): ?>
                <div class="divRow2" >
                    <div class="divCellCen2"
                         style="width:170px;"><?php echo system_date_format($ticket['FechayHora']); ?></div>
                    <div class="divCellCen2"
                         style="width:120px;"><?php echo '&cent;' . system_number_format($ticket['Monto']); ?></div>
                    <div class="divCellCen2" style="width:120px;"><?php echo $ticket['Number']; ?></div>
                    <div class="divCellCen2" style="width: 120px;" ><a class="various fancybox.ajax" href="seccion_consulta_ticket_info.php?TID=<?php echo $ticket['ID'] ?>" >View</a></div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
<?php
// Incluye Footer
include("footer.php");
?>