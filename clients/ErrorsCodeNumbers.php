<?php

/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 8/25/14
 * Time: 10:28 PM
 */
class ErrorsCodeNumbers
{

    public static $_ERR_NUMBER_INSUFICIENT_MONEY = 100; //el jugador no tiene suficiente dinero en balances
    public static $_ERR_NUMBER_OVER_TOP_MONEY = 101; // el numero que desea jugar tiene disponible para apuesta, pero no tanto como se le esta apostando
    public static $_ERR_NUMBER_RESTRICTED_NUMBERS = 102; //error de restricted number, sobrepasa el % permitido por ticket
    public static $_ERR_NUMBERR_NOT_AVAILABLE = 103;
    public static $_ERR_NUMBER_CLOSED_SORTEO = 104; //el numero tiene cero disponible para apostarle

} 