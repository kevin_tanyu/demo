<?php
include_once 'paths.php';
include_once($global_system_path . "/config.ini.php");
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>
<html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>SigueTuSuerte.com</title>
    <link rel="stylesheet" type="text/css" href="css/<?php
    if ($isMobile) echo "handheld.css";
    else echo "main.css";
    ?>"/>
    <?php if ($isMobile): ?>
        <meta name="viewport" content="user-scalable=no,initial-scale=1.0, maximum-scale=1.0, width=device-width"/>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/canvas.js"></script>
    <?php
    endif;
    if (!$isMobile):
        ?>
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <?php endif; ?>
    <script src="bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="bower_components/jquery-validation/src/localization/messages_es.js"></script>
</head>
<body>
<div id="container" class="container">
    <section id="content">
        <form action="register.php" method="post" id="form_register">
            <h1>Registro</h1>

            <?php if(isset($_GET['IDM']) && $_GET['IDM'] == 'CPINV'): ?>
                <div class="errors"> El cupon es invalido, revise el n&uacute;mero de cup&oacute;n e intente de nuevo.
                </div>
            <?php endif; ?>
            <?php if(isset($_GET['IDM']) && $_GET['IDM'] == 'INTERR'): ?>
                <div class="errors"> Error interno, intente de nuevo m&acute;s tarde.</div>
            <?php endif; ?>
            <?php if(isset($_GET['IDM']) && $_GET['IDM'] == 'EMEX'): ?>
                <div class="errors"> El email ya fue registrado en el sistema.</div>
            <?php endif; ?>
            <div>
                <input type="text" placeholder="Nombre" required="" id="username" name="username"/>
            </div>
            <div>
                <input type="email" placeholder="Email" required="" id="email" name="email"/>
            </div>
            <div>
                <input type="number" required placeholder="Teléfono Casa" id="telefono_casa" name="telefono_casa"/>
            </div>
            <div>
                <input type="number" required placeholder="Teléfono Celular" id="telefono_celular"
                       name="telefono_celular"/>
            </div>
            <div>
                <input type="text" required placeholder="Domicilio" id="domicilio" name="domicilio"/>
            </div>
            <div>
            <input type="number" placeholder="C&eacute;dula" required="" id="cedula" name="cedula"/>
            </div>
            <div>
                <input type="password" placeholder="Contrase&ntilde;a" required="" id="password" name="password"/>
            </div>
            <div>
                <input type="password" placeholder="Repita contrase&ntilde;a" required="" id="password_conf"
                       name="password_conf"/>
            </div>
            <div>
                <input type="text" placeholder="Pregunta secreta" required="" id="pregunta" name="pregunta"/>
            </div>
            <div>
                <input type="text" placeholder="Respuesta secreta" required="" id="respuesta" name="respuesta"/>
            </div>
            <div>
                <input type="text" placeholder="Cupon" required="" id="cupon" name="cupon"
                       style="text-transform:uppercase;"/>
            </div>
            <div>
                <input type="submit" value="Registrar"/>
                <a href="index.php">Login</a>
                 <a href="/index.html">Home</a>
            </div>
        </form>
        <!-- form -->
    </section>
    <!-- content -->
</div>
<!-- container -->
<canvas id="canvas">
    Canvas de HTML5 no esta soportado por este navegador.
</canvas>
<div id="fix"></div>
<script language='javascript' type='text/javascript'>

    $('#form_register').validate({
        rules: {
            password: "required",
            password_conf: {
                equalTo: "#password"
            }
        },
        messages: {
            password_conf: "Por favor, escribe la misma contraseña"
        },
        errorPlacement: function (error, element) {
            $(element).before(error);
        }
    });

</script>
<style>
    .info, .success, .warning, .errors, .validation {
        border: 1px solid;
        margin: 10px 0px;
        padding:15px 10px 15px 50px;
        background-repeat: no-repeat;
        background-position: 10px center;
        width: 100%;
    }
    .info {
        color: #00529B;
        background-color: #BDE5F8;
        background-image: url('images/info.png');
    }
    .success {
        color: #4F8A10;
        background-color: #DFF2BF;
        background-image:url('images/success.png');
    }
    .warning {
        color: #9F6000;
        background-color: #FEEFB3;
        background-image: url('images/warning.png');
    }

    .errors {
        color: #D8000C;
        background-color: #FFBABA;
        background-image: url('images/error.png');
    }

    .error {
        color: #D8000C;
        position: relative;
        top: -0.6em;
    }
</style>
</body>
</html>