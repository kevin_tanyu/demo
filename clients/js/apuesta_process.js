/**
 * Created by jorge on 7/20/14.
 */

//global variables
_ERROR_CODE_RESTRICTED_NUMBER = -1;
_ERROR_CODE_TOTAL_RESTRICTED = -2;

//esta 'clase' sirve para guardar los codigos de error, como tiene que ser
//consistente con los numeros del server, se supone que se llenen los codigos
//contra los codigos que vienen del servidors, esto en la parte en que se use esta clase.
//Por esto, sirve como una estructura de datos para acceder a los codigos.
//los codigos al momento eran ( se obtienen de la clase ErrorsCodeNumbers):
//_ERR_NUMBER_INSUFICIENT_MONEY = 100;//el jugador no tiene suficiente dinero en balances
//_ERR_NUMBER_OVER_TOP_MONEY = 101;// el numero que desea jugar tiene disponible para apuesta, pero no tanto como se le esta apostando
//_ERR_NUMBER_RESTRICTED_NUMBERS = 102;//error de restricted number, sobrepasa el % permitido por ticket
//_ERR_NUMBERR_NOT_AVAILABLE = 103;
//_ERR_NUMBER_CLOSED_SORTEO
function ErrorCodes(){}

/* Apuesta 'class'  */

function Apuesta(number,amount)
{
    this.number = parseInt(number);
    this.amount = parseInt(amount.systemCleanMoneyMask());
    this.error = null;
}

Apuesta.prototype.getNumber = function()
{
    return this.number;
};

Apuesta.prototype.getStringNumber = function()
{
    var numberString = this.number < 10? "0" + (""+this.number) : this.number + "";
    return numberString;
};

Apuesta.prototype.getAmount = function()
{
    return this.amount;
};

Apuesta.prototype.addToAmount = function(addAmount)
{
    this.amount = this.amount + parseInt(addAmount);
};

//de momento solo setea el error, si se necesita habra que cambiarlo para que maneje varios errores
Apuesta.prototype.addError = function(apuestaError)
{
    this.error = apuestaError;
};

Apuesta.prototype.hasError = function()
{
    return this.error != null;
};

Apuesta.prototype.getError = function()
{
    return this.error;
};

Apuesta.prototype.cleanErrors = function()
{
    this.error = null;
};

function ApuestaError(codigo,numero,disponible)
{
    this.codigo = parseInt(codigo);
    this.numero = parseInt(numero);
    this.disponible = parseInt(disponible);
}

ApuestaError.prototype.getErrorString = function()
{
    var errorString = "Unknown error";

    switch (this.codigo)
    {
        case ErrorCodes._ERR_NUMBER_OVER_TOP_MONEY:
            if(this.disponible > 0)
            {
                errorString = "Disponible solo: &cent;" + this.disponible.systemFormatMoney();
            }
            break;

        case ErrorCodes._ERR_NUMBER_RESTRICTED_NUMBERS:
            errorString = "Disponible solo: &cent;" + this.disponible.systemFormatMoney();
            break;

        case ErrorCodes._ERR_NUMBERR_NOT_AVAILABLE:
            errorString = "No disponible";
            break;
    }

    return errorString;
};


/* Ticket 'class' */

function Ticket(restrictedNumbers,restrictedPercent)
{
    this.restrictedNumbers = (restrictedNumbers != null) ? restrictedNumbers : [];
    this.restrictedPercent = (restrictedPercent!=null)?restrictedPercent:30;
    this.apuestas = [];
    this.totalAmount = 0;
    this.lastAddedApuesta = null;
    this.sorteoId = -1;
}

Ticket.prototype.addApuesta = function(apuestaToAdd)
{
    var alreadyIn = false;
    this.apuestas.forEach(function(apuesta)
    {
        if(apuesta.getNumber() == apuestaToAdd.getNumber() )
        {
           apuesta.addToAmount(apuestaToAdd.getAmount());
           alreadyIn = true;
        }

    });

    if(!alreadyIn)
    {
        this.apuestas.push(apuestaToAdd);
        this.sortApuestas();
    }
    this.lastAddedApuesta = apuestaToAdd;
    this.incrementTotalAmount(apuestaToAdd.getAmount());

};

Ticket.prototype.deleteApuesta = function(number)
{
    intNumber = parseInt(number);
    apuestaToDelete = null;
    this.apuestas.forEach(function(apuesta)
    {
        if(apuesta.getNumber() == intNumber)
        {
            apuestaToDelete = apuesta;
        }

    });
    this.decrementTotalAmount(apuestaToDelete.getAmount());
    this.apuestas = this.apuestas.filter(function(apuesta){
        return apuesta.getNumber() !== intNumber;
    });
};

Ticket.prototype.checkTicketValidity = function()
{

    restrictedLimitValue = (this.restrictedPercent * this.totalAmount) / 100 ;
    totalAmountRestricted = 0;
    errors = [];
    //is a restricted number
    this.apuestas.forEach(function(apuesta){
        if( this.restrictedNumbers.indexOf( apuesta.getNumber() ) >= 0 )
        {
            if(apuesta.getAmount() > restrictedLimitValue )
            {
                errorTemp = new TicketError(_ERROR_CODE_RESTRICTED_NUMBER, apuesta );
                errors.push(errorTemp);
            }
            totalAmountRestricted += apuesta.getAmount();
        }
    });
    if(totalAmountRestricted >= restrictedLimitValue  )
    {
        errorTemp = new TicketError(_ERROR_CODE_TOTAL_RESTRICTED);
        errors.push(errorTemp);
    }
    this.isRestrictedValid = (errors.length == 0);
    return errors;
};


Ticket.prototype.sortApuestas = function()
{
    this.apuestas.sort(function(ap1,ap2){
        return ap1.getNumber() - ap2.getNumber();
    });
};

Ticket.prototype.getApuestas = function()
{
    return this.apuestas;
};

Ticket.prototype.incrementTotalAmount = function(increment)
{
    this.totalAmount += parseInt(increment);
};

Ticket.prototype.decrementTotalAmount = function(decrement)
{
    this.totalAmount -= parseInt(decrement);
};

Ticket.prototype.cleanApuestaErrors = function()
{
    this.apuestas.forEach(function(apuesta){
       apuesta.cleanErrors();
    });
};

//TicketErrors 'class'
function TicketError(errorCode,info)
{
    this.errorCode = errorCode;
    this.info = info;
}


