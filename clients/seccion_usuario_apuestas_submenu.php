<link rel="stylesheet" href="jqwidgets/styles/jqx.base.css" type="text/css" />
<script type="text/javascript" src="jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="jqwidgets/jqxcheckbox.js"></script>
<script type="text/javascript" src="jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.selection.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.filter.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.sort.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.edit.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.selection.js"></script>
<script type="text/javascript" src="jqwidgets/jqxcalendar.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdatetimeinput.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.pager.js"></script>
<script type="text/javascript" src="jqwidgets/jqxnumberinput.js"></script>
<script type="text/javascript" src="jqwidgets/globalization/globalize.js"></script>



<script type="text/javascript">
    function getDemoTheme() {
        var theme =  $.data(document.body, 'theme');
        if (theme == null) {
            theme = '';
        }
        else {
            return theme;
        }
        var themestart = window.location.toString().indexOf('?');
        if (themestart == -1) {
            return '';
        }

        var theme = window.location.toString().substring(1 + themestart);
        var url = "../../jqwidgets/styles/jqx." + theme + '.css';

        if (document.createStyleSheet != undefined) {
            var hasStyle = false;
            $.each(document.styleSheets, function (index, value) {
                if (value.href != undefined && value.href.indexOf(theme) != -1) {
                    hasStyle = true;
                    return false;
                }
            });
            if (!hasStyle) {
                document.createStyleSheet(url);
            }
        }
        else $(document).find('head').append('<link rel="stylesheet" href="' + url + '" media="screen" />');

        return theme;
    };


</script>
<aside_submenu>
    <h3>Opciones</h3>
    <div id="list4">

        <ul>
            <li><a href="seccion_usuario_apuesta.php"><img src="images/iniciar_apuesta.png" /> Iniciar Apuesta</a></li>
        </ul>
    </div>
</aside_submenu>