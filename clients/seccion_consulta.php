<?php
session_start();

require_once 'paths.php';

// Incluye datos generales y conexion a DB
require_once $global_system_path . '/config.ini.php';
require_once $global_system_path . '/conectadb.php';

// Incluye Header
include("header.php");

include("seccion_consulta_submenu.php");
?>
<script type="text/javascript">
    $(document).ready(function() {
        $(".various").fancybox({
            fitToView	: true,
            width		: '100%',
            height		: '100%',
            autoSize	: true,
            closeClick	: false,
            openEffect	: 'fade',
            closeEffect	: 'elastic'
        });
    });
</script>
<style type="text/css">
    .fancybox-custom .fancybox-skin {
        box-shadow: 0 0 50px #222;
    }
</style>
<div id="ContenedorGeneral">
</div>
<?php
// Incluye Footer
include("footer.php");
?>