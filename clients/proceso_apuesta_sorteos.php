<?php
session_start();
require_once 'paths.php';
require_once $global_system_path . '/config.ini.php';
require_once $global_system_path . '/conectadb.php';

$loteriaId = $_POST['loteria_id'];

$sqlSorteosDia = "select NombreSorteo , sp.FechaYHora,sp.id from SorteosDefinicion sd join SorteosProgramacion sp on sd.id = sp.IDSorteoDefinicion where sp.FechaYHora >= '". date("Y-m-d H:i") ."' and flagactivo = 1 and sd.id = " . $loteriaId;
$sorteosRs = mysql_query($sqlSorteosDia);

$cantidadSorteos = mysql_num_rows($sorteosRs);

?>
<style>
    #sorteos-list li{
        cursor: default;
    }
</style>
<form action="proceso_apuesta_ticket.php" id="sorteos_form" method="post" >
    <h3>Seleccione Fecha del Sorteo</h3>
    <div id="sorteos-list" class="divTable" >
        <?php if($cantidadSorteos > 0){ ?>
        <?php while ($row=mysql_fetch_assoc($sorteosRs)): ?>
                <div class="divRow2" data-sorteo_id="<?php echo $row['id'] ?>"
                     data-nombre="<?php echo $row['NombreSorteo'] . ' para ' . system_date_format($row['FechaYHora']) ?>"> <?php echo $row['NombreSorteo'] . ' para ' . system_date_format($row['FechaYHora']) ?> </div>
            <?php endwhile; ?>
        <?php }else{ ?>
            <h4>No hay sorteos disponibles</h4>
        <?php } ?>
    </div>
    <div><h4>Fecha seleccionada <span id="sorteo_seleccionado" style="color: orangered" ></span> </h4> </div>
    <input type="hidden" value="" id="sorteo_id" name="sorteo_id"  />
    <input type="hidden" value="<?php echo $loteriaId ?>" name="loteria_id" />
</form>

<script>
    $(function(){
        var options = {
            target: '#process'   // target element(s) to be updated with server response

            // other available options:
            //url:       url         // override for form's 'action' attribute
            //type:      type        // 'get' or 'post', override for form's 'method' attribute
            //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)
            //clearForm: true        // clear all form fields after successful submit
            //resetForm: true        // reset the form after successful submit

            // $.ajax options can be used here too, for example:
            //timeout:   3000
        };

        // bind form using 'ajaxForm'
        $('#sorteos_form').ajaxForm(options);

        var $sorteos_list = $("#sorteos-list");

        $sorteos_list.on('click', 'div', function () {
            var value = $(this).data('sorteo_id');
            $('#sorteo_seleccionado').html( $(this).data('nombre') );
            $('#sorteo_id').val(value);
            $('#sorteos_form').submit();
            $('#process').block();
        });

        $sorteos_list.on('mouseenter', 'div', function () {
            var value = $(this).data('sorteo_id');
            $('#sorteo_seleccionado').html($(this).data('nombre'));
            $('#sorteo_id').val(value);
        });

    });
</script>