<?php
/**
 * helper file for display info about ticket
 * User: jorge
 * Date: 7/24/14
 * Time: 11:18 PM
 */

require_once 'paths.php';

require_once $global_system_path . '/config.ini.php';
require_once $global_system_path . '/conectadb.php';

$ticketId = $_GET['TID'];

$ticketInfoSql = "SELECT * FROM CLIENTE_TicketsInfo_View WHERE ID=$ticketId";
$ticketRs = mysql_query($ticketInfoSql);
$ticket = mysql_fetch_assoc($ticketRs);

$apuestasSql = "SELECT * FROM CLIENTE_SorteoApuesta_View WHERE IDTicket = $ticketId";
$apuestasRs = mysql_query($apuestasSql);
$apuestas = array();

while($apuesta = mysql_fetch_assoc($apuestasRs))
{
    $apuestas[] = $apuesta;
}

$sorteoSql = "SELECT * FROM SorteosDefinicion sd JOIN SorteosProgramacion sp ON sp.IDSorteoDefinicion = sd.ID WHERE sp.ID = " . $apuestas[0]['IDSorteoProgramacion'];
$sorteoRs = mysql_query($sorteoSql);
$sorteo = mysql_fetch_assoc($sorteoRs);

?>


<div class="divTable">
    <div class="divRow" >
        <div class="divCellHeader2" style="width: 150px;" >Ticket # <?php echo $ticket['Number'] ?></div>
    </div>
    <div class="divRow" >
        <div class="divCellHeader2" style="width: 60px;" >Sorteo</div>
        <div class="divCellCen2" style="width: 90px;" ><?php echo $sorteo['NombreSorteo'] ?></div>
    </div>

    <div class="divRow" >
        <div class="divCellHeader2" style="width: 60px;" >Fecha</div>
        <div class="divCellCen2" style="width: 90px;"><?php echo system_date_format($sorteo['FechayHora']) ?></div>
    </div>

</div>

<h4 style="margin-bottom: 0px;" >Apuestas</h4>
<div class="divTable" >
    <div class="divRow" >
        <div class="divCellHeader2" style="width: 60px;" >Numero</div>
        <div class="divCellHeader2" style="width: 90px;" >Monto</div>
    </div>
    <?php foreach($apuestas as $apuesta): ?>
        <div class="divRow" >
            <div class="divCellCen2" style="width: 60px;" ><?php echo $apuesta['Numero']; ?></div>
            <div class="divCellCen2" style="width: 90px;" ><?php echo '&cent;' . system_number_format($apuesta['Cantidad']) ?></div>
        </div>
    <?php endforeach; ?>
    <div class="divRow" >
        <div class="divCellHeader2" style="width: 60px;" >Total</div>
        <div class="divCellHeader2" style="width: 90px;" ><?php echo '&cent;' . system_number_format($ticket['Monto']) ?></div>
    </div>
</div>