<?php

include("config.ini.php");
include("conectadb.php");

$QUERY = "SELECT sp.FechayHora,
													sp.NumeroDeSuerte,
													sd.NombreSorteo,
													sd.PagaPorcentaje,
													sp.id,

													CONCAT(
					LPAD(FLOOR(HOUR(TIMEDIFF(sp.FechayHora, NOW())) / 24),2,'0'), ':',
					LPAD(MOD(HOUR(TIMEDIFF(sp.FechayHora, NOW())), 24),2,'0'), ':',
					LPAD(MINUTE(TIMEDIFF(sp.FechayHora, NOW())),2,'0'), ':',
					LPAD(SECOND(TIMEDIFF(sp.FechayHora, NOW())),2,'0'))
					,sp.FechayHora > NOW() as esProximo
									FROM 		SorteosProgramacion sp,
													SorteosDefinicion sd
									WHERE 	sp.IDSorteoDefinicion=sd.ID
										  AND 	sd.FlagActivo<>2
										  and sp.ID not in (select idsorteoprogramacion from SorteosNumerosGanadores)
										  and sp.FechayHora <= NOW()
									ORDER BY sp.FechayHora DESC
									LIMIT 0,10
										  ";
$rs = mysql_query($QUERY);
?>

    <h4 style="line-height:1px;">Sorteos Finalizados</h4>
    <h6 style="line-height:1px;">Estos sorteos necesitan reportar numeros ganadores</h6>

<div class="divTable">
    <div class="divRow">
        <div class="divCellHeader" style="width:90px; font-size:10px;">Fecha</div>
        <div class="divCellHeader" style="width:160px; font-size:10px; text-align:left;">Sorteo</div>
        <div class="divCellHeader" style="width:30px; font-size:10px; text-align:center;">NS</div>
    </div>
<?php while ($row = mysql_fetch_row($rs)): ?>
    <div class="divRow2 various2 fancybox.ajax" href="seccion_datos_numerosganadores_add_form.php?id=<?= $row[4]; ?>"
         onclick="AbreVentanaNumerosGanadores();">
        <div class="divCellCen letra" style="width:90px;"><?= substr($row[0], 0, 10); ?></div>
        <div class="divCellIzq letra" style="width:160px;">Loteria <?= $row[2]; ?></div>
        <div class="divCellCen letra" style="width:30px;"><?= $row[1]; ?></div>
    </div>
<?php endwhile; ?>
</div>