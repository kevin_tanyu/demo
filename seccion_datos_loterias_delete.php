<?php
session_start();

if (!isset($_SESSION['ActivoFlag'])) {
	// Verifica si hay session creada, de lo contrario redirige al index
	header("Location: index.php?IDM=TO");
	exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

$QUERY="SELECT * FROM SorteosDefinicion WHERE id=".$_GET['id'];
$rs=mysql_query($QUERY);
$row=mysql_fetch_array($rs);
?>

					<h3 style="line-height:1px;">Deshabilitar Loteria</h3>
					<div style="text-align:center;">
						<form name="formulario1" id="formulario1">
						<div class="divTable" style="width:500px;">
							<div class="divRow">
								<div class="divCellIzq" style="font-weight:bold;">Nombre de la Loteria</div>
								<div class="divCellDer"><?=$row['NombreSorteo']; ?></div>
							</div>
							<div class="divRow">
								<div class="divCellIzq" style="font-weight:bold;">Cantidad numeros Ganadores</div>
								<div class="divCellDer"><?=$row['NumerosGanadores']; ?></div>
							</div>
							<div class="divRow">
								<div class="divCellIzq" style="vertical-align:center; top-margin:20px; font-weight:bold;">Debe acertar multiples numeros?</div>
								<div class="divCellDer"><?php if ($row['FlagMultiple']==1)  echo "Si"; else echo "No"; ?></div>
							</div>
							<div class="divRow">
								<div class="divCellIzq" style="font-weight:bold;">Cantidad de Numeros a Acertar</div>
							    <div class="divCellDer"><?=$row['FlagMultipleMinimoGana']; ?></div>
							</div>
							<div class="divRow">
								<div class="divCellIzq" style="font-weight:bold;">Porcentaje en que Paga</div>
								<div class="divCellDer"><?=$row['PagaPorcentaje']; ?>%</div>
							</div>
						</div>
						<div class="divTable">
							<div class="divRow">
								<div class="divCellHeader">L</div>
								<div class="divCellHeader">M</div>
								<div class="divCellHeader">M</div>
								<div class="divCellHeader">J</div>
								<div class="divCellHeader">V</div>
								<div class="divCellHeader">S</div>
								<div class="divCellHeader">D</div>
							</div>
							<div class="divRow">
								<div class="divCellCen"><?php if ($row['LunesFlag']==1)  		echo "Si"; else echo "No"; ?></div>
								<div class="divCellCen"><?php if ($row['MartesFlag']==1)  	echo "Si"; else echo "No"; ?></div>
								<div class="divCellCen"><?php if ($row['MiercolesFlag']==1)  echo "Si"; else echo "No"; ?></div>
								<div class="divCellCen"><?php if ($row['JuevesFlag']==1)  	echo "Si"; else echo "No"; ?></div>
								<div class="divCellCen"><?php if ($row['ViernesFlag']==1)  	echo "Si"; else echo "No"; ?></div>
								<div class="divCellCen"><?php if ($row['SabadoFlag']==1)  	echo "Si"; else echo "No"; ?></div>
								<div class="divCellCen"><?php if ($row['DomingoFlag']==1)  	echo "Si"; else echo "No"; ?></div>
							</div>
						</div>
						<input type="button" value="Deshabilitar" class="button" id="BotonFormulario" onclick="javascript:FormularioDeleteLoteria('<?=$_GET['id']; ?>');">
						</form>

					</div>
					<br><br>
