<?php
session_start();

if (!isset($_SESSION['ActivoFlag']))
{
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
require_once("config.ini.php");
require_once("conectadb.php");
require_once("lib/seccion_datos_numerosganadores_lib.php");

$idUser = $_SESSION['IDUsuario'];

$idSorteoProgramacion = $_POST['IDSorteoProgramacion'];

$sqlSorteoProg = "SELECT SP.ID, SD.NombreSorteo, SP.FechayHora
    FROM SorteosProgramacion SP
    JOIN SorteosDefinicion SD ON SD.ID = SP.IDSorteoDefinicion
    WHERE SP.ID = :sorteo_prog_id";
$sorteoStmt    = $pdoConn->prepare($sqlSorteoProg);
$sorteoStmt->execute(array(':sorteo_prog_id' => $idSorteoProgramacion));
$infoSorteo = $sorteoStmt->fetch(PDO::FETCH_ASSOC);

//datos de vendedores para envio de correos
$vendedores = sdn_obtener_vendedores_calle_con_tickets($pdoConn, $idSorteoProgramacion);

//datos de revendedores para envio de correos
$re_vendedores = sdn_obtener_vendedores_web_con_tickets($pdoConn, $idSorteoProgramacion);

$usuariosAgrupados = array('calle' => $vendedores, 'web' => $re_vendedores);

//envio de correos
sdn_enviaCorreos($usuariosAgrupados, $infoSorteo);
