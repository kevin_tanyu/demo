<?php

session_start();

require_once 'config.ini.php';
require_once 'conectadb.php';

$usuarioId = $_POST['uid'];
$printerType = $_POST['tipo_impresora'];

$error = false;

$sqlUpdate = "UPDATE Usuarios SET printer_type = :printer_type WHERE ID = :usuario_id";
$stmtUpdate = $pdoConn->prepare($sqlUpdate);

try{
    $stmtUpdate->execute(array(':printer_type' => $printerType, ':usuario_id' => $usuarioId ));
}catch(Exception $e)
{
    $error = true;
}


?>

<?php if ($error)
{
    ?>
    <div class="error">Hubo un error en la actualizacion revise los parametros, e intente de nuevo</div>
<?php
} else
{
    ?>
    <div class="success">Actualizacion exitosa</div>
<?php } ?>