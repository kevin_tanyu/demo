<?php
session_start();

// Incluye datos generales y conexion a DB
include("../config.ini.php");
include("../conectadb.php");

//CAPTURA LA FECHAS DEL FORM
$dateFrom = isset($_POST['fromDate']) ? $_POST['fromDate'] : date('Y-m-d');
$dateTo = isset($_POST['fromDate']) ? $_POST['fromDate'] . ' 23:59' : date('Y-m-d 23:59');

//RECUPERAR SORTEOS DEL DIA
$sqlSorteosDelDia = "SELECT SP.ID, SD.NombreSorteo, SP.FechayHora
                     FROM SorteosProgramacion SP
                     JOIN SorteosDefinicion SD
                     ON SP.IDSorteoDefinicion = SD.ID
                     WHERE SP.FechayHora > ? AND SP.FechayHora < ?
                     ORDER BY SD.NombreSorteo DESC";
$stmtSorteosDelDia = $pdoConn->prepare($sqlSorteosDelDia);
$stmtSorteosDelDia->execute(array($dateFrom, $dateTo));
$sorteosDelDia = $stmtSorteosDelDia->fetchAll(PDO::FETCH_ASSOC);

/*CONSULTA TOTAL DEL SORTEO*/
$sqlTotalSorteo = "SELECT SUM(sa.Cantidad) as total
                   FROM SorteosProgramacion sp
                   JOIN SorteosDefinicion sd on sd.ID = sp.IDSorteoDefinicion and DATEDIFF(sp.FechayHora,NOW()) = 0
                   JOIN sorteoapuesta_validas_puestos sa on sa.IDSorteoProgramacion = sp.ID and sa.IDUsuario = :id_usuario
                   WHERE sp.ID = :sorteo_prog_id
                   group by sp.ID, sd.NombreSorteo
                   order by sp.FechayHora";
$stmtTotalSorteo = $pdoConn->prepare($sqlTotalSorteo);

/*GET EXCESOS*/
$sqlListaEnviada = "SELECT * FROM SorteoDevolucion
                    WHERE sorteo_prog_id = ? AND usuario_id = ?";
$stmtListaEnviada = $pdoConn->prepare($sqlListaEnviada);


$sqlMontoDevolucion = "SELECT SUM(monto_devuelto) as 'total' FROM SorteoDevolucion_Detalle
                       WHERE sorteodevolucion_id = ?";
$stmtMontoDevolucion = $pdoConn->prepare($sqlMontoDevolucion);

$sqlNumerosExcesos = "SELECT numero, monto_devuelto FROM SorteoDevolucion_Detalle
                      WHERE sorteodevolucion_id = ?";
$stmtNumerosExcesos = $pdoConn->prepare($sqlNumerosExcesos);

/*CONSULTA ESTADO REVISION*/
$sqlStatusSorteo = "SELECT RS.id, S.name, S.display_name
                    FROM COF_RevisionStatus_View RS
                    JOIN COF_Status S ON RS.status_id = S.id
                    WHERE RS.sorteo_programacion_id = ? AND RS.usuario_vendedor_calle_id = ?";
$stmtStatusSorteo = $pdoConn->prepare($sqlStatusSorteo);


?>

<?php if($stmtSorteosDelDia->rowCount() == 0):?>

    <h1><i class="fa fa-frown-o"></i> No hubo sorteos</h1>

<?php endif?>

<?php foreach ($sorteosDelDia as $sorteoDelDia): ?>

<div class="row">

    <div class="col-lg-4 col-md-4 col-sm-8">

        <div class="showback">
            <h4><i class="fa fa-angle-right"></i> <?php echo $sorteoDelDia['NombreSorteo']?></h4>
            <?php
            /*TOTAL SORTEO*/
            $stmtTotalSorteo->execute(array(':id_usuario' => $_SESSION['IDUsuario'], ':sorteo_prog_id' => $sorteoDelDia['ID']));
            $totalSorteo = $stmtTotalSorteo->fetch();
            $totalBruto = $totalSorteo['total'];

            /*EXCESOS*/
            $stmtListaEnviada->execute(array( $sorteoDelDia['ID'], $_SESSION['IDUsuario']));
            $lista = $stmtListaEnviada->fetch();

            $stmtMontoDevolucion->execute(array($lista['id']));
            $devolucion = $stmtMontoDevolucion->fetch();
            $montoDevuelto = $devolucion['total'];

            $stmtNumerosExcesos->execute(array($lista['id']));
            $numeroDevolucion = $stmtNumerosExcesos->fetchAll(PDO::FETCH_ASSOC);

            /*ESTADO REVISION*/
            $stmtStatusSorteo->execute(array( $sorteoDelDia['ID'], $_SESSION['IDUsuario']));
            $status = $stmtStatusSorteo->fetch();


            ?>
            <table style="font-size: 20px; ">
                <tr style="border: 1px solid black;">
                    <td style="border: 1px solid black;">Lista Bruta: </td>
                    <td style="border: 1px solid black;"><?php echo system_number_money_format($totalBruto)?></td>
                </tr>
                <tr style="border: 1px solid black;">
                    <td style="border: 1px solid black;">Excesos: </td>
                    <td style="border: 1px solid black;"><?php echo system_number_money_format($montoDevuelto)?></td>
                    <td style="width: 100px">
                        <?php foreach($numeroDevolucion as $numero):?>
                            <label><?php echo $numero['numero']?> = <?php echo system_number_money_format($numero['monto_devuelto'])?></label></br>
                        <?php endforeach?>
                    </td>
                </tr>
                <tr style="border: 1px solid black;">
                    <td style="border: 1px solid black;">Lista Neta: </td>
                    <td style="border: 1px solid black;"><?php echo system_number_money_format($totalBruto-$montoDevuelto)?></td>
                </tr>
                <tr>
                    <td style="border: 1px solid black;">Estado: </td>
                    <td style="border: 1px solid black;"> <?php if($stmtStatusSorteo->rowCount() > 0){?>
                            <strong style="font-size: 20px; color: green"><i class="fa fa-check hidden-sm hidden-xs"></i><?php echo $status['display_name'];?></strong>
                        <?php }else{?>
                            <strong style="font-size: 20px; color: red"><i class="fa fa-times hidden-sm hidden-xs"></i>&nbsp;No se ha <br>recibido revisión</strong>
                        <?php }?></td>
                </tr>
            </table>
        </div><!--/showback -->

    </div>
</div>

<?php endforeach?>


