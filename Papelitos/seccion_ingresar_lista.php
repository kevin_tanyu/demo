<?php
session_start();

if (!isset($_SESSION['ActivoFlag'])) {
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: ../index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("../config.ini.php");
include("../conectadb.php");

// Incluye Header
include("header.php");

$sorteoID = $_GET['SID'];

/*CONSULTA TOTAL DEL SORTEO*/
$sqlTotalSorteo = "SELECT SUM(sa.Cantidad) as total
                   FROM SorteosProgramacion sp
                   JOIN SorteosDefinicion sd on sd.ID = sp.IDSorteoDefinicion and DATEDIFF(sp.FechayHora,NOW()) = 0
                   JOIN sorteoapuesta_puestos sa on sa.IDSorteoProgramacion = sp.ID and sa.IDUsuario = :id_usuario
                   WHERE sp.ID = :sorteo_prog_id
                   group by sp.ID, sd.NombreSorteo
                   order by sp.FechayHora";
$stmtTotalSorteo = $pdoConn->prepare($sqlTotalSorteo);
$stmtTotalSorteo->execute(array(':id_usuario' => $_SESSION['IDUsuario'], ':sorteo_prog_id' => $sorteoID));
$TotalSorteo = $stmtTotalSorteo->fetch();

/*INFO DEL SORTEO*/
$sqlSorteoDelDia = "SELECT SD.NombreSorteo, SP.FechayHora
                    FROM SorteosProgramacion SP
                    JOIN SorteosDefinicion SD
                    ON SP.IDSorteoDefinicion = SD.ID
                    WHERE SP.ID = ?";
$stmtSorteoDelDia = $pdoConn->prepare($sqlSorteoDelDia);
$stmtSorteoDelDia->execute(array($sorteoID));
$sorteoInfo = $stmtSorteoDelDia->fetch();

/*SQL RESTRINGIDOS DEL SORTEO***/
//Captura los restringidos del dia
$sqlNumRestringidos = "SELECT SR.Numero as numero_restringido
                       FROM SorteosNumerosRestringidos SR
                       WHERE SR.IDSorteoProgramacion = $sorteoID";

$stmtNumRestringidos = $pdoConn->prepare($sqlNumRestringidos);
$stmtNumRestringidos->execute();
$numRestringidos = $stmtNumRestringidos->fetchAll(PDO::FETCH_ASSOC);

/*GET MONTO ACUMULADO POR NUMERO*/
$sqlAcumuladoNumero = "SELECT Cantidad
                       FROM sorteoapuesta_puestos
                       WHERE IDUsuario = ? and IDSorteoProgramacion = ? and Numero = ?";
$stmtAcumuladoNumero = $pdoConn->prepare($sqlAcumuladoNumero);

?>

<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-calendar-o"></i>&nbsp;Ingresar lista para sorteo <?php echo $sorteoInfo['NombreSorteo']?></h3>
        <div class="row mt">
            <div class="col-lg-12">
                <h1><i class="fa fa-arrow-right"></i>Total acumulado: <?php echo system_number_money_format($TotalSorteo['total'])?></h1>
            </div>


        <div class="row">

            <div class="col-md-12">
                <div class="content-panel">
                    <h4><i class="fa fa-list-ol"></i> Lista</h4>
                    <table class="table">
                        <tbody>
                        <?php for($a = 1; $a <= 34; $a++):?>
                            <tr>
                                <?php if($a >= 10 && $a < 34){?>
                                    <td class="form-inline">
                                        <label class="col-sm-2 col-sm-2 control-label" style="font-size: 18px"><?php echo $a?></label>
                                        <input class="form-control" id="txtMonto<?php echo $a?>" type="text" style="width: 150px; font-size: 20px" value="" onkeydown="cambiarFocus(event, <?php echo $a?>)" onfocus="limpiar(<?php echo $a?>)" onkeypress="return mascara(this,cpf);">
                                        <?php
                                        $stmtAcumuladoNumero->execute(array($_SESSION['IDUsuario'], $sorteoID, $a));
                                        $acumulado = $stmtAcumuladoNumero->fetch();
                                        ?>
                                        <label style="font-size: 18px" id="lblAcum<?php echo $a?>">&nbsp;<?php echo system_number_money_format($acumulado['Cantidad'])?></label>
                                    </td>
                                <?php }elseif($a < 10){?>
                                    <td class="form-inline">
                                        <label class="col-sm-2 col-sm-2 control-label" style="font-size: 18px"><?php echo 0 . $a?></label>
                                        <input class="form-control" id="txtMonto<?php echo $a?>" type="text" style="width: 150px; font-size: 20px" value="" onkeydown="cambiarFocus(event, <?php echo $a?>)" onfocus="limpiar(<?php echo $a?>)" onkeypress="return mascara(this,cpf);">
                                        <?php
                                        $stmtAcumuladoNumero->execute(array($_SESSION['IDUsuario'], $sorteoID, $a));
                                        $acumulado = $stmtAcumuladoNumero->fetch();
                                        ?>
                                        <label style="font-size: 18px" id="lblAcum<?php echo $a?>">&nbsp;<?php echo system_number_money_format($acumulado['Cantidad'])?></label>
                                    </td>
                                <?php }else{ ?>
                                    <td></td>

                                <?php } ?>
                                <td class="form-inline">
                                    <?php if($a+33 != 67):?>
                                        <label class="col-sm-2 col-sm-2 control-label" style="font-size: 18px"><?php echo $a+33?></label>
                                        <input class="form-control" id="txtMonto<?php echo $a+33?>" type="text" style="width: 150px; font-size: 20px" value="" onkeydown="cambiarFocus(event, <?php echo $a+33?>)" onfocus="limpiar(<?php echo $a+33?>)" onkeypress="return mascara(this,cpf);">
                                        <?php
                                        $stmtAcumuladoNumero->execute(array($_SESSION['IDUsuario'], $sorteoID, $a+33));
                                        $acumulado = $stmtAcumuladoNumero->fetch();
                                        ?>
                                        <label style="font-size: 18px" id="lblAcum<?php echo $a+33?>">&nbsp;<?php echo system_number_money_format($acumulado['Cantidad'])?></label>
                                    <?php endif ?>
                                </td>
                                <td class="form-inline">
                                    <?php if($a+66 != 101):?>
                                        <label class="col-sm-2 col-sm-2 control-label" style="font-size: 18px"><?php echo $a+66?></label>
                                        <input class="form-control" id="txtMonto<?php echo $a+66?>" type="text" style="width: 150px; font-size: 20px" value="" onkeydown="cambiarFocus(event, <?php echo $a+66?>)" onfocus="limpiar(<?php echo $a+66?>)" onkeypress="return mascara(this,cpf);">
                                        <?php
                                        if($a+66 == 100){
                                            $stmtAcumuladoNumero->execute(array($_SESSION['IDUsuario'], $sorteoID, 0));
                                            $acumulado = $stmtAcumuladoNumero->fetch();
                                        }else{
                                            $stmtAcumuladoNumero->execute(array($_SESSION['IDUsuario'], $sorteoID, $a+66));
                                            $acumulado = $stmtAcumuladoNumero->fetch();
                                        }
                                        ?>
                                        <label style="font-size: 18px" id="lblAcum<?php echo $a+66?>">&nbsp;<?php echo system_number_money_format($acumulado['Cantidad'])?></label>
                                    <?php endif ?>
                                </td>
                            </tr>
                        <?php endfor ?>
                        <tr>
                            <td><label class="col-sm-2 col-sm-2 control-label" style="font-size: 18px; width: 220px" >Subtotal ¢ <label style="font-size: 18px" id="lblsub1">0</label></label> </td>
                            <td><label class="col-sm-2 col-sm-2 control-label" style="font-size: 18px; width: 220px" >Subtotal ¢ <label style="font-size: 18px" id="lblsub2">0</label></label> </td>
                            <td><label class="col-sm-2 col-sm-2 control-label" style="font-size: 18px; width: 220px" >Subtotal ¢ <label style="font-size: 18px" id="lblsub3">0</label></label> </td>
                        </tr>
                        <tr>
                            <td><label class="alert alert-info" style="font-size: 18px; width: 220px" >Total ¢ <label style="font-size: 18px" id="lblTotal">0</label></label></td>
                            <td>
                                <label class="alert alert-warning" style="font-size: 18px; width: 220px" id="lblExcesos"></label>
                            </td>
                            <td>
                                <button type="button" class="btn btn-info btn-lg" id="btnVerificar" onclick="verificarLista()">Analizar Lista</button>
                                <button type="button" class="btn btn-success btn-lg" id="btnEnviar" onclick="enviarLista()" style="visibility: hidden">Enviar Lista</button><br><br>
                                <label class="col-sm-6 col-sm-6 control-label" style="font-size: 18px; padding-left: 150px" id="lblMensaje"></label>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div><! --/content-panel -->
            </div><!-- /col-md-8 -->
        </div><!-- row -->


    </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->

<script>

    var restringidos =  new Array();

    $( document ).ready(function() {

        <?php foreach($numRestringidos as $restringido):?>
        restringidos.push(<?php echo $restringido['numero_restringido']?>);
        <?php endforeach?>
/*
        var total = 0;
        for(var i = 1; i <= 33; i++){
            var getAcum = $('#lblAcum' + i).text().replace(/[\¢]/g,'');
            total = total + parseInt(getAcum.replace(/[\.]/g,''));

        }

        if(total.toString().length > 3){
            total = formatoMoney(total.toString());
        }

        $('#lblsub1').text(total);

        total = 0;
        for(var i = 34; i <=66; i++){
            var getAcum = $('#lblAcum' + i).text().replace(/[\¢]/g,'');
            total = total + parseInt(getAcum.replace(/[\.]/g,''));
        }

        if(total.toString().length > 3){
            total = formatoMoney(total.toString());
        }

        $('#lblsub2').text(total);

        total = 0;
        for(var i = 67; i <= 100; i++){
            var getAcum = $('#lblAcum' + i).text().replace(/[\¢]/g,'');
            total = total + parseInt(getAcum.replace(/[\.]/g,''));
        }

        if(total.toString().length > 3){
            total = formatoMoney(total.toString());
        }

        $('#lblsub3').text(total);*/

    });

    function cambiarFocus(e, numero){

        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            if(numero < 100){
                $("#txtMonto"+(numero+1)).focus();
            }else{
                $("#btnVerificar").click();
            }
        }
    }

    /************************MASCARA*********************/
    function mascara(o,f){
        v_obj=o;
        v_fun=f;
        setTimeout("execmascara()",1);
    }
    function execmascara(){
        v_obj.value=v_fun(v_obj.value);
    }
    function cpf(v){
        v=v.replace(/([^0-9\,]+)/g,'');//Acepta solo números y comas.
        v=v.replace(/^[\,]/,'');//Quita coma al inicio
        v=v.replace(/[\,][\,]/g,'');//Elimina dos comas juntas.
        v=v.replace(/\,(\d)(\d)(\d)/g,',$1$2'); // Si encuentra el patrón .123 lo cambia por .12.
        v=v.replace(/\,(\d{1,2})\./g,',$1');//Si encuentra el patrón .1. o .12. lo cambia por .1 o .12.
        v = v.toString().split('').reverse().join('').replace(/(\d{3})/g,'$1.');//Pone la cadena al revés Si encuentra el patrón 123 lo cambia por 123,.
        v = v.split('').reverse().join('').replace(/^[\.]/,'');//Si inicia con un punto la reemplaza por nada.
        return v;
    }

    /*********************************************************/

    function limpiar(i){
        $('#txtMonto' + i).val('');
        document.getElementById("btnEnviar").style.visibility = "hidden";
        document.getElementById("btnVerificar").style.visibility = "visible";
    }

    function verificarLista(){
        var totalGeneral = 0;

        for(var i = 1; i< 101; i++){

            if($('#txtMonto' + i).val() == ''){
                //alert(i);
                totalGeneral = totalGeneral + 0;
                $('#txtMonto' + i).val(0);
            }else{
                //alert($('#txtMonto' + i).val());
                totalGeneral = totalGeneral + parseInt($('#txtMonto' + i).val().replace(/[\.]/g,''));
            }
        }

        var tempTotal  = totalGeneral;
        if(totalGeneral.toString().length > 3){
            totalGeneral = formatoMoney(totalGeneral.toString());
        }


        $('#lblTotal').text(totalGeneral);

        /*******************CALCULA LOS SUBTOTAlES DE CADA COLUMNA*******************************/
        var total = 0;

        for(var i = 1; i <= 33; i++){
            //montos.push($('#txtMonto' + i).val().replace(/[\.]/g,''));
            total = total + parseInt($('#txtMonto' + i).val().replace(/[\.]/g,''));

        }

        if(total.toString().length > 3){
            total = formatoMoney(total.toString());
        }

        $('#lblsub1').text(total);

        total = 0;
        for(var i = 34; i <=66; i++){
            //  montos.push($('#txtMonto' + i).val().replace(/[\.]/g,''));
            total = total + parseInt($('#txtMonto' + i).val().replace(/[\.]/g,''));
        }

        if(total.toString().length > 3){
            total = formatoMoney(total.toString());
        }

        $('#lblsub2').text(total);

        total = 0;
        for(var i = 67; i <= 100; i++){
            //   montos.push($('#txtMonto' + i).val().replace(/[\.]/g,''));
            total = total + parseInt($('#txtMonto' + i).val().replace(/[\.]/g,''));
        }

        if(total.toString().length > 3){
            total = formatoMoney(total.toString());
        }

        $('#lblsub3').text(total);


        /*******************CALCULA LOS SUBTOTAlES DE CADA COLUMNA*******************************/


        document.getElementById("btnEnviar").style.visibility = "visible";
        document.getElementById("btnVerificar").style.visibility = "hidden";

        /*************CALCULA RESTRINGIDOS************/
        var montoTope = tempTotal * (<?php echo $_SESSION['Devolucion']?> / 100);
        //   alert(totalGeneral);
        montoTope = redondeo12(montoTope.toFixed(0));

        var mensaje = "Excesos: </br>";
        var montoARebajar = 0;

        //  alert(montoTope);

        for(var i = 0 ; i < restringidos.length; i++){
            var monto = parseInt($('#txtMonto' + restringidos[i]).val().replace(/[\.]/g,''));

            //alert(monto);
            if(monto > montoTope){
                var aRebajar = monto - montoTope;
                montoARebajar = montoARebajar + aRebajar;

                if(aRebajar.toString().length > 3){
                    aRebajar = formatoMoney(aRebajar.toString());
                }

                mensaje = mensaje + " " + restringidos[i] + " = " + aRebajar + "</br>";

            }//Fin if


        }//FOR

        var neto = tempTotal-montoARebajar;

        if(neto.toString().length > 3){
            neto = formatoMoney(neto.toString());
        }


        mensaje = mensaje + "<hr>Lista Neta: ¢" + (neto);

        // $('#lblExcesos')
        document.getElementById('lblExcesos').innerHTML = mensaje;
        /*************CALCULA RESTRINGIDOS************/



    }//FIN verificar

    function enviarLista(){

        var montos = new Array();
        var total = 0;


        for(var i = 1; i < 101; i++){
            montos.push($('#txtMonto' + i).val().replace(/[\.]/g,''));
            total = total + parseInt($('#txtMonto' + i).val().replace(/[\.]/g,''));
        }

        var confirmacion = confirm("¿ESTA SEGURO QUE DESEA ENVIAR ESTA LISTA BRUTA DE ¢" + total + "?");

        if(confirmacion == true){
            var parametros = {
                "Montos" : montos,
                "sorteoID" : <?php echo $sorteoID?>
            };
            $.ajax({
                data : parametros,
                url: 'seccion_ingresar_lista_action.php',
                type: 'post',
                beforeSend: function(){
                    $("#lblMensaje").html("Procesando...");
                },
                success: function(response){
                    if(response == 'ERROR'){
                        $("#lblMensaje").html('Ha ocurrido un error. Intente de nuevo');
                    }else{
                        alert('Se ha ingresado su lista. Muchas Gracias!');
                        $("#lblMensaje").html(response);
                        window.location = 'principal.php';
                    }


                }
            });
        }


    }//Fin enviarLista

    function formatoMoney(monto){

        var montoFormato = "";//Almacena el nuevo monto con formato
        var contador = 0;//Contador para al tercer loop concatene un "."
        monto = monto.split("").reverse().join("");//Invertimos la cadena para ir colocando los "."

        for(var i = 0; i < monto.length; i++){
            if(contador == 3){//Para al tercer loop concatene un "."
                montoFormato = montoFormato + ".";
                contador = 0;
            }
            montoFormato = montoFormato + monto.substring(i, i+1);
            contador++;
        }//Fin for


        return montoFormato.split("").reverse().join("");//Invertimos para que quede como estaba

    }//Fin formatoMoney

    //REDONDEO AL 50
    function redondeo(numeroAredondear){
        var numero = numeroAredondear.substring(numeroAredondear.length-2);
        var redondear = 100 - numero;
        if(redondear < 50){
            return parseInt(numeroAredondear) + redondear;
        }else if(redondear > 50 && redondear < 100){
            var aSumar = 50 - numero;
            return parseInt(numeroAredondear) + aSumar;
        }else{
            return numeroAredondear;
        }

    }//Fin redondeo

    //Redondeo al 0,25,50,75
    function redondeo12(numeroAredondear){

        var numero = numeroAredondear.substring(numeroAredondear.length-2);
        var redondear = 100 - numero;
        if(redondear < 50 && redondear > 0){
            if(redondear <= 25){
                redondear = 25 - redondear;
                //Entre 88-99
                if(redondear <= 12){
                    return parseInt(numeroAredondear) - redondear;
                }else{//Entre 75-87
                    redondear = 25 - redondear;
                    return parseInt(numeroAredondear) + redondear;
                }
            }else{
                redondear = redondear - 25;
                //Entre 63-74
                if(redondear <= 12){
                    return parseInt(numeroAredondear) + redondear;
                }else{//Entre 51-62
                    redondear = 25 - redondear;
                    return parseInt(numeroAredondear) - redondear;
                }
            }

        }else if(redondear > 50 && redondear < 100){
            redondear = 50 - numero;
            if(redondear <= 25){
                redondear = 25 - redondear;
                //Entre 38-49
                if(redondear <= 12){
                    return parseInt(numeroAredondear) - redondear;
                }else{//Entre 26-37
                    redondear = 25 - redondear;
                    return parseInt(numeroAredondear) + redondear;
                }
            }else{
                redondear = redondear - 25;
                //Entre 13-24
                if(redondear <= 12){
                    return parseInt(numeroAredondear) + redondear;
                }else{//Entre 1-12
                    redondear = 25 - redondear;
                    return parseInt(numeroAredondear) - redondear;
                }
            }
        }else{
            return numeroAredondear;
        }

    }


</script>


