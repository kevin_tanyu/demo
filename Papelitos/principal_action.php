<?php

session_start();

// Incluye datos generales y conexion a DB
include("../config.ini.php");
include("../conectadb.php");

try{

    $sorteoID = $_POST['sorteoID'];
    $montoRevision = $_POST['totalLista'];


    $sqlInsertCOF = "INSERT INTO COF_Revision(usuario_vendedor_calle_id, sorteo_programacion_id)
                     VALUES(?,?)";
    $stmtInsertCOF = $pdoConn->prepare($sqlInsertCOF);
    $stmtInsertCOF->execute(array($_SESSION['IDUsuario'], $sorteoID));

    $cofID = $pdoConn->lastInsertId();

    $sqlInsertRevision = "INSERT INTO COF_Revision_Log(cof_revision_id, cof_new_status_id, created_at, user_id, jugada1, jugada2, revision_monto, monto_terminal, devolucion)
                          VALUES(?, 3, ?, ?, -1, -1, ?, 0, 0)";
    $stmtInsertRevision = $pdoConn->prepare($sqlInsertRevision);
    $stmtInsertRevision->execute(array($cofID, date("Y-m-d H:i:s"), $_SESSION['IDUsuario'], $montoRevision));

    echo '<strong style="font-size: 20px; color: green"><i class="fa fa-check hidden-sm hidden-xs"></i>Revisado</strong>';


}catch(Exception $e){
    echo 'ERROR';
}

?>