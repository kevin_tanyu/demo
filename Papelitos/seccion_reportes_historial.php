<?php

session_start();

// Incluye datos generales y conexion a DB
include("../config.ini.php");
include("../conectadb.php");

// Incluye Header
include("header.php");

$dateFrom = date('Y-m-d');
$dateTo = date('Y-m-d');

?>

<head>
    <link href="./datepicker/css/datepicker.css" rel="stylesheet"  type="text/css">

</head>

<!-- **********************************************************************************************************************************************************
     MAIN CONTENT
     *********************************************************************************************************************************************************** -->
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> Historial</h3>
        <div class="row mt">
            <div class="col-lg-12">
                <p>
                <form>
                    <label for="dateFrom">FECHA</label> <input type="text" value="<?php echo $dateFrom ?>" id="fromDate" class="datepicker">

                    <input type="submit" value="VER" class="button" onclick="enviarFechas(); return false"/>
                </form>

                <div id="result" style="margin-top: 2em">

                    </p>
                </div>
            </div>

    </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->


<!--main content end-->
<!--footer start-->
<?php include("footer.php");?>
<!--footer end-->
</section>

<script>
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<-Ant  ',
        nextText: '  Sig->',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'yy-mm-dd',
        firstDay: 0

    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

    $('.datepicker').datepicker();


    function enviarFechas(){
        var parametros = {
            "fromDate" : $('#fromDate').val()
        };
        $.ajax({
            data : parametros,
            url: 'seccion_reportes_historial_display.php',
            type: 'post',
            beforeSend: function(){
                $("#result").html("Procesando...");
            },
            success: function(response){
                $("#result").html(response);
            }
        });
    }




</script>

