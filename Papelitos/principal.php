<?php
session_start();

if (!isset($_SESSION['ActivoFlag'])) {
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: ../index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("../config.ini.php");
include("../conectadb.php");

// Incluye Header
include("header.php");


/*GET NUMERO GANADOR*/
$sqlNumeroGanador = "SELECT Numero FROM SorteosNumerosGanadores WHERE IDSorteoProgramacion = ?
                     LIMIT 1";
$stmtNumeroGanador = $pdoConn->prepare($sqlNumeroGanador);

//Consulta para recuperar los sorteos del dia
$sqlSorteosDelDia = "SELECT SP.ID, SD.NombreSorteo, SP.FechayHora
                     FROM SorteosProgramacion SP
                     JOIN SorteosDefinicion SD
                     ON SP.IDSorteoDefinicion = SD.ID
                     WHERE DATEDIFF(NOW(), SP.FechayHora) = 0
                     ORDER BY SD.NombreSorteo DESC
";

try
{
    $stmtSorteosDelDia = $pdoConn->prepare($sqlSorteosDelDia);
    $stmtSorteosDelDia->execute();
    $sorteosDelDia = $stmtSorteosDelDia->fetchAll(PDO::FETCH_ASSOC);
} catch (PDOException $e)
{
    $sorteosDelDia = array();
    echo $e->getMessage();
}

/*CONSULTA ESTADO REVISION*/
$sqlStatusSorteo = "SELECT RS.id, S.name, S.display_name
                    FROM COF_RevisionStatus_View RS
                    JOIN COF_Status S ON RS.status_id = S.id
                    WHERE RS.sorteo_programacion_id = ? AND RS.usuario_vendedor_calle_id = ?";
$stmtStatusSorteo = $pdoConn->prepare($sqlStatusSorteo);

/*CONSULTA TOTAL DEL SORTEO*/
$sqlTotalSorteo = "SELECT SUM(sa.Cantidad) as total
                   FROM SorteosProgramacion sp
                   JOIN SorteosDefinicion sd on sd.ID = sp.IDSorteoDefinicion and DATEDIFF(sp.FechayHora,NOW()) = 0
                   JOIN sorteoapuesta_validas_puestos sa on sa.IDSorteoProgramacion = sp.ID and sa.IDUsuario = :id_usuario
                   WHERE sp.ID = :sorteo_prog_id
                   group by sp.ID, sd.NombreSorteo
                   order by sp.FechayHora";
$stmtTotalSorteo = $pdoConn->prepare($sqlTotalSorteo);

/*GET EXCESOS*/
$sqlListaEnviada = "SELECT * FROM SorteoDevolucion
                    WHERE sorteo_prog_id = ? AND usuario_id = ?";
$stmtListaEnviada = $pdoConn->prepare($sqlListaEnviada);


$sqlMontoDevolucion = "SELECT SUM(monto_devuelto) as 'total' FROM SorteoDevolucion_Detalle
                       WHERE sorteodevolucion_id = ?";
$stmtMontoDevolucion = $pdoConn->prepare($sqlMontoDevolucion);

/*GET PREMIO*/
$sqlMontoGanador = "SELECT Cantidad as 'Total' FROM sorteoapuesta_puestos
                     WHERE IDUsuario = ? AND IDSorteoProgramacion = ? AND Numero = ?";
$stmtMontoGanador = $pdoConn->prepare($sqlMontoGanador);


?>

<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i>Bienvenido(a)</h3>
        <div class="row mt">
            <div class="col-lg-12">
                <p>El Dia de la Suerte  - 2015</p>
            </div>
        </div>



                <?php foreach ($sorteosDelDia as $sorteoDelDia): ?>
                    <?php
                    /*NUMERO GANADOR*/
                    $stmtNumeroGanador->execute(array( $sorteoDelDia['ID']));
                    $ganador = $stmtNumeroGanador->fetch();

                    /*ESTADO REVISION*/
                    $stmtStatusSorteo->execute(array( $sorteoDelDia['ID'], $_SESSION['IDUsuario']));
                    $status = $stmtStatusSorteo->fetch();

                    /*TOTAL SORTEO*/
                    $stmtTotalSorteo->execute(array(':id_usuario' => $_SESSION['IDUsuario'], ':sorteo_prog_id' => $sorteoDelDia['ID']));
                    $totalSorteo = $stmtTotalSorteo->fetch();
                    $totalBruto = $totalSorteo['total'];

                    /*EXCESOS*/
                    $stmtListaEnviada->execute(array( $sorteoDelDia['ID'], $_SESSION['IDUsuario']));
                    $lista = $stmtListaEnviada->fetch();

                    $stmtMontoDevolucion->execute(array($lista['id']));
                    $devolucion = $stmtMontoDevolucion->fetch();
                    $montoDevuelto = $devolucion['total'];

                    /*PREMIO*/
                    $stmtMontoGanador->execute(array($_SESSION['IDUsuario'],$sorteoDelDia['ID'], $ganador['Numero'] ));
                    $montoGanador = $stmtMontoGanador->fetch();
                    $premio = $_SESSION['PagaPorcentaje'] * $montoGanador['Total'];

                    $nowTime = date('Y-m-d H:i:s');
                    $nowTime = date('Y-m-d H:i:s', strtotime($nowTime . ' - 10 minutes'));

                    ?>
        <div class="row">
                    <div class="col-md-6 col-sm-6  mb">
                        <div class="stock card">
                            <div class="stock current-price">
                                <div class="row">
                                    <div class="info col-sm-6 col-xs-6"><abbr>Numero Ganador</abbr>
                                        <time></time>
                                    </div>
                                    <div class="changes col-sm-6 col-xs-6">
                                        <?php if($stmtNumeroGanador->rowCount() > 0){?>
                                                 <div class="value up" style="color: #000000"><i class="fa fa-slack hidden-sm hidden-xs"></i><?php echo $ganador['Numero']; ?></div>
                                        <?php }else{?>
                                                 <div class="value up" style="color: #000000"><i class="fa fa-slack hidden-sm hidden-xs"></i>ND</div>
                                        <?php }?>
                                        <div class="change hidden-sm hidden-xs"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="stock current-price">
                                <div class="row">
                                    <div class="info col-sm-6 col-xs-6"><abbr>Lista Bruta</abbr>
                                        <time></time>
                                    </div>
                                    <div class="changes col-sm-6 col-xs-6">
                                        <div class="value up"  style="color: #000000"><i class="fa fa fa-tag hidden-sm hidden-xs"></i><?php echo system_number_money_format($totalBruto)?></div>
                                        <div class="change hidden-sm hidden-xs"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="stock current-price">
                                <div class="row">
                                    <div class="info col-sm-6 col-xs-6"><abbr>Excesos</abbr>
                                        <time></time>
                                    </div>
                                    <div class="changes col-sm-6 col-xs-6">
                                        <div class="value up"><i class="fa fa-exclamation-triangle hidden-sm hidden-xs" style="color: #DF0101"></i><span style="color: #000000"><?php echo system_number_money_format($montoDevuelto)?></span></div>
                                        <div class="change hidden-sm hidden-xs"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="stock current-price">
                                <div class="row">
                                    <div class="info col-sm-6 col-xs-6"><abbr>Lista Neta</abbr>
                                        <time></time>
                                    </div>
                                    <div class="changes col-sm-6 col-xs-6">
                                        <div class="value up"  style="color: #000000" id="divNeta<?php echo $sorteoDelDia['ID']?>"><i class="fa fa-bookmark hidden-sm hidden-xs"></i><?php echo system_number_money_format($totalBruto-$montoDevuelto)?></div>
                                    </div>
                                </div>
                            </div>

                            <div class="stock current-price">
                                <div class="row">
                                    <div class="info col-sm-6 col-xs-6"><abbr>Premios</abbr>
                                        <time></time>
                                    </div>
                                    <div class="changes col-sm-6 col-xs-6">
                                        <div class="value up" ><i class="fa fa fa-trophy hidden-sm hidden-xs" style="color: #EAC117"></i><span style="color: #000000"><?php echo system_number_money_format($premio)?></span></div>
                                    </div>
                                </div>
                            </div>


                            <div class="stock current-price">
                                <div class="row">
                                    <div class="info col-sm-6 col-xs-6"><abbr>

                                    </abbr>
                                        <time></time>
                                    </div>
                                    <div class="changes col-sm-6 col-xs-6">
                                        <?php if($nowTime < $sorteoDelDia['FechayHora'] && $stmtStatusSorteo->rowCount() == 0):?>
                                            <button type="button" class="btn btn-primary" id="btnPapelito<?php echo $sorteoDelDia['ID']?>" onclick="ingresarPapelitos(<?php echo $sorteoDelDia['ID']?>)">Ingresar Papelitos</button>
                                        <?php endif?>
                                        <?php if($nowTime < $sorteoDelDia['FechayHora'] && $stmtStatusSorteo->rowCount() == 0):?>
                                            <button type="button" class="btn btn-primary" id="btnLista<?php echo $sorteoDelDia['ID']?>" onclick="ingresarLista(<?php echo $sorteoDelDia['ID']?>)">Ingresar Lista</button>
                                        <?php endif?>
                                    </div>
                                </div>
                            </div>





                            <div class="summary">
                                <strong style="font-size: 20px"><?php echo $sorteoDelDia['NombreSorteo'];?></strong> <span></span></br>
                                <div id="divRevision<?php echo $sorteoDelDia['ID']?>">
                                    <?php if($stmtStatusSorteo->rowCount() > 0){?>
                                        <strong style="font-size: 20px; color: green"><i class="fa fa-check hidden-sm hidden-xs"></i><?php echo $status['display_name'];?></strong>
                                    <?php }else{
                                        ?>
                                        <?php if($nowTime > $sorteoDelDia['FechayHora']){?>
                                              <strong style="font-size: 20px; color: red"><i class="fa fa-times hidden-sm hidden-xs"></i>&nbsp;No se recibió revisión</strong>
                                           <?php }else{?>
                                             <button type="button" class="btn btn-theme04" onclick="enviarRevision(<?php echo $sorteoDelDia['ID']?>, <?php echo $totalBruto-$montoDevuelto?>)"><i class="fa fa-check-square-o"></i>&nbsp;Enviar Revisión</button>
                                           <?php }?>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div><! -- /col-md-4 -->
        </div></br></br></br></br></br></br></br>

                <?php endforeach ?>



    </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
<!--footer start-->
<?php include("footer.php");?>
<!--footer end-->

<script>

    function ingresarPapelitos(idSorteo){

        window.location = 'seccion_ingresar_papelitos.php?SID=' + idSorteo;

    }//FIn ingresarPapelitos

    function ingresarLista(idSorteo){

        window.location = 'seccion_ingresar_lista.php?SID=' + idSorteo;

    }//FIn ingresarPapelitos

    function enviarRevision(idSorteo, montoRevision){

        var enviar = confirm("Se envia a revision una lista neta de " + $('#divNeta'+idSorteo).text());

        if(enviar == true){
            var parametros = {
                "Accion" : 'Revision',
                "sorteoID" : idSorteo,
                "totalLista" : montoRevision
            };
            $.ajax({
                data : parametros,
                url: 'principal_action.php',
                type: 'post',
                beforeSend: function(){
                    $("#divRevision"+idSorteo).html('<strong style="font-size: 20px;"><i class="fa fa-spinner hidden-sm hidden-xs"></i>&nbsp;Procesando</strong>');
                },
                success: function(response){
                    if(response == 'ERROR'){
                        $("#divRevision"+idSorteo).html('<strong style="font-size: 20px;"><i class="fa fa-times hidden-sm hidden-xs"></i>&nbsp;Ha ocurrido un error. Intente de nuevo</strong>');
                    }else{
                        $("#divRevision"+idSorteo).html(response);
                        $("#btnPapelito"+idSorteo).css('visibility', 'hidden');
                        $("#btnLista"+idSorteo).css('visibility', 'hidden');
                    }


                }
            });
        }

    }//Fin enviarRevision

</script>