<?php
session_start();

// Incluye datos generales y conexion a DB
include("../config.ini.php");
include("../conectadb.php");


try{

    $montos = $_POST['Montos'];
    $sorteoID = $_POST['sorteoID'];


    //Crear el ticket con el ID del vendedor y la fecha y hora
    $sqlTicketPuesto  = "INSERT INTO ticket_puestos (IDUsuario, FechayHora) VALUES (:id_usuario,:date)";
    $stmtTicketPuesto = $pdoConn->prepare($sqlTicketPuesto);

    //Inserta los numeros y montos que se guardaron del excel a la tabla de sorteoapuestapuestos
    $sqlSorteoApuestaPuesto = "INSERT INTO sorteoapuesta_puestos (IDUsuario, IDSorteoProgramacion
                           , FechayHora, IDTicket, Cantidad, Numero) VALUES
                           (:id_usuario, :id_sorteo, :dateT, :ticket_id, :amount, :numbers)";
    $stmtSorteoApuestaPuesto = $pdoConn->prepare($sqlSorteoApuestaPuesto);

    //Select para comprobar si el ticket y los numeros se ha ingresado en la tabla sorteoapuesta_puestos
    $sqlComprobarTicket = "SELECT * FROM sorteoapuesta_puestos
                               WHERE IDUsuario = :id_user and IDSorteoProgramacion = :id_sorteo";
    $stmtComprobarTicket = $pdoConn->prepare($sqlComprobarTicket);
    $stmtComprobarTicket->execute(array(':id_user' => $_SESSION['IDUsuario'], ':id_sorteo' => $sorteoID));
    $Tiquetes = $stmtComprobarTicket->fetchAll(PDO::FETCH_ASSOC);


    //Update de los numeros y montos del ticket correspondiente
    $sqlUpdateMontos = "UPDATE sorteoapuesta_puestos
                        SET Cantidad = :amount, FechayHora = :date
                        WHERE IDUsuario = :id_user and IDSorteoProgramacion = :id_sorteo and Numero = :numbers";
    $stmtUpdateMontos = $pdoConn->prepare($sqlUpdateMontos);


    //Si no se ha ingresado el ticket realiza inserccion de lo contrario hace un update
    if($stmtComprobarTicket ->rowCount() == 0){

        $stmtTicketPuesto->execute(array(':id_usuario' => $_SESSION['IDUsuario'], ':date' => date("Y-m-d H:i:s")));
        $ticketId = $pdoConn->lastInsertId();

        $tempo = 0;

        for($i = 0; $i < 100; $i++){
            if($i == 99){
                $stmtSorteoApuestaPuesto->execute(array(':id_usuario' => $_SESSION['IDUsuario'], ':id_sorteo' => $sorteoID,
                    ':dateT' => date("Y-m-d H:i:s"), ':ticket_id' => $ticketId,
                    ':amount' => $montos[$i], ':numbers' => 0));
                // $tempo = $tempo + $numero['amount'];
            }else{
                $stmtSorteoApuestaPuesto->execute(array(':id_usuario' => $_SESSION['IDUsuario'], ':id_sorteo' => $sorteoID,
                    ':dateT' => date("Y-m-d H:i:s"), ':ticket_id' => $ticketId,
                    ':amount' => $montos[$i], ':numbers' => $i+1));
            }

        }

    }else{

        $contador = 0;

        foreach($Tiquetes as $tiquete){
            $total = $tiquete['Cantidad'];

            if($contador == 99){
                $stmtUpdateMontos-> execute(array(':amount' => $montos[$contador] + $total,
                    ':id_user' => $_SESSION['IDUsuario'],
                    ':date' => date("Y-m-d H:i:s"),
                    ':id_sorteo' => $sorteoID,
                    ':numbers' => 0));

            }else{
                $stmtUpdateMontos-> execute(array(':amount' => $montos[$contador] + $total,
                    ':id_user' => $_SESSION['IDUsuario'],
                    ':date' => date("Y-m-d H:i:s"),
                    ':id_sorteo' => $sorteoID,
                    ':numbers' => $contador+1));

            }//Fin if/else

            $contador++;

        }//Fin foreach


    }//Fin if/else

    /*CALCULA EXCESOS*/
    /*TOTAL ACUMULADO*/
    $sqlTotalSorteo = "SELECT SUM(Cantidad) as 'total' FROM sorteoapuesta_puestos
                       WHERE IDUsuario = ? AND IDSorteoProgramacion = ?";
    $stmtTotalSorteo = $pdoConn->prepare($sqlTotalSorteo);
    $stmtTotalSorteo->execute(array($_SESSION['IDUsuario'], $sorteoID));
    $totalSorteo = $stmtTotalSorteo->fetch();

    $montoSorteo = $totalSorteo['total'];

    $sqlCheckDevo = "SELECT * FROM SorteoDevolucion WHERE sorteo_prog_id = ? AND usuario_id = ?";
    $stmtCheckDevo = $pdoConn->prepare($sqlCheckDevo);
    $stmtCheckDevo->execute(array($sorteoID, $_SESSION['IDUsuario']));

    if($stmtCheckDevo->rowcount() == 0){

        $sqlInsertDevo = "INSERT INTO SorteoDevolucion(sorteo_prog_id, usuario_id, monto_lista, fechayhora)
                              VALUES(?,?,?, NOW())";
        $stmtInsertDevo = $pdoConn->prepare($sqlInsertDevo);
        $stmtInsertDevo->execute(array($sorteoID, $_SESSION['IDUsuario'] , $montoSorteo));
        $devoID = $pdoConn->lastInsertId();

    }else{

        $devolucionSQL = $stmtCheckDevo->fetch();
        $devoID = $devolucionSQL['id'];

        $sqlUpdateDevo = "UPDATE SorteoDevolucion
                          SET monto_lista = ?
                          WHERE id =  ?";
        $stmtUpdateDevo = $pdoConn->prepare($sqlUpdateDevo);
        $stmtUpdateDevo->execute(array($montoSorteo, $devoID));


    }//FIN if/else

    /*CALCULA LOS MONTOS TOPE*/
    $montoARebajar = 0;
    $montoTope = round($montoSorteo * ($_SESSION['Devolucion']/100));
    $montoTope = redondeo12($montoTope);

    $sqlTotalNumero = "SELECT Cantidad FROM sorteoapuesta_puestos
                       WHERE IDUsuario = ? AND IDSorteoProgramacion = ? AND Numero = ?";
    $stmtTotalNumero = $pdoConn->prepare($sqlTotalNumero);

    //Captura los restringidos del dia
    $sqlNumRestringidos = "SELECT Numero
                           FROM SorteosNumerosRestringidos
                           WHERE IDSorteoProgramacion = $sorteoID";
    $stmtNumRestringidos = $pdoConn->prepare($sqlNumRestringidos);
    $stmtNumRestringidos->execute();
    $numRestringidos = $stmtNumRestringidos->fetchAll(PDO::FETCH_ASSOC);

    $sqlDeleteDetalleDevo = "DELETE FROM SorteoDevolucion_Detalle WHERE sorteodevolucion_id = ?";
    $stmtDeleteDetalleDevo = $pdoConn->prepare($sqlDeleteDetalleDevo);
    $stmtDeleteDetalleDevo->execute(array($devoID));

    $sqlInsertDetalle = "INSERT INTO SorteoDevolucion_Detalle(sorteodevolucion_id, numero, monto_devuelto, fechayhora)
                         VALUES(?,?,?, NOW())";
    $stmtInsertDetalle = $pdoConn->prepare($sqlInsertDetalle);

    foreach($numRestringidos as $restringido){
        $numero = $restringido['Numero'];

        $stmtTotalNumero->execute(array($_SESSION['IDUsuario'], $sorteoID, $numero));
        $totalNumero = $stmtTotalNumero->fetch();

        if($totalNumero['Cantidad'] > $montoTope){
            $aRebajar = $totalNumero['Cantidad'] - $montoTope;
            $stmtInsertDetalle->execute(array($devoID, $numero, $aRebajar));

        }


    }//FIN foreach






}catch(Exception $e){
    echo 'ERROR';
}


//Redondeo al 0,25,50,75
function redondeo12($numeroAredondear){
    $numero = substr($numeroAredondear, sizeof($numeroAredondear)-3);
    $redondear = 100 - $numero;
    if($redondear < 50 && $redondear > 0){
        if($redondear <= 25){
            $redondear = 25 - $redondear;
            //Entre 88-99
            if($redondear <= 12){
                return $numeroAredondear - $redondear;
            }else{//Entre 75-87
                $redondear = 25 - $redondear;
                return $numeroAredondear + $redondear;
            }
        }else{
            $redondear = $redondear - 25;
            //Entre 63-74
            if($redondear <= 12){
                return $numeroAredondear + $redondear;
            }else{//Entre 51-62
                $redondear = 25 - $redondear;
                return $numeroAredondear - $redondear;
            }
        }

    }else if($redondear > 50 && $redondear < 100){
        $redondear = 50 - $numero;
        if($redondear <= 25){
            $redondear = 25 - $redondear;
            //Entre 38-49
            if($redondear <= 12){
                return $numeroAredondear - $redondear;
            }else{//Entre 26-37
                $redondear = 25 - $redondear;
                return $numeroAredondear + $redondear;
            }
        }else{
            $redondear = $redondear - 25;
            //Entre 13-24
            if($redondear <= 12){
                return $numeroAredondear + $redondear;
            }else{//Entre 1-12
                $redondear = 25 - $redondear;
                return $numeroAredondear - $redondear;
            }
        }
    }else{
        return $numeroAredondear;
    }

}//Fin redondeo12

?>