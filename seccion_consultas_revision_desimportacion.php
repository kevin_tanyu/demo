<?php

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

/*VENDEDORES*/
$sqlVendedores = "SELECT * FROM Usuarios WHERE TipoUsuario=5 and importacion = 1
                  ORDER BY NombreUsuario"; // VENDEDORES
$stmtVendedores = $pdoConn->prepare($sqlVendedores);
$stmtVendedores->execute();
$Vendedores = $stmtVendedores->fetchAll(PDO::FETCH_ASSOC);

$sqlSorteos = "SELECT SP.ID, SD.NombreSorteo, SP.FechayHora
               FROM SorteosProgramacion SP
               JOIN SorteosDefinicion SD
               ON SP.IDSorteoDefinicion = SD.ID
               WHERE SP.ID = ?";
$stmtSorteo = $pdoConn->prepare($sqlSorteos);
$stmtSorteo->execute(array($_GET['SID']));
$sorteo = $stmtSorteo->fetch();

/*VERIFICAR SI ENVIO*/
$sqlCheckSiReviso = "SELECT * FROM SorteoApuesta
                     WHERE IDUsuario = ? AND IDSorteoProgramacion = ?";
$stmtCheckSiReviso = $pdoConn->prepare($sqlCheckSiReviso);

/*DELETE TICKET*/
$sqlDeleteTicket = "DELETE FROM Ticket WHERE id = ?";
$stmtDeleteTicekt = $pdoConn->prepare($sqlDeleteTicket);

?>

<style type="text/css">
    .fancybox-custom .fancybox-skin {
        box-shadow: 0 0 50px #222;
    }

    input[type=checkbox] {
        visibility: visible;
        height: 15px;
    }


</style>

<h3>REMOVER LISTA DEL SORTEO <?php echo $sorteo['NombreSorteo'] . ' DEL ' . $sorteo['FechayHora']?></h3>
<div class="divTable">
    <div class="divRow" style="width: 250px">
        <div class="divCellHeader2" style="width: 180px">Lista</div>
        <div class="divCellHeader2" style="width: 150px">Estado</div>
        <div class="divCellHeader2" style="width: 150px">Desimportar</div>
    </div>
    <?php foreach ($Vendedores as $vendedor): ?>
        <div class="divRow2">
            <?php
            if (strpos($vendedor['NombreUsuario'],'IMPORT') === false){
                $nombreUsuario = $vendedor['NombreUsuario'];
            }else{
                $nombreUsuario = substr($vendedor['NombreUsuario'], 7);
            }
            ?>
            <div class="divCellCen2" style="width: 180px; text-align: center; font-weight: bold; font-size: 15px">
                <?php echo $nombreUsuario ?>
            </div>
            <?php

            $stmtCheckSiReviso->execute(array($vendedor['ID'], $_GET['SID']));
            $disponible = false;
            if($stmtCheckSiReviso->rowcount() > 0){
                $disponible = true;
            }

            ?>
            <?php if($disponible == true){?>
                <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px">
                    <?php echo 'Revisado '?>
                </div>
            <?php }else{?>
                <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px">
                    <?php echo 'No envio' ?>
                </div>
            <?php }?>

            <?php if($disponible == true){?>
                <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px">
                    <input name="importacion[]" type="checkbox" value="<?php echo $vendedor['ID']?>"/>
                </div>
            <?php }else{?>
                <div class="divCellCen2" style="width: 150px; text-align: center; font-weight: bold; font-size: 15px">

                </div>
            <?php }?>
        </div>
    <?php endforeach; ?>
    <div class="divCellCen2" style="width: 300px; text-align: center; font-weight: bold; font-size: 15px">
        <input type="button" id="btnImportar" class="button"
               value="Remover Lista(s)" onclick="desimportarListas()"><div id="resultRemover"></div>
    </div>
</div>


<script>
    function desimportarListas(){

        var desimportacion = [];
        $(':checkbox:checked').each(function() {
            desimportacion.push($(this).val());
        });
        if(desimportacion.length == 0){
            $("#result").html('Seleccione una lista');
        }else{
            var parametros = {
                "desimportacion" : desimportacion,
                "sorteoID" : <?php echo $_GET['SID'];?>,
                "accion" : 'Desimportar'
            };
            $.ajax({
                data : parametros,
                url: 'seccion_consultas_revision_importacion_data.php',
                type: 'post',
                beforeSend: function(){
                    $("#resultRemover").html("Procesando");
                },
                success: function(response){
                    $("#resultRemover").html(response);
                }
            });
        }


    }//Fin importarListas

</script>