<?php
session_start();

if (!isset($_SESSION['ActivoFlag']))
{
    // Verifica si hay session creada, de lo contrario redirige al index
    header("Location: index.php?IDM=TO");
    exit;
}

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

// Incluye Header
include("header.php");

include("seccion_usuarios_submenu.php");
?>
<script type="text/javascript">
    $(document).ready(function () {
        $(".various2").fancybox({
            fitToView: true,
            width: '70%',
            height: '70%',
            autoSize: true,
            closeClick: false,
            openEffect: 'fade',
            closeEffect: 'elastic'
        });


    });

    function FormularioAddUsuario() {
        $.ajax({
            type: "POST",
            url: 'seccion_usuarios_vendedores_add.php',
            data: $('#formulario1').serialize(),
            success: function (response) {
                $("#jqxgrid").jqxGrid("updatebounddata");
                $.fancybox.close();
            }
        });

        return false;
    }


</script>
<style type="text/css">
    .fancybox-custom .fancybox-skin {
        box-shadow: 0 0 50px #222;
    }
</style>

<div id="ContenedorGeneral">

    <h3 style="line-height:1px;">Usuarios Vendedores Calle</h3>
    <h6 style="line-height:1px;">Agregar, Modificar o Borrar los usuarios con permisos de vendedor calle dentro del
        sistema.</h6>

    <script type="text/javascript">

        $(document).ready(function () {
            // prepare the data
            var data = {};
            var theme = 'classic';
            var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'NombreUsuario' },
                    { name: 'Contrasena' },
                    { name: 'Email' },
                    { name: 'UsuarioNumerico' },
                    { name: 'PassNumerico' },
                    { name: 'ID' },
                    { name: 'MAC_ADDRESS' },

                ],
                id: 'ID',
                url: 'seccion_usuarios_vendedores_getdata.php',
                updaterow: function (rowid, rowdata, commit) {
                    // synchronize with the server - send update command
                    var data = "update=true&NombreUsuario=" + rowdata.NombreUsuario + "&Contrasena=" + rowdata.Contrasena + "&Email=" + rowdata.Email;
                    data = data + "&UsuarioNumerico=" + rowdata.UsuarioNumerico + "&PassNumerico=" + rowdata.PassNumerico;
                    data = data + "&ID=" + rowdata.ID;
                    data = data + "&MAC_ADDRESS=" + rowdata.MAC_ADDRESS;
               //     data = data + "&Paga=" + rowdata.Paga + "&Jugada=" + rowdata.Jugada + "&Devolucion=" + rowdata.Devolucion + "&Seguros=" + rowdata.Seguros;
                    $.ajax({
                        dataType: 'json',
                        url: 'seccion_usuarios_vendedores_getdata.php',
                        data: data,
                        success: function (data, status, xhr) {
                            // update command is executed.
                            commit(true);
                        },
                        error: function () {
                            // cancel changes.
                            commit(false);
                        }
                    });
                },
                deleterow: function (rowid, commit) {
                    // synchronize with the server - send update command
                    var data = "delete=true&ID=" + rowid;
                    $.ajax({
                        dataType: 'json',
                        url: 'seccion_usuarios_vendedores_getdata.php',
                        data: data,
                        success: function (data, status, xhr) {
                            // update command is executed.
                            commit(true);
                        },
                        error: function () {
                            // cancel changes.
                            commit(false);
                        }
                    });

                }

            };
            // initialize jqxGrid
            $("#jqxgrid").jqxGrid(
                {
                    width: 820,
                    height: 350,
                    source: source,
                    theme: theme,
                    editable: true,
                    selectionmode: 'row',
                    columns: [
                        { text: 'NombreUsuario', editable: false, datafield: 'NombreUsuario', width: 130 },
                        { text: 'Contrasena', datafield: 'Contrasena', width: 100 },
                        { text: 'Email', datafield: 'Email', width: 180 },
                        { text: 'UsuarioNumerico', datafield: 'UsuarioNumerico', width: 120 },
                        { text: 'PassNumerico', datafield: 'PassNumerico', width: 110 },

                        { text: 'ID', datafield: 'ID', width: 60 },

                        { text: 'MAC-ADDRESS', datafield: 'MAC_ADDRESS', width: 120 },

                    ]
                });
            //$("#deleterowbutton").jqxButton({ theme: theme });
            $("#deleterowbutton").on('click', function () {
                var selectedrowindex = $("#jqxgrid").jqxGrid('getselectedrowindex');
                var rowscount = $("#jqxgrid").jqxGrid('getdatainformation').rowscount;
                if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                    if (confirm("Esta seguro de querer borrar el usuario?")) {
                        var id = $("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                        var commit = $("#jqxgrid").jqxGrid('deleterow', id);
                    }
                } else {
                    alert('Debe seleccionar una fila para borrarla');
                }
            });

            $('#modificarrowbutton').on('click', function () {
                var selectedrowindex = $("#jqxgrid").jqxGrid('getselectedrowindex');
                var rowscount = $("#jqxgrid").jqxGrid('getdatainformation').rowscount;
                if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                    var id = $("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                    stringUrl = 'seccion_usuarios_vendedores_parametros_form.php?id=' + id;
                    $.fancybox({
//                        width: 400,
//                        height: 400,
                        autoSize: true,
                        href: stringUrl,
                        type: 'ajax'
                    });
                } else {
                    alert('Debe seleccionar una fila para modificar los parametros de venta.');
                }
            });
            $('#modificar_tablet_rowbutton').on('click', function () {
                var selectedrowindex = $("#jqxgrid").jqxGrid('getselectedrowindex');
                var rowscount = $("#jqxgrid").jqxGrid('getdatainformation').rowscount;
                if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                    var id = $("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                    stringUrl = 'seccion_usuarios_vendedores_tablet_form.php?id=' + id;
                    $.fancybox({
                        width: 400,
                        height: 350,
                        autoSize: false,
                        href: stringUrl,
                        type: 'ajax'
                    });
                } else {
                    alert('Debe seleccionar una fila para modificar los parametros tablet.');
                }
            });
            $('#modificar_datos_rowbutton').on('click', function () {
                var selectedrowindex = $("#jqxgrid").jqxGrid('getselectedrowindex');
                var rowscount = $("#jqxgrid").jqxGrid('getdatainformation').rowscount;
                if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                    var id = $("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                    stringUrl = 'seccion_usuarios_vendedores_datos_form.php?id=' + id;
                    $.fancybox({
                        width: 370,
                        height: 400,
                        autoSize: false,
                        href: stringUrl,
                        type: 'ajax'
                    });
                } else {
                    alert('Debe seleccionar una fila para modificar los parametros tablet.');
                }
            });

        });


    </script>

    <div id='jqxWidget' style="font-size: 13px; font-family: Verdana; float: left;">
        <a class="various2  fancybox.ajax button" href="seccion_usuarios_vendedores_form.php"
           style="padding-top:3px; padding-bottom:3px; font-size:10px;  float:left; margin-bottom:10px;">Agregar
            Usuario</a>

        <!--<input type="button" id="modificarrowbutton" class="button"
               style="font-size:10px; padding-top:6px; padding-bottom:6px; float:left; margin-bottom:10px;"
               value="Modificar parametros venta"/>-->

        <!--<input type="button" id="modificar_tablet_rowbutton" class="button"
               style="font-size:10px; padding-top:6px; padding-bottom:6px; float:left; margin-bottom:10px;"
               value="Modificar Tablet"/>-->

        <input type="button" id="modificar_datos_rowbutton" class="button"
               style="font-size:10px; padding-top:6px; padding-bottom:6px; float:left; margin-bottom:10px;"
               value="Modificar Datos"/>

        <input type="button" id="deleterowbutton" class="button"
               style="color:#ff0000; font-size:10px; padding-top:6px; padding-bottom:6px; float:left; margin-bottom:10px;"
               value="Borrar Usuario Seleccionado">


        <div style="float: left;" id="jqxgrid">
        </div>
    </div>
</div>
<?php
// Incluye Footer
include("footer.php");
?>
