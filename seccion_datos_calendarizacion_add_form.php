<?php
include("config.ini.php");
include("conectadb.php");

$param_dia = isset($_GET['dia']) ? $_GET['dia'] : date('Y-m-d');

?>
<script type="text/javascript">
    function ActivaFlags(L) {
        var Arreglo = L.split(',');
        document.formulario1.FlagLunes.value = Arreglo[1];
        document.formulario1.FlagMartes.value = Arreglo[2];
        document.formulario1.FlagMiercoles.value = Arreglo[3];
        document.formulario1.FlagJueves.value = Arreglo[4];
        document.formulario1.FlagViernes.value = Arreglo[5];
        document.formulario1.FlagSabado.value = Arreglo[6];
        document.formulario1.FlagDomingo.value = Arreglo[7];
        document.formulario1.IDSorteoDefinicionValor.value = Arreglo[0];
    }

    function doChosen() {
        $(".chzn-select").chosen();
        $(".chzn-select-deselect").chosen({allow_single_deselect: true});
    }

    function AgregarFechas() {
        obj = document.getElementById('MasFechas');
        if (obj.style.visibility == "hidden") { //Esta Escondido
            obj.style.visibility = "visible";

            var dd;
            var d = new Date();
            var wd = new Array(document.formulario1.FlagDomingo.value, document.formulario1.FlagLunes.value, document.formulario1.FlagMartes.value, document.formulario1.FlagMiercoles.value, document.formulario1.FlagJueves.value, document.formulario1.FlagViernes.value, document.formulario1.FlagSabado.value);
            var Dias = new Array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado');

            var Contenido = '';
            var Opciones = '';
            for (y = 0; y <= 99; y++) {
                Opciones += '<option value="' + y + '">' + y + '</option>';
            }


            //	MasFechas.innerHTML+='';
            for (x = 1; x <= 30; x++) {
                dd = new Date(d.getFullYear(), d.getMonth(), d.getDate() + x);

//				alert(x +' = '+wd[dd.getDay()]);

                if (wd[dd.getDay()] == 1) {
                    //Fecha Disponible

                    Contenido = '<div class="divTable"><div class="divRow"><div class="divCellCen">' + Dias[dd.getDay()] + '</div><div class="divCellCen" style="width:100px;"><input name="FechaAdd' + x + '" type="text" class="campotexto" style="width:60px; font-size:12px; margin-right:5px; margin-top:1px;" value="' + dd.getFullYear() + '-' + (dd.getMonth() + 1) + '-' + dd.getDate() + '"></div><div class="divCellCen" style="width:100px;">Num. Suerte</div><div class="divCellCen" style="width:100px;"><select name="SuerteAdd' + x + '" data-placeholder="Numero de la Suerte" class="chzn-select" style="width:60px;" id="SuerteAdd' + x + '">' + Opciones + '</select></div><div class="divCellCen" style="width:120px;">Num. Restringidos</div><div class="divCellCen"><select name="NumeroResAdd' + x + '[]" data-placeholder="Numeros Restringidos" class="chzn-select" style="width:150px;" id="NumerosRes' + x + '" multiple>' + Opciones + '</select></div></div></div>';
                    $('#MasFechas').append(Contenido);
                    //alert(Contenido);
                }

            }
            doChosen();

//			MasFechas.innerHTML+='<input type="button" value="Cancelar Fechas" class="button" id="BotonCancelar" onclick="javascript:AgregarFechas();">';
            //		MasFechas.innerHTML+='';


        } else { // Esta Visible
            obj.style.visibility = "hidden";
            document.getElementById('BotonCancelar').checked = false;
            MasFechas.innerHTML = "";
        }

    }
</script>
<h3 style="line-height:1px;">Calendarizacion de Loteria</h3>
<div style="text-align:center;">
    <form name="formulario1" id="formulario1">
        <div class="divTable" style="width:650px;">
            <div class="divRow">
                <div class="divCellIzq">Loteria a Calendarizar</div>
                <div class="divCellDer"><select name="IDSorteoDefinicion" data-placeholder="Sorteo" class="chzn-select"
                                                style="width:150px;" tabindex="1" id="chosen"
                                                onchange="ActivaFlags(this.value);">
                        <?php

                        list($ano, $mes, $dia) = explode("-", $param_dia);
                        $DiaDeLaSemana = date("w", mktime(0, 0, 0, $mes, $dia, $ano));

                        $AGREGADO = "";

                        switch ($DiaDeLaSemana)
                        {
                            case 0:
                                $AGREGADO = " AND DomingoFlag=1 ";
                                break;
                            case 1:
                                $AGREGADO = " AND LunesFlag=1 ";
                                break;
                            case 2:
                                $AGREGADO = " AND MartesFlag=1 ";
                                break;
                            case 3:
                                $AGREGADO = " AND MiercolesFlag=1 ";
                                break;
                            case 4:
                                $AGREGADO = " AND JuevesFlag=1 ";
                                break;
                            case 5:
                                $AGREGADO = " AND ViernesFlag=1 ";
                                break;
                            case 6:
                                $AGREGADO = " AND SabadoFlag=1 ";
                                break;
                        }

                        echo $QUERY = "SELECT ID, NombreSorteo, LunesFlag, MartesFlag, MiercolesFlag, JuevesFlag, ViernesFlag, SabadoFlag, DomingoFlag FROM SorteosDefinicion WHERE FlagActivo=1" . $AGREGADO . " ORDER BY Orden ASC";
                        $rs = mysql_query($QUERY);
                        $F = 0;
                        if (mysql_affected_rows() > 0)
                        {
                            $F = 1;
                            while ($row = mysql_fetch_row($rs))
                            {
                                ?>
                                <option
                                    value="<?= $row[0]; ?>,<?= $row[2]; ?>,<?= $row[3]; ?>,<?= $row[4]; ?>,<?= $row[5]; ?>,<?= $row[6]; ?>,<?= $row[7]; ?>,<?= $row[8]; ?>"><?= $row[1]; ?></option>
                            <?php
                            }
                        }
                        ?>
                    </select>
                    <?php
                    if ($F == 0)
                    {
                        ?>
                        <span class="letra" style="color:#ff0000;">No hay loterias activas para este dia.</span>
                    <?php
                    }
                    ?>
                    <script type="text/javascript">
                        ActivaFlags(document.formulario1.IDSorteoDefinicion.value);
                    </script>
                </div>
            </div>
            <div class="divRow">
                <div class="divCellIzq">Fecha y Hora</div>
                <div class="divCellDer"><input type="text" name="Fecha" value="<?= $param_dia; ?>" class="campotexto"
                                               id="datepicker"
                                               style="width:60px; font-size:12px; margin-right:5px; margin-top:1px;"
                                               required>
                    <script>
                        $(function () {
                            $("#datepicker").datepicker({ constrainInput: true, dateFormat: "yy-mm-dd" });
                        });
                    </script>
                    <select name="Hora" data-placeholder="Hora" class="chzn-select" style="width:60px;" tabindex="1"
                            id="Hora"><?php
                        for ($h = 0; $h <= 23; $h++)
                        {
                            ?>
                            <option value="<?= $h; ?>"<?php if ($h == date('H')) echo " SELECTED"; ?>><?php if ($h < 10) echo "0";
                            echo $h; ?></option><?php
                        } ?></select>
                    <select name="Minutos" data-placeholder="Min" class="chzn-select" style="width:60px;" tabindex="1"
                            id="Minutos"><?php
                        for ($h = 0; $h <= 59; $h = $h + 5)
                        {
                            ?>
                            <option value="<?= $h; ?>"<?php if ($h == date('H')) echo " SELECTED"; ?>><?php if ($h < 10) echo "0";
                            echo $h; ?></option><?php
                        } ?></select>
                </div>
            </div>
            <div class="divRow">
                <div class="divCellIzq">Numero de la Suerte</div>
                <div class="divCellDer"><select name="NumeroSuerte[]" data-placeholder="Numeros de la Suerte"
                                                class="chzn-select" multiple="multiple" style="width:200px;">
                        <?php
                        for ($x = 0; $x <= 99; $x++)
                        {
                            ?>
                            <option value="<?= $x; ?>"><?= $x; ?></option><?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="divRow">
                <div class="divCellIzq">Numeros Restringidos</div>
                <div class="divCellDer"><select name="NumeroRestringido[]" data-placeholder="Numeros Restringidos"
                                                class="chzn-select" multiple="multiple" style="width:200px;">
                        <?php
                        for ($x = 0; $x <= 99; $x++)
                        {
                            ?>
                            <option value="<?= $x; ?>"><?= $x; ?></option><?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="divRow">
                <div class="divCellIzq">Desea programar varias fechas?</div>
                <div class="divCellDer"><input type="checkbox" value="1" name="VariasFechas" style="visibility:visible;"
                                               onclick="AgregarFechas();"></div>
            </div>
        </div>
        <div id="MasFechas" style="visibility:hidden;"></div>
        <input type="button" value="Salvar" class="button" id="BotonFormulario"
               onclick="javascript:FormularioAddLoteria();">
        <input type="hidden" name="FlagDomingo" value="0">
        <input type="hidden" name="FlagLunes" value="0">
        <input type="hidden" name="FlagMartes" value="0">
        <input type="hidden" name="FlagMiercoles" value="0">
        <input type="hidden" name="FlagJueves" value="0">
        <input type="hidden" name="FlagViernes" value="0">
        <input type="hidden" name="FlagSabado" value="0">
        <input type="hidden" name="IDSorteoDefinicionValor" value="">
    </form>
    <script type="text/javascript">
        $('.chzn-select').chosen();
    </script>

</div>
<div id="Resultado"></div>
<br><br>
